var _common = {
    init: function () {
        $.ajaxSetup({
            cache: false
        });

        gen.common = {
            DATE_FORMAT: "dd-MM-yyyy",
            DATE_PICKER_FORMAT: "dd MM yyyy",
            DATE_TIME_FORMAT: "dd-MM-yyyy HH:mm:ss",
            DATE_DB_FORMAT: "yyyy-MM-dd HH:mm:ss",
            NUMBER_FORMAT: "#,###,###,##0.00",
            TODAY: Date.today().set({
                hour: 23,
                minute: 59,
                second: 59,
                millisecond: 997
            }),
            PROXY: window.location.protocol + '//' + window.location.hostname + ":" + window.location.port + '/MobileProxy/JsonProxy',
            RESET_PAGE: '/mobile/reset.html',
            FOLDER: ""
        };

        gen.common.PHONEGAP = false;
        if (window.location.hostname == "")
            gen.common.PHONEGAP = true;

        if (window.location.href.indexOf("index.html") >= 0) {
            if (gen.common.PHONEGAP) {
                gen.common.FOLDER = localStorage.getItem('folder');
                gen.common.PROXY = localStorage.getItem('proxy');
                gen.common.RESET_PAGE = localStorage.getItem('reset_page');
                if (!gen.common.PROXY || gen.common.PROXY === "null" || gen.common.PROXY === "")
                    _common.goToClient();
                else
                    _common.goToLogin();

                setTimeout(_common.linkNavigation, 100);
            } else {
                _common.goToLogin();
                setTimeout(_common.linkNavigation, 100);
            }
        }
    },

    goToClient: function () {

        var url = gen.S3_BUCKET + "/clients.xml";
        $("#pageLoadingProgress").text("loading clients");

        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            // Need to wait for the DONE state or you'll get errors 
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 200) {

                    setTimeout(_common.hidePreLoaders, 500);

                    gen.Clients = $.xml2json(request.responseXML);

                    _clients.init();
                    $("#navToClientLink").trigger("click");

                } else {
                    _common.showNotification("Could not reach client list");
                }
            }
        }

        // Make sure whatever you post is URI encoded 
        var encodedString = encodeURIComponent(url);
        // This is for a POST request but GET etc. work fine too 
        request.open("GET", url, false); // only async supported 


        // Post types other than forms should work fine too but I've not tried 
        request.setRequestHeader("cache-control", "no-cache");
        // Form data is web service dependent - check parameter format 
        var requestString = "text=" + encodedString;
        request.send(requestString);
    },

    goToLogin: function () {
        $("#pageLoadingProgress").text("loading css");

        var cssURL = "css/client/client.css";
        if (gen.common.PHONEGAP)
            cssURL = gen.S3_BUCKET + "/" + gen.common.FOLDER + "/" + gen.S3_VERSION + "/css/client.css";

        var cssLink = $('<link rel="stylesheet" type="text/css" />').attr('href', cssURL);
        $('head').append(cssLink);
        $("#navToLoginLink").trigger("click");

        gen.SettingsCounter = 0;
        _common.getSettingsLoop();
        _login.preInit();
    },

    setCookieInDays: function (cookie_name, cookie_value) {
        var exdate = new Date();
        if (gen.common.COOKIE_VALIDITY_DAYS > 0) {
            exdate.setDate(exdate.getDate() + gen.common.COOKIE_VALIDITY_DAYS);
            _common.setCookie(cookie_name, cookie_value, exdate);
        } else
            _common.deleteCookie(cookie_name);
    },

    setCookieInMinutes: function (cookie_name, cookie_value) {
        var exdate = new Date();
        if (gen.common.COOKIE_VALIDITY_MINUTES > 0) {
            exdate.setTime(exdate.getTime() + (60000 * gen.common.COOKIE_VALIDITY_MINUTES));
            _common.setCookie(cookie_name, cookie_value, exdate);
        } else
            _common.deleteCookie(cookie_name);
    },

    setCookie: function (cookie_name, cookie_value, expiryDate) {
        var cookie_value_expires = escape(cookie_value) + "; expires=" + expiryDate.toUTCString();
        document.cookie = cookie_name + "=" + cookie_value_expires;
    },

    getCookie: function (cookie_name) {
        var c_value = document.cookie;
        var c_start = c_value.indexOf(" " + cookie_name + "=");
        if (c_start == -1)
            c_start = c_value.indexOf(cookie_name + "=");

        if (c_start == -1) {
            c_value = null;
        } else {
            c_start = c_value.indexOf("=", c_start) + 1;

            var c_end = c_value.indexOf(";", c_start);
            if (c_end == -1)
                c_end = c_value.length;

            c_value = unescape(c_value.substring(c_start, c_end));
        }
        return c_value;
    },

    deleteCookie: function (cookie_name) {
        document.cookie = cookie_name + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
    },

    encodeStr: function (uncoded) {
        var key = "SXGWLZPDOKFIVUHJYTQBNMACERxswgzldpkoifuvjhtybqmncare";
        uncoded = uncoded.toUpperCase().replace(/^\s+|\s+$/g, "");
        var coded = "";
        var chr;
        for (var i = uncoded.length - 1; i >= 0; i--) {
            chr = uncoded.charCodeAt(i);
            coded += (chr >= 65 && chr <= 90) ?
                key.charAt(chr - 65 + 26 * Math.floor(Math.random() * 2)) :
                String.fromCharCode(chr);
        }
        return encodeURIComponent(coded);
    },

    decodeStr: function (coded) {
        var key = "SXGWLZPDOKFIVUHJYTQBNMACERxswgzldpkoifuvjhtybqmncare";
        coded = decodeURIComponent(coded);
        var uncoded = "";
        var chr;
        for (var i = coded.length - 1; i >= 0; i--) {
            chr = coded.charAt(i);
            uncoded += (chr >= "a" && chr <= "z" || chr >= "A" && chr <= "Z") ?
                String.fromCharCode(65 + key.indexOf(chr) % 26) :
                chr;
        }
        return uncoded;
    },

    endsWith: function (str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    },

    getSettingsLoop: function () {
        switch (gen.SettingsCounter) {
            case 0:
                {
                    _common.getCommonSettingsGD();
                }
                break;
            case 1:
                {
                    _common.getLocalizationSettingsGD();

                }
                break;
            case 2:
                {
                    _common.getCampaignsSettingsGD();
                }
                break;
            case 3:
                {
                    _common.getLeadsSettingsGD();
                }
                break;
            case 4:
                {
                    $("#pageLoadingProgress").text("settings done");
                    setTimeout(_common.hidePreLoaders, 500);
                    _login.init();
                }
                break;
        }
    },


    getCommonSettingsGD: function () {
        $("#pageLoadingProgress").text("common settings");

        var commonURL = gen.COMMON_XML;
        if (gen.common.PHONEGAP)
            commonURL = gen.S3_BUCKET + "/" + gen.common.FOLDER + "/" + gen.S3_VERSION + "/" + gen.COMMON_XML;
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            // Need to wait for the DONE state or you'll get errors 
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 200) {

                    var commonSettings = $.xml2json(request.responseXML);

                    if (commonSettings.CookieValidityDays)
                        gen.common.COOKIE_VALIDITY_DAYS = parseInt(commonSettings.CookieValidityDays.Value);
                    else
                        gen.common.COOKIE_VALIDITY_DAYS = 60;

                    if (commonSettings.CookieValidityMinutes)
                        gen.common.COOKIE_VALIDITY_MINUTES = parseInt(commonSettings.CookieValidityMinutes.Value);
                    else
                        gen.common.COOKIE_VALIDITY_MINUTES = 0;

                    if (commonSettings.DateFormat)
                        gen.common.DATE_FORMAT = commonSettings.DateFormat.Value;

                    if (commonSettings.DatePickerFormat)
                        gen.common.DATE_PICKER_FORMAT = commonSettings.DatePickerFormat.Value;

                    if (commonSettings.DateTimeFormat)
                        gen.common.DATE_TIME_FORMAT = commonSettings.DateTimeFormat.Value;

                    if (commonSettings.DefaultLanguage)
                        gen.Language = commonSettings.DefaultLanguage.Value;
                    else
                        gen.Language = "English";

                    if (commonSettings.DashboardURL)
                        gen.common.DashboardURL = commonSettings.DashboardURL.Value;
                    else
                        gen.common.DashboardURL = "";

                    if (commonSettings.TwilioURL)
                        gen.common.TwilioURL = commonSettings.TwilioURL.Value;
                    else
                        gen.common.TwilioURL = "";

                    if (commonSettings.TwilioURLAndroid)
                        gen.common.TwilioURLAndroid = commonSettings.TwilioURLAndroid.Value;
                    else
                        gen.common.TwilioURLAndroid = "";

                    if (commonSettings.TwilioCLI)
                        gen.common.TwilioCLI = commonSettings.TwilioCLI.Value;
                    else
                        gen.common.TwilioCLI = "";

                    if (commonSettings.ResetPage)
                        gen.common.RESET_PAGE = commonSettings.ResetPage.Value;

                    if (commonSettings.MaskDialNumbers)
                        gen.common.MaskDialNumbers = commonSettings.MaskDialNumbers.Value === "true";
                    else
                        gen.common.MaskDialNumbers = false;

                    if (commonSettings.BaseMobileProxyUrl)
                        gen.common.BaseMobileProxyUrl = commonSettings.BaseMobileProxyUrl.Value;
                    else
                        gen.common.BaseMobileProxyUrl = "http://localhost:8080";

                    if (commonSettings.EnableLoginAutocomplate)
                        gen.common.EnableLoginAutocomplate = commonSettings.EnableLoginAutocomplate.Value;
                    else
                        gen.common.EnableLoginAutocomplate = "off";

                    if (commonSettings.EnableLocalization) {
                        gen.common.EnableGlobalization = commonSettings.EnableLocalization.Value;
                    }

                    if (commonSettings.HelpQuestionURL) {
                        gen.common.helpQuestionURL = commonSettings.HelpQuestionURL.Value;
                    }

                    if (commonSettings.HelpSuggestionAddress) {
                        gen.common.helpSuggestionAddress = commonSettings.HelpSuggestionAddress.Value;
                    }

                    if (commonSettings.HelpBugAddress) {
                        gen.common.helpBugAddress = commonSettings.HelpBugAddress.Value;
                    }

                    $("#pageLoadingProgress").text("common settings done");
                    gen.SettingsCounter++;
                    _common.getSettingsLoop();

                } else {
                    _common.commonSettingsError();

                }
            }
        }

        // Make sure whatever you post is URI encoded 
        var encodedString = encodeURIComponent(commonURL);


        // This is for a POST request but GET etc. work fine too 
        request.open("GET", commonURL, false); // only async supported 


        // Post types other than forms should work fine too but I've not tried 
        request.setRequestHeader("cache-control", "no-cache");
        // Form data is web service dependent - check parameter format 
        var requestString = "text=" + encodedString;

        request.send(requestString);
    },


    getLocalizationSettingsGD: function () {

        $("#pageLoadingProgress").text("localization settings");
        gen.getLocalizationSettings = null;
        gen.Languages = [];
        gen.LanguageCodes = [];

        var $inputIndex = $('#languageSelectIndex');
        var $inputCampaigns = $('#languageSelectCampaigns');
        var $inputLeads = $('#languageSelectLeads');
        var $inputFeedback = $('#languageSelectFeedback');

        //• GET COOKIE FOR LAST SAVED LANGUAGE
        var currentLanguage = _common.getCookie("Language");
        if (!currentLanguage)
            currentLanguage = gen.Language;
        else
            gen.Language = currentLanguage;

        var localizationURL = gen.LOCALIZATION_XML;
        if (gen.common.PHONEGAP)
            localizationURL = gen.S3_BUCKET + "/" + gen.common.FOLDER + "/" + gen.S3_VERSION + "/" + gen.LOCALIZATION_XML;

        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            // Need to wait for the DONE state or you'll get errors 
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 200) {

                    var localizationSettings = $.xml2json(request.responseXML);
                    gen.getLocalizationSettings = localizationSettings;
                    var englishAdded = false;
                    jQuery.each(localizationSettings, function (i, val) {
                        gen.Languages.push(i);

                        var languageCode = "en-US";
                        if (val.LanguageCode) {
                            languageCode = val.LanguageCode;
                            gen.LanguageCodes.push(languageCode);
                        }

                        if (i == gen.Language)
                            gen.LanguageCode = val.LanguageCode;

                        if (i == "English")
                            englishAdded = true;

                        if (i == currentLanguage) {
                            $inputIndex.append('<option value="' + i + '" selected>' + i + '</option>');
                            $inputCampaigns.append('<option value="' + i + '" selected>' + i + '</option>');
                            $inputLeads.append('<option value="' + i + '" selected>' + i + '</option>');
                            $inputFeedback.append('<option value="' + i + '" selected>' + i + '</option>');
                        } else {
                            $inputIndex.append('<option value="' + i + '">' + i + '</option>');
                            $inputCampaigns.append('<option value="' + i + '">' + i + '</option>');
                            $inputLeads.append('<option value="' + i + '">' + i + '</option>');
                            $inputFeedback.append('<option value="' + i + '">' + i + '</option>');
                        }
                    });

                    if (!englishAdded) {
                        $inputIndex.prepend('<option value="English">English</option>');
                        $inputCampaigns.prepend('<option value="English">English</option>');
                        $inputLeads.prepend('<option value="English">English</option>');
                        $inputFeedback.prepend('<option value="English">English</option>');
                    }

                    if (gen.Languages.length > 0) {
                        $inputIndex.selectmenu();
                        $inputIndex.selectmenu("refresh");

                        $inputCampaigns.selectmenu();
                        $inputCampaigns.selectmenu("refresh");

                        $inputLeads.selectmenu();
                        $inputLeads.selectmenu("refresh");

                        $inputFeedback.selectmenu();
                        $inputFeedback.selectmenu("refresh");

                        _common.displayLanguage();
                    } else {
                        $inputIndex.parent().hide();
                        $inputCampaigns.parent().hide();
                        //$inputLeads.parent().hide();
                        $inputLeads.attr("readonly", "readonly");
                        $inputFeedback.parent().hide();
                    }

                    $("#pageLoadingProgress").text("localization settings done");
                    gen.SettingsCounter++;
                    _common.getSettingsLoop();


                } else {
                    $("#pageLoadingProgress").text("localization settings error");
                    $inputIndex.parent().hide();
                    $inputCampaigns.parent().hide();
                    //$inputLeads.parent().hide();
                    $inputLeads.prepend('<option value="English">English</option>');
                    $inputFeedback.parent().hide();

                    gen.SettingsCounter++;
                    _common.getSettingsLoop();
                }
            }
        }

        // Make sure whatever you post is URI encoded 
        var encodedString = encodeURIComponent(localizationURL);
        // This is for a POST request but GET etc. work fine too 
        request.open("GET", localizationURL, false); // only async supported 
        // Post types other than forms should work fine too but I've not tried 
        request.setRequestHeader("cache-control", "no-cache");
        // Form data is web service dependent - check parameter format 
        var requestString = "text=" + encodedString;

        request.send(requestString);
    },


    getCampaignsSettingsGD: function () {

        $("#pageLoadingProgress").text("campaign settings");
        gen.CampaignConfiguration = {};

        var campaignsURL = gen.CAMPAIGNS_XML;
        if (gen.common.PHONEGAP)
            campaignsURL = gen.S3_BUCKET + "/" + gen.common.FOLDER + "/" + gen.S3_VERSION + "/" + gen.CAMPAIGNS_XML;
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {

            // Need to wait for the DONE state or you'll get errors 
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 200) {

                    gen.CampaignConfiguration = $.xml2json(request.responseXML);

                    $("#pageLoadingProgress").text("campaign settings done");
                    gen.SettingsCounter++;
                    _common.getSettingsLoop();

                } else {
                    $("#pageLoadingProgress").text("campaign settings error");
                    gen.SettingsCounter++;
                    _common.getSettingsLoop();
                }
            }
        }

        // Make sure whatever you post is URI encoded 
        var encodedString = encodeURIComponent(campaignsURL);
        // This is for a POST request but GET etc. work fine too 
        request.open("GET", campaignsURL, false); // only async supported 
        // Post types other than forms should work fine too but I've not tried 
        request.setRequestHeader("cache-control", "no-cache");
        // Form data is web service dependent - check parameter format 
        var requestString = "text=" + encodedString;

        request.send(requestString);
    },

    getLeadsSettingsGD: function () {
        $("#pageLoadingProgress").text("lead settings");
        gen.DefaultConfiguration = {};

        var leadsURL = gen.LEADS_XML;
        if (gen.common.PHONEGAP)
            leadsURL = gen.S3_BUCKET + "/" + gen.common.FOLDER + "/" + gen.S3_VERSION + "/" + gen.LEADS_XML;

        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            // Need to wait for the DONE state or you'll get errors 
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 200) {

                    gen.DefaultConfiguration = $.xml2json(request.responseXML);

                    for (var counter = 1; counter < 9; counter++) {
                        var position = "Position" + counter;
                        _common.setDefaultPositionProperties(gen.DefaultConfiguration.Default.LeadList, position);
                    }

                    $("#pageLoadingProgress").text("lead settings done");
                    gen.SettingsCounter++;
                    _common.getSettingsLoop();

                } else {

                    $("#pageLoadingProgress").text("lead settings error");
                    gen.SettingsCounter++;
                    _common.getSettingsLoop();
                }
            }
        }

        // Make sure whatever you post is URI encoded 
        var encodedString = encodeURIComponent(leadsURL);
        // This is for a POST request but GET etc. work fine too 
        request.open("GET", leadsURL, false); // only async supported 


        // Post types other than forms should work fine too but I've not tried 
        request.setRequestHeader("cache-control", "no-cache");
        // Form data is web service dependent - check parameter format 
        var requestString = "text=" + encodedString;
        request.send(requestString);
    },

    selectLanguage: function (select) {
        gen.Language = select.value;
        gen.LanguageCode = _common.getLanguageCode(select.selectedIndex);

        var exdate = new Date();
        exdate.setDate(exdate.getDate() + 30);
        this.setCookie("Language", select.value, exdate);

        $('#languageSelectIndex').val(gen.Language);
        $('#languageSelectCampaigns').val(gen.Language);
        $('#languageSelectLeads').val(gen.Language);
        $('#languageSelectFeedback').val(gen.Language);

        $('#languageSelectIndex').selectmenu("refresh");
        $('#languageSelectCampaigns').selectmenu("refresh");
        $('#languageSelectLeads').selectmenu("refresh");
        $('#languageSelectFeedback').selectmenu("refresh");

        this.displayLanguage();
    },

    getLanguageCode: function (index) {
        if (gen.LanguageCodes)
            return gen.LanguageCodes[index];
    },

    displayLanguage: function () {

        if (gen.Languages && gen.Languages.length > 0) {
            _leads.setSortOptions();
            //• UPDATE ALL LABELS
            $("label").each(function () {
                if (this.attributes["data-localization-key"] && this.attributes["data-localization-default"])
                    $(this).text(_common.getLocalization(this.attributes["data-localization-key"].value, this.attributes["data-localization-default"].value));
            });

            //• UPDATE ALL BUTTONS
            $("input").each(function () {
                if (this.attributes["data-localization-key"] && this.attributes["data-localization-default"]) {
                    if (this.type == "text" || this.type == "password")
                        $(this).attr("placeholder", _common.getLocalization(this.attributes["data-localization-key"].value, this.attributes["data-localization-default"].value));

                    if (this.type == "button") {

                        $(this).button().text(_common.getLocalization(this.attributes["data-localization-key"].value, this.attributes["data-localization-default"].value)).button("refresh");
                    }
                }
            });

            //• UPDATE ALL ANCHORS
            $("a").each(function () {
                if (this.attributes["data-localization-key"] && this.attributes["data-localization-default"]) {
                    $(this).text(_common.getLocalization(this.attributes["data-localization-key"].value, this.attributes["data-localization-default"].value));
                }
            });
            //• UPDATE ALL H1
            $("h1").each(function () {
                if (this.attributes["data-localization-key"] && this.attributes["data-localization-default"])
                    $(this).text(_common.getLocalization(this.attributes["data-localization-key"].value, this.attributes["data-localization-default"].value));
            });

            $("h3").each(function () {
                if (this.attributes["data-localization-key"] && this.attributes["data-localization-default"])
                    $(this).text(_common.getLocalization(this.attributes["data-localization-key"].value, this.attributes["data-localization-default"].value));
            });

            //• UPDATE ALL OPTIONS
            $("option").each(function () {
                if (this.attributes["data-localization-key"] && this.attributes["data-localization-default"])
                    $(this).text(_common.getLocalization(this.attributes["data-localization-key"].value, this.attributes["data-localization-default"].value));
            });

            //• FILTER BOXES FOR LISTS
            var filterLocal = _common.getLocalization("filter", "Filter");
            $('*[data-type="search"]').attr("placeholder", filterLocal);

        } else {

            $('#languageSelectContainer').hide();
            $("#languageSelectContainer").css("display", "none");
        }

        _common.updateLabels();
    },

    getLocalization: function (key, originalValue) {
        if (gen.getLocalizationSettings && gen.getLocalizationSettings[gen.Language] && gen.getLocalizationSettings[gen.Language][key])
            return gen.getLocalizationSettings[gen.Language][key];
        else
            return originalValue;
    },

    setDefaultPositionProperties: function (leadListJSON, position) {
        var defaultPositionValues = {};
        defaultPositionValues.FontSize = 10;
        defaultPositionValues.FontWeight = "normal";
        defaultPositionValues.Color = "Black";
        defaultPositionValues.InputType = "text";
        if (leadListJSON[position]) {
            if (!leadListJSON[position].FontSize)
                leadListJSON[position].FontSize = defaultPositionValues.FontSize;

            if (!leadListJSON[position].FontWeight)
                leadListJSON[position].FontWeight = defaultPositionValues.FontWeight;

            if (!leadListJSON[position].Color)
                leadListJSON[position].Color = defaultPositionValues.Color;

            if (!leadListJSON[position].InputType)
                leadListJSON[position].InputType = defaultPositionValues.InputType;
        } else {
            leadListJSON[position] = defaultPositionValues;
        }
    },

    getAttribute: function (attributesToSearch, attributeToFind) {
        var result = "";
        $.each(attributesToSearch, function (i, attrib) {
            var attributeName = attrib.name;
            var attributeValue = attrib.value;
            if (attributeName == attributeToFind)
                result = attributeValue;
        });
        return result;
    },

    linkNavigation: function () {
        //• LINK NAVIGATION
        if (window.location.search.length > 0) {
            var searchString = window.location.search.replace('?', '');
            if (searchString.toLowerCase().indexOf("-") >= 0) {
                var result = searchString.split('-');
                gen.LinkNavigation = {};
                gen.LinkNavigation.Campaign = result[0];
                gen.LinkNavigation.Lead = result[1];
            }
        }

        if (window.location.hash == "" || window.location.hash == "#" || window.location.hash == "#loginPage") {
            var lastRefreshToken = _common.getCookie("refresh_token");
            if (lastRefreshToken) {
                gen.token = new Object();
                gen.token.access_token = '!@#$%';
                gen.token.refresh_token = lastRefreshToken;
                $("#navToMainLink").trigger("click");
            }
        }
    },

    showPreLoaders: function () {
        $("#pageLoading").fadeIn(100);
        $("#pageLoadingLogo").fadeIn(100);
    },

    hidePreLoaders: function () {
        $("#pageLoading").fadeOut(500);
        $("#pageLoadingLogo").fadeOut(500);
        $("#pageLoadingProgress").fadeOut(500);
    },

    showLoading: function () {
        $("#pageLoadingLogo").fadeIn(100);
    },

    hideLoading: function () {
        $("#pageLoadingLogo").fadeOut(500);
    },

    resetTokens: function () {
        gen.token = new Object();
        _common.deleteCookie("refresh_token");
    },

    showNotification: function (message, executeFunction) {
        var okText = _common.getLocalization("ok", "OK");

        noty({
            text: message,
            type: 'alert',
            modal: true,
            dismissQueue: false,
            layout: 'center',
            theme: 'defaultTheme',
            buttons: [{
                addClass: 'ui-btn ui-shadow ui-btn-corner-all ui-mini ui-btn-up-d ui-btn-inner',
                text: okText,
                onClick: function ($noty) {
                    $noty.close();

                    if (executeFunction)
                        executeFunction();
                }
            }]
        });
    },

    getTimeZoneOffset: function (date) {
        var offset = date.getTimezoneOffset() / 60;
        var result = "00";
        var operator = "+";

        if (offset > 0)
            operator = "-";

        result = Math.abs(offset);

        return operator + _common.zeroFill(result, 2) + ":00";
    },

    zeroFill: function (num, numZeros) {
        var n = Math.abs(num);
        var zeros = Math.max(0, numZeros - Math.floor(n).toString().length);
        var zeroString = Math.pow(10, zeros).toString().substr(1);
        if (num < 0)
            zeroString = '-' + zeroString;

        return zeroString + n;
    },

    replaceAll: function (original, find, replace) {
        return original.replace(new RegExp(find, 'g'), replace);
    },

    updateLabels: function () {
        window.setTimeout(function () {
            $('input').each(function () {
                $(this).on('focus', function () {
                    $(this).parents('.inputAnimation').addClass('active');
                });
                $(this).on('blur', function () {
                    if ($(this).val().length == 0)
                        $(this).parents('.inputAnimation').removeClass('active');
                });

                if ($(this).val() != '')
                    $(this).parents('.inputAnimation').addClass('active');
            });
        }, 500);
    },

    //To parse the GDHttpRequest Response
    FormatJson: function (data) {
        data = data.replace("getdata(", "").replace(")", "");
        return JSON.parse(data);
    }

};