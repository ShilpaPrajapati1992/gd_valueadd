var _reset =
{
    init: function ()
    {
        $.ajaxSetup({ cache: false });

        this.ui = {};
        this.ui.resetHint = $("#resetHint");
        this.ui.newPassword = $("#passwordInput");
        this.ui.confirmPassword = $("#confirmPasswordInput");
        this.ui.changePasswordButton = $("#changePasswordButton");
        $("#changePasswordButtonContainer").css("opacity", 1);

        this.ui.resetHint.off().on("click", $.proxy(_reset.onResetHintClick, this));
        this.ui.confirmPassword.off().on("keypress", $.proxy(_reset.onConfirmEnter, this));
        this.ui.changePasswordButton.off().on("click", $.proxy(_reset.onChangeClick, this));

        var languageSelectContainer = $("#languageSelectContainerReset");

        if(gen.common.EnableGlobalization){
            languageSelectContainer.show();
        }
        else{
            languageSelectContainer.hide();
        }
        
        _common.displayLanguage();
    },

    onResetHintClick: function ()
    {
        var resetPasswordHint = _common.getLocalization("passwordRequirements", "Password Requirements");
        _common.showNotification(resetPasswordHint);
    },

    onConfirmEnter: function ()
    {
        if (event.which == 13)
        {
            event.preventDefault();
            this.onChangeClick();
        }
    },

    onChangeClick: function ()
    {
        if (this.ui.newPassword.val() == "" && this.ui.confirmPassword.val() == "")
        {
            var noNewPassword = _common.getLocalization('noNewPassword', 'You have not entered a new password');
            _common.showNotification(noNewPassword);
            return;
        }

        this.ui.newPassword.blur();
        this.ui.confirmPassword.blur();

        var changingPassword = _common.getLocalization('changingPassword', 'Changing Password');
        $.mobile.loading( 'show',
        {
            text: changingPassword,
            textVisible: true,
            theme: "a",
            html: ""
        });

        var context = this;
        $.ajax(
        {
            type: "POST",
            dataType: "jsonp",
            headers : { "cache-control": "no-cache" },
            data:
            {
                method: "POST",
                action: "private/password/reset",
                base: true,
                data: JSON.stringify({ newPassword: this.ui.newPassword.val(), verifyPassword: this.ui.confirmPassword.val(), token: gen.resetToken })
            },
            beforeSend: function (request)
            {
                request.setRequestHeader("Accept-Language", gen.LanguageCode);
            },
            url: gen.common.PROXY
        })
        .done(function (data, textStatus, jqXHR)
        {
            $.mobile.loading( 'hide', {});
            _common.showNotification(data);
            _reset.loadIndex();
        })
        .fail(function (jqXHR, textStatus)
        {
            $.mobile.loading( 'hide', {});

            if (jqXHR.responseJSON)
            {
                if (jqXHR.responseJSON.error_description)
                    _common.showNotification(jqXHR.responseJSON.error_description);
                else if (jqXHR.responseJSON.message)
                    _common.showNotification(jqXHR.responseJSON.message);
                else
                    _common.showNotification("Login Error");
            }
            else
                _common.showNotification("Login Error");
        });
    },
onChangeClickGD: function () {
        var method = "POST";
        var url = gen.common.PROXY;
        var timeout = 30;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;

       var resetOnChangeClickRequest = window.plugins.GDHttpRequest.createRequest("POST", gen.common.PROXY + "?callback=getdata", timeout, isAsync, user, password, auth, isIncremental);
        resetOnChangeClickRequest.disablePeerVerification = false;
        resetOnChangeClickRequest.disableHostVerification = false;
        resetOnChangeClickRequest.addRequestHeader("dataType", "jsonp");
        resetOnChangeClickRequest.addRequestHeader("cache-control", "no-cache");
        resetOnChangeClickRequest.addRequestHeader("Accept-Language", gen.LanguageCode);
        resetOnChangeClickRequest.addRequestHeader("crossDomain", "true");
        resetOnChangeClickRequest.addPostParameter("method", "POST");
        resetOnChangeClickRequest.addPostParameter("action", "private/password/reset", );
        resetOnChangeClickRequest.addPostParameter("base", "true");
        resetOnChangeClickRequest.addPostParameter("callback", "getdata");
        function sendSuccess(response) {
             try {
            console.log("Received valid response from the send request");
            var responseObj = window.plugins.GDHttpRequest.parseHttpResponse(response);
            var resonsedata = _common.FormatJson(responseObj.responseText.toString());
           
                $.mobile.loading('hide', {});
                _common.showNotification(data);
                _reset.loadIndex();
            } catch (e) {

            }
        };

        function sendFail(e) {
          $.mobile.loading( 'hide', {});

            if (jqXHR.responseJSON)
            {
                if (jqXHR.responseJSON.error_description)
                    _common.showNotification(jqXHR.responseJSON.error_description);
                else if (jqXHR.responseJSON.message)
                    _common.showNotification(jqXHR.responseJSON.message);
                else
                    _common.showNotification("Login Error");
            }
            else
                _common.showNotification("Login Error");
        }
        resetOnChangeClickRequest.send(sendSuccess, sendFail);

    },
    loadIndex: function()
    {
        history.pushState({}, "", "index.html");
        location.reload();
    }
};
