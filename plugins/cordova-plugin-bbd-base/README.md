BBD Cordova Base plugin
=======================
> The BBD Cordova Base plugin adds all the needed configuration to be able to use BlackBerry Dynamics in your Cordova application. All the other BBD Cordova plugins require the Base plugin to be installed as dependency.

Location
========
`Good_Dynamics_for_PhoneGap_<version>/plugins/cordova-plugin-bbd-base`


Setup
=====
1. Install dependencies for BBD Cordova Base plugin

    ```
    $ cd <path/to/package>/Good_Dynamics_for_PhoneGap_<version>/plugins/cordova-plugin-bbd-base
    $ npm install
    ```

2. Check/update path to BlackBerry Dynamics SDKs
    - `ANDROID`:
    To get it work correctly for Android platform you need to set absolute path to BlackBerry Dynamics Android SDK in `cordova-plugin-bbd-base/scripts/gradle/gradle.properties` before installing the plugin. 
    If the path to BlackBerry Dynamics Android SDK was not set, please do it and reinstall the plugin. The path must end with `sdk` in the end.
    Example of correct setting path in `gradle.properties`:
    `pathToGdAndroidSdk=/Users/your_user/path/to/BlackBerry_Dynamics_SDK/sdk`

    - `IOS`:
    To get it work correctly for iOS platform please check the path to `GDAssets.bundle` in `plugin.xml`.
    If the path to your XCode is different, please set the correct path and reinstall the plugin.
    If you are using Cordova 5x please select Cordova's `build.xcconfig` from `platforms/ios/cordova` folder as build configuration file for your project in XCode.

3. We recommend to create your Cordova application in the same folder as plugins
    ```
    $ cd <path/to/package>/Good_Dynamics_for_PhoneGap_<version>/plugins
    $ cordova create cordovaApp your.app.id App
    ```

Installation
============
To add this plugin to your application, run the following command in the project directory:
```
$ cd <path/to/package>/Good_Dynamics_for_PhoneGap_<version>/plugins/cordovaApp
$ cordova plugin add ../cordova-plugin-bbd-base
```