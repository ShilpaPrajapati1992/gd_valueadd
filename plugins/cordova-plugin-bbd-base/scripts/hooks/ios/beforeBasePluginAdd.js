#!/usr/bin/env node

/*
 * (c) 2016 BlackBerry Limited. All rights reserved.
 */

require('shelljs/global');

module.exports = function(context) {
	var fs = context.requireCordovaModule('fs');
	var path = context.requireCordovaModule('path');
	var xmlParser = require('xml-parser');
	
	var pluginPath = context.opts.plugin.dir;
	var platformRoot = path.join(context.opts.projectRoot, 'platforms', 'ios');
	var platformFiles = fs.readdirSync(platformRoot);
	var srcDirName = null;
	var xcodeProjectName = null;

	for (var valueIndex in platformFiles) {
		var value = platformFiles[valueIndex];
		if (value.indexOf('.xcodeproj') > -1) {
			xcodeProjectName = value;
			var strEndPosition = value.indexOf('.');
			srcDirName = value.substring(0, strEndPosition);
			break;
		}
	}

	// We should get path to GD.framework
	// Then, we should get path to GDAssets.bundle
	// If GD.framework is not installed we should notify user to do it
	var defaultXcodePath = '/Applications/Xcode.app/Contents/Developer'
	var xcodePath = exec('xcode-select -p', {silent:true});
	if (xcodePath.output) {
		xcodePath = xcodePath.output.replace('\n', "");
	} else {
		xcodePath = xcodePath.stdout.replace('\n', "");
	}
	
	var xcodePlatforms = path.join(xcodePath, 'Platforms');
	var frameworksPath = path.join(xcodePath, 'Platforms', 'iPhoneOS.platform', 'Developer', 'SDKs', 'iPhoneOS.sdk', 'System', 'Library', 'Frameworks');
	var gdPath = path.join(frameworksPath, 'GD.framework');
	var gdAssetsPath = path.join(gdPath, 'Resources', 'GDAssets.bundle');
	
	if (fs.existsSync(gdPath)) {
		var pluginXmlContent = fs.readFileSync(path.join(pluginPath, 'plugin.xml'), 'utf8');
		var pluginXmlObj = xmlParser(pluginXmlContent);

		// Accessing <platform name="ios"> tag
		var platformIos;
		for (var index in pluginXmlObj.root.children) {
			if (pluginXmlObj.root.children[index].name === 'platform' && pluginXmlObj.root.children[index].attributes.name === 'ios') {
				platformIos = pluginXmlObj.root.children[index];
			}
		}
		// Accessing <platform name="ios"><resource-file src=".../Frameworks/GD.framework/Resources/GDAssets.bundle"> tags
		var gdAssetsBundleObj;
		for (var index in platformIos.children) {
			if (platformIos.children[index].name === 'resource-file' && platformIos.children[index].attributes.src.indexOf('GDAssets.bundle') >= 0) {
				gdAssetsBundleObj = platformIos.children[index];
			}
		}

		if (xcodePath !== defaultXcodePath && gdAssetsPath !== gdAssetsBundleObj.attributes.src) {
			throw new Error('\nGD.framework is installed in custom location at path:\n"' + gdPath  + '"\nPlease correct following settings\n<platform name="ios">\n\t<resource-file src=".../GD.framework/Resources/GDAssets.bundle" />\n</platform>\nin\n"' + pluginPath + '/plugin.xml"\nand reinstall the plugin.');
		}
	} else {
		throw new Error("GD.framework is not installed in " + frameworksPath + ". Please install GD iOS SDK. If GD.framework is located in different path please also update plugin.xml with correct path to GDAssets.bundle and reinstall the plugin.");
	}	

	// Manage LaunchScreen.storyboard file to have actual ${PRODUCT_NAME}
	var storyboardPath = path.join(pluginPath, 'src', 'ios', 'resources', 'LaunchScreen.storyboard');
	chmod(660, storyboardPath);
	if (fs.existsSync(storyboardPath)) {
		var storyboardContent = fs.readFileSync(storyboardPath, { encoding: 'utf8' });

		if (storyboardContent.indexOf('PRODUCT_NAME') > 0) {
			storyboardContent = storyboardContent.replace('PRODUCT_NAME', srcDirName);
			fs.writeFileSync(storyboardPath, storyboardContent, { encoding: 'utf8' });
		}
	}
}