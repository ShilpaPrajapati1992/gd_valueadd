#!/usr/bin/env node

/*
 * (c) 2016 BlackBerry Limited. All rights reserved.
 */

require('shelljs/global');

module.exports = function(context) {
	var fs = context.requireCordovaModule('fs');
	var path = context.requireCordovaModule('path');

	var platformRoot = path.join(context.opts.projectRoot, 'platforms', 'ios');
	var platformFiles = fs.readdirSync(platformRoot);
	var srcDirName = null;
	var xcodeProjectName = null;

	for (var valueIndex in platformFiles) {
		var value = platformFiles[valueIndex];
		if (value.indexOf('.xcodeproj') > -1) {
			xcodeProjectName = value;
			var strEndPosition = value.indexOf('.');
			srcDirName = value.substring(0, strEndPosition);
			break;
		}
	}

	if (srcDirName == null) {
		throw new Error("srcDirName == null");
	}

	// revert changes made by Base plugin in Cordova's build.xcconfig file
	var cordovaBuildScconfigPath = path.join(context.opts.projectRoot, 'platforms', 'ios', 'cordova', 'build.xcconfig');

	if (fs.existsSync(cordovaBuildScconfigPath)) {
		var cordovaBuildXcconfigContent = fs.readFileSync(cordovaBuildScconfigPath, { encoding: 'utf8' });
		var FIPS_PACKAGE_PROP = 'FIPS_PACKAGE';
		var comment = '// Build configuration for GD iOS SDK. Please do not remove it!!!';

		if (cordovaBuildXcconfigContent.indexOf(FIPS_PACKAGE_PROP) > -1) {
			var indexOfComment = cordovaBuildXcconfigContent.indexOf(comment);
			var contextToRemove = cordovaBuildXcconfigContent.substring(indexOfComment - 2, cordovaBuildXcconfigContent.length);

			var noGDStuff = cordovaBuildXcconfigContent.replace(contextToRemove, '');
			fs.writeFileSync(cordovaBuildScconfigPath, noGDStuff, 'utf8');
		}

	}

	// revert changes made by Base plugin in Cordova's *.plist file
	var platformRoot = path.join(context.opts.projectRoot, 'platforms', 'ios');
	var plist = require('plist');
	var CordovaUtil = context.requireCordovaModule("cordova-lib/src/cordova/util");
	var ConfigParser = require('cordova-common').ConfigParser;
    var projectRoot = CordovaUtil.isCordova();
    var xml = CordovaUtil.projectConfig(projectRoot);
    var cfg = new ConfigParser(xml);
	var packageName = cfg.packageName();
	var plistFilePath = path.join(platformRoot, srcDirName , srcDirName + "-Info.plist" );
	var plistObj = plist.parse(fs.readFileSync(plistFilePath, 'utf8'));

	if (plistObj.CFBundleURLTypes === undefined) {
		plistObj.CFBundleURLTypes = [
			{
				CFBundleURLSchemes : []
			}
		];
	}

	delete plistObj.GDApplicationID;
	delete plistObj.GDApplicationVersion;
	delete plistObj.GDConsoleLogger;
	delete plistObj.UILaunchStoryboardName;

	var outPlist = plist.build(plistObj);
	fs.writeFileSync(plistFilePath, outPlist, { encoding: 'utf8' });

	// TODO: it is better to swizzle UIApplicationMain method in main.m file
	// When swizzling is done we should remove this section
	// revert changes made by Base plugin in Cordova's main.m file
	var pluginPath = context.opts.plugin.dir;
	cd(path.join(platformRoot, srcDirName));
	chmod(660, 'main.m');
	rm(path.join(platformRoot, srcDirName, 'main.m'));
	cp(path.join(pluginPath, 'src', 'ios', 'temp', 'main.m'), path.join(platformRoot, srcDirName, 'main.m'));
}
