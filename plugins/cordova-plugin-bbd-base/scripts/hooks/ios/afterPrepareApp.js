#!/usr/bin/env node

/*
 * (c) 2016 BlackBerry Limited. All rights reserved.
 */

require('shelljs/global');

// Cordova breaks all our configuration setup by BBD Base plugin when running 'cordova update' command.
// This script is to re-install BBD Base plugins after 'cordova update' command is run.
// There is no a hook that fires when 'cordova update' command is run. 
// 'after_prepare' hook is fired after 'cordova update' but not only in this case (aldo when 'cordova build', 'cordova prepare' ...). 
// So we need to run this hook only when 'cordova update' command is run. 

module.exports = function(context) {
	var fs = context.requireCordovaModule('fs');
	var path = context.requireCordovaModule('path');
	var platformRoot = path.join(context.opts.projectRoot, 'platforms', 'ios');

	// It appears that after 'cordova update' all BBD Cordova plugins are present in the app sources, but all the configuration is broken. 
	// We can check this by running 'cordova plugins list' command.
	// So if BBD Base plugin is in the list AND at the same time Cordova's build.xcconfig does not have FIPS settings it means that 'cordova update' was run

	var buildXcconfigPath = path.join(platformRoot, 'cordova', 'build.xcconfig');
	var pluginDir = context.opts.plugin.dir;
	var gradlePropertiesPath = path.join(pluginDir, 'scripts', 'gradle', 'gradle.properties');
	var pluginsPath;
	
	if (fs.existsSync(gradlePropertiesPath)) {
		var gradlePropertiesData = fs.readFileSync(gradlePropertiesPath, { encoding: 'utf8' });
		var key = 'bbdCordovaPluginsDir';
		pluginsPath = gradlePropertiesData.substring(gradlePropertiesData.lastIndexOf(key) + key.length + 1, gradlePropertiesData.length);
	}

	if (fs.existsSync(buildXcconfigPath)) {
		var buildXcconfigContent = fs.readFileSync(buildXcconfigPath, { encoding: 'utf8' });

		if (buildXcconfigContent.indexOf('FIPS_PACKAGE') < 0 && fs.existsSync(pluginDir)) {
			// Re-installing BBD Cordova plugins here

			var pluginsListStr = exec('cordova plugins list', {silent:true});
			if (pluginsListStr.output) {
				pluginsListStr = pluginsListStr.output;
			} else {
				pluginsListStr = pluginsListStr.stdout;
			}
			
			// Get all plugins list (BBD Cordova plugins + Cordova plugin + some else)
			var pluginsListArr = pluginsListStr.split('\n');
			pluginsListArr.pop();
			// Move BBD Cordova plugins to separate array
			var bbdPluginListArr = [];
			for (var plugin in pluginsListArr) {
				if (pluginsListArr[plugin].indexOf('cordova-plugin-bbd-') >= 0) {
					bbdPluginListArr.push(pluginsListArr[plugin].substring(0, pluginsListArr[plugin].indexOf(' ')));
				}
			}

			// Then we need to kind of 'sort' the array of BBD Cordova plugins to correclty manage dependencies
			// BBD Base plugin should be last one
			// BBD Storage plugin should go lust before BBD Base plugin
			// BBD All plugin should be first one if added
			for (var plugin in bbdPluginListArr) {
				if (bbdPluginListArr[plugin] === 'cordova-plugin-bbd-storage') {
					bbdPluginListArr.push(bbdPluginListArr.splice(plugin, 1)[0]);
				}
				if (bbdPluginListArr[plugin] === 'cordova-plugin-bbd-base') {
					bbdPluginListArr.push(bbdPluginListArr.splice(plugin, 1)[0]);
				}

				// If we have BBD All plugin it should be removed with all its dependencies
				if (bbdPluginListArr[0] === 'cordova-plugin-bbd-all') {
					exec('cordova plugin rm cordova-plugin-bbd-all', {silent:true});
					exec('cordova plugin add ' + path.join(pluginsPath, 'cordova-plugin-bbd-all'), {silent:true});
					break;
				} else {
					// remove all plugins in loop based on sorted array
					exec('cordova plugin rm ' + bbdPluginListArr[plugin], {silent:true});
				}
			}

			// Install BBD Cordova plugins back to the app
			for (var plugin in bbdPluginListArr) {
				exec('cordova plugin add ' + path.join(pluginsPath, bbdPluginListArr[plugin]), {silent:true});
			}
		}
	} else {
		throw new Error("Wrong path to build.xcconfig file.");
	}
}