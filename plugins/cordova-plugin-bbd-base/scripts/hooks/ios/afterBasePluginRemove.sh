#!/bin/sh

# (c) 2016 BlackBerry Limited. All rights reserved.

PLUGINS_LIST="$(cordova plugins list)"

# This hook will re-install ios platform only in the case after all BBD Cordova plugins are removed from the application
# This is needed because Cordova does not clear all the stuff set by BBD Base plugin via plugin.xml and pure Cordova application crashes on the start 
# After this hook is executed application will be in default Cordova state 

if [[ ${PLUGINS_LIST} == *"cordova-plugin-bbd-"* ]]
then
	exit 0
else
	echo "No BBD Cordova plugins installed"

	sed -i -e 's/<hook type="after_plugin_rm" src="hooks\/afterBasePluginRemove.sh" \/>//g' "config.xml"
    rm -rf "config.xml-e"

	cordova platform rm ios
	cordova platform add ios

	rm -f hooks/afterBasePluginRemove.sh
fi