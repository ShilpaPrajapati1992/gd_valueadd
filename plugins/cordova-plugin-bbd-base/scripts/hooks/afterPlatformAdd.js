#!/usr/bin/env node

/*
 * (c) 2016 BlackBerry Limited. All rights reserved.
 */

require('shelljs/global');

// This hook will be executed on 'after_platform_add' in case when we have one of the platforms installed (iOS/Andriod)
// also we have BBD Base plugin installed and than we add other platform (Android/iOS)

module.exports = function(context) {
	var fs = context.requireCordovaModule('fs');
	var path = context.requireCordovaModule('path');
	var platformRoot = path.join(context.opts.projectRoot, 'platforms', 'ios');

	var pluginDir = context.opts.plugin.dir;
	var gradlePropertiesPath = path.join(pluginDir, 'scripts', 'gradle', 'gradle.properties');
	var pluginsPath;
	
	if (fs.existsSync(gradlePropertiesPath)) {
		var gradlePropertiesData = fs.readFileSync(gradlePropertiesPath, { encoding: 'utf8' });
		var key = 'bbdCordovaPluginsDir';
		pluginsPath = gradlePropertiesData.substring(gradlePropertiesData.lastIndexOf(key) + key.length + 1, gradlePropertiesData.length);
	}

	var pluginsListStr = exec('cordova plugins list', {silent:true});
	if (pluginsListStr.output) {
		pluginsListStr = pluginsListStr.output;
	} else {
		pluginsListStr = pluginsListStr.stdout;
	}
	
	// Get all plugins list (BBD Cordova plugins + Cordova plugin + some else)
	var pluginsListArr = pluginsListStr.split('\n');
	pluginsListArr.pop();
	// Move BBD Cordova plugins to separate array
	var bbdPluginListArr = [];
	for (var plugin in pluginsListArr) {
		if (pluginsListArr[plugin].indexOf('cordova-plugin-bbd-') >= 0) {
			bbdPluginListArr.push(pluginsListArr[plugin].substring(0, pluginsListArr[plugin].indexOf(' ')));
		}
	}

	// Then we need to kind of 'sort' the array of BBD Cordova plugins to correclty manage dependencies
	// BBD Base plugin should be last one
	// BBD Storage plugin should go just before BBD Base plugin
	// BBD All plugin should be first one if added
	for (var plugin in bbdPluginListArr) {
		if (bbdPluginListArr[plugin] === 'cordova-plugin-bbd-storage') {
			bbdPluginListArr.push(bbdPluginListArr.splice(plugin, 1)[0]);
		}
		if (bbdPluginListArr[plugin] === 'cordova-plugin-bbd-base') {
			bbdPluginListArr.push(bbdPluginListArr.splice(plugin, 1)[0]);
		}

		// If we have BBD All plugin it should be removed with all its dependencies
		if (bbdPluginListArr[0] === 'cordova-plugin-bbd-all') {
			exec('cordova plugin rm cordova-plugin-bbd-all', {silent:true});
			exec('cordova plugin add ' + path.join(pluginsPath, 'cordova-plugin-bbd-all'), {silent:true});
			break;
		} else {
			// remove all plugins in loop based on sorted array
			exec('cordova plugin rm ' + bbdPluginListArr[plugin], {silent:true});
		}
	}

	// Install BBD Cordova plugins back to the app
	for (var plugin in bbdPluginListArr) {
		exec('cordova plugin add ' + path.join(pluginsPath, bbdPluginListArr[plugin]), {silent:true});
	}
}