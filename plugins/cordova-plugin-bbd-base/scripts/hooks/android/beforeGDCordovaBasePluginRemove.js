#!/usr/bin/env node

/*
 * (c) 2016 BlackBerry Limited. All rights reserved.
 */

module.exports = function(context) {
    var fs = context.requireCordovaModule('fs'),
        path = context.requireCordovaModule('path');

    var platformRoot = path.join(context.opts.projectRoot, 'platforms','android');
    var manifestFile = path.join(platformRoot, 'AndroidManifest.xml');
    var packageName;

    // reverting changes to AndroidManifest.xml, MainActivity.java
    if (fs.existsSync(manifestFile)) {
        var manifestFileContent = fs.readFileSync(manifestFile, 'utf8');
        packageName = manifestFileContent.substring(manifestFileContent.indexOf('package="') + 9, manifestFileContent.indexOf('xmlns:android') - 2);
        var packageToPath = path.join.apply(null,packageName.split('.'));
        var packageFullPath = path.join(context.opts.projectRoot, 'platforms','android','src',packageToPath);
        var mainActivity = path.join(packageFullPath, 'MainActivity.java');

        var mainActivityContent = fs.readFileSync(mainActivity, 'utf8');
        
        if (mainActivityContent.indexOf("GDCordovaActivity") > 0) {      
            var gdCordovaActivityImport = mainActivityContent.replace(/import com.good.gd.cordova.core.GDCordovaActivity;/g, '');
            var cordovaActivity = gdCordovaActivityImport.replace(/extends GDCordovaActivity \/\*extends CordovaActivity\*\//g, 'extends CordovaActivity');
        
            fs.writeFileSync(mainActivity, cordovaActivity, 'utf8');
        }

        var applicationName = ' android:name="com.good.gd.cordova.core.GDCordovaApp"';
        if (manifestFileContent.indexOf(applicationName) > 0) {
            var noApplicationName = manifestFileContent.replace(/ android:name="com.good.gd.cordova.core.GDCordovaApp"/g, '');
            var noApplicationIcon = noApplicationName.replace('@drawable/com_bbd_default_logo', '@drawable/icon');
            var supportRtl = noApplicationIcon.replace(/ android:fullBackupContent="@xml\/gd_backup_scheme"/g, ' android:supportsRtl="true"');
            var activityAlwayRetainTaskState = supportRtl.replace(/<activity android:alwaysRetainTaskState="true"/g, '<activity ');

            fs.writeFileSync(manifestFile, activityAlwayRetainTaskState, 'utf8');
        }

    }

    // removing 'gd' and 'gd_backup_support' modules from the project
    var gd = path.join(platformRoot, 'gd');
    var gdBackupSupport = path.join(platformRoot,'gd_backup_support');
    var deleteFolderRecursive = function(deletePath) {
        if( fs.existsSync(deletePath) ) {
            fs.readdirSync(deletePath).forEach(function(file,index){
                var curPath = path.join(deletePath,file);
                if(fs.lstatSync(curPath).isDirectory()) {
                    deleteFolderRecursive(curPath);
                } else {
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(deletePath);
        }
    };
    deleteFolderRecursive(gd);
    deleteFolderRecursive(gdBackupSupport);

    // remove gd dependencies from project.properties file
    var projPropertiesFilePath = path.join(platformRoot, 'project.properties');

    var propertiesContent = fs.readFileSync(projPropertiesFilePath, 'utf8');

    var removedPropertiesContent = propertiesContent
                .replace(/^\s*android\.library\.reference\.\d+=(gd)(?:\s|$)/mg,'')
                .replace(/^\s*android\.library\.reference\.\d+=(gd_backup_support)(?:\s|$)/mg,'');

    fs.writeFileSync(projPropertiesFilePath, removedPropertiesContent, 'utf8');
};