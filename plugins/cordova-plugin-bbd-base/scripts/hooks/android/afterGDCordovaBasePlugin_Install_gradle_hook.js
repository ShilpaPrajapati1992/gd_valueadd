#!/usr/bin/env node

/*
 * (c) 2016 BlackBerry Limited. All rights reserved.
 */

var ncp = require('ncp').ncp;
var os = require('os');
var exec = require('child_process').exec;

module.exports = function(context) {

	var cordovaVersion;
	exec("cordova -v", function (error, stdout, stderr) {
		cordovaVersion = stdout;
		if (error !== null) {
		    console.log('exec error: ' + error);
       	} else {
       		var fs = context.requireCordovaModule('fs');
			var path = context.requireCordovaModule('path');
		    var platformRoot = path.join(context.opts.projectRoot, 'platforms','android');
		    var cordovaBuildGradle = path.join(platformRoot,'CordovaLib','build.gradle');
		    var mainBuildGradle = path.join(platformRoot,'build.gradle');

		    if (cordovaVersion.includes("5.0.0") || cordovaVersion.includes("5.1.1")) {
				if (fs.existsSync(cordovaBuildGradle)) {
			    	fs.readFile(cordovaBuildGradle, 'utf8', function (err,data) {
			    		if (err) {
					        throw new Error('Unable to find cordova build.gradle: ' + err);
					    }

					    //replace android gradle plugin version
				    	var modified = data.replace(/if\s*\(gradle.gradleVersion[\s\S]*else\s*\{[\s\S]*classpath.*[\n\r]*.*[\n\r].*\}/gm, "dependencies {\n\t\tclasspath 'com.android.tools.build:gradle:1.5.0'\n\t}")
				    					   .replace(/apply\splugin:\s*\'android-library\'/gm, "apply plugin: \'com.android.library\'")
				    					   .replace(/android\s*\{[\n\r]\s/gm, "android {\n\tuseLibrary 'org.apache.http.legacy'\n");
				    	
					    fs.chmodSync(cordovaBuildGradle,'660');

				    	fs.writeFile(cordovaBuildGradle, modified, 'utf8', function (err) {
					 		if (err) throw new Error('Unable to write into cordova build.gradle: ' + err);
					 	});				
			   		});
			    } else {
			    	console.log(cordovaBuildGradle + " doesn't exist");
			    }
			} 

			if(cordovaVersion.includes("5.0.0") || cordovaVersion.includes("5.1.1") || cordovaVersion.includes("5.4.1")) {
			    if (fs.existsSync(mainBuildGradle)) {
			    	fs.readFile(mainBuildGradle, 'utf8', function (err,data) {
			    		if (err) {
					        throw new Error('Unable to find main build.gradle: ' + err);
					    }

					    //replace android gradle plugin version
				    	var modified = data.replace(/if\s*\(gradle.gradleVersion[\s\S]*else\s*\{[\s\S]*classpath.*[\n\r]*.*[\n\r].*\}/gm, "dependencies {\n\t\tclasspath 'com.android.tools.build:gradle:1.5.0'\n\t}")
				    					   .replace(/apply\splugin:\s*\'android\'/gm, "apply plugin: \'com.android.application\'");
				    	
					    fs.chmodSync(mainBuildGradle,'660');

				    	fs.writeFile(mainBuildGradle, modified, 'utf8', function (err) {
					 		if (err) throw new Error('Unable to write into main build.gradle: ' + err);
					 	});						
			   		});
			    } else {
			    	console.log(cordovaBuildGradle + " doesn't exist");
			    }
			}     		
       	}
	});
}
