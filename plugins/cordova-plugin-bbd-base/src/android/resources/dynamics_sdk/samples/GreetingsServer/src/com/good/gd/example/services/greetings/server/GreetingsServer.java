/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.services.greetings.server;

import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.good.gd.GDAndroid;
import com.good.gd.GDStateListener;
import com.good.gd.icc.GDService;
import com.good.gd.icc.GDServiceException;

/**
 * 
 * This is main application activity. This activity initializes GD library.
 * 
 */
public class GreetingsServer extends Activity implements GDStateListener,
		OnClickListener {

	static final String TAG = GreetingsServer.class.getSimpleName();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG,
				"######################## Greetings ServerServer onCreate ########################\n");

		GDAndroid.getInstance().activityInit(this);

		setContentView(R.layout.main);
	}

	@Override
	public void onResume() {
		Log.d(TAG,
				"######################## Greetings Server onResume ########################\n");
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	private void showAuthorizedUI() {
		TextView t = (TextView) findViewById(R.id.authStatus);
		t.setText("Authorized");
		t.setTextColor(Color.rgb(0, 255, 0));
	}

	private void showNotAuthorizedUI() {
		TextView t = (TextView) findViewById(R.id.authStatus);
		t.setText("Not Authorized");
		t.setTextColor(Color.rgb(255, 0, 0));
	}

	private void displayMessage(String title, String message) {
		Log.d(TAG, "displayMessage title: " + title + " message:" + message
				+ "\n");
		final Builder alert = new AlertDialog.Builder(this).setTitle(title)
				.setMessage(message)
				.setNeutralButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int value) {
						// Just dismiss the dialog
					}
				});

		this.runOnUiThread(new Runnable() {
			public void run() {
				alert.show();
			}
		});
	}

	@Override
	public void onAuthorized() {
		Log.d(TAG,
				"######################## Greetings Server onAuthorized ########################\n");
		showAuthorizedUI();
	}

	@Override
	public void onLocked() {
		Log.d(TAG,
				"######################## Greetings Server onLocked ########################\n");
		showNotAuthorizedUI();
	}

	@Override
	public void onWiped() {
		Log.d(TAG,
				"######################## Greetings Server onWiped ########################\n");
	}

	@Override
	public void onUpdatePolicy(Map<String, Object> policyValues) {
		Log.d(TAG,
				"######################## Greetings Server onUpdatePolicy ########################\n");

	}

	@Override
	public void onUpdateServices() {
		Log.d(TAG,
				"######################## Greetings Server onUpdateServices ########################\n");

	}

    @Override
    public void onUpdateDataPlan() {
        Log.d(TAG,
                "######################## Greetings Server onUpdateDataPlan ########################\n");

    }

	@Override
	public void onUpdateConfig(Map<String, Object> settings) {
		Log.d(TAG,
				"######################## Greetings Server onUpdateConfig ########################\n");

	}

    @Override
    public void onUpdateEntitlements() {
        Log.d(TAG,
              "######################## Greetings Server onUpdateEntitlements ########################\n");
        
    }

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.action_bring_to_front:
			Log.d(TAG, "+ bringToFront");
			try {
				GDService
						.bringToFront("com.good.gd.example.services.greetings.client");
			} catch (GDServiceException e) {
				displayMessage("Error", e.getMessage());
			}
			Log.d(TAG, "- bringToFront");
			break;

		default:
			break;
		}

	}
}