/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.policy;

import android.os.Bundle;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.good.gd.Activity;
import com.good.gd.GDAndroid;
import com.good.gd.log.GDLogManager;
import com.good.gd.widget.GDTextView;

import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

/**
 * ApplicationPolicyMainActivity is an observer for ApplicationPolicyModel class.
 * updatePolicyUI method will be called on each Application policy changing.
 */

public class ApplicationPolicyMainActivity extends Activity implements Observer {

	private static final String TAG = ApplicationPolicyMainActivity.class.getSimpleName();

	private static final String DISPLAY = "display";
	private static final String TABS = "tabs";
	private static final String UPDATE = "update";
	private static final String CAN_UPDATE = "canUpdate";
	private static final String VERSION = "version";
	
	private static final String PERS = "Pers";
	private static final String PRESS = "Press";
	private static final String PROJ = "Proj";
	private static final String SALES = "Sales";
	private ApplicationPolicyModel _appPolicyModel;

	private boolean userEnabledLogging;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        if (isFinishing()) {
        	// In the case where the application is unloaded with some activities in the stack
        	// the GD library will automatically finish() this activity in super.onCreate.
        	ApplicationPolicy.doAuthorization(this);
        	return;
        }
        // Get shared instance of application policy model
        _appPolicyModel = ApplicationPolicyModel.getInstance();
        
        // Add himself as observer of application policy changes
		_appPolicyModel.addObserver(this);
        setContentView(R.layout.main);
		updatePolicyUI();
		updateLogStatusText();
		updateSecurityUI();
		updateAppConfigUI();

		final Button refreshLoggingStatusButton = (Button) findViewById(
				R.id.refreshLoggingStatusButton);
		refreshLoggingStatusButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				updateLogStatusText();
			}
		});

		final Button detailedLoggingButton = (Button) findViewById(R.id.detailedLoggingButton);
		detailedLoggingButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				long duration = userEnabledLogging ? 0 : TimeUnit.MINUTES.toMillis(1);

				boolean success = GDLogManager.getInstance().detailedLoggingFor(duration);
				Log.i(TAG, "Detailed logging called with result: " + success);
				if (!userEnabledLogging) {
					// user successfully enabled user logging
					userEnabledLogging = success;
				} else {
					// user tried to turn off detailed logging, so toggle back to false only if it succeeded.
					userEnabledLogging = !success;
				}

				updateLogStatusText();
			}
		});

		final Button diagnosticsButton = (Button) findViewById(R.id.diagnosticsGoButton);
		diagnosticsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				updateSecurityUI();
			}
		});

		final Button appConfigButton = (Button) findViewById(R.id.appconfigGoButton);
		appConfigButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				updateAppConfigUI();
			}
		});
	}

	private void updateLogStatusText() {
		Map<String, Object> appConfig = GDAndroid.getInstance().getApplicationConfig();
		boolean preventUserDetailedLogging =
				(Boolean) appConfig.get(GDAndroid.GDAppConfigKeyPreventUserDetailedLogs);
		boolean loggingEnabled = (Boolean) appConfig.get(GDAndroid.GDAppConfigKeyDetailedLogsOn);
		GDTextView loggingStatusText = (GDTextView) findViewById(R.id.loggingStatus);
		loggingStatusText.setText(getString(R.string.loggingStatus,
				loggingEnabled ? "ON" : "OFF",
				preventUserDetailedLogging ? "ON" : "OFF"));

		// figure out the User Detailed Logging button text
		final Button detailedLoggingButton = (Button) findViewById(R.id.detailedLoggingButton);
		if (userEnabledLogging) {
			if (loggingEnabled) {
				detailedLoggingButton.setText(getResources().getString(R.string.disableDetailedLogging));
			} else {
				// user turned on detailed logging but it is not enabled any more.  must have expired.
				userEnabledLogging = false;
			}
		}

		if (!userEnabledLogging) {
			detailedLoggingButton.setText(getResources().getString(R.string.enableDetailedLogging));
			// disable the button if they aren't allowed to turn it on.
			detailedLoggingButton.setEnabled(!preventUserDetailedLogging);
		}
	}

	private void updateAppConfigUI() {
		final Activity app = this;
		final String diagnostics = _appPolicyModel.getAppConfigString();
		Time startTime = new Time();
		startTime.setToNow();
		final String datetime = DateUtils.formatDateTime(app, startTime.toMillis(true),
				DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME);
		this.runOnUiThread(new Runnable() {
			public void run() {
				TextView t = (TextView) findViewById(R.id.appconfigResponse);
				t.setText(getString(R.string.updated,diagnostics,datetime));

			}
		});
	}

	private void updateSecurityUI() {
		final Activity app = this;
		final String diagnostics = _appPolicyModel.getPolicyDiagnosticsString();
		Time startTime = new Time();
		startTime.setToNow();
		final String datetime = DateUtils.formatDateTime(app, startTime.toMillis(true),
				DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME);
		this.runOnUiThread(new Runnable() {
			public void run() {
				TextView t = (TextView) findViewById(R.id.diagnosticsResponse);
				t.setText(getString(R.string.updated,diagnostics,datetime));

			}
		});
	}

	// UI
	// Used from framework callback, therefore we have to make sure we run this
	// on the UI thread.
	// This demonstrates updating some UI elements based on the policy. Some other
	// policy values are output to the debug logs for reference.
	private void updatePolicyUI() {
		final Activity app = this;
		Log.d(TAG, "+ updatePolicyUI");
		
		// Retrieve actual application-specific policy settings in a JSON string
		final String policy = _appPolicyModel.getPolicyString();
		Log.d(TAG, "policy=" + policy);
		
		// Retrieve actual collection of application-specific policy settings
		final Map<String, Object> policies = _appPolicyModel.getPolicyMap();
		Log.d(TAG, "policies=" + policies.toString());
		
		// Retrieve timestamp for actual application-specific policy settings
		Time startTime = new Time();
		startTime.setToNow();
		final String datetime = DateUtils.formatDateTime(app, startTime.toMillis(true), 
			DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME);
		
		this.runOnUiThread(new Runnable() {
			public void run() {				
				TextView t= (TextView)findViewById(R.id.policy);
				t.setText(getString(R.string.policyUpdated,policy,datetime));

				CheckBox checkBoxPersonnel = (CheckBox)findViewById(R.id.checkPersonnel);
				CheckBox checkBoxPress = (CheckBox)findViewById(R.id.checkPress);
				CheckBox checkBoxProjects = (CheckBox)findViewById(R.id.checkProjects);
				CheckBox checkBoxSales = (CheckBox)findViewById(R.id.checkSales);
				
				checkBoxPersonnel.setChecked(false);
				checkBoxPress.setChecked(false);
				checkBoxProjects.setChecked(false);
				checkBoxSales.setChecked(false);

                @SuppressWarnings("unchecked")
                Map<String, Object> display = (Map<String, Object>)policies.get(DISPLAY);
				Log.d(TAG, "display=" + display);
				if (display != null) {
                    @SuppressWarnings("unchecked")
                    Vector<Object> tabs = (Vector<Object>)display.get(TABS);
					Log.d(TAG, "tabs=" + tabs);
					if (tabs != null) {
						Iterator<Object> tabIterator = tabs.iterator();
						while (tabIterator.hasNext()) {  
							String tab = (String)tabIterator.next();
							Log.d(TAG, "-tab=" + tab);
							if (tab.equals(PERS)) {
								checkBoxPersonnel.setChecked(true);
							} else if (tab.equals(PRESS)) {
								checkBoxPress.setChecked(true);
							} else if (tab.equals(PROJ)) {
								checkBoxProjects.setChecked(true);
							} else if (tab.equals(SALES)) {
								checkBoxSales.setChecked(true);
							}
						}
					}
					else {
						Log.e(TAG, "No tab's settings in policy!");
					}
				}
				else {
					Log.e(TAG, "No display policy!");
				}
			}
		});
		// Example of retrieving Boolean and Number values from the policy
        @SuppressWarnings("unchecked")
        Map<String, Object> update = (Map<String, Object>)policies.get(UPDATE);
		if (update != null) {
			Boolean canUpdate = (Boolean)update.get(CAN_UPDATE);
			Log.d(TAG, "canUpdate=" + canUpdate);
		}
		else {
			Log.e(TAG, "No update policy!");
		}
		
		Object version = policies.get(VERSION);
		if (version != null) {
			Log.d(TAG, "version=" + version.toString());
		}
		else {
			Log.e(TAG, "No version!");
		}
		Log.d(TAG, "- updatePolicyUI");
	}

	@Override
	/**
	 * This method is called whenever any one from observed objects is changed
	 */
	public void update(Observable observable, Object data) {
		Log.d(TAG, "+ update()");
		// Check if changed observable object is application policy then update policy UI
		if (observable == _appPolicyModel) {
			updatePolicyUI();
			updateSecurityUI();
			updateAppConfigUI();
		}
		Log.d(TAG, "- update()");
	}
}
