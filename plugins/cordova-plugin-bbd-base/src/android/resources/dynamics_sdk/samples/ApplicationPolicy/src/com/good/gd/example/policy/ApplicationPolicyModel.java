/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.policy;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import android.util.Log;

import com.good.gd.GDAndroid;
import com.good.gd.diagnostic.GDDiagnostic;
import com.good.gd.error.GDNotAuthorizedError;

/**
 * A singleton observable class for application policies.
 * A single shared instance of this class will be used to retrieve it from the Good Dynamics Runtime and
 * store actual values of application policies, update it on receive events about policy changes.
 * This class is observable and any other classes can start observing of it to retrieve policy change events.
 */
public class ApplicationPolicyModel extends Observable {
	private static final String TAG = ApplicationPolicyModel.class.getSimpleName();
	private String _policyString;
	private Map<String, Object> _policyMap;
	private static ApplicationPolicyModel _instance;
	private GDAndroid _gdAndroid;

	/**
	 * The constructor sets initial values of policies or sets empty values in case of unauthorized state
	 */
	private ApplicationPolicyModel() {
		Log.d(TAG, "+ ApplicationPolicyModel()");
		_gdAndroid = GDAndroid.getInstance();
		try {
			_policyString = _gdAndroid.getApplicationPolicyString();
			_policyMap = _gdAndroid.getApplicationPolicy();
		} catch (GDNotAuthorizedError e) {
			Log.d(TAG, "GDNotAuthorizedError " + e.getMessage());
			_policyString = "";
			_policyMap = new HashMap<String, Object>();
		}
		Log.d(TAG, "policyString= " + _policyString);
		Log.d(TAG, "policyMap= " + _policyMap.toString());
		Log.d(TAG, "- ApplicationPolicyModel()");
	}

	/**
	 * @return return single shared instance of class
	 */
	public static synchronized ApplicationPolicyModel getInstance() {
		if (_instance == null) {
			_instance = new ApplicationPolicyModel();
		}
		return _instance;
	}
	
	/**
	 * The updatePolicy method updates model with new values of application policy by the 
	 * Good Dynamics Runtime calls and notifies observers about changes
	 */
	public void updatePolicy() {
		Log.d(TAG, "+ updatePolicy()");
		synchronized (this) {
			try {
				_policyString = _gdAndroid.getApplicationPolicyString();
				_policyMap = _gdAndroid.getApplicationPolicy();
			} catch (GDNotAuthorizedError e) {
				Log.d(TAG, "GDNotAuthorizedError " + e.getMessage());
				// Set empty values in case of unauthorized state
				_policyString = "";
				_policyMap = new HashMap<String, Object>();
			}

		}
		Log.d(TAG, "policyString= " + _policyString);
		Log.d(TAG, "policyMap= " + _policyMap);
		setChanged();
		notifyObservers();
		Log.d(TAG, "- updatePolicy()");
	}

	public synchronized String getPolicyDiagnosticsString() {
		String policyInfo = GDDiagnostic.getInstance().getCurrentSettings();
		Log.d(TAG, "getPolicyDiagnosticsString() = " + policyInfo);

		return policyInfo;
	}

	public synchronized String getAppConfigString() {
		String appConfig = _gdAndroid.getApplicationConfig().toString();
		Log.d(TAG, "getAppConfigString() = " + appConfig);
		return appConfig;
	}

	/**
	 * @return application-specific policy settings in a JSON string. 
	 */
	public synchronized String getPolicyString() {
		Log.d(TAG, "getPolicyString() = " + _policyString);
		return _policyString;	
	}

	/**
	 * @return a collection of application-specific policy settings
	 */
	public synchronized Map<String, Object> getPolicyMap() {
		Log.d(TAG, "getPolicyMap() = " + _policyMap);
		return _policyMap;	
	}
}
