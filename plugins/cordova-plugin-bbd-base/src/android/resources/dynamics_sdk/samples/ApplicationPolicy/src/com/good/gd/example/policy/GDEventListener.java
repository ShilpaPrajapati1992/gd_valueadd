/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.policy;

import android.content.Context;
import android.content.Intent;

import com.good.gd.GDAppEvent;
import com.good.gd.GDAppEventListener;
import com.good.gd.GDAppEventType;

/**
 * Handles GD events. Notice that an instance of this class is passed to authorize(),
 * which will retain it as the callback. 
 */
public class GDEventListener implements GDAppEventListener {

	private Intent _uiIntent = null;
	private Context _context = null;
	private boolean _uiLaunched = false;
	private ApplicationPolicyModel _appPolicyModel;
	
	
    /** onGDEvent - handles events from the GD library including authorization
     * and withdrawal of authorization, policies updating. 
     * 
     * @see com.good.gd.GDAppEventListener#onGDEvent(com.good.gd.GDAppEvent)
     */
    public void onGDEvent(GDAppEvent anEvent) {
    	GDAppEventType eventType = anEvent.getEventType();
    	// Get a shared instance of application policy model
    	if (_appPolicyModel == null) {
    		_appPolicyModel = ApplicationPolicyModel.getInstance();
    	}
        if (eventType == GDAppEventType.GDAppEventAuthorized) {
        	// Update application policy immediately after passing of authorization
        	_appPolicyModel.updatePolicy();
        	if (!_uiLaunched && _uiIntent != null && _context != null) {
        		_context.startActivity(_uiIntent);
        		_uiLaunched = true;
        	}
        } else if (eventType == GDAppEventType.GDAppEventPolicyUpdate) {
        	// Update application policy after receiving of an event about a change to one or more
        	// application-specific policy settings
				_appPolicyModel.updatePolicy();
        	}
    }

    /** setUILaunchIntent - specifies the intent that is sent in order to launch the
     * application UI once the application is authorized.
     */
	public void setUILaunchIntent(Intent i, Context ctx) {		
		_uiIntent = i;
		_context = ctx;
	}
}
