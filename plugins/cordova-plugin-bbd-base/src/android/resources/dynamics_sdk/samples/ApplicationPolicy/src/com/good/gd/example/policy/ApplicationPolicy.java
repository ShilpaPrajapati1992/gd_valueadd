/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.policy;

import android.content.Context;
import android.content.Intent;

import com.good.gd.Activity;
import com.good.gd.GDAndroid;

/** LaunchActivity - the entry point activity which will start authorization with Good Dynamics
 * and once done launch the application UI.
 */
public class ApplicationPolicy extends Activity {

    private GDAndroid _gd = null;
    private boolean _authorizeCalled = false;

    /** onResume - used to call authorize to initialize the GD library
     */
    @Override
    public void onResume() {
        super.onResume();

        if (!_authorizeCalled) {

            // Create a GDEventListener which will receive the library events and invoke
            // the UI specified in the intent once authorized.
            GDEventListener listener = new GDEventListener();
            listener.setUILaunchIntent(new Intent(this, ApplicationPolicyMainActivity.class), this);

            _gd = GDAndroid.getInstance();
            _gd.authorize(listener);
            _authorizeCalled = true;

        } else {
            // If this launch activity is resumed and it's already called authorize then it needs
            // to be removed. The user launching the application again will get a fresh LaunchActivity
            // which will call authorize again.
            finish();
        }
    }

    /** doAuthorization - entry point for any other activity to call if it detects
     * on starting that it is not authorized.
     */
    public static void doAuthorization(Context ctx) {
        Intent i = new Intent(ctx, ApplicationPolicy.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        ctx.startActivity(i);
    }

}
