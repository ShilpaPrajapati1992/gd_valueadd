package com.good.gd.example.services.greetings.client;

import android.os.Bundle;
import android.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class GreetingsClientDialogFragment extends DialogFragment {


    private static final String TITLE_KEY = "title_key";
    private static final String MESSAGE_KEY = "message_key";

    private String message;
    private String title;

    static GreetingsClientDialogFragment createInstance(String title, String message) {

        GreetingsClientDialogFragment mess = new GreetingsClientDialogFragment();
        Bundle bdl = new Bundle(2);
        bdl.putString(TITLE_KEY, title);
        bdl.putString(MESSAGE_KEY, message);
        mess.setArguments(bdl);
        return mess;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        message = getArguments().getString(MESSAGE_KEY);
        title = getArguments().getString(TITLE_KEY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_fragment, container);
        TextView t = (TextView) view.findViewById(R.id.message);
        t.setText(message);
        getDialog().setTitle(title);

        Button b = (Button) view.findViewById(R.id.dismiss_button);
        b.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }

        });

        return view;
    }




}
