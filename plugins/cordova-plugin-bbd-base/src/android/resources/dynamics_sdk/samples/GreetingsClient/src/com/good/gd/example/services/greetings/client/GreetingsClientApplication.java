package com.good.gd.example.services.greetings.client;

import com.good.gd.icc.GDService;
import com.good.gd.icc.GDServiceException;
import com.good.gd.icc.GDServiceClient;
import com.good.gd.icc.GDServiceClientListener;
import com.good.gd.icc.GDServiceListener;

import android.app.Application;
import android.util.Log;


public class GreetingsClientApplication extends Application {

	static final String TAG = GreetingsClientApplication.class.getSimpleName();
	
	@Override
	public void onCreate() {
		super.onCreate();
	
		GDServiceClientListener client = GreetingsClientGDServiceListener.getInstance();
		GDServiceListener server = GreetingsClientGDServiceListener.getInstance();
		
		Log.e(TAG , "GreetingsClientApplication::onCreate() service client Listener = " + client + "\n");
		
		if (client != null) {
    		//Set the Client Service Listener to get responses from server
    		try {
				GDServiceClient.setServiceClientListener(client);
				GDService.setServiceListener(server);
    		} catch (GDServiceException e) {
    			Log.e(TAG , "GreetingsClientApplication::onCreate()  Error Setting GDServiceClientListener --" + e.getMessage()  + "\n");
    		}
    	}
	}

	
	
	
}
