package com.good.gd.example.push;

import java.util.Map;

import com.good.gd.GDAndroid;
import com.good.gd.GDStateListener;

import android.app.Application;

public class PushApplication extends Application implements GDStateListener {


	private PushEventHandler _pushEventHandler;
	private PushActivityCallback _currentActivity;
	private boolean authorized = false;
	
	//Application singleton. holds the singleton Push Handler and receives GD state updates. Manages sending updates to Activities
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		GDAndroid.getInstance().setGDStateListener(this);
	}

	public void registerActivity(PushActivityCallback callback) {
	
		_currentActivity = callback;
		
		if(authorized){
			_currentActivity.onAuthorized(_pushEventHandler);
		}
		
	}
	
	public void unregisterActivity(PushActivityCallback callback){
		
		if(_currentActivity==callback){
			_currentActivity = null;
		}
	}
	
	
	@Override
	public void onAuthorized() {

		authorized = true;
		_pushEventHandler = PushEventHandler.getInstance();
		
		if(_currentActivity!=null){
			_currentActivity.onAuthorized(_pushEventHandler);
		}
	}

	@Override
	public void onLocked() {

	}

	@Override
	public void onUpdateConfig(Map<String, Object> arg0) {

	}

	@Override
	public void onUpdatePolicy(Map<String, Object> arg0) {

	}

	@Override
	public void onUpdateServices() {

	}

    @Override
    public void onUpdateDataPlan() {

    }

    @Override
    public void onUpdateEntitlements() {

    }

	@Override
	public void onWiped() {
		
		authorized = false;
		if(_currentActivity!=null){
			_currentActivity.onNotAuthorized(_pushEventHandler);
		}
		
	}

	
	
}
