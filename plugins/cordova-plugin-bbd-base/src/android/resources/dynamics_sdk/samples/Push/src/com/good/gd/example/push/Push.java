/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2015 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.push;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ScrollView;
import android.widget.TextView;

import com.good.gd.GDAndroid;
import com.good.gd.GDStateListener;
import com.good.gd.push.PushChannelListener;
import com.good.gd.push.PushConnectionListener;
import com.good.gd.widget.GDEditText;

/**
 * Push activity; shows all incoming events, plus internal events like onPause,
 * onResume, and when buttons are clicked.
 */
public class Push extends Activity implements
		PushConnectionListener, PushChannelListener, GDStateListener,
		OnClickListener {

	private TextView _statusTitle;
	private TextView _statusTextView;
	private ScrollView _scroller;
	private DateFormat _timeFormatter;

	private PushEventHandler _pushEventHandler;

	private final String STATE_STATUS_TEXT = "status_text";
	String statusText = null;

	public Push() {
		super();
		_timeFormatter = new SimpleDateFormat("HH:mm:ss", Locale.US);
	}

	/** onCreate - called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		GDAndroid.getInstance().activityInit(this);
		setContentView(R.layout.main);

		if (savedInstanceState != null) {
			statusText = savedInstanceState.getString(STATE_STATUS_TEXT);
		}
		else {
			// link to the various elements of the main view
			_scroller = (ScrollView) findViewById(R.id.status_scroller);
			_statusTitle = (TextView) findViewById(R.id.status_title);
			_statusTextView = (TextView) findViewById(R.id.status_view);
		}

		if (statusText != null) {
			_statusTextView.setText(statusText);
		}
	}

	/** onDestroy - called when the activity is being destroyed */
	@Override
	public void onDestroy() {
		super.onDestroy();
		// clear the listener for the push connection
		if (_pushEventHandler != null) {
			_pushEventHandler.setPushListener(null);
			_pushEventHandler.setChannelListener(null);
		}
	}

    @Override
    public void onResume() {
        super.onResume();
        updateTitleAndButtons();
        logConnectionStatus();
    }
            
	public boolean isOnline() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
		return (netInfo != null && netInfo.isConnectedOrConnecting());
	}

	public void showErrorDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		// Customize error dialog
		builder.setPositiveButton(R.string.push_error_dialog_button_ok, null);
		builder.setTitle(R.string.push_error_dialog_title);
		builder.setMessage(R.string.push_error_dialog_message);
		builder.setIcon(android.R.drawable.ic_dialog_alert);

		// Create the AlertDialog
		builder.create().show();
	}

	/**
	 * showPushMessageDialog - shows a dialog which will capture from the user a
	 * message to be sent
	 */
	public void showPushMessageDialog() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle(R.string.push_message);
		final GDEditText input = new GDEditText(this);
		alert.setView(input);
		input.setId(R.id.pushMessage);
		alert.setPositiveButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						String message = input.getText().toString();
						_pushEventHandler.sendLoopbackMessage(message);
					}
				});
		alert.setNegativeButton(android.R.string.cancel, null);
		alert.show();
	}

	/** onStatus - */
	@Override
	public void onStatus(int status) {
		updateTitleAndButtons();
		logConnectionStatus();
	}

	/** onChannelOpen - */
	@Override
	public void onChannelOpen(String token) {
		logStatus("Channel open token = " + token);
		updateTitleAndButtons();
	}

	/** onChannelClose - */
	@Override
	public void onChannelClose(String token) {
		logStatus("Channel close token = " + token);
		updateTitleAndButtons();
	}

	/** onChannelError - */
	@Override
	public void onChannelError(int error) {
		logStatus("Channel error error = " + error);
	}

	/** onChannelMessage - */
	@Override
	public void onChannelMessage(String message) {
		logStatus("Channel message = " + message);
	}

	/** onChannelPingFail - */
	@Override
	public void onChannelPingFail(int error) {
		logStatus("Channel ping fail error = " + error);
	}

	/** logConnectionStatus - logs an UP or DOWN message for the push connection */
	private void logConnectionStatus() {
        if (_pushEventHandler != null) {
            logStatus(_pushEventHandler.isPushConnected() ? "Push connection up"
                      : "Push connection down");
        }
	}

	/**
	 * logStatus - write the specified message to the on-screen log (with
	 * timestamp), and also to the android log
	 */
	private void logStatus(String message) {
		if (_statusTextView == null) {
			_statusTextView = (TextView) findViewById(R.id.status_view);
		}
		
		String outputmessage = (message.length() > 0) ? _timeFormatter
				.format(new Date()) + " " + message + "\n\n" : "\n";
		_statusTextView.append(outputmessage);
		if (_scroller == null) {
			_scroller = (ScrollView) findViewById(R.id.status_scroller);
		}
		_scroller.post(new Runnable() {
			@Override
			public void run() {
				_scroller.fullScroll(View.FOCUS_DOWN);
			}
		});
		Log.v(this.getClass().getPackage().getName(), message + "\n");
	}

	/**
	 * updateTitleAndButtons - used to update the button states and title text
	 * based on state
	 */
	private void updateTitleAndButtons() {
        if (_pushEventHandler != null)
        {
            boolean connected = _pushEventHandler.isPushConnected();
		
            if (_statusTitle == null) {
                _statusTitle = (TextView) findViewById(R.id.status_title);
            }
		
            _statusTitle.setText(connected ? R.string.push_connected_title
                                 : R.string.push_disconnected_title);
            _statusTitle.setTextColor(connected ? Color.GREEN : Color.RED);

            View v = findViewById(R.id.action_connect);
            if (v != null) {
                v.setEnabled(!connected);
            }
            v = findViewById(R.id.action_disconnect);
            if (v != null) {
                v.setEnabled(connected);
            }
            v = findViewById(R.id.action_open_channel);
            if (v != null) {
                v.setEnabled((!_pushEventHandler.isChannelConnected()) && connected);
            }
            v = findViewById(R.id.action_loopback_message);
            if (v != null) {
                v.setEnabled((_pushEventHandler.isChannelConnected()) && connected);
            }
            v = findViewById(R.id.action_close_channel);
            if (v != null) {
                v.setEnabled((_pushEventHandler.isChannelConnected()) && connected);
            }
        }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onAuthorized() {
        
		// set this as a listener for the push connection
		_pushEventHandler = PushEventHandler.getInstance();
		_pushEventHandler.setPushListener(this);
		_pushEventHandler.setChannelListener(this);

        updateTitleAndButtons();
        logConnectionStatus();
	}

	@Override
	public void onLocked() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onWiped() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpdateConfig(Map<String, Object> settings) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpdatePolicy(Map<String, Object> policyValues) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpdateServices() {
		// TODO Auto-generated method stub

	}

    @Override
    public void onUpdateDataPlan() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onUpdateEntitlements() {
        // TODO Auto-generated method stub
        
    }

	@Override
	public void onClick(View v) {
		if (!isOnline()) {
			showErrorDialog();
		}

		switch (v.getId()) {
		case R.id.action_connect:
			_pushEventHandler.connectPush();
			break;
		case R.id.action_disconnect:
			_pushEventHandler.disconnectPush();
			break;
		case R.id.action_open_channel:
			_pushEventHandler.connectChannel();
			break;
		case R.id.action_loopback_message:
			showPushMessageDialog();
			break;
		case R.id.action_close_channel:
			_pushEventHandler.disconnectChannel();
			break;
		default:
			break;
		}

	}
}
