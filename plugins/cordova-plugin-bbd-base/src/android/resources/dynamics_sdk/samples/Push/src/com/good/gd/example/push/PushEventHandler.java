/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.push;

import com.good.gd.push.PushChannel;
import com.good.gd.push.PushChannelListener;
import com.good.gd.push.PushConnection;
import com.good.gd.push.PushConnectionListener;

/** 
 * PushEventHandler - a handler which is responsible for communicating in both directions
 * with the PushConnection and the PushChannel. This example code demonstrates usage of a single
 * Push Channel but an application may open as many as required.
 */
public class PushEventHandler implements PushConnectionListener, PushChannelListener {

	// Singleton so this can manage the relationship with the single connection and channel
	private static PushEventHandler _instance = null;
	public static synchronized PushEventHandler getInstance() {
		if (_instance == null) {
			_instance = new PushEventHandler();
		}
		return _instance;
	}
	
	// Store members which represent the connection, a channel and the token 
	// for the channel. It is possible for an app to open as many channels
	// as it requires, this example just shows one.
	private PushConnection _connection = null;
	private PushChannel    _channel    = null;
	private String         _token      = null;
	
	// Separate listeners are used for the connection and channel events.
	private PushConnectionListener _pushListener    = null;
	private PushChannelListener    _channelListener = null;

	private PushEventHandler() {
		_connection = PushConnection.getInstance();
		_connection.setListener(this);
	}
	
	/* Push Connection */
	
	/** connectPush - open the underlying push connection */ 
	public void connectPush() {
		_connection.connect();
	}
	
	/** disconnectPush - close the underlying push connection */
	public void disconnectPush() {
		_connection.disconnect();
	}
	
	/** isPushConnected - checks whether the underlying connection is up */
	public boolean isPushConnected() {
		return _connection.isConnected();
	}
	
	/** setPushListener - sets a listener for events relating to the push connection */
	public void setPushListener(PushConnectionListener listener) {
		_pushListener = listener;
	}	
	
	/** onStatus - events relating to the underlying connection result in a call to this */
	public void onStatus(int code) {
		if (_pushListener != null) {
			_pushListener.onStatus(code);
		}
	}
	
	/* Push Channel */
	
	/** connectChannel - open the channel, creating it if necessary */
	public void connectChannel() {
		if (_channel == null) {
			_channel = new PushChannel();
			_channel.setListener(this);
		}
		_channel.connect();
	}
	
	/** disconnectChannel - close the channel if it exists, otherwise no-op */
	public void disconnectChannel() {
		if (_channel != null) {
			_channel.disconnect();
			_channel = null;
		}
	}
	
	/** isChannelConnected - returns whether a valid channel is open */
	public boolean isChannelConnected() {
		return (_channel != null && _token != null);
	}
	
	/** setChannelListener - sets a listener for channel related events */
	public void setChannelListener(PushChannelListener listener) {
		_channelListener = listener;
	}

	/** onChannelOpen - */
	public void onChannelOpen(String token) {
		// store the token of the currently open push channel
		_token = token;
		if (_channelListener != null) {
			_channelListener.onChannelOpen(token);
		}		
	}

	/** onChannelClose - */
	public void onChannelClose(String token) {
		// clear the currently active token
		_token = null;		
		if (_channelListener != null) {
			_channelListener.onChannelClose(token);
		}
	}

	/** onChannelError - */
	public void onChannelError(int error) {
		if (_channelListener != null) {
			_channelListener.onChannelError(error);
		}
	}

	/** onChannelMessage - */
	public void onChannelMessage(String message) {
		if (_channelListener != null) {
			_channelListener.onChannelMessage(message);
		}		
	}

	/** onChannelPingFail - */
	public void onChannelPingFail(int error) {
		if (_channelListener != null) {
			_channelListener.onChannelPingFail(error);
		}		
	}
	
	/** sendLoopbackMessage - triggers a message to be sent to the currently open
	 * Push Channel. THis function exists purely for demonstration purposes. In
	 * a normal scenario this message will be sent by an application server.
	 */
	public void sendLoopbackMessage(String message) {
		LoopbackMessage.send(message,  _token);
	}
}