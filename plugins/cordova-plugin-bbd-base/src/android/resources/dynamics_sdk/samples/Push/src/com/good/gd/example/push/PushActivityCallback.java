package com.good.gd.example.push;

public interface PushActivityCallback {

	public void onAuthorized(PushEventHandler handler);
	public void onNotAuthorized(PushEventHandler handler);
}
