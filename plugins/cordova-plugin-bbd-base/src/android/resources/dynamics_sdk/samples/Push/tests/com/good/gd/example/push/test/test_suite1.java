package com.good.gd.example.push.test;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import com.good.gd.GDAndroid;
import com.good.automated_test_support.GDAutomatedTestSupport;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;

import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;

/*
Tests purpose - Ensure Push sample app correct basic operation
 */

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class test_suite1 {

    /*
    Note - The test order of these tests is significant hence the explict test numbering
     */

    /*
    Setup Test, like all tests makes use of helper functions in GD_UIAutomator_Lib Test library project
     */
    @BeforeClass
    public static void setUpClass() {

        //Setup test support and register a GDStateListener
        GDAutomatedTestSupport.setupGDAutomatedTestSupport();

        GDAutomatedTestSupport.wakeUpDeviceIfNeeded();

        //Android Emulator when booted sometimes has error dialogues to dismiss
        GDAutomatedTestSupport.acceptSystemDialogues();

    }

    /*
    Test 1, if GD App is already activated ensure that it can be unlocked using password. If not activated ensure it
    can be activated using Email Address & Access Key. These are set in file com.good.gd.test.json at build time
     */
    @Test
    public void test_1_GDActivation_Push() throws Exception {

        GDAutomatedTestSupport.launchAppUnderTest();

        assertTrue(GDAutomatedTestSupport.loginOrProvisionGDApp());

        assertTrue(GDAutomatedTestSupport.checkGDAuthorized());

        GDAutomatedTestSupport.pressHome();

    }

    /*
    Test 2, test basic operation Connect, OpenChannel, Loopback Message, CloseChannel, Disconnect of Push sample app
    */
    @Test
    public void test_2_BisicOperations_Push(){

        GDAutomatedTestSupport.launchAppUnderTest();

        GDAutomatedTestSupport.waitForIdle(10000);

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_connect"));

        GDAutomatedTestSupport.waitForIdle(10000);

        assertTrue(GDAutomatedTestSupport.isTextShown("Push - Connected"));

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_open_channel"));

        assertTrue(GDAutomatedTestSupport.isTextShown("Channel open token"));

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_loopback_message"));

        assertTrue(GDAutomatedTestSupport.isTextShown("Push Message:"));

        onView(withId(GDAutomatedTestSupport.getResourceID("pushMessage"))).perform(typeText("TestMessage"), closeSoftKeyboard());

        assertTrue(GDAutomatedTestSupport.clickOnItemContainingText("OK"));

        assertTrue(GDAutomatedTestSupport.isTextShown("Channel message = TestMessage"));

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_close_channel"));
        assertTrue(GDAutomatedTestSupport.isTextShown("Channel close token"));

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_disconnect"));

        assertTrue(GDAutomatedTestSupport.isTextShown("Push - Disconnected"));

        GDAutomatedTestSupport.pressHome();
    }

    @After
    public void tearDown() throws Exception {

    }

}