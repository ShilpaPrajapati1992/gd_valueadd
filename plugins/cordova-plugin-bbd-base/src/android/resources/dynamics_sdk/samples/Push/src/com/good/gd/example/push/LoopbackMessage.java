/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.push;

import java.net.URI;

import com.good.gd.apache.http.client.HttpClient;
import com.good.gd.apache.http.client.methods.HttpPost;
import com.good.gd.apache.http.entity.StringEntity;
import com.good.gd.apache.http.impl.client.DefaultHttpClient;

/** 
 * LoopbackMessage - this class allows a message to be sent to the GNP service which simulates
 * what an application server needs to do in order to send a push message to a client device. This 
 * role will in practice be taken by an application server, this class exists merely for the purpose
 * of demonstration.
 */
public class LoopbackMessage {

	public synchronized static void send(final String message, final String token) {
		new Thread() {
			public void run() {
		        try {
		            HttpClient httpclient = new DefaultHttpClient();
		        	HttpPost request = new HttpPost();
		        	final String gnpHost = "gdmdc.good.com";
		        	
		        	// setup the request URL, headers and body
		        	request.setURI(new URI("https://"+gnpHost+"/GNP1.0?method=notify"));
		        	request.setHeader("X-Good-GNP-Token", token);
		        	request.setEntity(new StringEntity(message));
		        	
		        	// send it!
		        	httpclient.execute(request);
		        } catch (Exception exception) {
		            exception.printStackTrace();
		        }				
			}
		}.start();
    }
}

