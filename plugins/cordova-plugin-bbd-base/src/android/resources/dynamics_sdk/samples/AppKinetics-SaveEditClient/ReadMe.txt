### AppKinetics-SaveEditClient ###

================================================================================
DESCRIPTION:
An example of how to write a client which uses the GD Inter Container Comms API.

For more help of how to build and install this application, please see BUILD_NOTES.

For more examples visit the Good Developer Network:
https://begood.good.com/community/gdn

Support:
https://begood.good.com/community/gdn/support
================================================================================
(c) 2015 Good Technology Corporation. All rights reserved.