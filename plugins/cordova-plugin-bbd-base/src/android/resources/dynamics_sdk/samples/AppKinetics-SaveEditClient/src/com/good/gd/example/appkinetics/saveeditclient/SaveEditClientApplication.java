/**
 * SaveEditClientApplication.java
 */
package com.good.gd.example.appkinetics.saveeditclient;

import android.app.Application;
import android.util.Log;

import com.good.gd.icc.GDService;
import com.good.gd.icc.GDServiceClient;
import com.good.gd.icc.GDServiceClientListener;
import com.good.gd.icc.GDServiceException;
import com.good.gd.icc.GDServiceListener;

public class SaveEditClientApplication extends Application {

    private static final String TAG = SaveEditClientApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();

        final GDServiceClientListener clientListener = GDSaveEditClientListener.getInstance();
        final GDServiceListener serviceListener = GDSaveEditClientListener.getInstance();

        try {
            GDServiceClient.setServiceClientListener(clientListener);
            GDService.setServiceListener(serviceListener);
        } catch (final GDServiceException exception) {
            Log.e(TAG, "SaveEditClientApplication::onCreate()  " +
                    "Error Setting GDServiceClientListener -- " + exception.getMessage() + "\n");
        }
    }
}