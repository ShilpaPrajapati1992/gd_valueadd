/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.gdinteraction;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import com.good.gd.GDAppEvent;
import com.good.gd.GDAppEventListener;

/**
 * Handles GD events. An instance of this class is passed to authorize(),
 * which will retain it as the callback for incoming GDAppEvents.
 */
public class GDEventHandler implements GDAppEventListener {

    private boolean isAuthorized = false;           // set when we receive a sucessful authorized GD event

    private WeakReference<GDInteraction> _gdInteractionRef = null;
    private List<GDAppEvent> _eventLog = new ArrayList<GDAppEvent>();

    // singleton
    private static GDEventHandler _instance = null;

    /**
     * onGDEvent - handles events from the GD library including authorization
     * and withdrawal of authorization.
     *
     * @see com.good.gd.GDAppEventListener#onGDEvent(com.good.gd.GDAppEvent)
     */
    public void onGDEvent(GDAppEvent appEvent) {

        switch (appEvent.getEventType()) {
            case GDAppEventAuthorized:
                isAuthorized = true;
                break;

            case GDAppEventNotAuthorized:
                isAuthorized = false;
                break;

            /*
             * Events that can't be received in this handler.
             */
            case GDAppEventRemoteSettingsUpdate:
            case GDAppEventServicesUpdate:
            case GDAppEventPolicyUpdate:
                break;
		default:
			break;
        }

        addEventToLog(appEvent);
        
        if (_gdInteractionRef != null && _gdInteractionRef.get() != null) {
            /*
             * If there is an GDInteraction alive, we should inform it of
             * the new event, so that it can update the view.
             */
          	_gdInteractionRef.get().updateFromEventHandler();
        }
    }

    public boolean isAuthorized() {
        return isAuthorized;
    }

    /*
     * This gets called by the GDInteraction at creation time, so we can call it
     * back when we get a new event.
     */
    public void setGDInteractionActivity(GDInteraction gdInteraction) {
        _gdInteractionRef = new WeakReference<GDInteraction>(gdInteraction);
    }

    // Event log

    private void addEventToLog(GDAppEvent event) {
        _eventLog.add(event);
    }

    int eventCount() {
        return _eventLog.size();
    }

    GDAppEvent eventAtIndex(int index) {
        return _eventLog.get(index);
    }

    // Singleton

    private GDEventHandler() {
        super();
    }

    public static synchronized GDEventHandler getInstance() {
        if (_instance == null) {
            _instance = new GDEventHandler();
        }
        return _instance;
    }
}
