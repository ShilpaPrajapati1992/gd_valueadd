package com.good.gd.example.cutcopypaste;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

/**
    Regular view which uses system ClipboardManager.
    For comparision with SecureClipboardView implementation
 */

public class SystemClipboardView extends EditText {


    ClipboardManager clipboardManager;

    public SystemClipboardView(Context context) {
        super(context);
        init();
    }

    public SystemClipboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SystemClipboardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SystemClipboardView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){
        clipboardManager = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
    }

    @Override
    public boolean onTextContextMenuItem(int id) {

        switch (id) {
            case android.R.id.selectAll:
                return super.onTextContextMenuItem(id);

            case android.R.id.paste:

                if(clipboardManager.hasPrimaryClip()){
                    CharSequence text = clipboardManager.getPrimaryClip().getItemAt(0).getText();

                    getText().replace(getSelectionStart(),getSelectionEnd(),text == null?"":text);
                    return true;
                }

                return false;

            case android.R.id.cut: {
                String selectedText = getSelectedText();

                getText().replace(getSelectionStart(), getSelectionEnd(), "");

                clipboardManager.setPrimaryClip(ClipData.newPlainText("text", selectedText));

                return true;
            }

            case android.R.id.copy: {
                String selectedText = getSelectedText();
                clipboardManager.setPrimaryClip(ClipData.newPlainText("text", selectedText));
                    /* Force the popup menu to disappear.
                     * Since stopTextActionMode is hidden,
                     * we can't call that other than through reflection */
                getText().replace(getSelectionStart(), getSelectionEnd(), selectedText);
                return true;
            }

            // Android M text option - SHARE.
            case android.R.id.shareText:
                return super.onTextContextMenuItem(id);



        }

        return super.onTextContextMenuItem(id);
    }

    private String getSelectedText(){
        int selectionStart = getSelectionStart();
        int selectionEnd = getSelectionEnd();

        Log.i("SystemClipboardView", String.format("getSelectedText start:%d end:%d",selectionStart,selectionEnd));

        if(selectionStart < selectionEnd){
            return getText().subSequence(selectionStart,selectionEnd).toString();
        }

        return "";
    }


}
