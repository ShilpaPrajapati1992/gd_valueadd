/*
 * (c) 2016 BlackBerry Limited. All rights reserved.
 *
 */
package com.good.gd.example.cutcopypaste.dlp;

import com.good.gd.GDAndroid;

import java.util.Map;

// Helper class for checking DLP policies
public class DLPPolicies {

    private static DLPPolicies ourInstance = new DLPPolicies();

    public static DLPPolicies getInstance() {
        return ourInstance;
    }

    private DLPPolicies() {}


    /**
     * @return "Prevent copy from non-GD apps into GD apps" policy flag on GC
     */
    public boolean isInboundDlpEnabled(){
        final Map<String, Object> appConfig = GDAndroid.getInstance().getApplicationConfig();
        return (Boolean) appConfig.get(GDAndroid.GDAppConfigKeyPreventDataLeakageIn);
    }

    /**
     * @return "Prevent copy from GD apps into non-GD apps" policy flag on GC
     */
    public boolean isOutboundDlpEnabled(){
        final Map<String, Object> appConfig = GDAndroid.getInstance().getApplicationConfig();
        return (Boolean) appConfig.get(GDAndroid.GDAppConfigKeyPreventDataLeakageOut);
    }

    /**
     * @return "Prevent Android Dictation" policy flag on GC
     */
    public boolean isDictationPreventionEnabled(){
        final Map<String, Object> appConfig = GDAndroid.getInstance().getApplicationConfig();
        return (Boolean) appConfig.get(GDAndroid.GDAppConfigKeyPreventAndroidDictation);
    }

}
