package com.good.gd.example.cutcopypaste;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;

import com.good.gd.GDAndroid;
import com.good.gd.GDStateListener;
import com.good.gd.widget.GDWebView;

import java.util.Map;

/**
 * Activity which shows both GD text controls that use text encryption/decryption
 * before for performing cut/copy/paste operations and non-GD text controls.
 */
public class SecureCopyPasteActivity extends Activity implements GDStateListener {

    private static final String TAG = SecureCopyPasteActivity.class.getSimpleName();

    private static final String MIME_TYPE_HTML = "text/html";
    private static final String ENCODING_UTF_8 = "utf-8";

    private static final String GD_EDIT_TEXT_CONTENT_KEY = "gd_edit_text_data_key";
    private static final String GD_AUTO_COMPLETE_TEXT_VIEW_CONTENT_KEY = "gd_auto_complete_text_view_key";
    private static final String GD_SEARCH_VIEW_CONTENT_KEY = "gd_search_view_content_key";

    private static final String EDIT_TEXT_CONTENT_KEY = "edit_text_data_key";
    private static final String AUTO_COMPLETE_TEXT_VIEW_CONTENT_KEY = "auto_complete_text_view_key";
    private static final String SEARCH_VIEW_CONTENT_KEY = "search_view_content_key";
    private static final String SECURE_VIEW_CONTENT_KEY = "secure_view_content_key";
    private static final String SYSTEM_VIEW_CONTENT_KEY = "system_view_content_key";

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GDAndroid.getInstance().activityInit(this);

        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            restoreFromSavedInstanceState(savedInstanceState);
        }

        final GDWebView gdWebView = (GDWebView) findViewById(R.id.gd_web_view);
        gdWebView.loadData(getString(R.string.gd_web_view_text),
                MIME_TYPE_HTML,
                ENCODING_UTF_8);
        final WebView webView = (WebView) findViewById(R.id.non_gd_web_view);
        webView.loadData(getString(R.string.non_gd_web_view_text),
                MIME_TYPE_HTML,
                ENCODING_UTF_8);
    }

    private void restoreFromSavedInstanceState(final Bundle savedState) {
        final EditText gdEditText = (EditText) findViewById(R.id.gd_edit_text);
        final AutoCompleteTextView gdAutoCompleteTextView =
                (AutoCompleteTextView) findViewById(R.id.gd_auto_complete_text_view);
        final SearchView gdSearchView = (SearchView)
                findViewById(R.id.gd_search_view);

        final EditText editText = (EditText) findViewById(R.id.edit_text);
        final AutoCompleteTextView autoCompleteTextView =
                (AutoCompleteTextView) findViewById(R.id.auto_complete_text_view);
        final SearchView searchView = (SearchView)
                findViewById(R.id.search_view);
        final TextView secureClipboardView = (TextView)findViewById(R.id.text_view_gd_clipboard);
        final TextView systemClipboardView = (TextView)findViewById(R.id.text_view_system_clipboard);

        gdEditText.setText(savedState.getCharSequence(GD_EDIT_TEXT_CONTENT_KEY));
        gdAutoCompleteTextView.setText(
                savedState.getCharSequence(GD_AUTO_COMPLETE_TEXT_VIEW_CONTENT_KEY));
        gdSearchView.setQuery(
                savedState.getCharSequence(GD_SEARCH_VIEW_CONTENT_KEY), false);
        editText.setText(savedState.getCharSequence(EDIT_TEXT_CONTENT_KEY));
        autoCompleteTextView.setText(
                savedState.getCharSequence(AUTO_COMPLETE_TEXT_VIEW_CONTENT_KEY));
        searchView.setQuery(savedState.getCharSequence(SEARCH_VIEW_CONTENT_KEY), false);
        secureClipboardView.setText(savedState.getCharSequence(SECURE_VIEW_CONTENT_KEY));
        systemClipboardView.setText(savedState.getCharSequence(SYSTEM_VIEW_CONTENT_KEY));
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

        final EditText gdEditText = (EditText) findViewById(R.id.gd_edit_text);
        final AutoCompleteTextView gdAutoCompleteTextView =
                (AutoCompleteTextView) findViewById(R.id.gd_auto_complete_text_view);
        final SearchView gdSearchView = (SearchView)
                findViewById(R.id.gd_search_view);

        final EditText editText = (EditText) findViewById(R.id.edit_text);
        final AutoCompleteTextView autoCompleteTextView =
                (AutoCompleteTextView) findViewById(R.id.auto_complete_text_view);
        final SearchView searchView = (SearchView)
                findViewById(R.id.search_view);

        TextView secureClipboardView = (TextView)findViewById(R.id.text_view_gd_clipboard);
        final TextView systemClipboardView = (TextView)findViewById(R.id.text_view_system_clipboard);

        outState.putCharSequence(GD_EDIT_TEXT_CONTENT_KEY, gdEditText.getText());

        outState.putCharSequence(GD_AUTO_COMPLETE_TEXT_VIEW_CONTENT_KEY,
                gdAutoCompleteTextView.getText());

        outState.putCharSequence(GD_SEARCH_VIEW_CONTENT_KEY,
                gdSearchView.getQuery());

        outState.putCharSequence(EDIT_TEXT_CONTENT_KEY, editText.getText());

        outState.putCharSequence(AUTO_COMPLETE_TEXT_VIEW_CONTENT_KEY,
                autoCompleteTextView.getText());

        outState.putCharSequence(SEARCH_VIEW_CONTENT_KEY,
                searchView.getQuery());

        outState.putCharSequence(SECURE_VIEW_CONTENT_KEY,
                secureClipboardView.getText());

        outState.putCharSequence(SYSTEM_VIEW_CONTENT_KEY,
                systemClipboardView.getText());
    }

    public void launchPreferences(View aView){

        Intent i = new Intent();
        i.setClass(this.getApplicationContext(), PreferencesActivity.class);

        startActivity(i);
    }

    public void launchFingerprint(View aView){
        GDAndroid.getInstance().openFingerprintSettingsUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void clearAllFields(View view){

        recursiveLoopChildren((ViewGroup) findViewById(android.R.id.content), new ViewAction() {
            @Override
            public void execute(View view) {
                if(view instanceof EditText){
                    ((EditText)view).setText("");
                }
            }
        });
    }

    private void recursiveLoopChildren(ViewGroup parent,ViewAction viewAction) {
        for (int i = parent.getChildCount() - 1; i >= 0; i--) {
            final View child = parent.getChildAt(i);
            if (child instanceof ViewGroup) {
                recursiveLoopChildren((ViewGroup) child,viewAction);
            } else {
                if (child != null) {
                    viewAction.execute(child);
                }
            }
        }
    }

    interface ViewAction{
        void execute(View view);
    }

    @Override
    public void onAuthorized() {
        Log.d(TAG, "SecureCopyPasteActivity.onAuthorized()");
    }

    @Override
    public void onLocked() {
        Log.d(TAG, "SecureCopyPasteActivity.onLocked()");
    }

    @Override
    public void onWiped() {
        Log.d(TAG, "SecureCopyPasteActivity.onWiped()");
    }

    @Override
    public void onUpdateConfig(final Map<String, Object> settings) {
        Log.d(TAG, "SecureCopyPasteActivity.onUpdateConfig()");
    }

    @Override
    public void onUpdatePolicy(final Map<String, Object> policyValues) {
        Log.d(TAG, "SecureCopyPasteActivity.onUpdatePolicy()");
    }

    @Override
    public void onUpdateServices() {
        Log.d(TAG, "SecureCopyPasteActivity.onUpdateServices()");
    }

    @Override
    public void onUpdateDataPlan() {
        Log.d(TAG, "SecureCopyPasteActivity.onUpdateDataPlan()");
    }

    @Override
    public void onUpdateEntitlements() {
        Log.d(TAG, "SecureCopyPasteActivity.onUpdateEntitlements()");
    }
}