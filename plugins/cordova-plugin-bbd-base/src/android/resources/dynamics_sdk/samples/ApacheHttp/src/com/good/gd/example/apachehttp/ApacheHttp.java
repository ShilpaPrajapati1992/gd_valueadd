/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.apachehttp;

import android.content.Context;
import android.content.Intent;

import com.good.gd.Activity;
import com.good.gd.GDAndroid;

/**
   NOTES:
   1) this sample uses GD SDK built for Level 10. But the app itself can use upper levels if necessary.
   2) the purpose of this sample is to demonstrate Apache HTTP API. So, the application structure is kept at basic level
    and advanced UI/app features, application design, etc  are avoided.
   3) for easier code browsing, code which uses Apache HTTP API is placed in a separate "features" subpackage.

   General approach/pattern to integrate the app with GD:
   1) there is entry activity (launcher activity), in this case ApacheHttp, which kick starts the authorization.
    The role of this activity is just to start the authorization procedure.
   2) On successful authorization event, we kick start the real application activity.

 */

/** LaunchActivity - the entry point activity which will start authorization with Good Dynamics
 * and once done launch the application UI.
 */
public class ApacheHttp extends Activity {

    private GDAndroid _gd = null;
    private boolean _authorizeCalled = false;
    public static final String LOG_TAG = "ApacheHttp";

    /** onResume - used to call authorize to initialize the GD library
     */
    @Override
    public void onResume() {
        super.onResume();

        if (!_authorizeCalled) {

            // Create a GDEventListener which will receive the library events and invoke
            // the UI specified in the intent once authorized.
            GDEventListener listener = new GDEventListener();
            listener.setUILaunchIntent(new Intent(this, MainSelectionActivity.class), this);

            _gd = GDAndroid.getInstance();
            _gd.authorize(listener);
            _authorizeCalled = true;

        } else {
            // If this launch activity is resumed and it's already called authorize then it needs
            // to be removed. The user launching the application again will get a fresh LaunchActivity
            // which will call authorize again.
            finish();
        }
    }

    /** doAuthorization - entry point for any other activity to call if it detects
     * on starting that it is not authorized.
     */
    public static void doAuthorization(Context ctx) {
        Intent i = new Intent(ctx, ApacheHttp.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        ctx.startActivity(i);
    }
    
    public void onBackPressed() {
        super.onBackPressed();
    }


}
