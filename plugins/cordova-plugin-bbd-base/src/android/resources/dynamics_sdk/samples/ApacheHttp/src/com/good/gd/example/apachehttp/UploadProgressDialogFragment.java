package com.good.gd.example.apachehttp;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;

public class UploadProgressDialogFragment extends DialogFragment {

	public static UploadProgressDialogFragment newInstance(){
		UploadProgressDialogFragment frag = new UploadProgressDialogFragment();
		return frag;
	}
 
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
 
		 final ProgressDialog mDialog = new ProgressDialog(getActivity());
		
		 mDialog.setTitle("Please wait...");
		 mDialog.setMessage("Uploading data ...");
		 mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		 mDialog.setMax(100);
		 mDialog.setCancelable(false);

		return mDialog;
	}
 
}

