### Apache HTTPClient ###

================================================================================
DESCRIPTION:
An example of how to use Good Dynamics Secure Communication APIs to access resources behind the enterprise firewall.

For more help of how to build and install this application, please see BUILD_NOTES.

For more examples visit the Good Developer Network:
https://begood.good.com/community/gdn

Support:
https://begood.good.com/community/gdn/support
================================================================================
(c) 2015 Good Technology Corporation. All rights reserved.