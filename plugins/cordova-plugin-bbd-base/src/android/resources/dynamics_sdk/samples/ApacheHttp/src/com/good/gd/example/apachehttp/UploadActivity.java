/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.apachehttp;

import java.io.InputStream;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;

import com.good.gd.example.apachehttp.features.HttpRequestExecutor;
import com.good.gd.example.apachehttp.features.HttpRequestExecutor.UploadProgressListener;

/**
 * This activity shows a dialog which allows a user to select one of preconfigured files or type full file name.
 * Then, this file can be uploaded to a selected server
 */
public class UploadActivity extends com.good.gd.Activity implements View.OnClickListener {

    private static final String[] SERVER_URLS = new String[] {
        "http://GD-Dev-UK1.sqadev.qagood.com:8080",
        "http://www.example.com/resource",
        "http://172.16.2.128:8080/handler.php"
    };

    private static final String[] FILE_URLS = new String[] {
        "good-dynamics-logo-small.jpg",
        "Tree-medium.jpg",
        "Tree-large.jpg",
    };
  
    //@Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(ApacheHttp.LOG_TAG, "SynchronousRequestActivity.onCreate IN\n");
        
        super.onCreate(savedInstanceState);
		if (isFinishing()) {
			/*
			 * In the case where the application is unloaded with some
			 * activities in the stack the GD library will automatically
			 * finish() this activity in super.onCreate.
			 */
			ApacheHttp.doAuthorization(this);
			return;
		}

        setContentView(R.layout.upload_view_layout);
        this.setTitle("Uploading file using HTTP PUT");

        Button btn = (Button) findViewById(R.id.fileUploadButton);
        btn.setOnClickListener(this);

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                                                                 android.R.layout.simple_dropdown_item_1line, SERVER_URLS);
        AutoCompleteTextView urlEditView = (AutoCompleteTextView) findViewById(R.id.serverURL_textEdit);
        urlEditView.setAdapter(adapter1);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                                                                 android.R.layout.simple_dropdown_item_1line, FILE_URLS);
        AutoCompleteTextView pathEditView = (AutoCompleteTextView) findViewById(R.id.filepath_textEdit);
        pathEditView.setAdapter(adapter2);

        RadioButton radioBtnForPut = (RadioButton) findViewById(R.id.radioButton1);
        radioBtnForPut.setOnClickListener(this);
        RadioButton radioBtnForPost = (RadioButton) findViewById(R.id.radioButton2);
        radioBtnForPost.setOnClickListener(this);
        
        Log.d(ApacheHttp.LOG_TAG, "SynchronousRequestActivity.onCreate OUT\n");
    }

    //@Override
    protected void onStart() {
        Log.d(ApacheHttp.LOG_TAG, "SynchronousRequestActivity.onStart IN\n");
        
        super.onStart();

        RadioButton radioBtn = (RadioButton) findViewById(R.id.radioButton1);
        radioBtn.setChecked(false);
        
        Log.d(ApacheHttp.LOG_TAG, "SynchronousRequestActivity.onStart OUT\n");
    }


    //@Override
    protected void onDestroy() {
        Log.d(ApacheHttp.LOG_TAG, "SynchronousRequestActivity.onDestroy IN\n");
        
        dismissResult();
        super.onDestroy();
        
        Log.d(ApacheHttp.LOG_TAG, "SynchronousRequestActivity.onDestroy OUT\n");
    }

    /*
     * Here, we retrieve link and filename and whether we should use PUT or POST.
     * and then we start async task, which does file uploading using HTTP request executor class.
     * Also, if use clicks radio buttons, we alternate their checked status. So that we have either PUT or POST at the same time.
     * @see android.view.View.OnClickListener#onClick(android.view.View)
     */
    public void onClick(View v) {

        RadioButton radioBtnForPut = (RadioButton) findViewById(R.id.radioButton1);
        RadioButton radioBtnForPost = (RadioButton) findViewById(R.id.radioButton2);
    	
        if (v.getId() == R.id.fileUploadButton) {
            AutoCompleteTextView serverURL = (AutoCompleteTextView) findViewById(R.id.serverURL_textEdit);
            String url = serverURL.getText().toString().trim();

            AutoCompleteTextView fileURL = (AutoCompleteTextView) findViewById(R.id.filepath_textEdit);
            String path = fileURL.getText().toString().trim();

            UploadFileTask task = new UploadFileTask();
            task.execute(new String[] { url, path, Boolean.toString(radioBtnForPut.isChecked()) }
                         );
        }

        if (v.getId() == R.id.radioButton1) {

            radioBtnForPost.setChecked(false);
        }
        if (v.getId() == R.id.radioButton2) {

            radioBtnForPut.setChecked(false);
        }
    }
    
    private Dialog resultDialog = null;
    
    private void showResult(String result) {

        this.resultDialog = new AlertDialog.Builder(this)
                     .setTitle("Data upload result:")
                     .setMessage(result)
                     .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                            }
                                        }
                                        ).create();
        this.resultDialog.show();
    }

    synchronized private void dismissResult() {
        if (this.resultDialog != null && this.resultDialog.isShowing()) {
        	    this.resultDialog.dismiss();
        }
        this.resultDialog = null;
    }

    /*
     * This task opens a selected file in the assets folder and creates input stream from it.
     * Then it passes this input stream, together with URL and progress observer to the HTTP request executor.
     * This input stream is used as a data source by the HTTP request executor. Observer is essentially a callback,
     * which allows us to track the upload progress.
     */
	private class UploadFileTask extends AsyncTask<String, Integer, String> {

		private DialogFragment dialogFragment;

		// @Override
		protected void onPreExecute() {
			this.dialogFragment = UploadProgressDialogFragment.newInstance();
			this.dialogFragment.show(getFragmentManager(), "PROGRESS_DIALOG");
		}

		// @Override
		protected void onPostExecute(String result) {
			this.dialogFragment.dismiss();
			showResult(result);
		}

		// @Override
		protected String doInBackground(String... params) {
			final String url = params[0];
			final String path = params[1];
			final boolean usePut = Boolean.parseBoolean(params[2]);

			System.out.println("Trying to upload " + path + " to url " + url);

			long result = 0;
			try {
				HttpRequestExecutor executor = HttpRequestExecutor
						.getInstance();
				InputStream inputStream = getAssets().open(path);
				final long totalSize = inputStream.available();

				if (usePut) {
					result = executor.putFile(inputStream, url,
							new UploadProgressListener() {
								public void transferred(long num) {
									publishProgress((int) ((num / (float) totalSize) * 100));
								}
							}, 1024);
				} else {
					result = executor.postFile(inputStream, url,
							new UploadProgressListener() {
								public void transferred(long num) {
									publishProgress((int) ((num / (float) totalSize) * 100));
								}
							}, 1024);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			String resultMessage = null;
			
			if (result > 0) {
				resultMessage = "" + result + " bytes have been uploaded.";
			} else {
				resultMessage = "File upload failed!";
			}

			return resultMessage;
		}

		protected void onProgressUpdate(Integer... progress) {
			((ProgressDialog) dialogFragment.getDialog())
					.setProgress(progress[0]);
		}
	}

}
