/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.apachehttp.features;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.io.Reader;

import junit.framework.Assert;

import com.good.gd.apache.http.Header;
import com.good.gd.apache.http.HttpEntity;
import com.good.gd.apache.http.HttpResponse;
import com.good.gd.apache.http.HttpStatus;

import com.good.gd.apache.http.auth.AuthScope;
import com.good.gd.apache.http.auth.NTCredentials;

import com.good.gd.apache.http.client.ClientProtocolException;
import com.good.gd.apache.http.client.HttpClient;
import com.good.gd.apache.http.client.methods.HttpGet;
import com.good.gd.apache.http.client.methods.HttpPost;
import com.good.gd.apache.http.client.methods.HttpPut;

import com.good.gd.apache.http.entity.ContentProducer;
import com.good.gd.apache.http.entity.EntityTemplate;

import com.good.gd.apachehttp.auth.Kerberos5Credentials;
import com.good.gd.net.GDHttpClient;


/*
 * allows to make synchronous HTTP requests.
 * Uses GD Apache HTTP Client API.
 *
 * Remember that this class is not thread-safe. All Apache HTTP Client API usage is collected in this class, making it easier to understand this API.
 * So, this class must be used sequentially, otherwise, it will throw an exception with message "Invalid use of SingleClientConnManager: connection still allocated."
 *
 * Since this is just a sample app, we do not manage streams. In the real-world application, you need to close all streams when all work finished.
 *
 * In this API, you can mix all other existing org.apache.http api-s, as long as all requests are executed via GDHttpClient as opposed to DefaultHttpClient.
 * For demo purposes, we will show different ways of working with Apache HTTP client.
 *
 */
public class HttpRequestExecutor {
    
    public static interface UploadProgressListener {
        public void transferred(long num);
    }
    
    
    public static final int LONG_SSL_TIMEOUT = 35000;
    public static final String KERBEROS_AUTH = "Negotiate";
    public static final String NTLM_AUTH = "NTLM";
    public static final String NTLM_HOST_NAME = "test";
    
    GDHttpClient client = null;
    static GDHttpClient clientWithRelaxedHostnameCheck = null;
    static GDHttpClient clientWithRelaxedPeerCheck = null;
    static GDHttpClient clientWithRelaxedChecks = null;
    static HttpResponse lastResponse = null;
    
    private static HttpRequestExecutor instance = null;
    
    synchronized public static HttpRequestExecutor getInstance() {
        if (instance == null) {
            instance = new HttpRequestExecutor();
        }
        else {
            instance = new HttpRequestExecutor();
        }
        return instance;
    }
    
    /*
     * Version for Kerberos authorization
     */
    synchronized public static HttpRequestExecutor getInstance(final String login, final String password, final String host) {
        if (instance == null) {
            instance = new HttpRequestExecutor(login, password, host);
        }
        else {
            instance = new HttpRequestExecutor(login, password, host);
        }
        return instance;
    }
    
    /*
     * Version for NTLM authorization
     */
    synchronized public static HttpRequestExecutor getInstance(final String login, final String password, final String host, final String domain) {
        if (instance == null) {
            instance = new HttpRequestExecutor(login, password, host, domain);
        }
        else {
            instance = new HttpRequestExecutor(login, password, host, domain);
        }
        return instance;
    }
    
    /*
     * This method simply downloads given URL and then saves response. Then, using getBody() and getHeaders(), we can
     * retrieve response body and HTTP response headers from this saved response.
     */
    public boolean fetch(String url, boolean relaxPeerCheck, boolean relaxHostnameCheck) throws ClientProtocolException, IOException {
        
        HttpGet request = new HttpGet(url);
        HttpResponse response = null;
        
        if (!relaxPeerCheck && !relaxHostnameCheck) {
            response = client.execute(request);
        } else if (relaxPeerCheck && relaxHostnameCheck) {
            response = clientWithRelaxedChecks.execute(request);
        } else if (relaxPeerCheck) {
            System.out.println("Using executor with relaxed peer check.");
            response = clientWithRelaxedPeerCheck.execute(request);
        } else if (relaxHostnameCheck) {
            response = clientWithRelaxedHostnameCheck.execute(request);
        }
        
        lastResponse = response;
        
        return HttpStatus.SC_OK == response.getStatusLine().getStatusCode();
    }
    
    protected HttpRequestExecutor() {
        client = new GDHttpClient();
        setTimeouts(client, LONG_SSL_TIMEOUT);
        
        clientWithRelaxedHostnameCheck = new GDHttpClient();
        setTimeouts(clientWithRelaxedHostnameCheck, LONG_SSL_TIMEOUT);
        clientWithRelaxedHostnameCheck.disableHostVerification();
        
        clientWithRelaxedPeerCheck = new GDHttpClient();
        setTimeouts(clientWithRelaxedPeerCheck, LONG_SSL_TIMEOUT);
        clientWithRelaxedPeerCheck.disablePeerVerification();
        
        clientWithRelaxedChecks = new GDHttpClient();
        setTimeouts(clientWithRelaxedChecks, LONG_SSL_TIMEOUT);
        clientWithRelaxedChecks.disableHostVerification();
        clientWithRelaxedChecks.disablePeerVerification();
    }
    
    /*
     * Kerberos Authorization
     * AuthScope represents an authentication scope consisting of a host name,
     * a port number, a realm name and an authentication scheme name which
     * Credentials apply to.
     * Kerberos5Credentials can be used to store credentials for use with Kerberos
     * authentication as supported by the GDHttpClient class.
     */
    protected HttpRequestExecutor(String login, String password, String host) {
        client = new GDHttpClient();
        
        client.getCredentialsProvider().setCredentials(
                                                       new AuthScope(host, -1, null, KERBEROS_AUTH),
                                                       new Kerberos5Credentials(login, password, host));
        
        setTimeouts(client, LONG_SSL_TIMEOUT);
        
        clientWithRelaxedHostnameCheck = new GDHttpClient();
        setTimeouts(clientWithRelaxedHostnameCheck, LONG_SSL_TIMEOUT);
        clientWithRelaxedHostnameCheck.disableHostVerification();
        
        clientWithRelaxedPeerCheck = new GDHttpClient();
        setTimeouts(clientWithRelaxedPeerCheck, LONG_SSL_TIMEOUT);
        clientWithRelaxedPeerCheck.disablePeerVerification();
        
        clientWithRelaxedChecks = new GDHttpClient();
        setTimeouts(clientWithRelaxedChecks, LONG_SSL_TIMEOUT);
        clientWithRelaxedChecks.disableHostVerification();
        clientWithRelaxedChecks.disablePeerVerification();
    }
    /*
     * NTLM Authorization
     * AuthScope represents an authentication scope consisting of a host name,
     * a port number, a realm name and an authentication scheme name which
     * Credentials apply to.
     * NTCredentials is Credentials implementation for Microsoft Windows platforms
     * that includes Windows specific attributes such as name of the domain
     * the user belongs to.
     */
    protected HttpRequestExecutor(String login, String password, String host, String domain) {
        client = new GDHttpClient();
        
        client.getCredentialsProvider().setCredentials(
                                                       new AuthScope(host, -1, null, NTLM_AUTH),
                                                       new NTCredentials(login, password, NTLM_HOST_NAME, domain));
        
        setTimeouts(client, LONG_SSL_TIMEOUT);
        
        clientWithRelaxedHostnameCheck = new GDHttpClient();
        setTimeouts(clientWithRelaxedHostnameCheck, LONG_SSL_TIMEOUT);
        clientWithRelaxedHostnameCheck.disableHostVerification();
        
        clientWithRelaxedPeerCheck = new GDHttpClient();
        setTimeouts(clientWithRelaxedPeerCheck, LONG_SSL_TIMEOUT);
        clientWithRelaxedPeerCheck.disablePeerVerification();
        
        clientWithRelaxedChecks = new GDHttpClient();
        setTimeouts(clientWithRelaxedChecks, LONG_SSL_TIMEOUT);
        clientWithRelaxedChecks.disableHostVerification();
        clientWithRelaxedChecks.disablePeerVerification();
    }
    
    private void setTimeouts(GDHttpClient client, int timeout) {
        client.getParams().setParameter("http.socket.timeout", timeout); // milliseconds
    }
    
    /*
     * Retrieves HTTP response headers from the saved response to the last request. If no request was performed,
     * then null is returned.
     */
    public StringBuffer getHeaders() {
        if (lastResponse == null) {
            return null;
        }
        StringBuffer result = new StringBuffer("");
        String NL = System.getProperty("line.separator");
        
        Header[] headers = lastResponse.getAllHeaders();
        
        if (headers != null) {
            for (Header header : headers) {
                String headerValue = header.getValue();
                result.append(headerValue).append(NL);
            }
        }
        return result;
    }
    
    /*
     * Retrieves HTTP response body/content from the saved response to the last request. If no request was performed,
     * then null is returned.
     */
    public BufferedReader getBody() {
        if (lastResponse == null) {
            return null;
        }
        BufferedReader bufferedContentEntityReader = null;
        HttpEntity responseEntity = lastResponse.getEntity();
        if (null != responseEntity) {
            try {
                InputStream contentEntity = responseEntity.getContent();
                Reader contentEntityReader = new InputStreamReader(contentEntity);
                bufferedContentEntityReader = new BufferedReader(contentEntityReader);
                
                // in a real word application, you may wish to handle data via data adapters and then
                // close stream after adapter is not use anymore, using for example this:
                //  bufferedContentEntityReader.close();
                // but here, we use this stream to pass on results to UI.
            } catch (Exception e) {
                Assert.fail(": Exception: " + e.toString());
            }
        }
        return bufferedContentEntityReader;
    }
    
    /*
     * This class reads from the source stream into the HTTP entity's out stream.
     * This is required when we need to feed file content into POST or PUT entities.
     * Also, this class updates progress listener with how many bytes have been transfered. Updates happened at steps, specified
     * in the progressStep parameter.
     */
    class FileContentProducer implements ContentProducer {
        
        public FileContentProducer(InputStream outstream, final UploadProgressListener progressObserver,
                                   final long progressStep) {
            super();
            _sourceStream = outstream;
            _progressObserver = progressObserver;
            _progressStep = progressStep;
        }
        
        public void writeTo(OutputStream outstream) throws IOException {
            
            InputStream in = null;
            OutputStream out = null;
            long count = 0;
            try {
                in = new BufferedInputStream(_sourceStream);
                out = new BufferedOutputStream(outstream);
                
                while (true) {
                    int data = in.read();
                    if (data == -1) {
                        break;
                    }
                    count++;
                    out.write(data);
                    if ((count % _progressStep) == 0) {
                        _progressObserver.transferred(count);
                    }
                }
            }
            finally {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            }
            
            _result = count;
        }
        
        public long getResult() {
            return _result;
        }
        
        private InputStream _sourceStream = null;
        private long _result = 0;
        UploadProgressListener _progressObserver;
        long _progressStep;
    }
    
    /*
     * This method forms HTTP PUT request and then asks HTTP Client to execute it.
     * File content is feed into PUT request using custom content producer class.
     */
    public long putFile(final InputStream sourceStream, String urlString,
                        final UploadProgressListener progressObserver,
                        final long progressStep) {
        
        HttpClient client = this.client;
        
        FileContentProducer cp = new FileContentProducer(sourceStream, progressObserver, progressStep);
        HttpEntity entity = new EntityTemplate(cp);
        HttpPut put = new HttpPut(urlString);
        
        put.setEntity(entity);
        long result = 0;
        try {
            HttpResponse response = client.execute(put);
            System.out.println("Upload response status code:" + response.getStatusLine().toString());
            if (HttpStatus.SC_OK == response.getStatusLine().getStatusCode()) {
                result = cp.getResult();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return result;
    }
    
    /*
     * This method forms HTTP POST request and then asks HTTP Client to execute it.
     * File content is feed into PUT request using custom content producer class.
     */
    public long postFile(final InputStream sourceStream, String urlString,
                         final UploadProgressListener progressObserver,
                         final long progressStep) {
        
        HttpClient client = this.client;
        
        FileContentProducer cp = new FileContentProducer(sourceStream, progressObserver, progressStep);
        HttpEntity entity = new EntityTemplate(cp);
        HttpPost post = new HttpPost(urlString);
        
        post.setEntity(entity);
        long result = 0;
        try {
            HttpResponse response = client.execute(post);
            System.out.println("Upload (using POST) response status code:" + response.getStatusLine().toString());
            if (HttpStatus.SC_OK == response.getStatusLine().getStatusCode()) {
                result = cp.getResult();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
