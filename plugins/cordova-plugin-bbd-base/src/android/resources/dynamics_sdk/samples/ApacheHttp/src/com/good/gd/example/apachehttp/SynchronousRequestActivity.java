/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.apachehttp;

import java.io.BufferedReader;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.good.gd.example.apachehttp.features.HttpRequestExecutor;
import com.good.gd.net.GDConnectivityManager;
import com.good.gd.net.GDNetworkInfo;

/**
 * The activity presents a dialog, where user can type a URL and then HTTP GET is performed.
 */
public class SynchronousRequestActivity extends com.good.gd.Activity implements View.OnClickListener {
    private TextView tvResponseHeader = null;
    private TextView tvResponseBody = null;
    private ProgressDialog progressDialog = null;
    private Dialog errorDialog = null;
    private String urlForAuth = null;
    private URL kerberosHost = null;
    private URL ntlmHost = null;
    private boolean callDoInBackgroundWithKerberosAuthCred = false;
    private boolean callDoInBackgroundWithNTLMAuthCred = false;
    private boolean isKerberosRequired = false;
    private boolean isNtlmRequired = false;


    private static final String[] DEMO_URLS = new String[]{
        "http://gd-dev-uk1.gd.sw.rim.net:8080/index.html",
        "http://gd-dev-uk1.gd.sw.rim.net:8081/index.html",
        "https://gd-dev-uk1.gd.sw.rim.net/index.html",
        "https://begood.good.com/welcome",
        "http://www.example.com",
        "https://m.symantec.com/"
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(ApacheHttp.LOG_TAG, "SynchronousRequestActivity.onCreate IN\n");
        
        super.onCreate(savedInstanceState);
        if (isFinishing()) {
            /*
             * In the case where the application is unloaded with some
             * activities in the stack the GD library will automatically
             * finish() this activity in super.onCreate.
             */
            ApacheHttp.doAuthorization(this);
            return;
        }
        
        setContentView(R.layout.synchronous_view_layout);
        this.setTitle("Synchronous HTTP Request");
        
        this.tvResponseBody = (TextView) findViewById(R.id.responseTextView);
        this.tvResponseBody.setMovementMethod(new ScrollingMovementMethod());
        
        this.tvResponseHeader = (TextView) findViewById(R.id.responseHeadersTextView);
        this.tvResponseHeader.setMovementMethod(new ScrollingMovementMethod());
        
        setTextFieldsVisibility(View.INVISIBLE);
        
        Button urlGoButton = (Button) findViewById(R.id.urlGoButton);
        urlGoButton.setOnClickListener(this);
        
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                                                                android.R.layout.simple_dropdown_item_1line, DEMO_URLS);
        AutoCompleteTextView urlEditView = (AutoCompleteTextView) findViewById(R.id.url_textEdit);
        urlEditView.setAdapter(adapter);
        
        Log.d(ApacheHttp.LOG_TAG, "SynchronousRequestActivity.onCreate OUT\n");
    }
    
    @Override
    protected void onDestroy() {
        Log.d(ApacheHttp.LOG_TAG, "SynchronousRequestActivity.onDestroy() IN\n");
        
        dismissProgressDialog();
        dismissErrorPopup();
        super.onDestroy();
        
        Log.d(ApacheHttp.LOG_TAG, "SynchronousRequestActivity.onDestroy() OUT\n");
    }
    
    private void setTextFieldsVisibility(int visibility) {
        TextView tvResponseBodyLabel = (TextView) findViewById(R.id.responseTextViewLabel);
        tvResponseBodyLabel.setVisibility(visibility);
        this.tvResponseBody.setVisibility(visibility);
        
        TextView tvResponseHeadersLabel = (TextView) findViewById(R.id.responseHeadersTextViewLabel);
        tvResponseHeadersLabel.setVisibility(visibility);
        this.tvResponseHeader.setVisibility(visibility);
    }
    
    private void clearTextFields() {
        this.tvResponseBody.setText("");
        this.tvResponseHeader.setText("");
    }
    
    public void onClick(View v) {
        
        if (v.getId() == R.id.urlGoButton) {

            //Check GD Network Status
            GDNetworkInfo info = GDConnectivityManager.getActiveNetworkInfo();
            if(info != null) {
                Log.d(ApacheHttp.LOG_TAG, "SynchronousRequestActivity: GD Network status:" +
                        "  isConnected = " + info.isConnected() +
                        "  isBlocked = " + info.isBlocked() +
                        ", network type: " + info.getTypeName() +
                        ", isAvailable = " + info.isAvailable() +
                        "\n");

                if(info.isConnected() == false || info.isAvailable() == false) {
                    Log.d(ApacheHttp.LOG_TAG, "SynchronousRequestActivity: Network is not established\n");
                    showErrorPopup("Network is not established");
                    return;
                }
            }

            final AutoCompleteTextView editView = (AutoCompleteTextView) findViewById(R.id.url_textEdit);
            final String url = editView.getText().toString().trim();
            urlForAuth = url;
            
            /*
             * Variables to retrieve host from URL for Kerberos and NTLM auth
             */
            try {
                kerberosHost = new URL(url);
                ntlmHost = new URL(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            
            
            final CheckBox checkPeerVerify = (CheckBox) findViewById(R.id.checkBoxPeerVerify);
            final CheckBox checkHostVerify = (CheckBox) findViewById(R.id.checkBoxHostVerify);
            
            final DownloadWebPageTask task = new DownloadWebPageTask();
            task.execute(new String[] { url,
                Boolean.toString(checkPeerVerify.isChecked()),
                Boolean.toString(checkHostVerify.isChecked()) }
                         );
        }
    }
    
    private class DownloadWebPageTask extends AsyncTask<String, Void, String> {
        
        StringBuffer body = null;
        StringBuffer headers = null;
        
        DownloadWebPageTask() {
        }
        
        @Override
        protected void onPreExecute() {
            setTextFieldsVisibility(View.INVISIBLE);
            clearTextFields();
            this.body = null;
            this.headers = null;
            
            showProgressDialog();
        }
        
        @Override
        protected void onPostExecute(String result) {
            dismissProgressDialog();
            
            if (result == null) {
                setTextFieldsVisibility(View.VISIBLE);
                
                if (this.headers != null) {
                    tvResponseHeader.setText(this.headers.toString());
                    /*
                     * parsing cookie headers for Kerberos or NTLM auth
                     * if found - calling showAuthDialog with auth type
                     */
                    isKerberosRequired = (tvResponseHeader.getText().toString().contains("Negotiate"))
                    && (this.body != null && body.toString().contains("401"));
                    
                    isNtlmRequired = (tvResponseHeader.getText().toString().contains("NTLM"))
                    && (this.body != null && body.toString().contains("401"));
                }
                
                if (this.body != null) {
                    tvResponseBody.setText(this.body.toString());
                }
                if (isKerberosRequired) {
                    showAuthDialog("Kerberos");
                    isKerberosRequired = false;
                }
                else if (isNtlmRequired) {
                    showAuthDialog("NTLM");
                    isNtlmRequired = false;
                }
            } else {
                showErrorPopup(result);
            }
        }
        
        @Override
        protected String doInBackground(String...params) {
            Log.d(ApacheHttp.LOG_TAG, "DownloadWebPageTask.doInBackground IN\n");
            
            String url = params[0];
            boolean relaxPeerCheck = Boolean.parseBoolean(params[1]);
            boolean relaxHostnameCheck = Boolean.parseBoolean(params[2]);
            String result = null;
            
            System.out.println("Trying url:" + url
                               + ", relaxPeerCheck=" + relaxPeerCheck + ", relaxPeerCheck=" + relaxHostnameCheck);
            
            /*
             * Called if Kerberos auth is needed
             */
            
            if (callDoInBackgroundWithKerberosAuthCred) {
                callDoInBackgroundWithKerberosAuthCred = false;
                
                if ((params[3] != null) && (params[4] != null) && (params[5] != null)) {
                    HttpRequestExecutor exec = HttpRequestExecutor.getInstance(params[3],params[4],params[5]);
                    String NL = System.getProperty("line.separator");
                    try {
                        exec.fetch(url, relaxPeerCheck, relaxHostnameCheck);
                        
                        this.headers = exec.getHeaders();
                        
                        BufferedReader content = exec.getBody();
                        if (content != null) {
                            this.body = new StringBuffer("");
                            String line = "";
                            while ((line = content.readLine()) != null) {
                                this.body.append(line + NL);
                            }
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                        this.headers = new StringBuffer("");
                        result = "HTTP/HTTPS request failed!";
                    }
                }
            }
            /*
             * Called if NTLM auth is needed
             */
            else if (callDoInBackgroundWithNTLMAuthCred) {
                callDoInBackgroundWithNTLMAuthCred = false;
                
                if ((params[3] != null) && (params[4] != null) && (params[5] != null) && (params[6] != null)) {
                    HttpRequestExecutor exec = HttpRequestExecutor.getInstance(params[3],params[4],params[5],params[6]);
                    String NL = System.getProperty("line.separator");
                    try {
                        exec.fetch(url, relaxPeerCheck, relaxHostnameCheck);
                        
                        this.headers = exec.getHeaders();
                        
                        BufferedReader content = exec.getBody();
                        if (content != null) {
                            this.body = new StringBuffer("");
                            String line = "";
                            while ((line = content.readLine()) != null) {
                                this.body.append(line + NL);
                            }
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                        this.headers = new StringBuffer("");
                        result = "HTTP/HTTPS request failed!";
                    }
                }
            }
            else {
                HttpRequestExecutor exec = HttpRequestExecutor.getInstance();
                String NL = System.getProperty("line.separator");
                try {
                    exec.fetch(url, relaxPeerCheck, relaxHostnameCheck);
                    
                    this.headers = exec.getHeaders();
                    
                    BufferedReader content = exec.getBody();
                    if (content != null) {
                        this.body = new StringBuffer("");
                        String line = "";
                        while ((line = content.readLine()) != null) {
                            this.body.append(line + NL);
                        }
                    }
                    /*
                     * in a real world application, you may want to use data
                     * adapters and bind them to appropriate views, instead of
                     * passing the body content as string buffer as we are doing
                     * here, because body can be extremely large and also adapters
                     * provide convenient observing mechanism.
                     */
                }
                catch (Exception e) {
                    e.printStackTrace();
                    this.headers = new StringBuffer("");
                    result = "HTTP/HTTPS request failed!";
                }
            }
            
            Log.d(ApacheHttp.LOG_TAG, "DownloadWebPageTask.doInBackground OUT\n");
            return result;
        }
    }
    
    private void showErrorPopup(String result) {
        this.errorDialog = new AlertDialog.Builder(this).setTitle("Error:")
        .setMessage(result)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create();
        this.errorDialog.show();
    }
    
    /*
     * This method shows authorization prompt based on authorization schema
     * For Kerberos - login and password
     * For NTLM - login, password, domain
     */
    private void showAuthDialog(String authMethod) {
        
        AlertDialog.Builder authDialog = new AlertDialog.Builder(this);
        authDialog.setTitle(authMethod + " authorization required:");
        
        LinearLayout layout = new LinearLayout(this);
        
        final EditText login = new EditText(this);
        login.setHint("login");
        
        final EditText password = new EditText(this);
        password.setHint("password");
        
        final EditText domain = new EditText(this);
        domain.setHint("domain");
        
        if (authMethod == "Kerberos") {
            layout.addView(login);
            layout.addView(password);
            
            authDialog.setView(layout);
            
            authDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    
                    callDoInBackgroundWithKerberosAuthCred = true;
                    
                    final CheckBox checkPeerVerify = (CheckBox) findViewById(R.id.checkBoxPeerVerify);
                    final CheckBox checkHostVerify = (CheckBox) findViewById(R.id.checkBoxHostVerify);
                    
                    final DownloadWebPageTask task = new DownloadWebPageTask();
                    task.execute(new String[] { urlForAuth,
                        Boolean.toString(checkPeerVerify.isChecked()),
                        Boolean.toString(checkHostVerify.isChecked()),
                        login.getText().toString(),
                        password.getText().toString(),
                        kerberosHost.getHost().toString()
                    }
                                 );
                }
            });
            authDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Canceled.
                }
            });
        }
        
        else if (authMethod == "NTLM") {
            layout.addView(login);
            layout.addView(password);
            layout.addView(domain);
            
            authDialog.setView(layout);
            
            authDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    
                    callDoInBackgroundWithNTLMAuthCred = true;
                    
                    final CheckBox checkPeerVerify = (CheckBox) findViewById(R.id.checkBoxPeerVerify);
                    final CheckBox checkHostVerify = (CheckBox) findViewById(R.id.checkBoxHostVerify);
                    
                    final DownloadWebPageTask task = new DownloadWebPageTask();
                    task.execute(new String[] { urlForAuth,
                        Boolean.toString(checkPeerVerify.isChecked()),
                        Boolean.toString(checkHostVerify.isChecked()),
                        login.getText().toString(),
                        password.getText().toString(),
                        ntlmHost.getHost().toString(),
                        domain.getText().toString().toUpperCase()
                    }
                                 );
                    
                }
            });
            authDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Canceled.
                }
            });
        }
        authDialog.show();
    }
    
    synchronized private void dismissErrorPopup() {
        if (this.errorDialog != null && this.errorDialog.isShowing()) {
            this.errorDialog.dismiss();
        }
        this.errorDialog = null;
    }
    
    private void showProgressDialog() {
        if (this.progressDialog == null) {
            this.progressDialog = new ProgressDialog(
                                                     SynchronousRequestActivity.this);
            this.progressDialog.setMessage("Retrieving data ...");
            this.progressDialog.setTitle("Please wait...");
            this.progressDialog.setIndeterminate(true);
            this.progressDialog.setCancelable(false);
        }
        this.progressDialog.show();
    }
    
    synchronized private void dismissProgressDialog() {
        if (this.progressDialog != null && this.progressDialog.isShowing()) {
            this.progressDialog.dismiss();
        }
        this.progressDialog = null;
    }
}