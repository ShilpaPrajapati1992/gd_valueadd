/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.apachehttp;

import java.io.IOException;
import java.io.InputStream;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.good.gd.GDAndroid;
import com.good.gd.file.GDFileSystem;
import com.good.gd.log.GDLogManager;
import com.good.gd.net.GDConnectivityManager;
import com.good.gd.net.GDNetworkInfo;
import com.good.gd.widget.GDTextView;

/**
 * Displays introduction text and allows a user to select a demo via menu.
 */
public class MainSelectionActivity extends com.good.gd.Activity implements
		OnClickListener {

	private static final boolean RICH_SCROLL = true;
	public static final String TAG = "MainSelectionActivity";

    //@Override
    protected void onResume() {
        super.onResume();

        //register broadcast receiver to detect GD Network Connection changes
        GDAndroid.getInstance().registerReceiver(mConnectionStatusReceiver
                , new IntentFilter(GDConnectivityManager.GD_CONNECTIVITY_ACTION));
    }

    //@Override
    protected void onPause() {
        super.onPause();
        GDAndroid.getInstance().unregisterReceiver(mConnectionStatusReceiver);
    }

    private BroadcastReceiver mConnectionStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            GDNetworkInfo info = GDConnectivityManager.getActiveNetworkInfo();
            String str = "CONNECTED: " + info.isConnected()
                       + ",\n NETWORK TYPE: " + info.getTypeName()
                       + ",\n AVAILABLE: " + info.isAvailable()
                       + ",\n BLOCKED: " + info.isBlocked() + "\n";

            Log.d(TAG, "onReceive: " + str);
            Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();
        }
    };

    // @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFinishing()) {
			// In the case where the application is unloaded with some
			// activities in the stack
			// the GD library will automatically finish() this activity in
			// super.onCreate.
			ApacheHttp.doAuthorization(this);
			return;
		}

		GDTextView tv = new GDTextView(this);
		String styledText = null;
		try {
			InputStream is = getAssets().open("Introduction.html");

			int size = is.available();

			// Read the entire asset into a local byte buffer.
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();

			// Convert the buffer into a string.
			styledText = new String(buffer);

		} catch (IOException e) {
			e.printStackTrace();
			styledText = "Cannot load introduction HTML file!";
		}

		tv.setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);

		if (RICH_SCROLL) {
			ScrollView sv = new ScrollView(this);
			ViewGroup.LayoutParams svLp = new ScrollView.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);

			LinearLayout ll = new LinearLayout(this);
			ll.setLayoutParams(svLp);
			sv.addView(ll);

			Display display = getWindowManager().getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			int screenHeight = size.y;

			LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT, screenHeight); // increase
																		// screen
																		// height
																		// by 2,
																		// if
																		// you
																		// want
																		// to
																		// ensure
																		// nice
																		// scrolling
																		// effect.

			tv.setLayoutParams(llParams);
			ll.addView(tv);

			setContentView(sv);
		} else {
			tv.setMovementMethod(new ScrollingMovementMethod());
			setContentView(tv);
		}
	}

	// @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	private void showUploadLogsView() {
		//Show logs upload interface to upload logs to the server
		GDLogManager.getInstance().openLogUploadUI();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.action_sync_select_item: {
			Intent intent = new Intent();
			intent.setClass(this, SynchronousRequestActivity.class);
			startActivity(intent);
			break;
		}

		case R.id.action_upload_select_item: {
			Intent intent = new Intent();
			intent.setClass(this, UploadActivity.class);
			startActivity(intent);
			break;
		}

		case R.id.action_debug_upload_logs_upload_menu: {
			showUploadLogsView();
			break;
		}
		default:
		}

	}
}
