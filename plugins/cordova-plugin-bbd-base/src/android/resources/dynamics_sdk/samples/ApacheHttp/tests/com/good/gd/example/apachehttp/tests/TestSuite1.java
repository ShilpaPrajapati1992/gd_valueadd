/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2016 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.apachehttp.tests;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.Until;
import com.good.automated_test_support.GDAutomatedTestSupport;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeTextIntoFocusedView;
import static android.support.test.espresso.matcher.ViewMatchers.withId;


@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSuite1 {

    /*
    Setup Test, like all tests makes use of helper functions in GD_UIAutomator_Lib Test library project
     */
    @BeforeClass
    public static void setUpClass() {

        //Setup test support and register a GDStateListener
        GDAutomatedTestSupport.setupGDAutomatedTestSupport();

        GDAutomatedTestSupport.wakeUpDeviceIfNeeded();

        //Android Emulator when booted sometimes has error dialogues to dismiss
        GDAutomatedTestSupport.acceptSystemDialogues();

    }

    @Before
    public void setUp() throws Exception {
        GDAutomatedTestSupport.launchAppUnderTest();
        UiDevice uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        uiDevice.wait(Until.hasObject(By.text("Go!")), 5000);
    }

    @After
    public void tearDown() throws Exception {
        GDAutomatedTestSupport.pressHome();
    }

    /*
    Test 1, if GD App is already activated ensure that it can be unlocked using password. If not activated ensure it
    can be activated using Email Address & Access Key. These are set in file com.good.gd.test.json at build time
     */
    @Test
    public void test1Activation() throws Exception {

        assertTrue(GDAutomatedTestSupport.loginOrProvisionGDApp());

        assertTrue(GDAutomatedTestSupport.checkGDAuthorized());
    }

    /*
    Test 2: request without credentials
     */
    @Test
    public void test2RequestNoCredentialsInternal() {
        assertTrue(GDAutomatedTestSupport.clickOnItem("action_sync_select_item"));

        onView(withId(GDAutomatedTestSupport.getResourceID("url_textEdit"))).
                perform(typeTextIntoFocusedView("http://gd-dev-uk1.gd.sw.rim.net:8080/index.html"),
                        closeSoftKeyboard());

        assertTrue(GDAutomatedTestSupport.clickOnItem("urlGoButton"));

        while (GDAutomatedTestSupport.isTextShown("Retrieving data")) {
            //wait for popup to close
        }

        assertTrue(GDAutomatedTestSupport.isTextShown("hello"));
    }

    /*
    Test 3: request without credentials
     */
    @Test
    public void test3RequestNoCredentialsPublic() {

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_sync_select_item"));

        onView(withId(GDAutomatedTestSupport.getResourceID("url_textEdit"))).
                perform(clearText(),
                        typeTextIntoFocusedView("http://google.co.uk"),
                        closeSoftKeyboard());

        assertTrue(GDAutomatedTestSupport.clickOnItem("urlGoButton"));

        while (GDAutomatedTestSupport.isTextShown("Retrieving data")) {
            //wait for popup to close
        }

        assertFalse(GDAutomatedTestSupport.isTextShown("HTTP/HTTPS request failed!"));
    }

    /*
    Test 4: request with credentials
     */
    @Test
    public void test4RequestCredentials() {

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_sync_select_item"));

        onView(withId(GDAutomatedTestSupport.getResourceID("url_textEdit"))).
                perform(typeTextIntoFocusedView("http://gd-dev-uk1.gd.sw.rim.net:8081/index.html"),
                        closeSoftKeyboard());

        assertTrue(GDAutomatedTestSupport.clickOnItem("urlGoButton"));

        while (GDAutomatedTestSupport.isTextShown("Retrieving data")) {
            //wait for popup to close
        }

        onView(withHint("login")).perform(typeText("gdclienttest"));
        onView(withHint("password")).perform(typeText("gdtest"));
        onView(withHint("domain")).perform(typeText("gd"));

        GDAutomatedTestSupport.clickOnItemContainingText("OK");

        while (GDAutomatedTestSupport.isTextShown("Retrieving data")) {
            //wait for popup to close
        }

        assertTrue(GDAutomatedTestSupport.isTextShown("NTLM succeed"));
    }

    /*
    Test 5: Disable peer validation
     */
    @Test
    public void test5DisablePeerValidation() {
        assertTrue(GDAutomatedTestSupport.clickOnItem("action_sync_select_item"));

        onView(withId(GDAutomatedTestSupport.getResourceID("url_textEdit"))).
                perform(typeTextIntoFocusedView("https://gd-dev-uk1.gd.sw.rim.net/index.html"),
                        closeSoftKeyboard());

        GDAutomatedTestSupport.clickOnItem("checkBoxPeerVerify");

        assertTrue(GDAutomatedTestSupport.clickOnItem("urlGoButton"));

        while (GDAutomatedTestSupport.isTextShown("Retrieving data")) {
            //wait for popup to close
        }

        assertTrue(GDAutomatedTestSupport.isTextShown("hello ssl test page"));
    }

    /*
    Test 6: Disable hostname validation
     */
    @Test
    public void test6DisableHostnameValidation() {
        assertTrue(GDAutomatedTestSupport.clickOnItem("action_sync_select_item"));

        onView(withId(GDAutomatedTestSupport.getResourceID("url_textEdit"))).
                perform(typeTextIntoFocusedView("https://eticketing.co.uk"),
                        closeSoftKeyboard());

        assertTrue(GDAutomatedTestSupport.clickOnItem("checkBoxHostVerify"));
        assertTrue(GDAutomatedTestSupport.clickOnItem("urlGoButton"));

        while (GDAutomatedTestSupport.isTextShown("Retrieving data")) {
            //wait for popup to close
        }

        assertFalse(GDAutomatedTestSupport.isTextShown("HTTP/HTTPS request failed!"));
    }

}