### Secure SQL ###

================================================================================
DESCRIPTION:
An example of how to use the Secure SQL Database. Contacts can be added, edited, deleted & listed.

For more help of how to build and install this application, please see BUILD_NOTES.

For more examples visit the Good Developer Network:
https://begood.good.com/community/gdn

Support:
https://begood.good.com/community/gdn/support
================================================================================
(c) 2015 Good Technology Corporation. All rights reserved.