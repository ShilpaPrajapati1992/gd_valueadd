/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2016 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.securesql.test;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.good.gd.GDAndroid;
import com.good.automated_test_support.GDAutomatedTestSupport;


import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;

import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertFalse;

/*
Tests purpose - Ensure SecureSQL sample app correct basic operation
 */

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class test_suite1 {

    /*
    Note - The test order of these tests is significant hence the explict test numbering
     */
    
    /*
    Setup Test, like all tests makes use of helper functions in GD_UIAutomator_Lib Test library project
     */
    @BeforeClass
    public static void setUpClass() {

        //Setup test support and register a GDStateListener
        GDAutomatedTestSupport.setupGDAutomatedTestSupport();

        GDAutomatedTestSupport.wakeUpDeviceIfNeeded();

        //Android Emulator when booted sometimes has error dialogues to dismiss
        GDAutomatedTestSupport.acceptSystemDialogues();

    }

    /*
    Test 1, if GD App is already activated ensure that it can be unlocked using password. If not activated ensure it
    can be activated using Email Address & Access Key. These are set in file com.good.gd.test.json at build time
     */
    @Test
    public void test_1_GDActivateSecureSQL( ) throws Exception {

        GDAutomatedTestSupport.launchAppUnderTest();

        assertTrue(GDAutomatedTestSupport.loginOrProvisionGDApp());

        assertTrue(GDAutomatedTestSupport.checkGDAuthorized());

        GDAutomatedTestSupport.pressHome();
    }

    /*
    Test 2, ensure a Contact can be saved in SecureSQL sample app
     */
    @Test
    public void test_2_AddContactSecureSQL( ) throws Exception {

        GDAutomatedTestSupport.launchAppUnderTest();

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_add"));

        assertTrue(GDAutomatedTestSupport.clickOnItem("firstName"));

        onView(withId(GDAutomatedTestSupport.getResourceID("firstName"))).perform(typeText("Tony"), closeSoftKeyboard());

        GDAutomatedTestSupport.waitForIdle(800);

        assertTrue(GDAutomatedTestSupport.clickOnItem("secondName"));

        onView(withId(GDAutomatedTestSupport.getResourceID("secondName"))).perform(typeText("Yeboah"), closeSoftKeyboard());

        GDAutomatedTestSupport.waitForIdle(800);

        assertTrue(GDAutomatedTestSupport.clickOnItem("phoneNumber"));

        onView(withId(GDAutomatedTestSupport.getResourceID("phoneNumber"))).perform(typeText("0987654321"), closeSoftKeyboard());

        GDAutomatedTestSupport.waitForIdle(800);

        assertTrue(GDAutomatedTestSupport.clickOnItem("notes"));

        onView(withId(GDAutomatedTestSupport.getResourceID("notes"))).perform(typeText("A fantastic Leeds United Player"), closeSoftKeyboard());

        GDAutomatedTestSupport.waitForIdle(800);

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_save"));

        assertTrue(GDAutomatedTestSupport.isTextShown("Count = 1"));

        GDAutomatedTestSupport.pressHome();
    }

    /*
    Test 3, ensure 500 Contacts can be saved in SecureSQL sample app
    */
    @Test
    public void test_3_Add500ContactsSecureSQL( ) throws Exception {

        GDAutomatedTestSupport.launchAppUnderTest();

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_add_500"));

        assertTrue(GDAutomatedTestSupport.isTextShown("Count = 501", 30000)); // a longer time than default 5secs to wait as is expensive to add all contacts

        GDAutomatedTestSupport.pressHome();
    }

    /*
    Test 4, ensure a heavy query can be executed in SecureSQL sample app
    */
    @Test
    public void test_4_HeavyQuerySecureSQL( ) throws Exception {

        GDAutomatedTestSupport.launchAppUnderTest();

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_add_500"));

        assertTrue(GDAutomatedTestSupport.isTextShown("Count = 1001", 30000)); // a longer time than default 5secs to wait as is expensive to add all contacts

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_heavy_query"));

        assertTrue(GDAutomatedTestSupport.isTextShown("Count:", 15000));

        assertTrue(GDAutomatedTestSupport.clickOnItemContainingText("Close"));

        GDAutomatedTestSupport.pressHome();
    }

    /*
    Test 5, ensure a heavy query can be cancelled in SecureSQL sample app
    */
    @Test
    public void test_5_HeavyQueryCancelSecureSQL( ) throws Exception {

        GDAutomatedTestSupport.launchAppUnderTest();

        // add more records to the database so that the query takes longer and there is a better chance
        // of cancel working.
        assertTrue(GDAutomatedTestSupport.clickOnItem("action_add_500"));
        assertTrue(GDAutomatedTestSupport.isTextShown("Count = 1501", 30000)); // a longer time than default 5secs to wait as is expensive to add all contacts

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_add_500"));
        assertTrue(GDAutomatedTestSupport.isTextShown("Count = 2001", 30000)); // a longer time than default 5secs to wait as is expensive to add all contacts
        assertTrue(GDAutomatedTestSupport.clickOnItem("action_heavy_query", 5000, 0));

        // sometimes the query is already complete by the time cancel shows up.  adding more contacts
        // might help with timing.
        assertTrue(GDAutomatedTestSupport.clickOnItemContainingText("Cancel"));

        // There is a race condition where we may hit Cancel when it's already too late
        // and the Query Complete dialog is already shown.  Don't assert that the query complete
        // dialog is not shown.
        //assertFalse(GDAutomatedTestSupport.clickOnItemContainingText("Count:"));
        GDAutomatedTestSupport.clickOnItemContainingText("Close");

        GDAutomatedTestSupport.pressHome();
    }

    /*
    Test 6, ensure a contacts can be deleted from SecureSQL sample app
    */
    @Test
    public void test_6_DeleteContactsSecureSQL( ) throws Exception {

        GDAutomatedTestSupport.launchAppUnderTest();

        // It's possible that the heavy query dialog is still being shown, so try to close it.
        GDAutomatedTestSupport.clickOnItemContainingText("Close");

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_delete_all"));

        assertTrue(GDAutomatedTestSupport.isTextShown("No Contacts", 20000));

        GDAutomatedTestSupport.pressHome();
    }


    @After
    public void tearDown() throws Exception {

    }
}