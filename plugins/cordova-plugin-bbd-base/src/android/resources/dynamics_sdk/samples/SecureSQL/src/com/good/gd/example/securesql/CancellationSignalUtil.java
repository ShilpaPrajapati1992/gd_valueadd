/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2015 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.securesql;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CancellationSignal;
import android.widget.Toast;

import com.good.gd.example.utils.DbContract;

/**
 * Class CancellationSignalUtil is created to demonstrate functionality of CancellationSignal class
 * and ability to cancel query in progress.  
 * To see how CancellationSignal class works press “Add 500 contacts”, then after contacts will be added,
 * press “Heavy query” - and you will have ability to cancel this query.
 * This functionality is present only on devices with API 16 and above.
 */

public class CancellationSignalUtil {

    private static CancellationSignalUtil _instance = null;

    private boolean inProgress = false;
    private Context context;
    private AlertDialog alertDialog;
    private QueryResult result;
    private String message;
    private Object signal; // android.os.CancellationSignal, to support API less than 16


    private CancellationSignalUtil() {}

    public static CancellationSignalUtil getInstance() {
        if (_instance == null) {
            _instance = new CancellationSignalUtil();
        }
        return _instance;
    }

    public void update(Context context) {
        this.context = context;
        if(inProgress == true) {
            showAlertDialog();
        }
    }

    public void start() {
        if(inProgress == false) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                new HeavyQueryAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                Toast.makeText(context, "Cancellation Signal test is not available in Android API less than API 16.", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(context, "Busy Processing Previous Query. Try after some time", Toast.LENGTH_SHORT).show();
        }
    }

    private void showAlertDialog() {
        if(context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(false);
            builder.setMessage(message).setTitle("Heavy query");
            if(result != null ) {
                builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        alertDialog = null;
                        inProgress = false;
                        result = null;
                    }
                });
            } else {
                builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ((android.os.CancellationSignal) signal).cancel();
                        }
                    }
                });
            }

            alertDialog = builder.create();
            alertDialog.show();
        }
    }

    private class HeavyQueryAsyncTask extends AsyncTask<Void, Void, QueryResult> {
        private long startTime;

        public HeavyQueryAsyncTask() {
        }

        @Override
        protected void onPreExecute() {
            startTime = System.currentTimeMillis();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                signal = new CancellationSignal();
            }
            message = "Executing...";
            inProgress = true;
            showAlertDialog();
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected QueryResult doInBackground(Void... params) {
            inProgress = true;
            Cursor cursor = null;
            if(context != null) {
                final ContentResolver contentResolver = context.getContentResolver();
                try {
                    Uri uri = DbContract.CONTENT_CONTACTS_HEAVY_QUERY_URI;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        cursor = contentResolver.query(uri, null, null, null, null, ((android.os.CancellationSignal)signal));
                    } else {
                        cursor = contentResolver.query(uri, null, null, null, null);
                    }
                } catch(RuntimeException ex) {
                    return new QueryResult(0, true);
                }
            }

            return cursor != null ? new QueryResult(cursor.getCount(), false) : new QueryResult(0, false);
        }

        @Override
        protected void onPostExecute(QueryResult queryResult) {
            final double time = (System.currentTimeMillis() - startTime) / 1000d;

            result = queryResult;
            message = "Count: " + queryResult.count + "; Time: " + time + " s";

            if (alertDialog != null && alertDialog.isShowing()) {
                alertDialog.dismiss();
            }

            if (queryResult.isCanceled) {
                Toast.makeText(context, "Query canceled.", Toast.LENGTH_LONG).show();
                inProgress = false;
                result = null;
            } else {
                showAlertDialog();
            }
        }
    }

    private class QueryResult{
        public Integer count;
        public boolean isCanceled;

        public QueryResult(Integer count, boolean isCanceled) {
            this.count = count;
            this.isCanceled = isCanceled;
        }
    }
}
