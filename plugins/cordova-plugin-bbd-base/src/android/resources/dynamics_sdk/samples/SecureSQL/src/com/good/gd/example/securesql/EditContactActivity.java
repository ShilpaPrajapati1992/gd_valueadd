/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.securesql;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.good.gd.Activity;
import com.good.gd.example.utils.ContactsDBUtils.Contact;
import com.good.gd.example.utils.DbContract;

/**
 * EditContactActivity - used for editing existing contacts and adding new ones.
 * Must be passed the contact ID in order to edit it, otherwise it will add as
 * new.
 */
public class EditContactActivity extends Activity implements OnClickListener {

	private long _contactRecId = -1;

	/**
	 * onCreate - creates an editor based on the passed contact id.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_contact);

		Intent i = getIntent();
		if (i != null) {
			_contactRecId = i.getLongExtra(DbContract.CONTACTS_FIELD_ID,
					-1);
		}
		if (savedInstanceState != null) {
			_contactRecId = savedInstanceState
					.getLong(DbContract.CONTACTS_FIELD_ID);
		}

		if (_contactRecId > 0) {
			Contact c = null;
			Cursor cursor = getContentResolver().query(DbContract.CONTENT_URI, null, DbContract.CONTACTS_FIELD_ID + "=" + _contactRecId, null, null);
			cursor.moveToFirst();
	        if (!cursor.isAfterLast()) {
	            String firstName = cursor.getString(1);
	            String secondName = cursor.getString(2);
	            String phoneNumber = cursor.getString(3);
	            String notes = cursor.getString(4);
	            c = new Contact(firstName, secondName, phoneNumber, notes);
	        }
			((EditText) findViewById(R.id.firstName)).setText(c.getFirstName());
			((EditText) findViewById(R.id.secondName)).setText(c
					.getSecondName());
			((EditText) findViewById(R.id.phoneNumber)).setText(c
					.getPhoneNumber());
			((EditText) findViewById(R.id.notes)).setText(c.getNotes());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_contact_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.action_save:
			actionSave();
			finish();
			break;
		case R.id.action_cancel:
			finish();
			break;
		}

	}

	private void actionSave() {
		String firstName = ((EditText) findViewById(R.id.firstName)).getText()
				.toString();
		String secondName = ((EditText) findViewById(R.id.secondName))
				.getText().toString();
		String phoneNum = ((EditText) findViewById(R.id.phoneNumber)).getText()
				.toString();
		String notes = ((EditText) findViewById(R.id.notes)).getText()
				.toString();

		if ((firstName.trim().length() > 0 || secondName.trim().length() > 0)
				|| (phoneNum.length() > 0 || notes.trim().length() > 0)) {
			ContentValues v = new ContentValues();
	        v.put(DbContract.CONTACTS_FIELD_FIRSTNAME, firstName);
	        v.put(DbContract.CONTACTS_FIELD_SECONDNAME, secondName);
	        v.put(DbContract.CONTACTS_FIELD_PHONENUMBER, phoneNum);
	        v.put(DbContract.CONTACTS_FIELD_NOTES, notes);
			if (_contactRecId > 0) {
				// updating an existing contact
				getContentResolver().update(DbContract.CONTENT_URI, v, DbContract.CONTACTS_FIELD_ID + "= ?", new String[]{Long.toString(_contactRecId)});
			} else {
				// saving a new contact
				getContentResolver().insert(DbContract.CONTENT_URI, v);
			}
		}
	}

}
