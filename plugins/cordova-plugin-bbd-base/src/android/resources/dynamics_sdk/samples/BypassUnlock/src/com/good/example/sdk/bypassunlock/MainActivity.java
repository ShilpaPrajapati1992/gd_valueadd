/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2016 Good Technology Corporation. All rights reserved.
 */

package com.good.example.sdk.bypassunlock;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.good.gd.GDAndroid;
import com.good.gd.GDStateListener;
import com.good.gd.widget.GDTextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;


public class MainActivity extends Activity implements GDStateListener, View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_contacts: {
                Intent intent = new Intent();
                intent.setClass(this, ContactsActivity.class);
                startActivity(intent);
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GDAndroid.getInstance().activityInit(this);

        setContentView(R.layout.activity_main);

        GDTextView textView = (GDTextView) findViewById(R.id.text_introduction);
        String styledText = null;
        try {
            InputStream is = getAssets().open("Introduction.html");

            int size = is.available();

            // Read the entire asset into a local byte buffer.
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            // Convert the buffer into a string.
            styledText = new String(buffer);

        } catch (IOException e) {
            e.printStackTrace();
            styledText = "Cannot load introduction HTML file!";
        }
        textView.setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);
    }

    /**
     * onCreateOptionsMenu - called once to create the options menu structure
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /*
     * Activity specific implementation of GDStateListener.
     *
     * If a singleton event Listener is set by the application (as it is in this case) then setting
     * Activity specific implementations of GDStateListener is optional
     */
    @Override
    public void onAuthorized() {
        //If Activity specific GDStateListener is set then its onAuthorized( ) method is called when
        //the activity is started if the App is already authorized
        Log.i(TAG, "onAuthorized()");

        GDTextView textPolicy = (GDTextView) findViewById(R.id.text_policy);
        if(textPolicy != null) {
            textPolicy.setText(GDAndroid.getInstance().getApplicationPolicyString());
        }
    }

    @Override
    public void onLocked() {
        Log.i(TAG, "onLocked()");
    }

    @Override
    public void onWiped() {
        Log.i(TAG, "onWiped()");
    }

    @Override
    public void onUpdateConfig(Map<String, Object> settings) {
        Log.i(TAG, "onUpdateConfig()");
    }

    @Override
    public void onUpdatePolicy(Map<String, Object> policyValues) {
        GDTextView textPolicy = (GDTextView) findViewById(R.id.text_policy);
        if(textPolicy != null) {
            textPolicy.setText(policyValues.toString());
        }
    }

    @Override
    public void onUpdateServices() {
        Log.i(TAG, "onUpdateServices()");
    }

    @Override
    public void onUpdateDataPlan() {
        Log.i(TAG, "onUpdateDataPlan()");
    }

    @Override
    public void onUpdateEntitlements() {
        Log.i(TAG, "onUpdateEntitlements()");
    }
}
