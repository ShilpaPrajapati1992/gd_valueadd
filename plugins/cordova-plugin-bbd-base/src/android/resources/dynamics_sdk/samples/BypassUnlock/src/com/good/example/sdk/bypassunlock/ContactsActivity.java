/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2016 Good Technology Corporation. All rights reserved.
 */

package com.good.example.sdk.bypassunlock;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.good.gd.Activity;

public class ContactsActivity extends Activity {
    public static final String LOG_TAG = "ContactsActivity";

    @Override
    protected void onCreate(Bundle bundle) {
        Log.i(LOG_TAG, "onCreate\n");

        super.onCreate(bundle);

        setContentView(R.layout.contacts);

        String names[] = {
            "Mary Stansberry",
            "Daniel Drummond",
            "Virginia Rush",
            "Kathryn Dooley",
            "William Raper",
            "Peggy Dale",
            "Deloris Bromley",
            "Eric Morgan",
            "Jan Anderson",
            "Georgianna Catlin",
            "Eduardo Johnson",
            "Christian Kittinger",
            "Kerry Boggess",
            "Harold Stephens",
            "Wilma Burke",
            "Thomas Williams",
            "Gloria Hafford",
            "Charlene Hernandez",
            "Reginald Pauling",
            "William Bouchard"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                names);
        ListView listview = (ListView)findViewById(R.id.listview);
        listview.setAdapter(adapter);
    }
}
