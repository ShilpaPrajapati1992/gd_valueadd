### Bypass Idle Unlock Sample Application###

================================================================================
DESCRIPTION:
An example of how to use Good Dynamics Bypass Idle Lock functionality.

To enable Bypass functionality, update the Application policy on the Good Control server to allow parts
of the user interface to be displayed when idle lock is in place

Use adb command to open new Activity that is allowed to bypass Idle Lock Screen:

$ adb shell am broadcast -n com.good.example.sdk.bypassunlock/.EventReceiver

The Application Lock screen can be bypassed only when user is already authorized (after idle time out).
When user wants to start Bypass Activity from cold, he will be prompted to enter the password.

For more help of how to build and install this application, please see BUILD_NOTES.

For more examples visit the Good Developer Network:
https://begood.good.com/community/gdn

Support:
https://begood.good.com/community/gdn/support
================================================================================
(c) 2016 Good Technology Corporation. All rights reserved.