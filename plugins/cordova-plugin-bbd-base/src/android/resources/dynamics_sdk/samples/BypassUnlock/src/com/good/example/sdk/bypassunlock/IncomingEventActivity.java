/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2016 Good Technology Corporation. All rights reserved.
 */

package com.good.example.sdk.bypassunlock;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class IncomingEventActivity extends BaseActivity {
    public static final String LOG_TAG = "IncomingEventActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(LOG_TAG, "onCreate\n");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.incoming_event);

        Button btnAccept = (Button)findViewById(R.id.accept);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

                Intent appIntent = new Intent(IncomingEventActivity.this, InCallActivity.class);
                appIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(appIntent);

            }
        });

        Button btnDecline = (Button)findViewById(R.id.decline);
        btnDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setSwitchView(R.id.switchView);
    }
}
