/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2016 Good Technology Corporation. All rights reserved.
 */

package com.good.example.sdk.bypassunlock;

import android.app.Application;
import android.util.Log;

import com.good.gd.GDAndroid;
import com.good.gd.GDStateListener;

import java.util.Map;

public class BypassUnlockApplication extends Application implements GDStateListener {
    public static final String LOG_TAG = "BypassUnlockApplication";
    private boolean isAuthorized = false;

    public boolean getAuthorized() {
        return isAuthorized;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(LOG_TAG, "onCreate\n");
        //Singleton AppEvent listener is set in Application class so it receives events independently of Activity lifecycle
        GDAndroid.getInstance().setGDStateListener(this);
    }

    @Override
    public void onAuthorized() {
        Log.i(LOG_TAG, "onAuthorized\n");
        isAuthorized = true;
    }

    @Override
    public void onLocked() {
        Log.i(LOG_TAG, "onLocked\n");
        isAuthorized = false;
    }

    @Override
    public void onWiped() {
        isAuthorized = false;
    }

    @Override
    public void onUpdateConfig(Map<String, Object> settings) {

    }

    @Override
    public void onUpdatePolicy(Map<String, Object> policyValues) {

    }

    @Override
    public void onUpdateServices() {

    }

    @Override
    public void onUpdateDataPlan() {
    }

    @Override
    public void onUpdateEntitlements() {

    }
}
