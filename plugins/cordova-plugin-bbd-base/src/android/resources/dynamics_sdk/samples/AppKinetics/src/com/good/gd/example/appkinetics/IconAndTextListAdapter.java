package com.good.gd.example.appkinetics;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.good.gd.GDServiceProvider;

public class IconAndTextListAdapter extends ArrayAdapter<GDServiceProvider> {

    public IconAndTextListAdapter(Activity activity, List<GDServiceProvider> gdServiceProviders) {
        super(activity, 0, gdServiceProviders);
    }

    @SuppressLint("ViewHolder")
	@SuppressWarnings("deprecation")
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Activity activity = (Activity) getContext();
        LayoutInflater inflater = activity.getLayoutInflater();

        // Inflate the views from XML
        View rowView = inflater.inflate(R.layout.listviewlayout, parent, false);
        GDServiceProvider gdServiceProvider = getItem(position);

        // Load the image and set it on the ImageView
        ImageView imageView = (ImageView) rowView.findViewById(R.id.image);
        imageView.setImageDrawable(new BitmapDrawable((gdServiceProvider.getIcon())));

        // Set the text on the TextView
        TextView textView = (TextView) rowView.findViewById(R.id.text);
        textView.setText(gdServiceProvider.getName());
        
        return rowView;
    }

   
}