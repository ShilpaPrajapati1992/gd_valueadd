/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.appkinetics;

import android.app.Application;
import android.util.Log;

import com.good.gd.GDAndroid;
import com.good.gd.icc.GDService;
import com.good.gd.icc.GDServiceClient;
import com.good.gd.icc.GDServiceException;

/**
 * Application class
 */
public class AppKineticsApplication extends Application {

    // Public Methods ---------------------------------------------------------
    @Override
    public void onCreate() {
        super.onCreate();

        AppKineticsModel.getInstance().setContext(this);
        GDAndroid.getInstance().setGDStateListener(AppKineticsModel.getInstance());

        final AppKineticsGDServiceListener serviceListener =
                AppKineticsGDServiceListener.getInstance();

        Log.e(AppKineticsHelpers.LOGTAG,
                "AppKineticsApplication::onCreate() service Listener = " + serviceListener + "\n");

        if (serviceListener != null) {

            // set the Client Service Listener to get responses from server
            // (which is another AppKinetics client)
            try {
                GDServiceClient.setServiceClientListener(serviceListener);
                GDService.setServiceListener(serviceListener);
            } catch (GDServiceException gdServiceException) {
                Log.e(AppKineticsHelpers.LOGTAG, "AppKineticsApplication::onCreate() " +
                                "- error setting GDServiceClientListener: " + gdServiceException.getMessage() + "\n"
                );
            }
        }
    }
}