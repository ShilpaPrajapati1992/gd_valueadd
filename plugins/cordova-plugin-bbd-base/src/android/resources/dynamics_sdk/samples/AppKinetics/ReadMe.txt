### App Kinetics ###

================================================================================
DESCRIPTION:
	- App utilizes Good Dynamic SDKs transfer-file service to send files over Good Dynamics Inter-Container Communication.
	- Currently support files named *SampleDoc* in the assets folder and operate on those
	- At this time only files with a .txt extension will be displayed locally, or if/when received from remote client
	- On the GC side
		(a) Create new app with full package name as GD Application ID
		(b) Under Versions tab for app, add service by Bind Service and selecting Transfer File Service
		(c) Under Advanced tab for app, add the Android Package Name including the IccReceivingActivity
			i) For example, com.mysampleapp.appkinetics.IccReceivingActivity


For more help of how to build and install this application, please see BUILD_NOTES.

For more examples visit the Good Developer Network:
https://begood.good.com/community/gdn

Support:
https://begood.good.com/community/gdn/support
================================================================================
(c) 2015 Good Technology Corporation. All rights reserved.