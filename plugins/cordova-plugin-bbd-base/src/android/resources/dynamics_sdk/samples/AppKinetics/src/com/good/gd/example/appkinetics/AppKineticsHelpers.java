/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.appkinetics;

public class AppKineticsHelpers {

	// Static Constants -------------------------------------------------------

	// version of the file transfer service for AppKinetics
	public final static String VERSION = "1.0.0.0";

	// tag for logging purposes
	public final static String LOGTAG = "AppKinetics";
	
	// substring pattern of file name to treat as transferrable
	public final static String FILENAMEPATTERN = "Sample - ";

	// name of Good service for transferring files
	public final static String SERVICENAME = "com.good.gdservice.transfer-file";

	// name of Good service method for transferring files
	public final static String SERVICEMETHODNAME = "transferFile";

	// for notifying about unsupported file types
	public final static String UNSUPPORTFILETYPE = "Cannot display this file type";

	// new line
	public final static String NEWLINE = System.getProperty("line.separator");
}
