/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.appkinetics;

import java.util.Comparator;

/**
 * Represents file data object
 * 
 */
public class FileModel {

	// Inner Class -----------------------------------------------------

	/**
	 * Comparator to sort the objects based on the file name
	 * 
	 */
	static public class NameComparator implements Comparator<FileModel> {
		@Override
		public int compare(FileModel o1, FileModel o2) {
			return o1.getName().compareToIgnoreCase(o2.getName());
		}
	}

	// Instance Variables -----------------------------------------------------
	private String name;
	private boolean selected;

	// Constructors -----------------------------------------------------------
	public FileModel(String name) {
		this.name = name;
		selected = false;
	}

	// Public Methods ---------------------------------------------------------
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
