/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.appkinetics;

import android.util.Log;

import com.good.gd.icc.GDServiceClientListener;
import com.good.gd.icc.GDServiceError;
import com.good.gd.icc.GDServiceListener;

/**
 * ICC service listener
 */
public class AppKineticsGDServiceListener implements
        GDServiceClientListener, GDServiceListener {

    // Static Variables -------------------------------------------------------
    private static AppKineticsGDServiceListener instance;

    // Static Methods ---------------------------------------------------------
    public static AppKineticsGDServiceListener getInstance() {
        if (instance == null) {
            synchronized (AppKineticsGDServiceListener.class) {
                instance = new AppKineticsGDServiceListener();
            }
        }
        return instance;
    }

    // Public Methods ---------------------------------------------------------

    // service client listener methods
    @Override
    public void onMessageSent(final String application,
                              final String requestId,
                              final String[] attachments) {
        Log.d(AppKineticsHelpers.LOGTAG, "AppKineticsGDServiceListener.onMessageSent");
    }

    @Override
    public void onReceivingAttachments(String application, int numberOfAttachments, String requestID) {
        Log.d(AppKineticsHelpers.LOGTAG, "AppKineticsGDServiceListener.onReceivingAttachments number of attachments: " + numberOfAttachments + " for requestID: " + requestID + "\n");
    }
    
    @Override
    public void onReceivingAttachmentFile(String application, String path, long size, String requestID) {
        Log.d(AppKineticsHelpers.LOGTAG, "AppKineticsGDServiceListener.onReceivingAttachmentFile attachment: " + path + " size: " + size + " for requestID: " + requestID + "\n");
    }

    // from serviceListener
    @Override
    public void onReceiveMessage(final String application, final String service,
                                 final String version, final String method,
                                 final Object params,
                                 final String[] attachments, final String requestId) {
        Log.d(AppKineticsHelpers.LOGTAG,
                "AppKineticsGDServiceListener.onReceiveMessage (from serviceListener)");
        if (method.equals("transferFile")) {
            AppKineticsModel.getInstance().addFilesToPendingSaveList(attachments);
        }
    }

    // from clientListener
    @Override
    public void onReceiveMessage(final String application, final Object params,
                                 final String[] attachments, final String requestId) {
        Log.d(AppKineticsHelpers.LOGTAG,
                "AppKineticsGDServiceListener.onReceiveMessage (from clientListener)");

        if (params instanceof GDServiceError){
            AppKineticsModel.getInstance().handleError((GDServiceError)params);
        }

    }
}