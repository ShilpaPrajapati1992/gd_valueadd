/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2013 Good Technology Corporation. All rights reserved.
 */

package com.good.gd.example.appkinetics;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.good.gd.GDAndroid;
import com.good.gd.GDServiceProvider;

/**
 * File list screen
 */
public class AppKinetics extends Activity implements OnClickListener {

    // Instance Variables -----------------------------------------------------
    private FileListAdapter adapter;

    // Public Methods ---------------------------------------------------------
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(AppKineticsHelpers.LOGTAG, "AppKinetics.onCreate()\n");

        GDAndroid.getInstance().activityInit(this);
        setContentView(R.layout.main);

        // setup the click handler
        final ListView listview = (ListView) findViewById(R.id.listview);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final String filePath = ((FileModel) parent
                        .getItemAtPosition(position)).getName();
                final Intent intent = new Intent(getApplicationContext(),
                        FileViewerActivity.class);
                intent.putExtra(FileViewerActivity.FILE_VIEWER_PATH, filePath);

                startActivity(intent);
            }
        });

        initList();
    }

    private void initList() {
        final ListView listView = (ListView) findViewById(R.id.listview);

        final List<FileModel> data =
                new ArrayList<FileModel>(AppKineticsModel.getInstance().getFiles());

        Collections.sort(data, new FileModel.NameComparator());

        adapter = new FileListAdapter(this, data);
        listView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
		Log.d(AppKineticsHelpers.LOGTAG, "AppKinetics.onResume() IN\n");		
		super.onResume();		

		if (AppKineticsModel.getInstance().isAuthorized()) {
			updateFileList();
		}
		Log.d(AppKineticsHelpers.LOGTAG, "AppKinetics.onResume() OUT\n");
    }
	
	@Override
	protected void onPostResume() {
		Log.d(AppKineticsHelpers.LOGTAG, "AppKinetics.onPostResume IN\n");
		super.onPostResume();		

		if (AppKineticsModel.getInstance().isAuthorized() ) {
			AppKineticsModel.getInstance().setAppKineticsActivity(this);
			
			this.runOnUiThread(new Thread(new Runnable() { 
		         public void run() {
		 			AppKineticsModel.getInstance().savePendingFiles();
					updateFileList();	
		         }
		     }));
		}
		
		Log.d(AppKineticsHelpers.LOGTAG, "AppKinetics.onPostResume OUT\n");
	}

    @Override
    public void onStart() {
        super.onStart();
        Log.d(AppKineticsHelpers.LOGTAG, "AppKinetics.onStart()\n");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(AppKineticsHelpers.LOGTAG, "AppKinetics.onStop()\n");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_send_to: {
                sendFiles();
                break;
            }
            case R.id.action_delete: {
                deleteSelectedFiles();
                updateFileList();
                break;
            }
            case R.id.action_reset: {
                resetFileList();
                updateFileList();
                break;
            }
        }
    }
    
    // Private Methods --------------------------------------------------------

    private void updateFileList() {
        Log.d(AppKineticsHelpers.LOGTAG, "AppKinetics.displayList()\n");

        final List<FileModel> data =
                new ArrayList<FileModel>(AppKineticsModel.getInstance().getFiles());

        Collections.sort(data, new FileModel.NameComparator());

        adapter.notifyDataSetChanged(data);
    }

    private void deleteSelectedFiles() {
        for (FileModel file : AppKineticsModel.getInstance().getFiles()) {
            if (file.isSelected())
                AppKineticsModel.getInstance().deleteFile(file.getName());
        }
    }

    private void resetFileList() {
        AppKineticsModel.getInstance().resetFileList();
    }

    private void sendFiles() {
        List<String> selectedFiles = new ArrayList<String>();
        for (FileModel file : AppKineticsModel.getInstance().getFiles()) {
            if (file.isSelected()) {
                selectedFiles.add(file.getName());
            }
        }
        if (selectedFiles.size() > 0)
            showSendToDialog(selectedFiles);
    }

    private void showSendToDialog(final List<String> files) {
        final FileTransferService fileTransferService = new FileTransferService(
                this);
        final List<GDServiceProvider> transferServices = fileTransferService.getList();

        
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.send_to)
                .setAdapter(
                        new IconAndTextListAdapter(this, transferServices),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                            	GDServiceProvider svc = transferServices.get(which);
                                String svcAddrs = fileTransferService
                                        .addressLookup((String) svc.getName());
                                AppKineticsModel.getInstance().sendFiles(
                                        svcAddrs, files);
                            }
                        }
                )
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        }
                ).setCancelable(true);
		AlertDialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}

	 void showOverwriteDialog(final String receivedFileName
                              , final String outFileName
                              , final InputStream inputStream) {
         Log.d(AppKineticsHelpers.LOGTAG, "AppKinetics.showOverwriteDialog IN\n");

         AlertDialog.Builder builder = new AlertDialog.Builder(this);

         builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
             public void onClick(DialogInterface dialog, int id) {
                 AppKineticsModel.getInstance().actualSaveFile(outFileName, inputStream);
                 try {
                     inputStream.close();
                 } catch (IOException e) {
                     e.printStackTrace();
                 }
                 updateFileList();
             }

         });
         builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
             public void onClick(DialogInterface dialog, int id) {
             }
         });
         builder.setCancelable(true);

         final ScrollView scrollView = new ScrollView(getApplicationContext());
         final TextView textView = new TextView(getApplicationContext());

         //set scrollable message with custom font size
         final String dialogMessageSubstr1 = getString(R.string.overwrite_dialog_substr1);
         final String dialogMessageSubstr2 = getString(R.string.overwrite_dialog_substr2);
         final String dialogMessage = dialogMessageSubstr1 + receivedFileName + dialogMessageSubstr2;
         textView.setText(dialogMessage);
         final Resources res = getResources();
         final float dialogFontSize = res.getDimension(R.dimen.overwrite_dilog_font_size);
         textView.setTextSize(dialogFontSize);
         textView.setBackgroundColor(ContextCompat.getColor(this,R.color.overwrite_dilog_background));
         textView.setTextColor(ContextCompat.getColor(this,R.color.overwrite_dilog_text_color));
         scrollView.addView(textView);
         
         builder.setCustomTitle(scrollView);
         
         AlertDialog overwriteDialog = builder.create();
         overwriteDialog.show();
     }
}
