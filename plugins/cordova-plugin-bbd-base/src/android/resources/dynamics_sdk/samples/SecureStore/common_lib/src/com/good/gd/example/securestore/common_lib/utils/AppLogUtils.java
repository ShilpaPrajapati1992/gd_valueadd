/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2014 Good Technology Corporation. All rights reserved.
 */
package com.good.gd.example.securestore.common_lib.utils;

import android.util.Log;

/**
 * App Log Utils - Simple class for debug logging. Can macro out logs or route to new destimation
 */
public class AppLogUtils {

    private final static String LOG_TAG = "SecureStore";

    public static void DEBUG_LOG(String aLogMessage){

        Log.d(LOG_TAG, aLogMessage);

    }

    public static void ERROR_LOG(String aLogMessage){

        Log.e(LOG_TAG, aLogMessage);

    }

}
