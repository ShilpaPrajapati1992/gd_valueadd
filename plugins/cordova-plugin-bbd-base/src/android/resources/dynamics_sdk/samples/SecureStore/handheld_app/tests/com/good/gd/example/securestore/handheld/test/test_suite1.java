package com.good.gd.example.securestore.handheld.test;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.widget.ListView;

import com.good.automated_test_support.GDAutomatedTestSupport;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;



@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class test_suite1 {

    //static final String secureStorePackageName = "com.good.gd.example.securestore";

    /*
    Setup Test, like all tests makes use of helper functions in GD_UIAutomator_Lib Test library project
     */
    @BeforeClass
    public static void setUpClass() {

        //Setup test support and do not register a GDStateListener
        GDAutomatedTestSupport.setupGDAutomatedTestSupport();

        GDAutomatedTestSupport.wakeUpDeviceIfNeeded();

        //Android Emulator when booted sometimes has error dialogues to dismiss
        GDAutomatedTestSupport.acceptSystemDialogues();

    }

    @After
    public void tearDown() throws Exception {

    }

    /*
    Test 1, if GD App is already activated ensure that it can be unlocked using password. If not activated ensure it
    can be activated using Email Address & Access Key. These are set in file com.good.gd.test.json at build time
     */
    @Test
    public void test_1_GDActivateSecureStore( ) throws Exception {

        GDAutomatedTestSupport.launchAppUnderTest();

        assertTrue(GDAutomatedTestSupport.loginOrProvisionGDApp());

        assertTrue(GDAutomatedTestSupport.checkGDAuthorized());

        GDAutomatedTestSupport.pressHome();

    }


    /*
    Test 2, ensure can access Container in SecureStore app
    */
    @Test
    public void test_2_canAccessContainer() {

        GDAutomatedTestSupport.launchAppUnderTest();

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_btn_container"));

        assertTrue(GDAutomatedTestSupport.isElementChecked("com.good.gd.example.securestore","action_btn_container"));

        GDAutomatedTestSupport.pressHome();

    }

    /*
    Test 3, ensure can create a new folder in  Container
    */
    @Test
    public void test_3_createFolderInContainer() {

        GDAutomatedTestSupport.launchAppUnderTest();

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_btn_container"));

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_create_folder"));

        assertTrue(GDAutomatedTestSupport.isTextShown("Directory Name:"));

        GDAutomatedTestSupport.enterTextScreenWithClass("android.widget.EditText", "a_container_folder");

        assertTrue(GDAutomatedTestSupport.clickOnItemContainingText("OK"));

        assertTrue(GDAutomatedTestSupport.isTextShown("a_container_folder"));

        GDAutomatedTestSupport.pressHome();

    }


    /*
   Test 4, ensure can delete a folder in Container
   */
    @Test
    public void test_4_deleteFolderInContainer() throws UiObjectNotFoundException {

        GDAutomatedTestSupport.launchAppUnderTest();

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_btn_container"));

        UiScrollable listView = new UiScrollable(new UiSelector().className("android.widget.ListView"));

        UiObject listItem = listView.getChildByText(new UiSelector().className(android.widget.TextView.class.getName()), "a_container_folder");

        listItem.dragTo(listItem, 10);

        assertTrue(GDAutomatedTestSupport.clickOnItemContainingText("Delete"));

        assertFalse(GDAutomatedTestSupport.isTextShown("a_container_folder"));

        GDAutomatedTestSupport.pressHome();

    }


    /*
    Test 5, ensure permission dialog is displayed when access SD Card and can deny the permission
    */
    @Test
    public void test_5_denyPermissionWhenAccessSDCard() {

        GDAutomatedTestSupport.launchAppUnderTest();

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_btn_sdcard"));

        assertTrue(GDAutomatedTestSupport.isScreenShown("com.android.packageinstaller","permission_message"));

        assertTrue(GDAutomatedTestSupport.clickOnItem("com.android.packageinstaller", "permission_deny_button"));

        assertFalse(GDAutomatedTestSupport.isScreenShown("com.android.packageinstaller", "permission_message"));

        assertTrue(GDAutomatedTestSupport.isTextShown("No files"));

        GDAutomatedTestSupport.pressHome();

    }

    /*
    Test 6, ensure permission dialog is displayed when access SD Card and can allow the permission
    */
    @Test
    public void test_6_allowPermissionWhenAccessSDCard() {

        GDAutomatedTestSupport.launchAppUnderTest();

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_btn_sdcard"));

        assertTrue(GDAutomatedTestSupport.isScreenShown("com.android.packageinstaller", "permission_message"));

        assertTrue(GDAutomatedTestSupport.clickOnItem("com.android.packageinstaller", "permission_allow_button"));

        assertFalse(GDAutomatedTestSupport.isScreenShown("com.android.packageinstaller", "permission_message"));

        assertFalse(GDAutomatedTestSupport.isTextShown("No files"));

        GDAutomatedTestSupport.pressHome();

    }


    /*
    Test 7, ensure can create a new folder in SD Card
    */
    @Test
    public void test_7_createFolderInSDCard() {

        GDAutomatedTestSupport.launchAppUnderTest();

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_btn_sdcard"));

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_create_folder"));

        assertTrue(GDAutomatedTestSupport.isTextShown("Directory Name:"));

        GDAutomatedTestSupport.enterTextScreenWithClass("android.widget.EditText", "a_sdcard_folder");

        assertTrue(GDAutomatedTestSupport.clickOnItemContainingText("OK"));

        assertTrue(GDAutomatedTestSupport.isTextShown("a_sdcard_folder"));

        GDAutomatedTestSupport.pressHome();

    }


    /*
    Test 8, ensure can delete a folder in SD Card
    */
    @Test
    public void test_8_deleteFolderInSDCard() throws UiObjectNotFoundException {

        GDAutomatedTestSupport.launchAppUnderTest();

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_btn_sdcard"));

        UiScrollable listView = new UiScrollable(new UiSelector().className("android.widget.ListView"));

        UiObject listItem = listView.getChildByText(new UiSelector().className(android.widget.TextView.class.getName()), "a_sdcard_folder");

        listItem.dragTo(listItem, 10);

        assertTrue(GDAutomatedTestSupport.clickOnItemContainingText("Delete"));

        assertFalse(GDAutomatedTestSupport.isTextShown("a_sdcard_folder"));

        GDAutomatedTestSupport.pressHome();

    }


    /*
    Test 9, ensure can open a file in Container
    */
    @Test
    public void test_9_openTxtContainer() {

        GDAutomatedTestSupport.launchAppUnderTest();

        assertTrue(GDAutomatedTestSupport.clickOnItem("action_btn_container"));

        assertTrue(GDAutomatedTestSupport.clickOnItemContainingText("root.txt"));

        GDAutomatedTestSupport.waitForIdle(10000);

        assertTrue(GDAutomatedTestSupport.isScreenShownWithDescription("root.txt"));

        GDAutomatedTestSupport.pressHome();
    }


}