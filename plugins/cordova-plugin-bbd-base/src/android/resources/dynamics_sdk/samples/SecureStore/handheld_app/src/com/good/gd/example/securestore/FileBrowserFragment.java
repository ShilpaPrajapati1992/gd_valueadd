package com.good.gd.example.securestore;

import android.Manifest;
import android.app.AlertDialog;
import android.app.backup.BackupManager;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.good.gd.GDAndroid;
import com.good.gd.example.securestore.common_lib.FileBrowserBaseFragment;
import com.good.gd.example.securestore.common_lib.iconifiedlist.IconifiedText;
import com.good.gd.example.securestore.common_lib.utils.ListUtils;
import com.good.gd.example.securestore.iconifiedlist.IconifiedTextListAdapter;
import com.good.gd.example.securestore.utils.FileUtils;
import com.good.gd.widget.GDEditText;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.good.gd.example.securestore.common_lib.utils.AppLogUtils.DEBUG_LOG;


/**
 * AndroidFileBrowser - a basic file browser list which supports multiple modes
 * (Container and insecure SDCard). Files can be deleted, moved to the container
 * and if they're .txt files they can be opened and viewed.
 */
public class FileBrowserFragment extends FileBrowserBaseFragment {

	private final int PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE = 1;
	private AlertDialog newFolderAlert;

	public FileBrowserFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.main, container, false);
		mView = v;

		return v;

	}

	@Override
	public void onStart() {
		super.onStart();

		getListView().setLongClickable(true);
		registerForContextMenu(getListView());

		/*
		 * Set the path the very first time the fragment is created. As fragment
		 * addition to the activity is asynchronous the FileBrowser activity
		 * will not call back directly in case the callback triggers any action
		 * that requires the fragment to be in the started state (typically view
		 * related operations). Therefore we can action the callback here
		 * instead if this is the first start after creation and we can tell
		 * this because the mCurrentPath member is null.
		 */
		if (mCurrentPath == null) {
			mCurrentPath = FileUtils.getInstance().getCurrentRoot();
			// now we've set the path update authorized state as we know it must
			// now be true
			onAuthorizeStateChange(true);
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if (newFolderAlert != null) newFolderAlert.dismiss();
	}

	/**
	 * onListItemClick - something on the list was clicked (not a long click)
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		String browseTo = !mCurrentPath.equals("/") ? mCurrentPath + "/"
				+ directoryEntries.get(position).getText() : mCurrentPath
				+ directoryEntries.get(position).getText();
		browseToPath(browseTo);
	}


	public void onAuthorizeStateChange(boolean authorized) {
		m_authorized = authorized;

		if (authorized) {
			// create backup manager if needed
			if (mBackupManager == null) {
				mBackupManager = new BackupManager(getActivity());
			}
			// handle any backup
            mBackupManager.dataChanged();

			// browse to path
			browseToPath(mCurrentPath);
		}
	}

	public void onPermissionResult(int requestCode, String[] permissions, int[] grantResults) {
		if(PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE == requestCode) {
			// Check if this fragment is attached to host Activity.
			// If yes, we can directly ask to show file list.
			// If no, this is because parent Activity has removed it with onPause callback,
			// and with onResume callback the new fragment instance will be created.
			// Fragment.onStart callback will show the needed file list, because we store FileUtils mode in an singleton.
			if(getActivity() != null) {
				//We don't check grantResult. If user doesn't grant permission, the No files message will be shown
				browseToPath(mCurrentPath);
			}
		}
	}

	private void handleSDcardAction() {
		// With Android M, we need to check and request permissions in runtime
		if (Build.VERSION.SDK_INT >= 23) {
			int permission = ContextCompat.checkSelfPermission(getActivity()
					, Manifest.permission.WRITE_EXTERNAL_STORAGE);
			if (permission == PackageManager.PERMISSION_GRANTED) {
				Log.d("onClick", "Permission is granted\n");
				browseToPath(mCurrentPath);
			} else {
				Log.w("onClick", "Permission is denied\n");
				ActivityCompat.requestPermissions(getActivity()
						, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}
						, PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
			}
		} else {
			browseToPath(mCurrentPath);
		}
	}

	/**
	 * onClick - one of the buttons was pressed
	 */
	public void onClick(int id) {
		switch (id) {
		case R.id.action_back:
			Log.d("onClick", "action_back");
			upOneLevel();
			break;

		case R.id.action_create_folder:
			Log.d("onClick", "action_create_folder");
			showNewDirUI();
			break;

		case R.id.action_btn_container:
			Log.d("onClick", "action_btn_container");
			FileUtils.getInstance().setMode(FileUtils.MODE_CONTAINER);
			mCurrentPath = FileUtils.CONTAINER_ROOT;
			browseToPath(mCurrentPath);
			break;

		case R.id.action_btn_sdcard:
			Log.d("onClick", "action_btn_sdcard");
			FileUtils.getInstance().setMode(FileUtils.MODE_SDCARD);
			mCurrentPath = FileUtils.SDCARD_ROOT;
			handleSDcardAction();
			break;

		case R.id.action_lock:
			Log.d("onClick", "action_lock");
			remoteLock();
			break;
		}
	}

	/**
	 * upOneLevel - switch the current working directory one layer up
	 */
	private void upOneLevel() {
		File parentFile = FileUtils.getInstance().getParentFile(mCurrentPath);
		if (parentFile != null
				&& FileUtils.getInstance().canGoUpOne(mCurrentPath)) {
			browseToPath(parentFile.getPath());
		}
	}

	/**
	 * browseToPath - load the file browser at the specified path
	 */
    @Override
	protected void browseToPath(String path) {
		if (m_authorized && (path != null)) {
			File file = FileUtils.getInstance().getFileFromPath(path);
			if (file.isDirectory()) {
				mCurrentPath = file.getPath();
				populateList(file.listFiles());
				updateButtonsAndTitle();
			} else {
				FileUtils.getInstance().openItem(getActivity(), path);
			}
		}
	}

	private void remoteLock() {
		GDAndroid.executeRemoteLock();
	}

    @Override
    protected void deleteItem(String aFilePath) {
        FileUtils.getInstance().deleteItem(aFilePath);
    }

    @Override
    protected void copyToContainer(String aFilePath) {
        FileUtils.getInstance().copyToContainer(aFilePath);
    }

    @Override
    protected int getFileMode() {
        return FileUtils.getInstance().getMode();
    }

    @Override
    protected boolean isDir(String aFilePath) {
        return FileUtils.getInstance().isDir(aFilePath);
    }

    @Override
    protected int getNumberFilesInCurrentPath() {
        File file = FileUtils.getInstance().getFileFromPath(mCurrentPath);
        File[] listFiles = file.listFiles();
        return listFiles.length;
    }

    @Override
    protected boolean doesSampleDataExist() {

        SharedPreferences sp = GDAndroid.getInstance().getGDSharedPreferences(SECURE_STORE_SHARED_PREFS,
                android.content.Context.MODE_PRIVATE);

        boolean ret = sp.getBoolean(SECURE_STORE_SAMPLE_DATA_KEY, false);

        DEBUG_LOG("doesSampleDataExist = " + ret);

        return ret;
    }

    @Override
    protected void setSampleDataExists() {

        SharedPreferences sp = GDAndroid.getInstance().getGDSharedPreferences(SECURE_STORE_SHARED_PREFS,
                android.content.Context.MODE_PRIVATE);

        sp.edit().putBoolean(SECURE_STORE_SAMPLE_DATA_KEY, true).commit();

    }

	/**
	 * populateList - takes a set of files and writes into the list adapter
	 */
	private void populateList(File[] files) {
		this.directoryEntries.clear();
		List<String> folderLst = new ArrayList<String>();
		List<String> fileLst = new ArrayList<String>();

		if (files != null) {
			for (File currentFile : files) {
				if (currentFile.isDirectory()) {
					ListUtils.insertAsc(folderLst, currentFile.getName());
				} else {
					ListUtils.insertAsc(fileLst, currentFile.getName());
				}
			}

            Drawable folderIcon = ContextCompat.getDrawable(getActivity(), R.drawable.fb_folder);

            Drawable fileIcon = (FileUtils.getInstance().getMode() == FileUtils.MODE_SDCARD) ? ContextCompat.getDrawable(getActivity(), R.drawable.fb_file) :
                                ContextCompat.getDrawable(getActivity(), R.drawable.fb_file_secure);

			for (String str : folderLst) {
				this.directoryEntries.add(new IconifiedText(str, folderIcon));
			}
			for (String str : fileLst) {
				this.directoryEntries.add(new IconifiedText(str, fileIcon));
			}
		}

		IconifiedTextListAdapter itla = new IconifiedTextListAdapter(
				getActivity());
		itla.setListItems(this.directoryEntries);
		setListAdapter(itla);
		setSelection(0);
	}

	/**
	 * updateButtonsAndTitle - redraw the toggle buttons, update the title and
	 * toggle the padlock
	 */
	private void updateButtonsAndTitle() {

		if (FileUtils.getInstance().getMode() == FileUtils.MODE_CONTAINER) {
			(mView.findViewById(R.id.fb_padlock))
					.setVisibility(View.VISIBLE);
		} else {
			(mView.findViewById(R.id.fb_padlock))
					.setVisibility(View.INVISIBLE);
		}

		// setTitle(mCurrentPath);
	}

	/**
	 * showNewDirUI - show some UI which gets a new directory name
	 */
	public void showNewDirUI() {
		newFolderAlert = createNewFolderDialog();
		newFolderAlert.show();

        Toast.makeText(getActivity().getApplicationContext(), "Hello World",
                Toast.LENGTH_LONG).show();

	}

	@NonNull
	private AlertDialog createNewFolderDialog() {
		AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
		alert.setTitle(R.string.DIR_NAME);
		final GDEditText input = new GDEditText(getActivity());
		alert.setView(input);
		alert.setPositiveButton(R.string.OK,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						String dirName = input.getText().toString();
						if (dirName.length() > 0) {
							FileUtils.getInstance().makeNewDir(mCurrentPath,
									dirName);
                            if (isAdded()) {
                                browseToPath(mCurrentPath);
                            }
						}
					}
				});
		alert.setNegativeButton("Cancel", null);
		return alert.create();
	}

	@Override
    public void fileReceived(String aFilePath) {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                //When we receive a file we refresh the current UI, incase the receipt of the file alters the UI (i.e adds new file or dir into current view)
                browseToPath(mCurrentPath);
            }
        };

        //We need to queue a Runnable to run on UI thread because it will update the UI
        getActivity().runOnUiThread(r);
    }
}
