/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2014 Good Technology Corporation. All rights reserved.
 */
package com.good.gd.example.securestore.utils;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.good.gd.example.securestore.FileViewer;
import com.good.gd.example.securestore.common_lib.utils.BaseFileUtils;

public class FileUtils extends BaseFileUtils {

    /*
        Singleton implementation
     */
    private static FileUtils sInstance;

    public static synchronized FileUtils getInstance() {
        if (sInstance == null) {
            sInstance = new FileUtils();
        }
        return sInstance;
    }

    private FileUtils() {

    }

    @Override
    public void openItem(Context ctx, String fullFilePath) {

        java.io.File file = (mCurrentMode == MODE_SDCARD) ? new java.io.File(fullFilePath) :
                (mCurrentMode == MODE_CONTAINER) ? new com.good.gd.file.File(fullFilePath) : null;
        if (fullFilePath.endsWith(".txt")) {
            if (file != null) {
                Intent i = new Intent();
                i.putExtra(FileViewer.FILE_VIEWER_PATH, fullFilePath);
                i.setClass(ctx, FileViewer.class);
                ctx.startActivity(i);
            }
        } else {
            Toast.makeText(ctx, "Only .txt files supported in sample", Toast.LENGTH_SHORT).show();
        }

    }
}
