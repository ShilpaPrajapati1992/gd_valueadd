/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2014 Good Technology Corporation. All rights reserved.
 */
package com.good.gd.example.securestore.common_lib;

/**
 * Listener App components implement to be notified of app state changes
 */
public interface AppGDStateControlListener {

    void onAppGDStateChanged(AppGDStateControl.State aNewState);

}
