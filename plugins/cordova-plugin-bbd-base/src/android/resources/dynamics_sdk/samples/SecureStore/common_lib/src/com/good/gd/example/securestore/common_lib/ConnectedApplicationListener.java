/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2014 Good Technology Corporation. All rights reserved.
 */
package com.good.gd.example.securestore.common_lib;

/**
 * Listener for App components wishing to be informed of changes to ConnectedApplication
 */
public interface ConnectedApplicationListener {

    void onConnectedApplicationStateChanged(ConnectedApplicationState aState);
}
