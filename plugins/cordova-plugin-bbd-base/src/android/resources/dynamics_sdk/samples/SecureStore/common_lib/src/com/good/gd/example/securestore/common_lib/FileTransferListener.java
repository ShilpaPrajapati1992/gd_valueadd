/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2014 Good Technology Corporation. All rights reserved.
 */
package com.good.gd.example.securestore.common_lib;

/**
Listener to receive when File is sent or received
 */
public interface FileTransferListener {

    void fileReceived(String aFilePath);

    void fileSentSuccess();

    void fileSentError(String aErrorMessage);

    void numberFilesReceived(int aNumberFiles);

}
