package com.good.gd.example.securestore;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.good.gd.example.securestore.common_lib.ConnectedApplicationControl;
import com.good.gd.example.securestore.common_lib.ConnectedApplicationListener;
import com.good.gd.example.securestore.common_lib.ConnectedApplicationState;

import static com.good.gd.example.securestore.common_lib.utils.AppLogUtils.DEBUG_LOG;

/**
 *
 */
public class ConnectedApplicationManagerFragment extends Fragment implements ConnectedApplicationListener {



    @Override
    public void onPause() {
        super.onPause();

        ConnectedApplicationControl.getInstance().removeConnectedAppStateListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        ConnectedApplicationControl.getInstance().addConnectedAppStateListener(this);


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.connected_application_management, container, false);

        Button b = (Button) v.findViewById(R.id.remove_button);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeApp();
            }
        });

        return v;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConnectedApplicationStateChanged(ConnectedApplicationState aState) {

    }

    private void removeApp(){

        ConnectedApplicationState state = ConnectedApplicationControl.getInstance().getCurrentState();

        String name = null;

        if(state.isAppConnected()) {

            name = state.getConnectedApps().iterator().next();

        } else if(state.isActivatedNotConnected()) {

            name = state.getActivatedNotConnectedApps().iterator().next();

        }

        DEBUG_LOG("ConnectedApplicationManagerFragment remove Device Name =" + name);

        if(name!=null) {
            ConnectedApplicationControl.getInstance().removeConnectedApplication(name);
        }

    }

}
