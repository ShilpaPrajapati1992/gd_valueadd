/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2014 Good Technology Corporation. All rights reserved.
 */
package com.good.gd.example.securestore;

import android.app.Application;

/**
 * SecureStore Application class. Started by the system as soon as process is created
 */
public class SecureStoreApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        AppStateManager.createInstance(getApplicationContext());

    }
}
