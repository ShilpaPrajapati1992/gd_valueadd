/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2016 Good Technology Corporation. All rights reserved.
 */
package com.good.automated_test_support;


import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;

import android.support.test.InstrumentationRegistry;


import com.good.gd.GDStateListener;
import com.good.gdwearable.GDAndroid;

/*
 Wearable SDK implementation of GDAutomatedTestSupportImplInterface

 Purpose is to provide handheld SDK specific functionality (where differs from Wearable SDK) such that
 all other Test code can be generic and function on either the Wearable or Handheld SDK

 Future additions would be anything where Wearable and Handheld implementations should differ
 */

public class GDAutomatedTestSupportBase implements GDAutomatedTestSupportImplInterface  {

    private static GDAutomatedTestSupportBase instance = null;

    public static synchronized GDAutomatedTestSupportBase getInstance() {
        if (instance == null) {
            instance = new GDAutomatedTestSupportBase();
        }
        return instance;
    }

    @Override
    public boolean provisionOrLoginGDApp() {

        if(GDAndroid.getInstance().isActivated(InstrumentationRegistry.getTargetContext())) {
            return GDAutomatedTestSupport.loginWearableGDApp();
        } else {
            return GDAutomatedTestSupport.provisionWearableGDApp();
        }
    }

    @Override
    public boolean provisionOrLoginGDApp(String packageName) {
        return false;
    }

    @Override
    public SharedPreferences getGDSharedPreferences(String aName, int aMode) {
        return GDAndroid.getInstance().getGDSharedPreferences(aName, aMode);
    }

    @Override
    public void registerGDStateReceiver(BroadcastReceiver receiver, IntentFilter filter) {
        // Todo: GDAndroid.getInstance().registerReceiver() is not implemented for the Wearable yet.
    }

    @Override
    public void unregisterGDStateReceiver(BroadcastReceiver receiver) {

    }

}