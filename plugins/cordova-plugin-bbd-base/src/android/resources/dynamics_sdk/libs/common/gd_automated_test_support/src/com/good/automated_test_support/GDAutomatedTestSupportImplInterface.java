/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2016 Good Technology Corporation. All rights reserved.
 */
package com.good.automated_test_support;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;


public interface GDAutomatedTestSupportImplInterface {

    public boolean provisionOrLoginGDApp();

    public boolean provisionOrLoginGDApp(String packageName);

    public SharedPreferences getGDSharedPreferences(String aName, int aMode);

    public void registerGDStateReceiver(BroadcastReceiver receiver, IntentFilter filter);

    public void unregisterGDStateReceiver(BroadcastReceiver receiver);

}