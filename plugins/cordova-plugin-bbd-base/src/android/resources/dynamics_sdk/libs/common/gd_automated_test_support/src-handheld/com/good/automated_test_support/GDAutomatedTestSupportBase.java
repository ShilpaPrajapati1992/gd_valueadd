/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2016 Good Technology Corporation. All rights reserved.
 */
package com.good.automated_test_support;


import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;

import android.support.test.InstrumentationRegistry;


import com.good.gd.GDAndroid;
import com.good.gd.GDStateListener;

/*
 Handheld SDK implementation of GDAutomatedTestSupportImplInterface

 Purpose is to provide handheld SDK specific functionality (where differs from Wearable SDK) such that
 all other Test code can be generic and function on either the Wearable or Handheld SDK

 Future additions would be anything where Wearable and Handheld implementations should differ
 */
public class GDAutomatedTestSupportBase implements GDAutomatedTestSupportImplInterface  {

    private static GDAutomatedTestSupportBase instance = null;

    public static synchronized GDAutomatedTestSupportBase getInstance() {
        if (instance == null) {
            instance = new GDAutomatedTestSupportBase();
        }
        return instance;
    }

    @Override
    public boolean provisionOrLoginGDApp() {
        return provisionOrLoginGDApp(GDAutomatedTestSupport.getAppPackageName());
    }

    @Override
    public boolean provisionOrLoginGDApp(String packageName) {

        //Ensure the Handheld login/provision helper methods are called because they at fundamentally different to the Wearable ones
        if(GDAndroid.getInstance().isActivated(InstrumentationRegistry.getTargetContext())) {
            return GDAutomatedTestSupport.loginHandheldGDApp(packageName);
        } else {
            return GDAutomatedTestSupport.provisionHandheldGDApp(packageName);
        }

    }

    @Override
    public SharedPreferences getGDSharedPreferences(String aName, int aMode) {
        // Shared Preferences are obtained through calling different GDAndroid instance (either com.good.gd.GDAndroid for Handheld or
        // com.good.gdwearable.GDAndroid for Wearable, so here we abstract that
        return GDAndroid.getInstance().getGDSharedPreferences(aName, aMode);
    }

    @Override
    public void registerGDStateReceiver(BroadcastReceiver receiver, IntentFilter filter) {
        GDAndroid.getInstance().registerReceiver(receiver, filter);
    }

    @Override
    public void unregisterGDStateReceiver(BroadcastReceiver receiver) {
        GDAndroid.getInstance().unregisterReceiver(receiver);
    }
}