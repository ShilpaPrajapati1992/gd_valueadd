/*
 *  This file contains Good Sample Code subject to the Good Dynamics SDK Terms and Conditions.
 *  (c) 2016 Good Technology Corporation. All rights reserved.
 */
package com.good.automated_test_support;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.test.InstrumentationRegistry;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;

import java.io.File;

public class GDAutomatedTestSupport {

    static final String TAG = "GD_TEST_BASE";

    //Default timeout value used in helper methods, time in milliseconds
    public static final int DEFAULT_TIMEOUT = 5000;

    // Default wait timeout (in milliseconds) after certain interactions to ensure state has transitioned
    public static final int DEFAULT_WAIT_TIMEOUT = 200;

    //Statically create correct implementation (which will either be Handheld or Wearable variant depending on which source code added)
    static GDAutomatedTestSupportImplInterface _Impl = GDAutomatedTestSupportBase.getInstance();

    /*
    Setup of AutomatedTestSupport, should be called before other methods
    */
    public static void setupGDAutomatedTestSupport() {

        GDTestSettings settings = GDTestSettings.getInstance();
        settings.initialize(InstrumentationRegistry.getContext(), getAppPackageName());
    }

    //Helper method which launches the app under test and waits for it to be on the screen
    public static void launchApp(String packageName) {

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        Context context = InstrumentationRegistry.getTargetContext();
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        device.wait(Until.hasObject(By.pkg(packageName)), DEFAULT_TIMEOUT);

        device.waitForIdle(DEFAULT_TIMEOUT);
        
        GDSDKStateReceiver.getInstance();
    }

    //Helper method which launches the app under test and waits for it to be on the screen
    //After launching the app, it also register a broadcast receiver to get GD SDK authorization
    //states notifications such as authorized/locked/wipped
    public static void launchAppUnderTest() {
        launchApp(getAppPackageName());
    }

    // Helper method which checks that GD has sent the  GD Authorized callback (which needs to be sent before app code can run)
    public static boolean checkGDAuthorized() {

        boolean ret = GDSDKStateReceiver.getInstance().checkAuthorized();

        if (ret) {
            //If already authorized return immediately
            Log.d(TAG, "checkGDAuthorized: already TRUE");
            return true;
        }

        // If we aren't already authorized we wait up to 10secs for auth change to occur (i.e. if we have just logged in or finished activation)
        // We are explicitly  waiting for 10seconds here, not the DEFAULT_TIMEOUT
        GDSDKStateReceiver.getInstance().waitForAuthorizedChange(10000);

        ret = GDSDKStateReceiver.getInstance().checkAuthorized();
        Log.d(TAG, "checkGDAuthorized: finished waiting result = " + ret);

        // This time we return value we receive
        return ret;

    }

    //Helper method which launches the app specific settings page within Settings app (which allows for items such as force stop and permission changes)
    public static void launchAppUnderTestSettings() {
		launchAppSettings(getAppPackageName());
    }
    
    //Helper method which launches the app specific settings page within Settings app (which allows for items such as force stop and permission changes)
    public static void launchAppSettings(String appPackageName) {

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        Context context = InstrumentationRegistry.getTargetContext();

        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + appPackageName));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

        context.startActivity(i);

        device.waitForIdle(DEFAULT_TIMEOUT);

    }

    //Helper method which presses HOME
    public static void pressHome() {

        // Simulate a short press on the HOME button to ensure we always start from a known State

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        device.pressHome();

        device.waitForIdle(DEFAULT_TIMEOUT);
    }

    //Helper method which waits for a certain time for device to become idle
    public static void waitForIdle(int aTimeout) {

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        device.waitForIdle(aTimeout);

    }

    //Helper method which wakes up device if needed
    public static void wakeUpDeviceIfNeeded() {

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        try {
            if (!device.isScreenOn()) {

                device.wakeUp();
            }

            device.pressKeyCode(82);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

    // Helper method which handles System Dialogues which may be showing and thus impact tests
    public static void acceptSystemDialogues() {

        acceptSystemWelcomeMessageIfShown();
        cancelSystemCrashDialogueIfShown();
    }

    //Helper method which accepts and dismisses System Welcome message (can be at first boot of emulator or device)
    public static void acceptSystemWelcomeMessageIfShown() {

        // We don't wait for long duration, if it is showing then deal with it, otherwise move on
        if (isTextShown("Welcome", 100)) {

            clickOnItemContainingText("GOT IT");
        }

    }

    //Helper method which cancels a system error message if shown (can be at start of emulator boot if part of system has crashed)
    public static void cancelSystemCrashDialogueIfShown() {

        // We don't wait for long duration, if it is showing then deal with it, otherwise move on
        if (isTextShown("has stopped", 100)) {

            clickOnItemContainingText("OK");
        }

    }

    //Helper method to determine if a screen (Either GD screen or App UI screen) containing specified ResourceID is shown (with default timeout and default current app)
    public static boolean isScreenShown(String aResourceID) {

        return isScreenShown(getAppPackageName(), aResourceID, DEFAULT_TIMEOUT);

    }

    //Helper method to determine if a screen in the provided app (Either GD screen or App UI screen) containing specified ResourceID is shown (with default timeout)
    public static boolean isScreenShown(String packageName, String aResourceID) {
        return isScreenShown(packageName, aResourceID, DEFAULT_TIMEOUT);
    }

    //Helper method to determine if a screen in the provided app (Either GD screen or App UI screen) containing specified ResourceID is shown waiting a certain timeout
    public static boolean isScreenShown(String packageName, String aResourceID, int aTimeMilliseconds) {

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        UiObject testScreen = null;

        String resourceID = packageName + ":id/" +  aResourceID;
        testScreen = device.findObject(new UiSelector().resourceId(resourceID));

        testScreen.waitForExists(aTimeMilliseconds);

        if (testScreen.exists()) {
            return true;
        }

        return false;

    }

    //Helper method to click on specific item, specified by resourceID, with a default timeout
    public static boolean clickOnItem(String aResourceID) {

        return clickOnItem(getAppPackageName(), aResourceID, DEFAULT_TIMEOUT, DEFAULT_WAIT_TIMEOUT);
    }

    //Helper method to click on specific item, specified by package name and resourceID, with a default timeout
    public static boolean clickOnItem(String packageName, String aResourceID) {
        return clickOnItem(packageName, aResourceID, DEFAULT_TIMEOUT, DEFAULT_WAIT_TIMEOUT);
    }

    //Helper method to click on specific item, specified by resourceID, with specified timeouts
    public static boolean clickOnItem(String aResourceID, int aTimeMSWaitExists, int aTimeMSWaitGone) {

        return clickOnItem(getAppPackageName(), aResourceID, aTimeMSWaitExists, aTimeMSWaitGone);
    }

    //Helper method to click on specific item, specified by resource ID
    public static boolean clickOnItem(String packageName, String aResourceID, int aTimeMSWaitExists, int aTimeMSWaitGone) {

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        try {

            UiObject testItem = null;

            String resourceID = packageName + ":id/" + aResourceID;
            testItem = device.findObject(new UiSelector().resourceId(resourceID));

            testItem.waitForExists(aTimeMSWaitExists);

            if (testItem.exists()) {
                testItem.click();

                if(aTimeMSWaitGone>0) {
                    //Small wait after clicking on item (ensures state has changed correctly)
                    testItem.waitUntilGone(aTimeMSWaitGone);
                }
                return true;
            }
        } catch (UiObjectNotFoundException e) {
            return false;
        }

        return true;

    }

    //Helper method to click on specific item with default timeout
    public static boolean clickOnItemContainingText(String aText) {

        return clickOnItemContainingText(aText, DEFAULT_TIMEOUT, DEFAULT_WAIT_TIMEOUT);
    }

    //Helper method to click on specific item with configurable timeout
    public static boolean clickOnItemContainingText(String aText, int aTimeMSWaitExists) {

        return clickOnItemContainingText(aText, aTimeMSWaitExists, DEFAULT_WAIT_TIMEOUT);
    }


    //Helper method to click on specific item
    public static boolean clickOnItemContainingText(String aText, int aTimeMSWaitExists, int aTimeMSWaitGone) {

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        try {

            UiObject objectContainsText = null;

            objectContainsText = device.findObject(new UiSelector().text(aText));

            objectContainsText.waitForExists(aTimeMSWaitExists);

            if (objectContainsText.exists()) {
                objectContainsText.click();

                if(aTimeMSWaitGone>0) {
                    objectContainsText.waitUntilGone(aTimeMSWaitGone);
                }
                return true;
            }


        } catch (UiObjectNotFoundException e) {
            return false;
        }
        return false;
    }

    // Helper method to toggle a permission switch in App Settings
    // View Hierarchy  can be determined in App Settings by using the UIAutomatorViewer tool in SDK/tools DIR
    public static boolean selectPermissionSwitchItemWithDescription(String aDescription) {

        UiScrollable permissionList = new UiScrollable(new UiSelector().className(android.support.v7.widget.RecyclerView.class));
        permissionList.waitForExists(DEFAULT_TIMEOUT);
        UiObject btItem = null;

        try {

            btItem = permissionList.getChildByText(new UiSelector().className("android.widget.RelativeLayout"), aDescription);

            btItem.click();

        } catch (UiObjectNotFoundException e) {
            return false;
        }

        return true;
    }

    //Helper method to enter text into screen element which has certain description with default timeout
    public static boolean clickOnItemContainingDescription(String aDescription) {

        return clickOnItemContainingDescription(aDescription, DEFAULT_TIMEOUT);
    }

    //Helper method to enter text into screen element which has certain description
    public static boolean clickOnItemContainingDescription(String aDescription, int aTimeMilliseconds) {

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        UiObject testScreen = null;

        testScreen = device.findObject(new UiSelector().descriptionContains(aDescription));

        testScreen.waitForExists(aTimeMilliseconds);

        if (testScreen.exists()) {

            try {
                testScreen.click();
            } catch (UiObjectNotFoundException e) {
                return false;
            }
            return true;
        }

        return false;

    }

    // Scroll to a UiObject that has matches exact text within a scrollable UiObject
    public static boolean scrollToText(String scrollableContainerId, String aText, int aTimeMilliseconds) {
        String scrollableResId = getAppPackageName() + ":id/" + scrollableContainerId;
        UiSelector scrollableSelector = new UiSelector().resourceId(scrollableResId);
        UiScrollable scrollable = new UiScrollable(scrollableSelector);
        try {
            UiSelector itemWithTextSelector = new UiSelector().text(aText);
            UiObject item = scrollable.getChildByText(itemWithTextSelector, aText, true);
            item.waitForExists(aTimeMilliseconds);
            if (item.exists()) {
                return true;
            }
        } catch (UiObjectNotFoundException e) {
            return false;
        }
        return false;
    }

    //Helper method to determine if specific text is shown on screen with default timeout
    public static boolean isTextShown(String aText) {

        return isTextShown(aText, DEFAULT_TIMEOUT);
    }


    //Helper method to determine if specific text is shown on screen
    public static boolean isTextShown(String aText, int aTimeMilliseconds) {

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        UiObject textObject = null;

        textObject = device.findObject(new UiSelector().textContains(aText));

        textObject.waitForExists(aTimeMilliseconds);

        if (textObject.exists()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean enterEmailAccessKey() {
        return enterEmailAccessKey(getAppPackageName());
    }

    // Helper method to set the provided user name and password
    public static boolean enterEmailAccessKey(String packageName) {

        clickOnItem(packageName, "COM_GOOD_GD_EPROV_EMAIL_FIELD");

        enterTextScreenWithResourceId(packageName, "COM_GOOD_GD_EPROV_EMAIL_FIELD", GDTestSettings.getInstance().getAppProvisionEmail(packageName));

        waitForIdle(200);

        String accessKey = GDTestSettings.getInstance().getAppProvisionAccessKey(packageName);

        clickOnItem(packageName, "editTextForPin1");

        enterTextScreenWithResourceId(packageName, "editTextForPin1", accessKey.substring(0, 5));

        clickOnItem(packageName, "editTextForPin2");

        enterTextScreenWithResourceId(packageName, "editTextForPin2", accessKey.substring(5, 10));

        clickOnItem(packageName, "editTextForPin3");

        enterTextScreenWithResourceId(packageName, "editTextForPin3", accessKey.substring(10));

        waitForIdle(200);

        return clickOnItem(packageName, "COM_GOOD_GD_EPROV_ACCESS_BUTTON");

    }

    public static boolean setPassword() {
        return setPassword(getAppPackageName());
    }

    // Helper method to set GD container password
    public static boolean setPassword(String packageName) {

        clickOnItem(packageName, "COM_GOOD_GD_EPROV_SET_PWD_DLG_NEW_PWD_EDIT");

        enterTextScreenWithResourceId(packageName, "COM_GOOD_GD_EPROV_SET_PWD_DLG_NEW_PWD_EDIT", GDTestSettings.getInstance().getAppProvisionPassword(packageName));

        waitForIdle(200); // Time to allow soft keyboard to close

        clickOnItem(packageName, "COM_GOOD_GD_EPROV_SET_PWD_DLG_CONFIRM_PWD_EDIT");

        enterTextScreenWithResourceId(packageName, "COM_GOOD_GD_EPROV_SET_PWD_DLG_CONFIRM_PWD_EDIT", GDTestSettings.getInstance().getAppProvisionPassword(packageName));

        waitForIdle(200);

        UiDevice.getInstance(InstrumentationRegistry.getInstrumentation()).pressEnter(); // Jump to accept button, in case it is off screen

        return clickOnItem(packageName, "COM_GOOD_GD_EPROV_ACCESS_BUTTON");
    }

    //Helper method to check if screen element with certain description is on screen
    public static boolean isScreenShownWithDescription(String aDescription) {

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        UiObject testScreen = null;

        testScreen = device.findObject(new UiSelector().descriptionContains(aDescription));

        testScreen.waitForExists(DEFAULT_TIMEOUT);

        if (testScreen.exists()) {
            return true;
        }

        return false;

    }

    //Helper method to enter text into screen element which has certain description
    public static boolean enterTextScreenWithResourceId(String packageName, String aResourceID, String aText) {
        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        try {

            UiObject testItem = null;

            String resourceID = packageName + ":id/" + aResourceID;
            testItem = device.findObject(new UiSelector().resourceId(resourceID));

            testItem.waitForExists(DEFAULT_TIMEOUT);

            if(testItem.exists()) {
                testItem.click();
                testItem.setText(aText);
                //Small wait after inputting text (ensures state has changed correctly)
                testItem.waitUntilGone(DEFAULT_WAIT_TIMEOUT);
            }



        } catch (UiObjectNotFoundException e) {

            return false;
        }

        return true;
    }

    //Helper method to enter text into screen element which has certain description
    public static boolean enterTextScreenWithDescription(String aDescription, String aText) {

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        UiObject testScreen = null;

        testScreen = device.findObject(new UiSelector().descriptionContains(aDescription));

        testScreen.waitForExists(DEFAULT_TIMEOUT);

        if (testScreen.exists()) {

            try {
                testScreen.click();
                testScreen.setText(aText);
            } catch (UiObjectNotFoundException e) {
                return false;
            }
            return true;
        }

        return false;

    }



    //Helper method to determine if checkbox/element checked
    public static boolean isElementChecked(String packageName, String aResourceID) {
        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        boolean isChecked = false;
        try {
            String resourceID = packageName + ":id/" + aResourceID;
            isChecked = device.findObject(new UiSelector().resourceId(resourceID)).isChecked();

        } catch (UiObjectNotFoundException e) {

            return false;
        }

        return isChecked;

    }

    //helper method to determine child count for element. Can be used for counting elements in list
    public static int listViewSize(String packageName, String aResourceID) {
        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        String resourceID = packageName + ":id/" + aResourceID;
        int childCount = 0;

        try {
            childCount = device.findObject(new UiSelector().resourceId(resourceID)).getChildCount();
        } catch (UiObjectNotFoundException e) {
            e.printStackTrace();
        }
        return childCount;
    }

    //Helper method to enter text into screen element which belongs to a certain class
    public static boolean enterTextScreenWithClass(String aClass, String aText) {

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        UiObject textScreen = null;
        textScreen = device.findObject(new UiSelector().className(aClass));
        textScreen.waitForExists(DEFAULT_TIMEOUT);

        if (textScreen.exists()) {

            try {
                textScreen.click();
                textScreen.setText(aText);
            } catch (UiObjectNotFoundException e) {
                return false;
            }
            return true;
        }
        return false;
    }

    // If target API level 23 permissions screen shown first and permissions are granted
    // (if not already granted)
    public static boolean grantPermissions(String packageName) {
        Log.d(TAG, "Grant Permissions: begin");
        // If running on API level 23 (or above) device and targeting API level 23 or above device then Permissions screen will be the first seen
        if (getAppTargetAPILevel() >= Build.VERSION_CODES.M && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isScreenShown(packageName, "gd_runtimepermissions_body")) {
                // If the Permissions screen does appear we want to allow the permission
                Log.d(TAG, "Grant Permissions: permissions screen appears");

                if (!clickOnItem(packageName, "gd_ok_button2")) {
                    Log.d(TAG, "Grant Permissions: end fail can't request permission");
                    return false;
                }

                if (!clickOnItem("com.android.packageinstaller", "permission_allow_button")) {
                    Log.d(TAG, "Grant Permissions: end fail can't accept permission");
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean loginHandheldGDApp() {
        return loginHandheldGDApp(getAppPackageName());
    }

    // Helper method to login to GD App
    public static boolean loginHandheldGDApp(String packageName) {

        if (isScreenShown(packageName, "gd_accept_button")) {
            //Disclaimer screen is shown
            clickOnItem(packageName, "gd_accept_button");
        }

        if (isScreenShownWithDescription("Enter Password")) {

            //This works as long as password screen is part of app under test (if it is auth delegation it won't be so will need to use the description method)

            enterTextScreenWithResourceId(packageName, "COM_GOOD_GD_LOGIN_VIEW_PASSWORD_FIELD", GDTestSettings.getInstance().getAppProvisionPassword(packageName));

            if (clickOnItem(packageName, "COM_GOOD_GD_EPROV_ACCESS_BUTTON")) {
                return true;
            }

        }

        return false;

    }

    //Helper method to login to Wearable GD App
    public static boolean loginWearableGDApp() {

        //Currently does not need to do anything
        return true;
    }

    /*
    GD Provisioning screens -

    - If target API level 23 permissions screen shown first (if not already granted)

    - If not activated will show Splash screen then Easy Activation selector screen
        - Then need to select Manual Activation and Enter Email & Access Key
        - Then need to go through Activation process and enter password

    - If Activated will need to Enter Password

     */
    public static boolean provisionHandheldGDApp() {
        return provisionHandheldGDApp(getAppPackageName());
    }

    public static boolean provisionHandheldGDApp(String packageName) {

        // When starting unprovisioned GD App will check NOC for Easy Activation options, if present will show screen prompting one can be installed
        // Otherwise will directly show the enter email and access key screen

        boolean success = false;

        Log.d(TAG, "Provision GD App: begin");

        if(!grantPermissions(packageName)) {
            Log.d(TAG, "Provision GD App: end fail can't request permission");
            return false;
        }

        // We first check if email address & access key screen is shown
        if (isScreenShown(packageName, "COM_GOOD_GD_EPROV_EMAIL_FIELD")) {
            Log.d(TAG, "Provision GD App: 1 Direct to Email address & Access Key");
            success = enterEmailAccessKey(packageName);
        } else if (isScreenShown(packageName, "activationKeyLink", 45000)) {
            //We wait 45secs because are talking to NOC to get Easy Activation options at this point

            Log.d(TAG, "Provision GD App: EA activation screen shown");

            success = clickOnItem(packageName, "activationKeyLink");

            if (success) {
                Log.d(TAG, "Provision GD App: Email & Access Key screen");
                success = enterEmailAccessKey(packageName);
            }
        } else if (isScreenShown(packageName, "COM_GOOD_GD_EPROV_EMAIL_FIELD")) {
            // Access key screen will be shown if there are no Easy Activation Delegates
            Log.d(TAG, "Provision GD App: 2 Direct to Email address & Access Key");
            success = enterEmailAccessKey(packageName);
        }

        if (!success) {
            Log.d(TAG, "Provision GD App: end fail 1");
            return false;
        }

        if (!isScreenShown(packageName, "COM_GOOD_GD_PROGRESS_SCROLL_VIEW")) {
            Log.d(TAG, "Provision GD App: end fail 2");
            return false;
        }

        Log.d(TAG, "Provision GD App: GD Provisioning ongoing");

        if (isScreenShown(packageName, "COM_GOOD_GD_EPROV_SET_PWD_DLG_NEW_PWD_EDIT", 180000)) {
            // We wait 180secs because are talking to NOC/GC to provision
            Log.d(TAG, "Provision GD App: Set Password Screen");
            success = setPassword(packageName);

        } else if (isScreenShown(packageName, "gd_accept_button")) {
            //Disclaimer screen is shown
            success = clickOnItem(packageName, "gd_accept_button");

            if (success) {
                success = setPassword(packageName);
            }

        } else {
            //Here If Auth Delegating we enter the Password of the app are auth delegating too
            Log.d(TAG, "check Provision GD App: Auth Del Screen");
            success = loginHandheldGDApp(packageName);
            Log.d(TAG, "check Provision GD App: Auth Del Screen result =" + success);
        }


        Log.d(TAG, "Provision GD App: end status = " + success);
        return success;
    }

    // Helper method for GD Wearable App provisioning
    public static boolean provisionWearableGDApp() {

        boolean success = false;

        // Initial screen should prompt user to start activation
        if (isScreenShown("gd_button_start_activation")) {

            success = clickOnItem("gd_button_start_activation");

            // After clicking on start activation button Activation should start, wait 30seconds for user validation
            if (success && isScreenShown(getAppPackageName(), "button1", 30000)) {

                success = clickOnItem("button1");

                if (success && isScreenShown(getAppPackageName(), "gd_activation_complete", 30000)) {
                    success = true;
                } else {
                    success = false;
                }

            }

        }

        return success;
    }

    public static boolean loginOrProvisionGDApp() {
        return loginOrProvisionGDApp(getAppPackageName());
    }

    // Helper method to provide a high level either login or provision. Will defer to implementation which handles Handheld/Wearable split
    public static boolean loginOrProvisionGDApp(String packageName) {

        if (GDAutomatedTestSupport.checkGDAuthorized()) {
            return true;
        } else {
            return _Impl.provisionOrLoginGDApp(packageName);
        }
    }

    // Helper method to capture screenshot (requires app to have the write external storage permission)
    public static boolean captureScreenshot(String filename) {

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        File dir = new File(Environment.getExternalStorageDirectory(), stack[3].getClassName() + File.separator + stack[3].getMethodName());

        boolean res = dir.exists();

        if (!res)
            res = dir.mkdirs();

        if (!res)
            return res;

        File file = new File(dir, filename);
        return device.takeScreenshot(file);

    }

    // Helper method to return GD Shared Preferences
    public static SharedPreferences getGDSharedPreferences(String aName, int aMode) {
        return _Impl.getGDSharedPreferences(aName, aMode);
    }

    public static void registerGDStateReceiver(BroadcastReceiver receiver, IntentFilter filter) {
        _Impl.registerGDStateReceiver(receiver, filter);
    }

    public static void unregisterGDStateReceiver(BroadcastReceiver receiver) {
        _Impl.unregisterGDStateReceiver(receiver);
    }

    //Helper method to get App Target API level
    public static int getAppTargetAPILevel() {
        return InstrumentationRegistry.getTargetContext().getApplicationInfo().targetSdkVersion;
    }

    //Helper method to get Resource Identifier (int) given the String name for the identifier
    public static int getResourceID(String aResourceIDName) {
        return getResourceID(getAppPackageName(), aResourceIDName);
    }

    public static int getResourceID(String packageName, String aResourceIDName) {
        return InstrumentationRegistry.getTargetContext().getResources().getIdentifier(aResourceIDName, "id", packageName);
    }

    //Helper method to get app under test Package Name
    public static String getAppPackageName() {
        return InstrumentationRegistry.getTargetContext().getPackageName();
    }
}