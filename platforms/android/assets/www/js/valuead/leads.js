var _leads = {

    preInit: function () {
        if (this.ui !== undefined)
            this.ui.leadsList.empty();

        this.setSelectedConfiguration();
        _leads.getCampaignSalesPeople();
    },

    setSelectedConfiguration: function () {
        $.each(gen.CampaignConfiguration.Campaigns.Campaign, function (i, item) {
            if (item.Id == gen.selectedCampaign.id)
                gen.selectedCampaignConfiguration = item;
        });

        //• CLONE DEFAULTS
        gen.SelectedConfiguration = jQuery.extend(true, {}, gen.DefaultConfiguration.Default);

        if (gen.DefaultConfiguration.Overrides) {
            $.each(gen.DefaultConfiguration.Overrides, function (overrideItems, overrideItem) {
                if (gen.selectedCampaign.id == parseInt(overrideItem.Campaign)) {
                    if (overrideItem.LeadList) {
                        for (var counter = 1; counter <= 10; counter++) {
                            var position = "Position" + counter;

                            if (overrideItem.LeadList[position]) {
                                gen.SelectedConfiguration.LeadList[position] = overrideItem.LeadList[position];
                                _common.setDefaultPositionProperties(gen.SelectedConfiguration.LeadList, position);
                            }
                        }
                    }

                    if (overrideItem.Sorting) {
                        $.each(overrideItem.Feedback, function (sortingOverrides, sortingOverride) {
                            gen.SelectedConfiguration.Sorting[sortingOverrides] = sortingOverride;
                        });
                    }

                    if (overrideItem.LeadInformation) {
                        if (overrideItem.LeadInformation.HideUnlisted)
                            gen.SelectedConfiguration.LeadInformation.HideUnlisted = overrideItem.LeadInformation.HideUnlisted;

                        if (overrideItem.LeadInformation.DisplayId)
                            gen.SelectedConfiguration.LeadInformation.DisplayId = overrideItem.LeadInformation.DisplayId;

                        $.each(overrideItem.LeadInformation, function (leadInformationOverrides, leadInformationOverride) {
                            gen.SelectedConfiguration.LeadInformation[leadInformationOverrides] = leadInformationOverride;
                        });
                    }

                    if (overrideItem.Feedback) {
                        $.each(overrideItem.Feedback, function (feedbackOverrides, feedbackOverride) {
                            gen.SelectedConfiguration.Feedback[feedbackOverrides] = feedbackOverride;
                        });
                    }

                    if (overrideItem.DialPage) {
                        $.each(overrideItem.DialPage, function (feedbackOverrides, feedbackOverride) {
                            gen.SelectedConfiguration.DialPage[feedbackOverrides] = feedbackOverride;
                        });
                    }

                    if (overrideItem.Translation) {
                        $.each(overrideItem.Translation, function (translationOverrides, translationOverrides) {
                            gen.SelectedConfiguration.Translation[translationOverrides] = translationOverrides;
                        });
                    }
                }
            })
        }
        _leads.setSortOptions();
        _leads.setFilterOptions();
    },

    setSearchOptions: function () {
        this.ui.searchSelect.selectmenu();
        this.ui.searchSelect.empty();
        //this.ui.searchSelect.append('<option value="standard" data-placeholder="true" data-localization-key="sortBy">' + _common.getLocalization("search", "Search") + '</option>');
        for (var counter = 1; counter <= 10; counter++) {
            var position = "Position" + counter;
            if (gen.SelectedConfiguration && gen.SelectedConfiguration.LeadList && gen.SelectedConfiguration.LeadList[position]) {
                if (gen.SelectedConfiguration.LeadList[position].Visible && gen.SelectedConfiguration.LeadList[position].Visible == "true") {
                    if (gen.SelectedConfiguration.LeadList[position].Property && gen.SelectedConfiguration.LeadList[position].Property != "") {
                        if (gen.SelectedConfiguration.LeadList[position].Property != "leadStatusDescription" && gen.SelectedConfiguration.LeadList[position].Property.indexOf("Date") == -1) {
                            var localizedCaption = _common.getLocalization(gen.SelectedConfiguration.LeadList[position].LocalizationKey, gen.SelectedConfiguration.LeadList[position].Caption);
                            this.ui.searchSelect.append('<option value="' + gen.SelectedConfiguration.LeadList[position].Property + '">' + localizedCaption + '</option>');
                        }
                    }
                }
            }
        }

        this.ui.searchSelect.selectmenu("refresh");
    },

    onSearchCancelButtonClick: function () {
        this.ui.emptyWildcard.hide();
        $("#searchPopup-popup").hide();
    },

    onSearchClearButtonClick: function () {
        this.ui.emptyWildcard.hide();
        $("#searchPopup-popup").hide();
        this.ui.searched = false;
        this.ui.searchValueInput.val("");
        _leads.onRefreshClick();
    },

    onSearchButtonClick: function () {
        if (this.ui.searched == true)
            this.ui.searchClearButton.show();
        else
            this.ui.searchClearButton.hide();
    },

    onSearchExecuteButtonClick: function () {
        if (this.ui.searchValueInput.val() === "") {
            this.ui.emptyWildcard.show();
            return;
        } else {
            this.ui.emptyWildcard.hide();
            $("#searchPopup-popup").hide();
            this.ui.searched = true;
            _leads.onRefreshClick(this.ui.searchSelect.val() + "==*" + this.ui.searchValueInput.val() + "*");
        }
    },

    setSortOptions: function () {
        if (gen.SelectedConfiguration && gen.SelectedConfiguration.Sorting && gen.SelectedConfiguration.Sorting.SortField.length > 0) {
            $("#sortOrderSelectContainer").show();
            $("#sortOrderSelectContainer").empty();
            var $sortOrderSelect = $('<select id="sortOrderSelect" data-native-menu="false"></select>');
            $sortOrderSelect.append('<option value="standard" data-placeholder="true" data-localization-key="sortBy">' + _common.getLocalization("sortBy", "Sort By") + '</option>');
            $("#sortOrderSelectContainer").append($sortOrderSelect);

            $.each(gen.SelectedConfiguration.Sorting.SortField, function (index, value) {
                $sortOrderSelect.append('<option data-localization-key="' + value.DataField + '" value="' + value.DataField + '">' + _common.getLocalization(value.DataField, value.Caption) + ' ' + _common.getLocalization("ascending", "Ascending") + '</option>');
                $sortOrderSelect.append('<option data-localization-key="' + value.DataField + '" value="' + value.DataField + '">' + _common.getLocalization(value.DataField, value.Caption) + ' ' + _common.getLocalization("descending", "Descending") + '</option>');
            })
            $sortOrderSelect.bind("change", _leads.sortLeads);
            $sortOrderSelect.selectmenu().selectmenu("refresh");
        } else
            $("#sortOrderSelectContainer").hide();
    },

    sortLeads: function (event) {
        _leads.sortLeadsHandler(false);
    },

    sortLeadsHandler: function (loadLast) {
        var sortField = "";
        var sortOrder = "asc";

        if (loadLast) {
            sortField = localStorage.getItem("lastLeadsListSort", sortField) || "";
            sortOrder = localStorage.getItem("lastLeadsListSortOrder", sortOrder) || "";
        } else {
            $("#sortOrderSelect option:selected").each(function () {
                sortField = this.value;

                if (_common.endsWith(this.text, _common.getLocalization("descending", "Descending")))
                    sortOrder = "desc";
            });

            localStorage.setItem("lastLeadsListSort", sortField);
            localStorage.setItem("lastLeadsListSortOrder", sortOrder);
        }

        if (sortField.length > 0) {
            _leads.leads.sort(function (a, b) {
                if (a[sortField] && b[sortField]) {
                    if (sortOrder == "asc") {
                        if (typeof a[sortField] === 'number' && typeof b[sortField] === 'number')
                            return a[sortField] - b[sortField];
                        else
                            return a[sortField].localeCompare(b[sortField]);
                    } else {
                        if (typeof a[sortField] === 'number' && typeof b[sortField] === 'number')
                            return b[sortField] - a[sortField];
                        else
                            return b[sortField].localeCompare(a[sortField]);
                    }
                } else
                    return false;
            });

            _leads.ui.leadsList.empty();
            _leads.buildLeadsList(null, true);
        }
    },

    setFilterOptions: function () {
        if (gen.SelectedConfiguration.LeadStatus && gen.SelectedConfiguration.LeadStatus.Status.length > 0) {
            $("#filterSelectContainer").show();
            $("#filterSelectContainer").empty();
            var $filterSelect = $('<select id="filterSelect" data-native-menu="false"></select>');
            $filterSelect.append('<option value="standard" data-placeholder="true" data-localization-key="filterBy">' + _common.getLocalization("filterBy", "Filtered By") + '</option>');
            $filterSelect.append('<option value="standard" data-placeholder="false" data-localization-key="filterClear">' + _common.getLocalization("filterClear", "Clear Filter") + '</option>');
            $("#filterSelectContainer").append($filterSelect);

            $.each(gen.SelectedConfiguration.LeadStatus.Status, function (index, value) {
                if (value.Filter && value.Filter == "true") {
                    if (value.Query && value.Query != "") {
                        var removeFutureDatedLeads = '';
                        if (value.RemoveFutureDatedLeads)
                            removeFutureDatedLeads = ' data-remove-future-dated-leads="' + value.RemoveFutureDatedLeads + '" ';

                        $filterSelect.append('<option data-localization-key="' + value.LocalizationKey + '" value="' + value.Query + '"' + removeFutureDatedLeads + '>' + _common.getLocalization(value.LocalizationKey, value.Caption) + '</option>');
                    } else
                        $filterSelect.append('<option data-localization-key="' + value.LocalizationKey + '" value="leadStatus==' + value.Key + '">' + _common.getLocalization(value.LocalizationKey, value.Caption) + '</option>');
                }
            })
            $filterSelect.bind("change", _leads.filterLeads);
            $filterSelect.selectmenu().selectmenu("refresh");
        } else
            $("#filterSelectContainer").hide();
    },

    filterLeads: function (evt) {
        _leads.filterLeadsHandler(false);
    },

    filterLeadsHandler: function (loadLast) {
        if (loadLast) {
            var filterFields = localStorage.getItem("lastLeadsListFilter", filterFields) || "";
            if (filterFields.length > 0) {
                var filterSplit = filterFields.split("|");
                for (var counter = 0; counter < filterSplit.length; counter++) {
                    var keyOrQuery = filterSplit[counter];
                    var index = _leads.getLeadStatusKeyOrQueryIndex(keyOrQuery);
                    if (index >= 0) {
                        gen.REMOVE_FUTURE_DATED_LEADS = gen.SelectedConfiguration.LeadStatus.Status[index].RemoveFutureDatedLeads;
                        $("#filterSelect option").eq(index + 1).prop("selected", true);
                        $("#filterSelect").selectmenu().selectmenu("refresh");
                    }
                }

                filterFields = filterFields.replace(/(\||,)/g, "")
            }
        } else {
            var filterFields = "";
            var filterFieldsSave = "";
            gen.REMOVE_FUTURE_DATED_LEADS = null;
            $("#filterSelect option:selected").each(function () {
                if (filterFields != "")
                    filterFields = filterFields + "";
                filterFields = filterFields + this.value;

                if (filterFieldsSave != "")
                    filterFieldsSave = filterFieldsSave + "|";
                filterFieldsSave = filterFieldsSave + this.value;

                if (this.attributes["data-remove-future-dated-leads"])
                    gen.REMOVE_FUTURE_DATED_LEADS = this.attributes["data-remove-future-dated-leads"].value;
            });

            localStorage.setItem("lastLeadsListFilter", filterFieldsSave);
        }
        _leads.onRefreshClick(filterFields);
    },

    init: function () {
        this.ui = {};
        this.ui.refreshButton = $("#leadsRefreshButton");
        this.ui.logoutButton = $("#leadsSignOutButton");
        this.ui.leadsList = $("#leadsList");
        this.ui.leadsBackButton = $("#leadsBackButton");
        this.ui.leadsCaptureButton = $("#leadsCaptureButton");
        this.ui.leadsCaptureButton.hide();
        this.ui.leadsBackButton.off().on("click", $.proxy(this.onBackClick, this));
        this.ui.leadsSignOutButton = $("#leadsSignOutButton");
        this.ui.leadsSignOutButton.off().on("click", $.proxy(this.onSignOutClick, this));
        this.ui.refreshButton.off().on("click", $.proxy(this.filterLeads, this));
        this.warnList = ["FOLLOW_UP", "ACTIVE_MATCH", "MISSED_DEADLINE", "PASSED_ON"];
        this.ui.leadsCaptureButton.off().on("click", $.proxy(this.onCaptureClick, this));

        this.ui.nextPageButton = $("#nextButton");
        this.ui.nextPageButton.off().on("click", $.proxy(this.onNextClick, this));
        this.ui.nextPageButton.hide();

        this.ui.leadsListFooter = $("#leadsListFooter");
        this.ui.footerHandle = $("#footerHandle");
        this.ui.footerHandleVisible = true;
        this.ui.footerHandle.off().on("click", $.proxy(this.onFooterHandleClick, this));
        this.onFooterHandleClick();

        this.ui.searchButton = $("#searchButton");
        this.ui.searchButton.off().on("click", $.proxy(this.onSearchButtonClick, this));
        this.ui.searchSelect = $("#searchSelect");
        this.ui.searchValueInput = $("#searchValueInput");
        this.ui.searchExecuteButton = $("#searchExecuteButton");
        this.ui.searchExecuteButton.off().on("click", $.proxy(this.onSearchExecuteButtonClick, this));
        this.ui.searchCancelButton = $("#searchCancelButton");
        this.ui.searchCancelButton.off().on("click", $.proxy(this.onSearchCancelButtonClick, this));
        this.ui.searchClearButton = $("#searchClearButton");
        this.ui.searchClearButton.off().on("click", $.proxy(this.onSearchClearButtonClick, this));
        this.ui.searched = false;
        this.ui.emptyWildcard = $("#emptyWildcard");
        this.ui.emptyWildcard.hide();

        var languageSelectContainer = $("#languageSelectContainerLeads");

        if (gen.common.EnableGlobalization) {
            languageSelectContainer.show();
        } else {
            languageSelectContainer.hide();
        }

        _leads.setSearchOptions();
        _common.displayLanguage();

        setTimeout(function () {
            _leads.filterLeadsHandler(true);
        }, 250);

        setTimeout(function () {
            _leads.buttonVisiblityHandler();
        }, 500);
    },

    buttonVisiblityHandler: function () {
        if (gen.SelectedConfiguration.LeadListButtons) {
            if (gen.SelectedConfiguration.LeadListButtons.Search && gen.SelectedConfiguration.LeadListButtons.Search.Visible) {
                if (gen.SelectedConfiguration.LeadListButtons.Search.Visible == "false")
                    $("#searchContainer").css('visibility', 'hidden');
                else
                    $("#searchContainer").css('visibility', 'visible');
            }

            if (gen.SelectedConfiguration.LeadListButtons.Filter && gen.SelectedConfiguration.LeadListButtons.Filter.Visible) {
                if (gen.SelectedConfiguration.LeadListButtons.Filter.Visible == "false")
                    $("#filterSelectContainer").css('visibility', 'hidden');
                else
                    $("#filterSelectContainer").css('visibility', 'visible');
            }

            if (gen.SelectedConfiguration.LeadListButtons.Sort && gen.SelectedConfiguration.LeadListButtons.Sort.Visible) {
                if (gen.SelectedConfiguration.LeadListButtons.Sort.Visible == "false")
                    $("#sortOrderSelectContainer").css('visibility', 'hidden');
                else
                    $("#sortOrderSelectContainer").css('visibility', 'visible');
            }
        }
    },

    onFooterHandleClick: function () {
        if (this.ui.footerHandleVisible) {
            this.ui.leadsListFooter.css("max-height", "32px");
            this.ui.footerHandle.css("background-image", "url('images/up.png')");
        } else {
            this.ui.leadsListFooter.css("max-height", "1000px");
            this.ui.footerHandle.css("background-image", "url('images/down.png')");
        }

        this.ui.footerHandleVisible = !this.ui.footerHandleVisible;
    },

    onCaptureClick: function () {
        this.showLeadFeedback(null);
    },

    onBackClick: function () {
        _campaigns.preInit();
    },

    onSignOutClick: function () {
        _common.resetTokens();
    },

    onRefreshClick: function (filterString) {
        if (!filterString || filterString == "") {
            if (gen.selectedCampaignConfiguration.ActiveLeadsFilter && gen.selectedCampaignConfiguration.ActiveLeadsFilter.length > 0)
                filterString = gen.selectedCampaignConfiguration.ActiveLeadsFilter;
            else
                filterString = "leadStatus==ACTIVE_MATCH;leadStatus==MISSED_DEADLINE;leadStatus==FOLLOW_UP;leadStatus==PASSED_ON;";
        }



        this.leads = [];
        this.ui.leadsList.empty();

        var loading = _common.getLocalization('loading', 'Loading ... ')

        $.mobile.loading('show', {
            text: loading,
            textVisible: true,
            theme: "a",
            html: ""
        });

        //_leads.onRefreshClickGD(filterString);
        // Get the active leads.
        $.ajax({
                type: "POST",
                dataType: "jsonp",
                headers: {
                    "cache-control": "no-cache"
                },
                data: {
                    token: gen.token.access_token,
                    refreshToken: gen.token.refresh_token,
                    method: "GET",
                    action: gen.selectedCampaign.subResources.leads,
                    base: false,
                    data: "?q=" + encodeURIComponent(filterString)
                },
                beforeSend: function (request) {
                    request.setRequestHeader("Accept-Language", gen.LanguageCode);
                },
                url: gen.common.PROXY
            })
            .done(function (data) {
                gen.counter2++;
                if (gen.REMOVE_FUTURE_DATED_LEADS && gen.REMOVE_FUTURE_DATED_LEADS != "") {
                    var removeTheseLeads = [];
                    for (var i = 0; i < data.leads.length; i++) {
                        var lead = data.leads[i];
                        if (lead[gen.REMOVE_FUTURE_DATED_LEADS]) {
                            var now = new Date();
                            var itemDate = new Date(lead[gen.REMOVE_FUTURE_DATED_LEADS]);
                            if (itemDate > now)
                                removeTheseLeads.push(i);
                        }
                    }
                    for (var i = removeTheseLeads.length - 1; i >= 0; i--) {
                        data.leads.splice(removeTheseLeads[i], 1);
                    }
                }

                _leads.ui.leadsList.empty();
                _leads.leads = data.leads;
                _leads.paging = data.paging;
                _leads.buildLeadsList();
                $.mobile.loading('hide', {});
            })
            .fail(function (jqXHR, textStatus) {
                var failedLeads = _common.getLocalization('failedLeads', 'Failed to get the leads');
                _common.showNotification(failedLeads);
                $.mobile.loading('hide', {});
            });
    },
    onRefreshClickGD: function (filterString) {
        var method = "POST";
        var url = gen.common.PROXY;
        var timeout = 30;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;
        //-- createRequest
        var onRefreshClickRequest = window.plugins.GDHttpRequest.createRequest("POST", url, timeout, isAsync, user, password, auth, isIncremental);
        onRefreshClickRequest.addRequestHeader("dataType", "jsonp");
        onRefreshClickRequest.addRequestHeader("cache-control", "no-cache");
        onRefreshClickRequest.addRequestHeader("Accept-Language", "undefined");
        onRefreshClickRequest.addRequestHeader("crossDomain", "true");
        onRefreshClickRequest.addPostParameter("callback", "getdata");
        onRefreshClickRequest.addPostParameter("refreshToken", gen.token.refresh_token);
        // onRefreshClickRequest.addPostParameter("q", encodeURIComponent(filterString));

        onRefreshClickRequest.addPostParameter("token", gen.token.access_token);
        onRefreshClickRequest.addPostParameter("action", gen.selectedCampaign.subResources.leads);
        onRefreshClickRequest.addPostParameter("method", "GET");
        onRefreshClickRequest.addPostParameter("base", "false");
        onRefreshClickRequest.addPostParameter("data", encodeURIComponent(filterString));

        // onRefreshClickRequest.addPostParameter("data", "?callback=getdata&q=" + encodeURIComponent(filterString));
        onRefreshClickRequest.disablePeerVerification = false;
        onRefreshClickRequest.disableHostVerification = false;
        //-- addPostParameter & clearPostParameters
        onRefreshClickRequest.addHttpBody("refreshmethod json" + JSON.stringify(data));
        //-- send
        function sendSuccess(response) {
            console.log("Received valid response from the onRefreshClickRequest request");
            var responseObj = window.plugins.GDHttpRequest.parseHttpResponse(response);
            var data = _common.FormatJson(responseObj.responseText.toString());
            try {
                gen.counter2++;
                if (gen.REMOVE_FUTURE_DATED_LEADS && gen.REMOVE_FUTURE_DATED_LEADS != "") {
                    var removeTheseLeads = [];
                    for (var i = 0; i < data.leads.length; i++) {
                        var lead = data.leads[i];
                        if (lead[gen.REMOVE_FUTURE_DATED_LEADS]) {
                            var now = new Date();
                            var itemDate = new Date(lead[gen.REMOVE_FUTURE_DATED_LEADS]);
                            if (itemDate > now)
                                removeTheseLeads.push(i);
                        }
                    }
                    for (var i = removeTheseLeads.length - 1; i >= 0; i--) {
                        data.leads.splice(removeTheseLeads[i], 1);
                    }
                }

                _leads.ui.leadsList.empty();
                _leads.leads = data.leads;
                _leads.paging = data.paging;
                _leads.buildLeadsList();
                $.mobile.loading('hide', {});
            } catch (e) {
                var salesPeopleFailed = _common.getLocalization("salesPeopleFailed", "Failed to get the Sales People!");
                _common.showNotification(salesPeopleFailed);
            }
        }

        function sendFail() {

            var salesPeopleFailed = _common.getLocalization("salesPeopleFailed", "Failed to get the Sales People!");
            _common.showNotification(salesPeopleFailed);
        }
        onRefreshClickRequest.send(sendSuccess, sendFail);

    },

    getCampaignSalesPeople: function () {
        _leads.getCampaignSalesPeopleGD();
    },
    getCampaignSalesPeopleGD: function () {
        var method = "POST";
        var url = gen.common.PROXY;
        var timeout = 30;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;

        //-- createRequest
        campaignSalesPeopleRequest = window.plugins.GDHttpRequest.createRequest("POST", url, timeout, isAsync, user, password, auth, isIncremental);

        campaignSalesPeopleRequest.disablePeerVerification = false;
        campaignSalesPeopleRequest.disableHostVerification = false;

        //-- addRequestHeader
        campaignSalesPeopleRequest.addRequestHeader("dataType", "jsonp");
        campaignSalesPeopleRequest.addRequestHeader("cache-control", "no-cache");
        campaignSalesPeopleRequest.addRequestHeader("Accept-Language", "undefined");
        campaignSalesPeopleRequest.addRequestHeader("crossDomain", "true");
        campaignSalesPeopleRequest.addPostParameter("dataType", "jsonp");
        campaignSalesPeopleRequest.addPostParameter("callback", "getdata");
        campaignSalesPeopleRequest.addPostParameter("token", gen.token.access_token);
        campaignSalesPeopleRequest.addPostParameter("refreshToken", gen.token.refresh_token);
        campaignSalesPeopleRequest.addPostParameter("action", "campaigns/" + gen.selectedCampaign.id + "/salesPersons");
        campaignSalesPeopleRequest.addPostParameter("method", "GET");
        campaignSalesPeopleRequest.addPostParameter("base", "true");
        campaignSalesPeopleRequest.addPostParameter("data", "");

        function sendSuccess(response) {
            console.log("Received valid response from the send request");
            var responseObj = window.plugins.GDHttpRequest.parseHttpResponse(response);
            var resonsedata = _common.FormatJson(responseObj.responseText.toString());
            try {
                gen.salesPeople = resonsedata.salesPersons;
                _leads.getLeadMetaData();
            } catch (e) {
                var salesPeopleFailed = _common.getLocalization("salesPeopleFailed", "Failed to get the Sales People!");
                _common.showNotification(salesPeopleFailed);
            }
        };

        function sendFail() {
            var salesPeopleFailed = _common.getLocalization("salesPeopleFailed", "Failed to get the Sales People!");
            _common.showNotification(salesPeopleFailed);
        }
        campaignSalesPeopleRequest.send(sendSuccess, sendFail);

    },

    getLeadMetaData: function () {
        _leads.getLeadMetaDataGD();
    },
    getLeadMetaDataGD: function () {

        var method = "POST";
        var url = gen.common.PROXY;
        var timeout = 30;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;

        //-- createRequest
        getLeadMetaDataRequest = window.plugins.GDHttpRequest.createRequest("POST", url, timeout, isAsync, user, password, auth, isIncremental);

        getLeadMetaDataRequest.disablePeerVerification = false;
        getLeadMetaDataRequest.disableHostVerification = false;

        //-- addRequestHeader
        getLeadMetaDataRequest.addRequestHeader("dataType", "jsonp");
        getLeadMetaDataRequest.addRequestHeader("cache-control", "no-cache");
        getLeadMetaDataRequest.addRequestHeader("Accept-Language", "undefined");
        getLeadMetaDataRequest.addRequestHeader("crossDomain", "true");
        getLeadMetaDataRequest.addPostParameter("dataType", "jsonp");
        getLeadMetaDataRequest.addPostParameter("callback", "getdata");
        getLeadMetaDataRequest.addPostParameter("token", gen.token.access_token);
        getLeadMetaDataRequest.addPostParameter("refreshToken", gen.token.refresh_token);
        getLeadMetaDataRequest.addPostParameter("action", gen.selectedCampaign.subResources.feedbackData);
        getLeadMetaDataRequest.addPostParameter("method", "GET");
        getLeadMetaDataRequest.addPostParameter("base", "false");
        getLeadMetaDataRequest.addPostParameter("data", "");

        //-- send
        function sendSuccess(response) {
            console.log("Received valid response from the leadmetadata request");
            var responseObj = window.plugins.GDHttpRequest.parseHttpResponse(response);
            var resonsedata = _common.FormatJson(responseObj.responseText.toString());
            try {
                _leads.feedbackData = resonsedata;
                _leads.getSelectedLeadMetaData();
            } catch (e) {
                var failedFeedbackData = _common.getLocalization('failedFeedbackData', 'Failed to get the feedback data');
                _common.showNotification(failedFeedbackData);
            }
        };

        function sendFail() {
            var failedFeedbackData = _common.getLocalization('failedFeedbackData', 'Failed to get the feedback data');
            _common.showNotification(failedFeedbackData);
        }
        getLeadMetaDataRequest.send(sendSuccess, sendFail);

    },
    getSelectedLeadMetaData: function () {
        // Get the selected lead metadata.
        _leads.getSelectedLeadMetaDataGD();
    },
    getSelectedLeadMetaDataGD: function () {
        var method = "POST";
        var url = gen.common.PROXY;
        var timeout = 30;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;

        //-- createRequest
        getSelectedLeadMetaDataRequest = window.plugins.GDHttpRequest.createRequest("POST", url, timeout, isAsync, user, password, auth, isIncremental);
        getSelectedLeadMetaDataRequest.addRequestHeader("dataType", "jsonp");
        getSelectedLeadMetaDataRequest.addRequestHeader("cache-control", "no-cache");
        getSelectedLeadMetaDataRequest.addRequestHeader("Accept-Language", "undefined");
        getSelectedLeadMetaDataRequest.addPostParameter("dataType", "jsonp");
        getSelectedLeadMetaDataRequest.addPostParameter("callback", "getdata");
        getSelectedLeadMetaDataRequest.addPostParameter("token", gen.token.access_token);
        getSelectedLeadMetaDataRequest.addPostParameter("refreshToken", gen.token.refresh_token);
        getSelectedLeadMetaDataRequest.addPostParameter("action", gen.selectedCampaign.subResources.leadMetadata);
        getSelectedLeadMetaDataRequest.addPostParameter("base", "false");
        getSelectedLeadMetaDataRequest.addPostParameter("method", "GET");
        getSelectedLeadMetaDataRequest.addPostParameter("data", "");
        getSelectedLeadMetaDataRequest.disablePeerVerification = false;
        getSelectedLeadMetaDataRequest.disableHostVerification = false;
        //-- send
        function sendSuccess(response) {
            console.log("Received valid response from the selected leadmetadata request");
            var responseObj = window.plugins.GDHttpRequest.parseHttpResponse(response);
            var resonsedata = _common.FormatJson(responseObj.responseText.toString());
            try {
                _leads.leadMetadata = resonsedata.leadMetadata;
                _leads.init();
            } catch (e) {
                var failedLeadData = _common.getLocalization('failedLeadData', 'Failed to get the lead data');
                _common.showNotification(failedLeadData);
            }
        };

        function sendFail() {
            var failedLeadData = _common.getLocalization('failedLeadData', 'Failed to get the lead data');
            _common.showNotification(failedLeadData);
        }
        getSelectedLeadMetaDataRequest.send(sendSuccess, sendFail);
    },

    onNextClick: function () {
        var loading = _common.getLocalization('loading', 'Loading ... ');
        $.mobile.loading('show', {
            text: loading,
            textVisible: true,
            theme: "a",
            html: ""
        });
        this.ui.leadsList.find("#nextPageLink").remove();
        // Get the next page.
      _feedback.onNextClickGD();
        // $.ajax({
        //         type: "POST",
        //         dataType: "jsonp",
        //         headers: {
        //             "cache-control": "no-cache"
        //         },
        //         data: {
        //             token: gen.token.access_token,
        //             refreshToken: gen.token.refresh_token,
        //             method: "GET",
        //             action: _leads.paging.nextPage,
        //             base: false,
        //             data: ""
        //         },
        //         beforeSend: function (request) {
        //             request.setRequestHeader("Accept-Language", gen.LanguageCode);
        //         },
        //         url: gen.common.PROXY
        //     })
        //     .done(function (data) {
        //         _leads.leads = _leads.leads.concat(data.leads);
        //         _leads.paging = data.paging;
        //         _leads.buildLeadsList(data.leads);
        //         $.mobile.loading('hide', {});
        //     })
        //     .fail(function (jqXHR, textStatus) {
        //         var failedNextPage = _common.getLocalization('failedNextPage', 'Failed to get the next page');
        //         _common.showNotification(failedNextPage);
        //         $.mobile.loading('hide', {});
        //     });
    },
onNextClickGD: function () {
        var method = "POST";
        var url = gen.common.PROXY;
        var timeout = 30;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;
        //-- createRequest
       var getNextClickLeadRequest = window.plugins.GDHttpRequest.createRequest("POST", url, timeout, isAsync, user, password, auth, isIncremental);
        getNextClickLeadRequest.addRequestHeader("dataType", "jsonp");
        getNextClickLeadRequest.addRequestHeader("cache-control", "no-cache");
        getNextClickLeadRequest.addRequestHeader("Accept-Language", "undefined");
        getNextClickLeadRequest.addPostParameter("dataType", "jsonp");
        getNextClickLeadRequest.addPostParameter("callback", "getdata");
        getNextClickLeadRequest.addPostParameter("token", gen.token.access_token);
        getNextClickLeadRequest.addPostParameter("refreshToken", gen.token.refresh_token);
        getNextClickLeadRequest.addPostParameter("action",_leads.paging.nextPage);
        getNextClickLeadRequest.addPostParameter("base", false);
        getNextClickLeadRequest.addPostParameter("method", "GET");
        getNextClickLeadRequest.addPostParameter("data", "");
        getNextClickLeadRequest.disablePeerVerification = false;
        getNextClickLeadRequest.disableHostVerification = false;
        //-- send
        function sendSuccess(response) {
            console.log("Received valid response from the selected leadmetadata request");
            var responseObj = window.plugins.GDHttpRequest.parseHttpResponse(response);
            var data = _common.FormatJson(responseObj.responseText.toString());
            try {
                  _leads.leads = _leads.leads.concat(data.leads);
                _leads.paging = data.paging;
                _leads.buildLeadsList(data.leads);
                $.mobile.loading('hide', {});
            } catch (e) {
                var failedLeadData = _common.getLocalization('failedLeadData', 'Failed to get the lead data');
                _common.showNotification(failedLeadData);
            }
        };

        function sendFail() {
             var failedNextPage = _common.getLocalization('failedNextPage', 'Failed to get the next page');
                _common.showNotification(failedNextPage);
                $.mobile.loading('hide', {});
        }
        getNextClickLeadRequest.send(sendSuccess, sendFail);
    },
    displayCampaignMessage: function () {
        var banner = "";
        var bannerDestination = "";
        var message = "";
        var labelColor = "black";
        var backgroundColor = "";
        var borderColor = "";

        $.each(gen.CampaignConfiguration.Campaigns.Campaign, function (i, item) {
            if (item.Id == gen.selectedCampaign.id) {
                if (item.BannerURL && item.BannerURL != "") {
                    banner = item.BannerURL;
                    bannerDestination = item.BannerDestination;
                } else {
                    if (item.MessageLocalizationKey)
                        message = _common.getLocalization(item.MessageLocalizationKey, message);

                    if (message.indexOf("#") > -1) {
                        var start = message.indexOf("#") + 1;
                        if (message.indexOf("#", start) > -1) {
                            var end = message.indexOf("#", start) - start;
                            var variable = message.substr(start, end);
                            message = message.replace("#" + variable + "#", gen.selectedCampaign[variable]);
                        }
                    }

                    if (item.LabelColor)
                        labelColor = item.LabelColor;
                    if (item.BackgroundColor)
                        backgroundColor = item.BackgroundColor;
                    if (item.BorderColor)
                        borderColor = item.BorderColor;
                }
            }
        })

        $("#campaignMessageContainer").empty();

        if (banner != "") {
            $("#campaignMessageContainer").removeClass("campaignMessageContainer");
            $("#campaignMessageContainer").show();
            $("#campaignMessageContainer").append("<a href='" + bannerDestination + "' target='_blank'><img class='campaignBanner' src='" + banner + "'/></a>");
        } else if (message != "") {
            $("#campaignMessageContainer").addClass("campaignMessageContainer");
            $("#campaignMessageContainer").show();
            $("#campaignMessageContainer").css("background-color", backgroundColor);
            $("#campaignMessageContainer").css("border-color", borderColor);
            $("#campaignMessageContainer").append('<label id="campaignMessageLabel">' + message + '</label');
            $("#campaignMessageLabel").css("color", labelColor);
        } else
            $("#campaignMessageContainer").hide();
    },

    buildLeadsList: function (appendingData, skipSort) {
        if (appendingData != null) {
            this.appendData(appendingData, this.ui.leadsList);
        } else {
            _leads.displayCampaignMessage();
            this.ui.leadsList.empty();
            this.appendData(_leads.leads, this.ui.leadsList);
        }

        this.ui.leadsList.find("a").off();

        if (_leads.paging.page < _leads.paging.numPages && _leads.paging.totalRecords != 0) {
            var loadNextPage = _common.getLocalization('loadNextPage', 'Load Next Page');
            var pre = _common.getLocalization('leadsPre', 'Lead(s) ')
            var middle = _common.getLocalization('of', ' of ')

            if (_leads.paging.numPages && _leads.paging.numPages != 0)
                this.ui.nextPageButton.text(pre + " 1 - " + _leads.paging.endRecord + " " + middle + " " + _leads.paging.totalRecords);
            else
                this.ui.nextPageButton.text(loadNextPage);

            this.ui.nextPageButton.show();
            this.ui.nextPageButton.addClass("ui-icon-plus");
            this.ui.nextPageButton.addClass("ui-btn-icon-left");
            this.ui.nextPageButton.off().on("click", $.proxy(this.onNextClick, this));
        } else {
            this.ui.nextPageButton.show();
            var caption = _common.getLocalization('leads', 'Lead(s)');
            this.ui.nextPageButton.text(_leads.paging.totalRecords + ' ' + caption);
            this.ui.nextPageButton.off();
        }

        setTimeout(function () {
            _leads.ui.leadsList.listview('refresh');
        }, 0);

        this.ui.leadsList.find("a").off().on("click", $.proxy(this.onLeadClick, this));

        //$.mobile.loading('hide', {});

        //• LINK NAVIGATION
        if (gen.LinkNavigation != null && gen.LinkNavigation.Lead != null) {
            var lead = this.leads.findByField("id", gen.LinkNavigation.Lead);
            if (lead == null) {
                this.leadNotFound();
                _common.hidePreLoaders();
            } else {
                $("#navToFeedback").trigger("click");
                this.showLeadFeedback(lead);
            }

            gen.LinkNavigation.Lead = null;
        }

        if (gen.selectedCampaignAllowsAdding == "true")
            this.ui.leadsCaptureButton.show();
        else
            this.ui.leadsCaptureButton.hide();

        if (!skipSort) {
            setTimeout(function () {
                _leads.sortLeadsHandler(true);
            }, 250);
        }
    },

    appendData: function (data, $holder) {
        if (data.length == 0) {
            $holder.append('<li><a href="#"><h3>No leads for ' + gen.selectedCampaign.name + '</h3></a></li>');
            return;
        }

        var h = "",
            item, deadline, cls, today = Date.today().set({
                hour: 23,
                minute: 59,
                second: 59,
                millisecond: 997
            });
        for (var i = 0; i < data.length; i++) {
            item = data[i];
            deadline = new Date(item.deadlineDate);
            cls = ((deadline < today && this.warnList.indexOf(item.leadStatus) != -1) ? " red" : "");
            h = '<li value=' + item.id + ' data-theme-c>';
            h += '<a href="#feedback_Page" data-transition="slide" data-id="' + item.id + '">';

            for (var counter = 1; counter <= 10; counter++) {
                var position = "Position" + counter;
                if (gen.SelectedConfiguration && gen.SelectedConfiguration.LeadList && gen.SelectedConfiguration.LeadList[position]) {
                    if (gen.SelectedConfiguration.LeadList[position].Visible && gen.SelectedConfiguration.LeadList[position].Visible == "true") {
                        if (gen.SelectedConfiguration.LeadList[position].Property && gen.SelectedConfiguration.LeadList[position].Property != "") {
                            var propertyValue = item[gen.SelectedConfiguration.LeadList[position].Property];
                            if (gen.SelectedConfiguration.LeadList[position].ChildProperty)
                                propertyValue = propertyValue[gen.SelectedConfiguration.LeadList[position].ChildProperty];

                            if (gen.SelectedConfiguration.LeadList[position].InputType == "date") {
                                if (propertyValue && propertyValue != "")
                                    propertyValue = new Date(propertyValue).toString(gen.common.DATE_TIME_FORMAT);
                                else
                                    propertyValue = "";
                            }

                            if (propertyValue) {
                                var localizedCaption = gen.SelectedConfiguration.LeadList[position].Caption;
                                if (gen.SelectedConfiguration.LeadList[position].LocalizationKey)
                                    localizedCaption = _common.getLocalization(gen.SelectedConfiguration.LeadList[position].LocalizationKey, gen.SelectedConfiguration.LeadList[position].Caption);

                                h += '<div class="ui-grid-a">';
                                h += '    <div class="ui-block-a"><div><label for="' + position + 'Label" style="font-size:' + gen.SelectedConfiguration.LeadList[position].FontSize + 'em;color:Gray;" data-localization-key="' + gen.SelectedConfiguration.LeadList[position].LocalizationKey + '" data-localization-default="' + gen.SelectedConfiguration.LeadList[position].Caption + '">' + localizedCaption + '</label></div></div>';

                                var localizationKey = _leads.getLeadStatusKeyLocalizationKey(propertyValue, "");
                                var labelColor = gen.SelectedConfiguration.LeadList[position].Color;
                                if (gen.SelectedConfiguration.LeadList[position].LeadStatusKey) {
                                    var leadStatusKey = gen.SelectedConfiguration.LeadList[position].LeadStatusKey;
                                    var leadStatusKeyValue = "";
                                    if (item[gen.SelectedConfiguration.LeadList[position].LeadStatusKey])
                                        leadStatusKeyValue = item[gen.SelectedConfiguration.LeadList[position].LeadStatusKey];

                                    var statusKeyCaption = _leads.getLeadStatusKeyCaption(leadStatusKeyValue, "");
                                    labelColor = _leads.getLeadStatusKeyLabelColor(leadStatusKeyValue, labelColor);
                                    propertyValue = _leads.getLeadStatusKeyCaption(leadStatusKeyValue, statusKeyCaption);
                                    localizationKey = _leads.getLeadStatusKeyLocalizationKey(leadStatusKeyValue, "");
                                }

                                var localizedValue = _common.getLocalization(localizationKey, propertyValue)
                                h += '    <div class="ui-block-b"><div><label id="' + position + 'Label" style="font-size:' + gen.SelectedConfiguration.LeadList[position].FontSize + 'em;font-weight:' + gen.SelectedConfiguration.LeadList[position].FontWeight + ';color:' + labelColor + ';">' + localizedValue + '</label></div></div>';
                                h += '</div>';
                            }
                        } else {
                            var localizedCaption = gen.SelectedConfiguration.LeadList[position].Caption;
                            if (gen.SelectedConfiguration.LeadList[position].LocalizationKey)
                                localizedCaption = _common.getLocalization(gen.SelectedConfiguration.LeadList[position].LocalizationKey, gen.SelectedConfiguration.LeadList[position].Caption);

                            var labelColor = gen.SelectedConfiguration.LeadList[position].Color;

                            h += '<div class="ui-grid-a">';
                            h += '    <div class="ui-block-a"></div>';
                            h += '    <div class="ui-block-b"><div><label id="' + position + 'Label" style="font-size:' + gen.SelectedConfiguration.LeadList[position].FontSize + 'em;font-weight:' + gen.SelectedConfiguration.LeadList[position].FontWeight + ';color:' + labelColor + ';" data-localization-key="' + gen.SelectedConfiguration.LeadList[position].LocalizationKey + '" data-localization-default="' + gen.SelectedConfiguration.LeadList[position].Caption + '">' + localizedCaption + '</label></div></div>';
                            h += '</div>';
                        }
                    }
                }
            }

            h += '</a>';
            h += '</li>';
            $holder.append(h);
        }
    },

    getLeadStatusKeyOrQueryIndex: function (key) {
        if (gen.SelectedConfiguration.LeadStatus) {
            for (var counter = 0; counter < gen.SelectedConfiguration.LeadStatus.Status.length; counter++) {
                if (gen.SelectedConfiguration.LeadStatus.Status[counter].Query && gen.SelectedConfiguration.LeadStatus.Status[counter].Query == key)
                    return counter;
                if (gen.SelectedConfiguration.LeadStatus.Status[counter].Key && gen.SelectedConfiguration.LeadStatus.Status[counter].Key == _common.replaceAll(key, "leadStatus==", ""))
                    return counter;
            }
            return -1;
        } else
            return -1;
    },

    getLeadStatusKeyLabelColor: function (key, defaultColor) {
        if (gen.SelectedConfiguration.LeadStatus) {
            for (var counter = 0; counter < gen.SelectedConfiguration.LeadStatus.Status.length; counter++) {
                if (gen.SelectedConfiguration.LeadStatus.Status[counter].Key == key)
                    return gen.SelectedConfiguration.LeadStatus.Status[counter].LabelColor;
            }
            return defaultColor;
        } else
            return defaultColor;
    },

    getLeadStatusKeyCaption: function (key, defaultCaption) {
        if (gen.SelectedConfiguration.LeadStatus) {
            for (var counter = 0; counter < gen.SelectedConfiguration.LeadStatus.Status.length; counter++) {
                if (gen.SelectedConfiguration.LeadStatus.Status[counter].Key == key)
                    return gen.SelectedConfiguration.LeadStatus.Status[counter].Caption;
            }
            return defaultCaption;
        } else
            return defaultCaption;
    },

    getLeadStatusKeyLocalizationKey: function (key, defaultLocalizationKey) {
        if (gen.SelectedConfiguration.LeadStatus) {
            for (var counter = 0; counter < gen.SelectedConfiguration.LeadStatus.Status.length; counter++) {
                if (gen.SelectedConfiguration.LeadStatus.Status[counter].Key == key)
                    return gen.SelectedConfiguration.LeadStatus.Status[counter].LocalizationKey;
            }
            return defaultLocalizationKey;
        } else
            return defaultLocalizationKey;
    },

    getValue: function (rec, field) {
        if (rec[field] === undefined)
            return "";
        else if (rec[field] == null)
            return "";
        else
            return rec[field];
    },

    onLeadClick: function (e) {
        var evt = e.originalEvent;
        var $target = $(evt.target);
        if ($target[0].nodeName.toLowerCase() != "a")
            $target = $target.closest("a");

        var leadID = $target.attr("data-id");
        if (leadID) {
            var lead = this.leads.findByField("id", leadID);
            if (lead == null)
                this.leadNotFound();
            else {
                var loading = _common.getLocalization('loading', 'Loading ... ')

                $.mobile.loading('show', {
                    text: loading,
                    textVisible: true,
                    theme: "a",
                    html: ""
                });

                this.showLeadFeedback(lead);
            }
        }
    },

    showLeadFeedback: function (lead) {
        _feedback.init(lead);
    },

    leadNotFound: function () {
        var localizedMessage = _common.getLocalization("leadNotFound", "The selected lead could not be found!");
        alert(localizedMessage);
    }
};