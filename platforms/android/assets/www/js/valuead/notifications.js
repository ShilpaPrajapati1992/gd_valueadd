var _notifications = {
    initPushNotifications: function () {
        try {
            /*
            var push = PushNotification.init(
            {
                android:
                {
                    senderID: "643202234943"
                },
                ios:
                {
                    alert: "true",
                    badge: true,
                    sound: 'false'
                },
                windows: {}
            });
*/
            var pushNotification = window.plugins.pushNotification;
            if (device.platform == 'android' || device.platform == 'Android' || device.platform == "amazon-fireos") {
                pushNotification.register(
                    successHandler,
                    errorHandler, {
                        "senderID": "643202234943",
                        "ecb": "_notifications.onNotification"
                    });
            } else {
                pushNotification.register(
                    tokenHandler,
                    errorHandler, {
                        "badge": "true",
                        "sound": "true",
                        "alert": "true",
                        "ecb": "_notifications.onNotificationAPN"
                    });
            }

            function successHandler(result) {
                console.log('successHandler: ' + result);
            }

            function errorHandler(error) {
                _common.showNotification('Notification Error: ' + error);
            }

            function tokenHandler(result) {
                _notifications.registerAPNDeviceGD(result, 'iOS');
                // _notifications.registerAPNDevice(result, 'iOS');
            }

        } catch (e) {
            console.log(e.message);
        }
    },

    onNotification: function (data) {
        if (data.event == "registered")
            _notifications.registerAPNDeviceGD(data.regid, 'Android');
        // _notifications.registerAPNDevice(data.regid, 'Android');
    },

    onNotificationAPN: function (data) {
        console.log(data);
    },

    registerAPNDeviceGD: function (guid, platform) {
        var domain = gen.common.PROXY.replace("https://", "").replace("/MobileProxy/JsonProxy", "");
        var url = "http://push.value-ad.com/api/userdevice/register";
        var method = "POST";
        var timeout = 30;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;
        //-- createRequest
        registerAPNDeviceRequest = window.plugins.GDHttpRequest.createRequest("POST", url, timeout, isAsync, user, password, auth, isIncremental);

        registerAPNDeviceRequest.disablePeerVerification = false;
        registerAPNDeviceRequest.disableHostVerification = false;

        //-- addRequestHeader
        registerAPNDeviceRequest.addRequestHeader("dataType", "jsonp");
        registerAPNDeviceRequest.addRequestHeader("cache-control", "no-cache");
        registerAPNDeviceRequest.addRequestHeader("VA_API_KEY", "89bcedf12ce64bf78144062f5ef00cf0");
        registerAPNDeviceRequest.addRequestHeader("contentType", "application/x-www-form-urlencoded");
        registerAPNDeviceRequest.addRequestHeader("crossDomain", "true");
        registerAPNDeviceRequest.addPostParameter("_id", guid);
        registerAPNDeviceRequest.addPostParameter("Platform", platform);
        registerAPNDeviceRequest.addPostParameter("Client", domain);
        registerAPNDeviceRequest.addPostParameter("User", gen.LoggedInUserId);

        alert(JSON.stringify(registerAPNDeviceRequest));
        //-- send
        function sendSuccess(response) {
            try {

                alert("Registerd device");
                console.log(data);
            } catch (e) {
                alert("APN Registration Failed:");
                console.log("APN Registration Failed: " + response.status);
            }
        };

        function sendFail() {
            alert("APN Registration Failed:");
            console.log("APN Registration Failed: " + response.status);
        }
        registerAPNDeviceRequest.send(sendSuccess, sendFail);
    }
};