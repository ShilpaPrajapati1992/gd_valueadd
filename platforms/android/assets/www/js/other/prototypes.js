﻿//===================================================================================
// Etana
// ff.snack.prototypes.js : A collection of prototypes that extend objects
//                          with "missing" functionality.
//===================================================================================
// Created: 2012/06/06 - Marius Smit
//===================================================================================
// Copyright (c) 2012 Etana.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//===================================================================================

$(function () {

    // Trim a string. Adds trim if it doesn't exist.
    if (!String.prototype.trim) {
        String.prototype.trim = function () {
            return this.replace(/^\s+|\s+$/g, '');
        };
    }

     // Checks a string for a value.
    String.prototype.contains = function (find) { return this.indexOf(find) != -1; };

    // Removes all spaces from a string.
    String.prototype.removeSpaces = function () { return this.split(' ').join(''); };

    // Remove all commas from a string.
    String.prototype.removeCommas = function () { return this.split(',').join(''); };

    // Array Remove - By John Resig (MIT Licensed).
    if (!Array.prototype.remove) {
        Array.prototype.remove = function (from, to) {
            var rest = this.slice((to || from) + 1 || this.length);
            this.length = from < 0 ? this.length + from : from;
            return this.push.apply(this, rest);
        };
    }

    // Find an item in the array by the field value.
    // If multiRow is passed, all matching records will be returned.
    Array.prototype.findByField = function (fieldName, fieldValue, multiRow) {
        var list = new Array(this);
        var result = [];
        if (multiRow == null) multiRow = false;
        list = list[0];
        for (var i = 0; i < list.length; i++) {
            if (list[i] === undefined) continue;
            if (list[i][fieldName] === undefined) continue;
            if (list[i][fieldName] == fieldValue) {
                if (multiRow) {
                    result.push(list[i]);
                }
                else {
                    return list[i];
                }
            }
        }
        if (multiRow) {
            return result;
        }
        else {
            return null;
        }
    };

    // Find an item in the array by multiple field values.
    // If multiRow is passed, all matching records will be returned.
    Array.prototype.findByFields = function (fieldNames, fieldValues, multiRow) {
        var list = new Array(this);
        var allEqual = true;
        var result = [];
        if (multiRow == null) multiRow = false;
        list = list[0];
        for (var i = 0; i < list.length; i++) {
            if (list[i] === undefined) continue;
            allEqual = true;
            for (var j = 0; j < fieldNames.length; j++) {
                if (list[i][fieldNames[j]] != fieldValues[j]) {
                    allEqual = false;
                }
            }
            if (allEqual) {
                if (multiRow) {
                    result.push(list[i]);
                }
                else {
                    return list[i];
                }
            }
        }
        if (multiRow) {
            return result;
        }
        else {
            return null;
        }
    };

    // Uses the filter method to find an item in the array by numeric field value.
    Array.prototype.findByNumber = function (fieldName, fieldValue) {
        var list = new Array(this);
        list = list[0];
        return list.filter(function (obj) {
            // Coerce both obj.id and id to numbers for val & type comparison.
            return +obj[fieldName] === +fieldValue;
        })[0];
    };

    // Uses the filter method to find an item in the array by string field value.
    Array.prototype.findByString = function (fieldName, fieldValue) {
        var list = new Array(this);
        list = list[0];
        return list.filter(function (obj) {
            return obj[fieldName] === fieldValue;
        })[0];
    };

    // Adds filter to Array if it doesn't exist.
    // Accepts a filter function.
    // `function isBigEnough(element, index, array) {
    //     return (element >= 10);
    // }
    // var filtered = [12, 5, 8, 130, 44].filter(isBigEnough);
    // \/\/filtered is [12, 130, 44]`
    if (!Array.prototype.filter) {
        Array.prototype.filter = function (fun) {
            "use strict";

            if (this == null)
                throw new TypeError();

            var t = Object(this);
            var len = t.length >>> 0;
            if (typeof fun != "function")
                throw new TypeError();

            var res = [];
            var thisp = arguments[1];
            for (var i = 0; i < len; i++) {
                if (i in t) {
                    var val = t[i]; // in case fun mutates this
                    if (fun.call(thisp, val, i, t))
                        res.push(val);
                }
            }

            return res;
        };
    }

    // Adds indexOf to Array if it doesn't exist. MDN implementation.
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function (searchElement /*, fromIndex */) {
            "use strict";
            if (this == null) {
                throw new TypeError();
            }
            var t = Object(this);
            var len = t.length >>> 0;
            if (len === 0) {
                return -1;
            }
            var n = 0;
            if (arguments.length > 0) {
                n = Number(arguments[1]);
                if (n != n) { // shortcut for verifying if it's NaN
                    n = 0;
                } else if (n != 0 && n != Infinity && n != -Infinity) {
                    n = (n > 0 || -1) * Math.floor(Math.abs(n));
                }
            }
            if (n >= len) {
                return -1;
            }
            var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
            for (; k < len; k++) {
                if (k in t && t[k] === searchElement) {
                    return k;
                }
            }
            return -1;
        }
    }

    // TODO: Add array sort by field + direction.

    // Duplicates the array.
    if (!Array.prototype.clone) {
        Array.prototype.clone = function (fun) {
            return this.slice(0);
        }
    }

    //// Object comparison.
    //Object.prototype.equals = function (x) {
    //    var p;
    //    for (p in this) {
    //        if (typeof (x[p]) == 'undefined') { return false; }
    //    }

    //    for (p in this) {
    //        if (this[p]) {
    //            switch (typeof (this[p])) {
    //                case 'object':
    //                    if (!this[p].equals(x[p])) { return false; } break;
    //                case 'function':
    //                    if (typeof (x[p]) == 'undefined' ||
    //                        (p != 'equals' && this[p].toString() != x[p].toString()))
    //                        return false;
    //                    break;
    //                default:
    //                    if (this[p] != x[p]) { return false; }
    //            }
    //        } else {
    //            if (x[p])
    //                return false;
    //        }
    //    }

    //    for (p in x) {
    //        if (typeof (this[p]) == 'undefined') { return false; }
    //    }

    //    return true;
    //}
    
});