cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "id": "cordova-plugin-device.device",
        "file": "plugins/cordova-plugin-device/www/device.js",
        "pluginId": "cordova-plugin-device",
        "clobbers": [
            "device"
        ]
    },
    {
        "id": "cordova-plugin-globalization.GlobalizationError",
        "file": "plugins/cordova-plugin-globalization/www/GlobalizationError.js",
        "pluginId": "cordova-plugin-globalization",
        "clobbers": [
            "window.GlobalizationError"
        ]
    },
    {
        "id": "cordova-plugin-globalization.globalization",
        "file": "plugins/cordova-plugin-globalization/www/globalization.js",
        "pluginId": "cordova-plugin-globalization",
        "clobbers": [
            "navigator.globalization"
        ]
    },
    {
        "id": "com.phonegap.plugins.PushPlugin.PushNotification",
        "file": "plugins/com.phonegap.plugins.PushPlugin/www/PushNotification.js",
        "pluginId": "com.phonegap.plugins.PushPlugin",
        "clobbers": [
            "PushNotification"
        ]
    },
    {
        "id": "cordova-plugin-clipboard-x.Clipboard",
        "file": "plugins/cordova-plugin-clipboard-x/www/clipboard.js",
        "pluginId": "cordova-plugin-clipboard-x",
        "clobbers": [
            "cordova.plugins.clipboard",
            "cordova.plugins.clipboard-x"
        ]
    },
    {
        "id": "cordova-plugin-inappbrowser.inappbrowser",
        "file": "plugins/cordova-plugin-inappbrowser/www/inappbrowser.js",
        "pluginId": "cordova-plugin-inappbrowser",
        "clobbers": [
            "cordova.InAppBrowser.open",
            "window.open"
        ]
    },
    {
        "id": "cordova-plugin-statusbar.statusbar",
        "file": "plugins/cordova-plugin-statusbar/www/statusbar.js",
        "pluginId": "cordova-plugin-statusbar",
        "clobbers": [
            "window.StatusBar"
        ]
    },
    {
        "id": "cordova-plugin-bbd-storage.FileReader",
        "file": "plugins/cordova-plugin-bbd-storage/assets/www/android/GDFileReader.js",
        "pluginId": "cordova-plugin-bbd-storage",
        "clobbers": [
            "window.FileReader"
        ]
    },
    {
        "id": "cordova-plugin-bbd-storage.requestFileSystem",
        "file": "plugins/cordova-plugin-bbd-storage/assets/www/android/GDRequestFileSystem.js",
        "pluginId": "cordova-plugin-bbd-storage",
        "clobbers": [
            "window.requestFileSystem"
        ]
    },
    {
        "id": "cordova-plugin-bbd-storage.resolveLocalFileSystemURI",
        "file": "plugins/cordova-plugin-bbd-storage/assets/www/android/GDResolveLocalFileSystemURI.js",
        "pluginId": "cordova-plugin-bbd-storage",
        "clobbers": [
            "window.resolveLocalFileSystemURI"
        ]
    },
    {
        "id": "cordova-plugin-bbd-storage.sqlite3enc_import",
        "file": "plugins/cordova-plugin-bbd-storage/assets/www/android/GDSQLite3encImport.js",
        "pluginId": "cordova-plugin-bbd-storage",
        "clobbers": [
            "window.sqlite3enc_import"
        ]
    },
    {
        "id": "cordova-plugin-bbd-storage.FileSystem",
        "file": "plugins/cordova-plugin-bbd-storage/assets/www/android/GDFileSystem.js",
        "pluginId": "cordova-plugin-bbd-storage",
        "clobbers": [
            "window.FileSystem"
        ]
    },
    {
        "id": "cordova-plugin-bbd-storage.DirectoryEntry",
        "file": "plugins/cordova-plugin-bbd-storage/assets/www/android/GDDirectoryEntry.js",
        "pluginId": "cordova-plugin-bbd-storage",
        "clobbers": [
            "window.DirectoryEntry"
        ]
    },
    {
        "id": "cordova-plugin-bbd-storage.DirectoryReader",
        "file": "plugins/cordova-plugin-bbd-storage/assets/www/android/GDDirectoryReader.js",
        "pluginId": "cordova-plugin-bbd-storage",
        "clobbers": [
            "window.DirectoryReader"
        ]
    },
    {
        "id": "cordova-plugin-bbd-storage.FileEntry",
        "file": "plugins/cordova-plugin-bbd-storage/assets/www/android/GDFileEntry.js",
        "pluginId": "cordova-plugin-bbd-storage",
        "clobbers": [
            "window.FileEntry"
        ]
    },
    {
        "id": "cordova-plugin-bbd-storage.FileWriter",
        "file": "plugins/cordova-plugin-bbd-storage/assets/www/android/GDFileWriter.js",
        "pluginId": "cordova-plugin-bbd-storage",
        "clobbers": [
            "window.FileWriter"
        ]
    },
    {
        "id": "cordova-plugin-bbd-storage.File",
        "file": "plugins/cordova-plugin-bbd-storage/assets/www/android/GDFile.js",
        "pluginId": "cordova-plugin-bbd-storage",
        "clobbers": [
            "window.File"
        ]
    },
    {
        "id": "cordova-plugin-bbd-storage.ProgressEvent",
        "file": "plugins/cordova-plugin-bbd-storage/assets/www/android/GDProgressEvent.js",
        "pluginId": "cordova-plugin-bbd-storage",
        "clobbers": [
            "window.ProgressEvent"
        ]
    },
    {
        "id": "cordova-plugin-bbd-storage.LocalFileSystem",
        "file": "plugins/cordova-plugin-bbd-storage/assets/www/android/GDLocalFileSystem.js",
        "pluginId": "cordova-plugin-bbd-storage",
        "clobbers": [
            "window.LocalFileSystem"
        ]
    },
    {
        "id": "cordova-plugin-bbd-storage.FileError",
        "file": "plugins/cordova-plugin-bbd-storage/assets/www/android/GDFileError.js",
        "pluginId": "cordova-plugin-bbd-storage",
        "clobbers": [
            "window.FileError"
        ]
    },
    {
        "id": "cordova-plugin-bbd-storage.Metadata",
        "file": "plugins/cordova-plugin-bbd-storage/assets/www/android/GDMetadata.js",
        "pluginId": "cordova-plugin-bbd-storage",
        "clobbers": [
            "window.Metadata"
        ]
    },
    {
        "id": "cordova-plugin-bbd-storage.localStorage",
        "file": "plugins/cordova-plugin-bbd-storage/assets/www/android/GDStorage.js",
        "pluginId": "cordova-plugin-bbd-storage",
        "clobbers": [
            "window.localStorage"
        ]
    },
    {
        "id": "cordova-plugin-bbd-httprequest.GDHttpRequestPlugin",
        "file": "plugins/cordova-plugin-bbd-httprequest/assets/www/android/GDHttpRequest.js",
        "pluginId": "cordova-plugin-bbd-httprequest",
        "clobbers": [
            "window.plugins.GDHttpRequest"
        ]
    },
    {
        "id": "cordova-plugin-bbd-httprequest.GDCacheController",
        "file": "plugins/cordova-plugin-bbd-httprequest/assets/www/android/GDCacheController.js",
        "pluginId": "cordova-plugin-bbd-httprequest",
        "clobbers": [
            "window.plugins.GDCacheController"
        ]
    },
    {
        "id": "cordova-plugin-bbd-xmlhttprequest.XmlHttpRequest",
        "file": "plugins/cordova-plugin-bbd-xmlhttprequest/assets/www/android/GDXmlHttpRequest.js",
        "pluginId": "cordova-plugin-bbd-xmlhttprequest",
        "clobbers": [
            "window.XmlHttpRequest"
        ]
    },
    {
        "id": "cordova-plugin-bbd-xmlhttprequest.FormData",
        "file": "plugins/cordova-plugin-bbd-xmlhttprequest/assets/www/android/GDFormData.js",
        "pluginId": "cordova-plugin-bbd-xmlhttprequest",
        "clobbers": [
            "window.FormData"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-device": "1.1.7-dev",
    "cordova-plugin-globalization": "1.0.8-dev",
    "com.phonegap.plugins.PushPlugin": "3.2.0",
    "cordova-plugin-clipboard-x": "1.0.1",
    "cordova-plugin-console": "1.0.8-dev",
    "cordova-plugin-inappbrowser": "1.7.2-dev",
    "cordova-plugin-statusbar": "2.2.4-dev",
    "cordova-plugin-whitelist": "1.3.3-dev",
    "cordova-plugin-bbd-base": "1.0.0",
    "cordova-plugin-bbd-storage": "1.0.0",
    "cordova-plugin-bbd-httprequest": "1.0.0",
    "cordova-plugin-bbd-xmlhttprequest": "1.0.0"
};
// BOTTOM OF METADATA
});