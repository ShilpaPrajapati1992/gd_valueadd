var _feedback = {
    init: function (lead) {
        _feedback.selectedOptionDate = new Date();

        _feedback.lead_existing = [];
        _feedback.lead_new = [];
        this.ui = {};

        this.ui.resetButton = $("#leadResetButton");
        $("#leadResetButton").val(_common.getLocalization("resetLead", "Reset"));
        $('#leadResetButton').find(".ui-btn-text").html($(this).html()).slideDown();

        this.ui.submitButton = $("#feedbackSubmitButton");
        $("#feedbackSubmitButton").val(_common.getLocalization("submitFeedback", "Submit Feedback"));
        $('#feedbackSubmitButton').find(".ui-btn-text").html($(this).html()).slideDown();

        var languageSelectContainer = $("#languageSelectContainerFeedback");

        if (gen.common.EnableGlobalization) {
            languageSelectContainer.show();
        } else {
            languageSelectContainer.hide();
        }

        this.ui.feedbackContainer = $("#feedbackContainer");
        this.ui.optionSet = null;

        this.ui.resetButton.off().on("click", $.proxy(this.onResetClick, this));
        _feedback.enableSubmit();

        this.ui.feedbackSignOutButton = $("#feedbackSignOutButton");
        this.ui.feedbackSignOutButton.off().on("click", $.proxy(this.onSignOutClick, this));

        _feedback.passingOn = false;
        _feedback.passingOnSubordinates = false;
        _feedback.passingOnIDs = [];
        _feedback.passingOnIDType = "";
        _feedback.passingOnIDPosition = "";

        //• PASS ON
        if (gen.SelectedConfiguration.Feedback) {
            var options = gen.SelectedConfiguration.Feedback.Options;

            if (options) {
                $.each(options.FeedbackType, function (options, optionItem) {
                    if (optionItem.PassOn && optionItem.PassOn == "true") {
                        if (optionItem.PassOnSubordinates && optionItem.PassOnSubordinates == "true") {
                            _feedback.passingOnSubordinates = true;

                            if (optionItem.SubordinateId && optionItem.SubordinateId != "") {
                                _feedback.passingOnIDs = optionItem.SubordinateId.split(',');

                                if (optionItem.SubordinateIdType && optionItem.SubordinateIdType != "") {
                                    if (optionItem.SubordinateIdType == "Include")
                                        _feedback.passingOnIDType = "Include";
                                    else
                                        _feedback.passingOnIDType = "Exclude";
                                }

                                if (optionItem.SubordinateIdPosition && optionItem.SubordinateIdPosition != "") {
                                    if (optionItem.SubordinateIdPosition == "StartsWith")
                                        _feedback.passingOnIDPosition = "StartsWith";
                                    else
                                        _feedback.passingOnIDPosition = "EndsWith";
                                }
                            }
                        } else
                            _feedback.passingOnSubordinates = false;
                    }
                });
            }
        }

        _feedback.subordinatesList = {};
        if (_feedback.passingOnSubordinates) {
            //• ADD PEOPLE REPORTED TO
            $.each(gen.salesPeople, function (i, item) {
                if (!_feedback.subordinatesList[item.reportsTo])
                    _feedback.subordinatesList[item.reportsTo] = [];

                _feedback.subordinatesList[item.reportsTo].push(item.suppliedId);
            });

            //• COMPLETE LIST RECURSIVELY BY ADDING SUBORDINATES OF SUBORDINATES
            $.each(_feedback.subordinatesList, function (i, itemOuter) {
                $.each(itemOuter, function (ii, itemInner) {
                    if (_feedback.subordinatesList[itemInner]) {
                        $.each(_feedback.subordinatesList[itemInner], function (iii, itemSub) {
                            if (itemOuter.indexOf(itemSub) < 0)
                                itemOuter.push(itemSub);
                        });
                    }
                });
            });
        }

        this.ui.feedbackContainer.empty();

        if (lead) {
            _feedback.capture = false;
            gen.selectedLead = lead;
            _feedback.selectedLead = lead;
            // Get the full lead object
            //Added gdcall 
            _feedback.getFullLeadObjectGD(lead)
        } else {
            _feedback.capture = true;
            _feedback.lead = {};
            _feedback.buildForm(_leads.feedbackData);
        }
        _common.displayLanguage();
    },

    getFullLeadObjectGD: function (lead) {
        var method = "POST";
        var url = gen.common.PROXY;
        var timeout = 30;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;

        var getFullLeadObjectRequest = window.plugins.GDHttpRequest.createRequest("POST", gen.common.PROXY, timeout, isAsync, user, password, auth, isIncremental);
        getFullLeadObjectRequest.disablePeerVerification = false;
        getFullLeadObjectRequest.disableHostVerification = false;
        getFullLeadObjectRequest.addRequestHeader("dataType", "jsonp");
        getFullLeadObjectRequest.addRequestHeader("cache-control", "no-cache");
        getFullLeadObjectRequest.addRequestHeader("Accept-Language", gen.LanguageCode);
        getFullLeadObjectRequest.addRequestHeader("crossDomain", "true");
        getFullLeadObjectRequest.addPostParameter("dataType", "jsonp");
        getFullLeadObjectRequest.addPostParameter("callback", "getdata");
        getFullLeadObjectRequest.addPostParameter("token", gen.token.access_token);
        getFullLeadObjectRequest.addPostParameter("refreshToken", gen.token.refresh_token);
        getFullLeadObjectRequest.addPostParameter("action", lead.uri);
        getFullLeadObjectRequest.addPostParameter("method", "GET");
        getFullLeadObjectRequest.addPostParameter("base", "false");
        getFullLeadObjectRequest.addPostParameter("data", "");

        function sendSuccess(response) {
            console.log("Received valid response from the full lead request");
            var responseObj = window.plugins.GDHttpRequest.parseHttpResponse(response);
            var data = _common.FormatJson(responseObj.responseText.toString());
            try {
                _feedback.lead = data;
                _feedback.buildForm(_leads.feedbackData);
            } catch (e) {
                var feedbackFormFailed = _common.getLocalization("feedbackFormFailed", "Failed to get the feedback form!");
                _common.showNotification(feedbackFormFailed);
            };
        }

        function sendFail() {
            var feedbackFormFailed = _common.getLocalization("feedbackFormFailed", "Failed to get the feedback form!");
            _common.showNotification(feedbackFormFailed);
        }
        getFullLeadObjectRequest.send(sendSuccess, sendFail);
    },
    onSignOutClick: function () {
        _common.resetTokens();
    },
    attachVerify: function (formElement) {
        formElement.verify({
            prompt: function (element, text) {
                if (text && gen.firstFail) {
                    var message = _common.getLocalization(_common.replaceAll(text, " ", "").toLowerCase(), text);
                    var lbl = $('label[for="' + $(element).attr("id") + '"]');
                    _common.showNotification(lbl.text().replace("*", "") + ": " + message);
                    gen.firstFail = false;
                }
            }
        });
    },
    attachVerify2: function (formElement) {
        formElement.verify({
            track: function (type, fieldId, result) {
                if (result && result == "Valid")
                    gen.validCount++;
            }
        });
    },
    buildForm: function (data) {
        setTimeout(_common.hidePreLoaders, 500);
        this.feedbackOption = data.options;
        this.buildFormInit();
    },

    buildFormInit: function () {
        this.buildInfoHelper(_feedback.lead, this.ui.feedbackContainer);
        this.buildFeedbackHelper(this.feedbackOption, this.ui.feedbackContainer);
        this.ui.feedbackContainer.trigger("create");

        _common.displayLanguage();
    },

    buildInfoHelper: function (dataLead, $container) {
        var data = $.extend({}, dataLead),
            $field;

        $container.empty();
        var title = _common.getLocalization("leadInformation", "Lead Information");
        $container.append("<h3>" + title + "</h3>");

        var $leadInformationForm = $('<form id="form_LeadInformation"/>');
        $container.append($leadInformationForm);
        _feedback.attachVerify($leadInformationForm);

        //////////////////////////////////////////////////////////////////////
        //• MATCH INFORMATION
        //////////////////////////////////////////////////////////////////////

        var displayId = false;
        if (gen.SelectedConfiguration.LeadInformation.DisplayId && gen.SelectedConfiguration.LeadInformation.DisplayId == "true")
            displayId = true;

        if (!_feedback.capture && displayId) {
            var leadIdLabel = _common.getLocalization("leadId", "Lead Reference");
            var leadId = data.id;
            this.addInput($leadInformationForm, "leadId", leadIdLabel, leadId, true, false, "textbox", null, null);
        }

        var displayLeadStatus = true;
        if (gen.SelectedConfiguration.LeadInformation.DisplayLeadStatus && gen.SelectedConfiguration.LeadInformation.DisplayLeadStatus == "false")
            displayLeadStatus = false;

        if (displayLeadStatus && data.matches && data.matches[0].leadStatus !== undefined)
            displayLeadStatus = true;
        else
            displayLeadStatus = false;

        if (displayLeadStatus) {
            var leadStatusLabel = "Lead Status";
            if (gen.SelectedConfiguration.LeadStatus && gen.SelectedConfiguration.LeadStatus.length > 0)
                leadStatusLabel = _common.getAttribute(gen.SelectedConfiguration.LeadStatus[0].attributes, "Label");

            var leadStatusValue = data.matches[0].leadStatus;
            var leadStatusCaption = leadStatusValue;
            var leadStatusColour = null;

            leadStatusCaption = this.getLeadStatusKeyCaption(leadStatusValue, leadStatusValue);
            leadStatusCaption = _common.getLocalization(this.getLeadStatusKeyLocalizationKey(leadStatusValue, leadStatusValue), leadStatusCaption);
            leadStatusColour = this.getLeadStatusKeyBackgroundColor(leadStatusValue, null);

            this.addInput($leadInformationForm, "leadStatus", leadStatusLabel, leadStatusCaption, true, false, "textbox", null, leadStatusColour);
        }

        var displayMatchDate = true;
        if (gen.SelectedConfiguration.LeadInformation.DisplayMatchDate && gen.SelectedConfiguration.LeadInformation.DisplayMatchDate == "false")
            displayMatchDate = false;

        if (displayMatchDate && data.matches && data.matches[0].matchDate !== undefined)
            displayMatchDate = true;
        else
            displayMatchDate = false;

        if (displayMatchDate) {
            var matchDateLabel = "Match Date";
            if (gen.MatchDate && gen.MatchDate.length > 0)
                matchDateLabel = _common.getAttribute(gen.MatchDate[0].attributes, "Label");

            this.addInput($leadInformationForm, "matchDate", matchDateLabel, new Date(data.matches[0].matchDate).toString(gen.common.DATE_TIME_FORMAT), true, false, "textbox", null, null);
        }

        var displayDeadlineDate = true;
        if (gen.SelectedConfiguration.LeadInformation.DisplayDeadlineDate && gen.SelectedConfiguration.LeadInformation.DisplayDeadlineDate == "false")
            displayDeadlineDate = false;

        if (displayDeadlineDate && data.matches && data.matches[0].deadlineDate !== undefined)
            displayDeadlineDate = true;
        else
            displayDeadlineDate = false;

        if (displayDeadlineDate) {
            var dealineDateLabel = "Feedback Date";
            if (gen.DeadlineDate && gen.DeadlineDate.length > 0)
                dealineDateLabel = _common.getAttribute(gen.MatchDate[0].attributes, "Label");

            this.addInput($leadInformationForm, "deadlineDate", dealineDateLabel, new Date(data.matches[0].deadlineDate).toString(gen.common.DATE_TIME_FORMAT), true, false, "textbox", null, null);
        }

        if (displayId || displayLeadStatus || displayMatchDate || displayDeadlineDate)
            this.addSeperator($leadInformationForm);

        //////////////////////////////////////////////////////////////////////
        //• PREVIOUS MATCHES
        //////////////////////////////////////////////////////////////////////

        if (data.matches && data.matches[0].feedback !== undefined) {
            var priorFeedback = "";
            $.each(data.matches[0].feedback, function (feedback, feedbackItem) {
                if (gen.SelectedConfiguration.Feedback) {
                    var options = gen.SelectedConfiguration.Feedback.Options;

                    if (options) {
                        $.each(options.FeedbackType, function (options, optionItem) {
                            var feedbackType = options;

                            if (optionItem.Type == feedbackItem.feedbackType) {
                                var localizedFeedback = feedbackItem.displayName;
                                if (optionItem.LocalizationKey)
                                    localizedFeedback = _common.getLocalization(optionItem.LocalizationKey, feedbackItem.displayName);

                                var feedbackComments = "";
                                if (feedbackItem.comments)
                                    feedbackComments = feedbackItem.comments;

                                priorFeedback = priorFeedback + new Date(feedbackItem.createdAt).toString(gen.common.DATE_TIME_FORMAT) + " " + localizedFeedback + "  " + feedbackComments + "\n";
                            }
                        });
                    }
                }
            });

            if (priorFeedback.length > 0) {
                this.addInput($leadInformationForm, "priorFeedback", "Prior Feedback", priorFeedback, true, false, "textarea", null, null);
                this.addSeperator($leadInformationForm);
            }
        }

        //////////////////////////////////////////////////////////////////////
        //• LEAD INFORMATION
        //////////////////////////////////////////////////////////////////////

        var context = this;

        //• FIRST ADD ALL SETTINGS ITEMS
        $.each(gen.SelectedConfiguration.LeadInformation, function (leadInformation, leadInformationItem) {
            if (leadInformation == "hr1" || leadInformation == "hr2" || leadInformation == "hr3") {
                _feedback.addSeperator($leadInformationForm);
                return;
            }

            if (leadInformationItem.Visible && leadInformationItem.Visible == "true") {
                var property = leadInformation;
                var propertyValue = data[property];

                if ((leadInformationItem.LeadProperty && leadInformationItem.LeadProperty != "") && _feedback.lead[leadInformationItem.LeadProperty]) {
                    propertyValue = _feedback.lead[leadInformationItem.ParentProperty];

                    if ((leadInformationItem.LeadChildProperty && leadInformationItem.LeadChildProperty != "") && _feedback.lead[leadInformationItem.LeadProperty][leadInformationItem.LeadChildProperty])
                        propertyValue = _feedback.lead[leadInformationItem.LeadProperty][leadInformationItem.LeadChildProperty];

                    if (propertyValue) {
                        var caption = leadInformationItem.Caption;
                        var localizedCaption = _common.getLocalization(property, caption);
                        context.addInput($leadInformationForm, property, localizedCaption, propertyValue, readOnly, false, "textbox", null, null, leadInformationItem);
                    }
                } else if (leadInformationItem.Hyperlink && leadInformationItem.Hyperlink != "") {
                    var label = _common.getLocalization(leadInformationItem.LocalizationKey, "URL");
                    if (propertyValue) {
                        context.addInput($leadInformationForm, property, label, propertyValue, true, false, "url", null, null, leadInformationItem);
                    } else {
                        if (leadInformationItem.Value && leadInformationItem.Value != "")
                            context.addInput($leadInformationForm, property, label, leadInformationItem.Value, true, false, "url", null, null, leadInformationItem);
                    }
                } else if (propertyValue || (leadInformationItem.HideIfEmpty && leadInformationItem.HideIfEmpty == "false") || _feedback.capture) {
                    var readOnly = leadInformationItem.Editable == "false";
                    if (_feedback.capture) {
                        readOnly = false;
                        propertyValue = "";
                    }

                    var mitem;
                    for (var i = 0; i < _leads.leadMetadata.length; i++) {
                        mitem = _leads.leadMetadata[i];
                        if (mitem.name == property) {
                            if (mitem.type == "date" && propertyValue != "")
                                propertyValue = new Date(propertyValue).toString(gen.common.DATE_FORMAT);

                            if (readOnly) {
                                switch (mitem.type) {
                                    case "date":
                                        {
                                            context.addInput($leadInformationForm, property, mitem.displayName, propertyValue, true, mitem.compulsory, "textbox", null, null, leadInformationItem);
                                        }
                                        break;

                                    case "textarea":
                                    case "textbox":
                                        {
                                            context.addInput($leadInformationForm, property, mitem.displayName, propertyValue, readOnly, mitem.compulsory, mitem.type, null, null, leadInformationItem);
                                        }
                                        break;

                                    case "combo":
                                        {
                                            context.addInput($leadInformationForm, property, mitem.displayName, propertyValue, readOnly, mitem.compulsory, "textbox", null, null, leadInformationItem);
                                        }
                                        break;
                                }
                            } else {
                                switch (mitem.type) {
                                    case "date":
                                        {
                                            context.addInput($leadInformationForm, property, mitem.displayName, propertyValue, true, mitem.compulsory, mitem.type, null, null, leadInformationItem);
                                        }
                                        break;

                                    case "textarea":
                                    case "textbox":
                                        {
                                            context.addInput($leadInformationForm, property, mitem.displayName, propertyValue, readOnly, mitem.compulsory, mitem.type, null, null, leadInformationItem);
                                        }
                                        break;

                                    case "combo":
                                        {
                                            context.addInput($leadInformationForm, property, mitem.displayName, propertyValue, readOnly, mitem.compulsory, mitem.type, mitem.values, null, leadInformationItem);
                                        }
                                        break;
                                }
                            }
                        }
                    }

                    delete data[property];
                }
            }
        });

        var hideUnlisted = true;
        if (gen.SelectedConfiguration.LeadInformation.HideUnlisted && gen.SelectedConfiguration.LeadInformation.HideUnlisted == "false")
            hideUnlisted = false;

        if (!hideUnlisted) {
            //• ADD REMAINING ITEMS
            var mitem;
            $.each(data, function (key, value) {
                for (var i = 0; i < _leads.leadMetadata.length; i++) {
                    mitem = _leads.leadMetadata[i];
                    if (mitem.name == key) {
                        if (mitem.type == "date" && value != "")
                            value = new Date(value).toString(gen.common.DATE_FORMAT);

                        switch (mitem.type) {
                            case "textarea":
                            case "textbox":
                                {
                                    context.addInput($leadInformationForm, key, mitem.displayName, value, false, mitem.compulsory, mitem.type, null, null);
                                }
                                break;

                            case "combo":
                                {
                                    context.addInput($leadInformationForm, key, mitem.displayName, value, false, mitem.compulsory, mitem.type, mitem.values, null);
                                }
                                break;
                        }
                    }
                }
            });
        }
    },

    getLeadStatusKeyLocalizationKey: function (key, defaultLocalizationKey) {
        if (gen.SelectedConfiguration.LeadStatus) {
            for (var counter = 0; counter < gen.SelectedConfiguration.LeadStatus.Status.length; counter++) {
                if (gen.SelectedConfiguration.LeadStatus.Status[counter].Key == key)
                    return gen.SelectedConfiguration.LeadStatus.Status[counter].LocalizationKey;
            }
            return defaultLocalizationKey;
        } else
            return defaultLocalizationKey;
    },

    getLeadStatusKeyCaption: function (key, defaultCaption) {
        if (gen.SelectedConfiguration.LeadStatus) {
            for (var counter = 0; counter < gen.SelectedConfiguration.LeadStatus.Status.length; counter++) {
                if (gen.SelectedConfiguration.LeadStatus.Status[counter].Key == key)
                    return gen.SelectedConfiguration.LeadStatus.Status[counter].Caption;
            }
            return defaultCaption;
        } else
            return defaultCaption;
    },

    getLeadStatusKeyBackgroundColor: function (key, defaultColor) {
        if (gen.SelectedConfiguration.LeadStatus) {
            for (var counter = 0; counter < gen.SelectedConfiguration.LeadStatus.Status.length; counter++) {
                if (gen.SelectedConfiguration.LeadStatus.Status[counter].Key == key)
                    return gen.SelectedConfiguration.LeadStatus.Status[counter].BackgroundColor;
            }
            return defaultColor;
        } else
            return defaultColor;
    },

    addInput: function ($container, name, label, text, readOnlyInput, requiredInput, inputType, possibleValues, backgroundColor, cfg) {
        var visible = true;
        var hideIfEmpty = false;
        var setting = gen.SelectedConfiguration.LeadInformation[name];

        var cssLabel = 'style="';
        var cssControl = 'style="';
        if (cfg) {
            cssLabel += cfg.Bold ? 'font-weight:bold;' : '';
            cssLabel += cfg.Italic ? 'font-style:italic;' : '';
            cssLabel += cfg.Underline ? 'text-decoration:underline;' : '';
            cssLabel += cfg.Color ? 'color:' + cfg.Color + ';' : '';
            cssLabel += cfg.NoShadow ? 'text-shadow:0 0 0;' : '';

            cssControl += cfg.Bold ? 'font-weight:bold;' : '';
            cssControl += cfg.Italic ? 'font-style:italic;' : '';
            cssControl += cfg.Underline ? 'text-decoration:underline;' : '';
            cssControl += cfg.Color ? 'color:' + cfg.Color + ';' : '';
            var bgCol = backgroundColor || cfg.BackColor; // backgroundColor takes preference.
            cssControl += bgCol ? 'background:' + bgCol + ';' : '';
            cssControl += cfg.NoShadow ? 'text-shadow:0 0 0;' : '';
        } else {
            cssControl += backgroundColor ? 'background:' + backgroundColor + ';' : '';
        }

        cssLabel += '"';
        cssControl += '"';

        if (setting) {
            if (setting.Visible && setting.Visible == "false")
                visible = false;

            if (setting.HideIfEmpty && setting.HideIfEmpty == "true")
                hideIfEmpty = true;

            if (setting.InputType && setting.InputType.length > 0)
                inputType = setting.InputType;
        }

        if (name == "leadStatus") {
            if (gen.SelectedConfiguration.LeadStatus) {
                if (gen.SelectedConfiguration.LeadStatus[text])
                    text = gen.SelectedConfiguration.LeadStatus[text];
            }
        }

        if (hideIfEmpty && (text === undefined || text == null || text == ""))
            visible = false;

        if (_feedback.capture && (setting && setting.Capture == "true"))
            visible = true;

        if (visible) {
            //• NEW ARRAY FROM HOLDING PROPERTIES TO BE UPDATED BACK TO API ON SUBMIT
            if (!readOnlyInput) {
                var updatetableProperty = {};
                updatetableProperty.Property = name;
                updatetableProperty.Type = inputType;
                updatetableProperty.PropertyType = "Lead";

                _feedback.lead_existing.push(updatetableProperty);
            }
            //• NEW ARRAY FROM HOLDING PROPERTIES TO BE UPDATED BACK TO API ON SUBMIT

            var readOnlyText = '';
            if (readOnlyInput)
                readOnlyText = 'readonly="readonly"';

            var requiredText = '';
            var requiredAttribute = '';
            if (requiredInput) {
                requiredText = "<span class='comp'>*</span>";
                requiredAttribute = 'required="required"';
            }

            //• GET TRANSLATED LABEL
            label = _common.getLocalization(name, label);

            var controlID = "";
            if (readOnlyInput)
                controlID = name + "Label";
            else
                controlID = name + "Input";

            var $field = $('<div data-role="fieldcontain" class="ui-field-contain"/>');
            $field.append('<label for="' + controlID + '" class="select" ' + cssLabel + '>' + label + requiredText + '</label>');

            switch (inputType) {
                case "url":
                    {
                        var $fieldSet = $('<fieldset data-role="controlgroup" class="urlButton">');
                        $fieldSet.append($('<a id="' + controlID + 'Button" class="darkButton" href="#" onclick="_feedback.onLinkClick(&quot;' + text + '&quot;);" data-role="button">' + label + '</a>'));

                        var $field2 = $('<div id="' + name + 'InputSet" data-role="fieldcontain"/>');
                        $field2.append($fieldSet);
                        $container.append($field2);
                    }
                    break;

                case "boolean":
                    {
                        var $fieldSet = $('<fieldset data-role="controlgroup">');
                        $fieldSet.append($('<legend>' + label + requiredText + '</legend>'));

                        $fieldSet.append($('<input type="checkbox" name="' + controlID + '" id="' + controlID + '" class="custom"/>'));
                        $fieldSet.append($('<label for="' + controlID + '" class="checkBoxLabel" ' + cssLabel + '>' + label + '</label>'));

                        var $field2 = $('<div data-role="fieldcontain"/>');
                        $field2.append($fieldSet);
                        $container.append($field2);
                    }
                    break;

                case "date":
                    {
                        var $input = $('<input type="text" id="' + controlID + '" class="ui-input-text ui-body-d ui-corner-all ui-shadow-inset ui-mini" value="' + text + '" ' + readOnlyText + '/>');
                        var setText = _common.getLocalization("set", "Set");
                        var cancelText = _common.getLocalization("cancel", "Cancel");

                        $input.scroller({
                            preset: 'datetime',
                            dateFormat: gen.common.DATE_PICKER_FORMAT,
                            dateOrder: gen.common.DATE_PICKER_FORMAT,
                            showNow: false,
                            theme: 'jqm',
                            display: 'bubble',
                            mode: 'mixed',
                            setText: setText,
                            cancelText: cancelText
                        });

                        $field.append($input);
                        $container.append($field);
                    }
                    break;

                case "textarea":
                    {
                        $field.append('<textarea id="' + controlID + '" class="ui-input-text ui-body-d ui-corner-all ui-shadow-inset ui-mini" ' + ' ' + cssControl + readOnlyText + '>' + text + '</textarea>');
                        $container.append($field);
                    }
                    break;

                case "textbox":
                    {
                        if (text === undefined || text === null || text === "null")
                            text = "";

                        var dialClass = "";
                        if (cfg && cfg.Dial == "true") {
                            var transparentColor = "";
                            if (gen.common.MaskDialNumbers)
                                transparentColor = 'style="color:transparent;"';

                            $field.append('<input ' + transparentColor + ' type="text" id="' + controlID + '" class="ui-input-text ui-body-d ui-corner-all ui-shadow-inset ui-mini dialInput" value="' + text + '" ' + readOnlyText + ' ' + cssControl + '/>');

                            if (text && (text.length > 7) && gen.common.MaskDialNumbers)
                                $field.append('<span class="dialMask">' + text.substring(0, 7) + '....</span>');
                        } else
                            $field.append('<input type="text" id="' + controlID + '" class="ui-input-text ui-body-d ui-corner-all ui-shadow-inset ui-mini" value="' + text + '" ' + readOnlyText + ' ' + cssControl + '/>');

                        $container.append($field);

                        $('.dialInput').off().on("click", $.proxy(this.onDialClick, this));
                    }
                    break;

                case "combo":
                    {
                        if (possibleValues) {
                            var idToSelect = 0;
                            var possibleValuesArray = [];
                            for (index = 0; index < possibleValues.length; ++index) {
                                possibleValuesArray.push({
                                    id: index + 1,
                                    text: possibleValues[index]
                                });
                                if (text.toUpperCase() == possibleValues[index].toUpperCase())
                                    idToSelect = index + 1;
                            }

                            var $input = $('<input id="' + controlID + '" type="hidden" style="width:78%;" ' + requiredAttribute + '/>');
                            $field.append($input);
                            $container.append($field);
                            $input.select2({
                                placeholder: "Select " + label,
                                data: possibleValuesArray
                            });
                            $input.select2("val", idToSelect);
                        }
                    }
                    break;
            }
        }
    },

    onLinkClick: function (url) {
        //if (device.platform == "Android")
        //    navigator.app.loadUrl(url, {openExternal : true});
        //else
        window.open(url, '_system', 'location=yes');
    },

    onDialClick: function (evt) {
        var cliNumber = null;

        $.each(gen.salesPeople, function (i, item) {
            if (item.suppliedId == gen.selectedLead.salesPerson.suppliedId) {
                if (item.cellNumber)
                    cliNumber = item.cellNumber;
            }
        });

        if (!cliNumber)
            cliNumber = gen.common.TwilioCLI;

        if (gen.common.PHONEGAP) {
            if (gen.DEV === true) {
                var device = {};
                device.platform = "";
            }

            /*
                        if (device.platform === "Android?")
                        {
                            var numberProperty = evt.target.id.replace("Label", "");
                            var callId = gen.selectedCampaign.id + "/" + gen.selectedLead.id + "/" + numberProperty + "/" + gen.selectedCampaignConfiguration.CountryCode;
                            var call = "?tocall=" + callId;

                            var returnURL = "";
                            if (gen.selectedCampaignConfiguration.DialReturnURL)
                                returnURL = "&returnUrl=" + gen.selectedCampaignConfiguration.DialReturnURL

                            var cli = "&cliNumber=" + cliNumber;
                            var fullURL = gen.common.TwilioURLAndroid + call + returnURL + cli;
                            navigator.app.loadUrl(fullURL, { openExternal: true });
                        }
                        else
            */
            {
                //• FIND DIAL PAGE INFO
                var dialInfo = [];
                var dialInfoPart = {};
                dialInfoPart.Label = "Number";
                dialInfoPart.Value = evt.target.value;
                dialInfo.push(dialInfoPart);

                for (var counter = 1; counter <= 5; counter++) {
                    var position = "Position" + counter;
                    if (gen.SelectedConfiguration && gen.SelectedConfiguration.DialPage && gen.SelectedConfiguration.DialPage[position]) {
                        if (gen.SelectedConfiguration.DialPage[position].Visible && gen.SelectedConfiguration.DialPage[position].Visible == "true") {
                            if (gen.SelectedConfiguration.DialPage[position].Property && gen.SelectedConfiguration.DialPage[position].Property != "") {
                                if (gen.SelectedConfiguration.DialPage[position].Property != "leadStatusDescription") {
                                    if (gen.selectedLead[gen.SelectedConfiguration.DialPage[position].Property] && gen.selectedLead[gen.SelectedConfiguration.DialPage[position].Property] != "") {
                                        var localizedCaption = _common.getLocalization(gen.SelectedConfiguration.DialPage[position].LocalizationKey, gen.SelectedConfiguration.DialPage[position].Caption);
                                        dialInfoPart = {};
                                        dialInfoPart.Label = localizedCaption;

                                        if (gen.SelectedConfiguration.DialPage[position].Property.indexOf("Date") == -1)
                                            dialInfoPart.Value = gen.selectedLead[gen.SelectedConfiguration.DialPage[position].Property];
                                        else
                                            dialInfoPart.Value = new Date(gen.selectedLead[gen.SelectedConfiguration.DialPage[position].Property]).toString(gen.common.DATE_TIME_FORMAT);

                                        dialInfo.push(dialInfoPart);
                                    }
                                }
                            }
                        }
                    }
                }


                _twilio.init(dialInfo, cliNumber);
                $("#navToDial").trigger("click");
            }
        } else {
            if (evt.target.value && evt.target.value != "null") {
                var numberProperty = evt.target.id.replace("Label", "");
                var callId = gen.selectedCampaign.id + "/" + gen.selectedLead.id + "/" + numberProperty + "/" + gen.selectedCampaignConfiguration.CountryCode;
                var call = "?tocall=" + callId;

                var returnURL = "";
                if (gen.selectedCampaignConfiguration.DialReturnURL)
                    returnURL = "&returnUrl=" + gen.selectedCampaignConfiguration.DialReturnURL

                var cli = "&cliNumber=" + cliNumber;

                var fullURL = gen.common.TwilioURL + call + returnURL + cli;
                window.open(fullURL, '_blank');
            }
        }
    },

    addSeperator: function ($container) {
        var $field = $('<div data-role="fieldcontain"/>');
        $field.append('<hr>');
        $container.append($field);
    },

    buildFeedbackHelper: function (data, $container) {
        var i, j, item, comp, hasValues;
        comp = "<span class='comp'>*</span>";

        // Create the inputs if any.
        if (data && data.length > 0) {
            var title = _common.getLocalization("feedback", "Feedback")
            $container.append("<h3>" + title + "</h3>");

            var $feedbackForm = $('<form id="form_Feedback"/>');
            $container.append($feedbackForm);
            _feedback.attachVerify($feedbackForm);

            var $detailsForm2 = $('<form id="form_Details2"/>');
            $container.append($detailsForm2);
            _feedback.attachVerify($detailsForm2);

            var passOnContainer = $('<div id="passOnHolder" data-role="fieldcontain" style="display:none;"/>');
            $detailsForm2.append(passOnContainer);

            //• ADD LISTED FEEDBACK OPTIONS FIRST
            if (gen.SelectedConfiguration.Feedback) {
                var optionsAdded = false;
                var detailsAdded = false;
                var detailCheckGroupsAdded = false;
                var detailGroupsAdded = false;

                for (var addCounter = 0; addCounter < 10; addCounter++) {
                    if (gen.SelectedConfiguration.Feedback.Options && gen.SelectedConfiguration.Feedback.Options.Order && parseInt(gen.SelectedConfiguration.Feedback.Options.Order) == addCounter) {
                        optionsAdded = true;
                        this.buildFeedbackOptionsHelper($feedbackForm, title, comp, data);
                    }

                    if (gen.SelectedConfiguration.Feedback.Details && gen.SelectedConfiguration.Feedback.Details.Order && parseInt(gen.SelectedConfiguration.Feedback.Details.Order) == addCounter) {
                        detailsAdded = true;
                        this.buildFeedbackDetailsHelper($container);
                    }

                    if (gen.SelectedConfiguration.Feedback.DetailCheckGroups && gen.SelectedConfiguration.Feedback.DetailCheckGroups.Order && parseInt(gen.SelectedConfiguration.Feedback.DetailCheckGroups.Order) == addCounter) {
                        detailCheckGroupsAdded = true;
                        this.buildFeedbackDetailCheckGroupsHelper($container);
                    }

                    if (gen.SelectedConfiguration.Feedback.DetailGroups && gen.SelectedConfiguration.Feedback.DetailGroups.Order && parseInt(gen.SelectedConfiguration.Feedback.DetailGroups.Order) == addCounter) {
                        detailGroupsAdded = true;
                        this.buildFeedbackDetailGroupsHelper($container);
                    }
                }

                if (!optionsAdded)
                    this.buildFeedbackOptionsHelper($feedbackForm, title, comp, data);

                if (!detailsAdded)
                    this.buildFeedbackDetailsHelper($container);

                if (!detailCheckGroupsAdded)
                    this.buildFeedbackDetailCheckGroupsHelper($container);

                if (!detailGroupsAdded)
                    this.buildFeedbackDetailGroupsHelper($container);
            }


            // Add the date.
            $field = $('<div id="optionDateHolder" data-role="fieldcontain" style="display:none;"/>');
            title = _common.getLocalization("reasonDate", "Date");
            $field.append('<label for="optionDate" class="select">' + title + comp + '</label>');
            $input = $('<input type="text" name="optionDate" id="optionDate" data-mini="true" placeholder="' + title + '"/>');
            $field.append($input);
            var now = new Date();
            var setText = _common.getLocalization("set", "Set");
            var cancelText = _common.getLocalization("cancel", "Cancel");

            // Set 'n max future date based on the campaign setting.
            var cp;
            for (var ci = 0; ci < gen.CampaignConfiguration.Campaigns.Campaign.length; ci++) {
                cp = gen.CampaignConfiguration.Campaigns.Campaign[ci];
                if (cp.Id == gen.selectedCampaign.id)
                    break;
            };
            var addYear = 0,
                addMonth = 0,
                addDay = 0;
            if (cp.AppointmentMaxDate && cp.AppointmentMaxDate != "") {
                var num = cp.AppointmentMaxDate.match(/\d+/g);
                if (num && num.length > 0) {
                    var n = parseInt(num[0])
                    if (cp.AppointmentMaxDate.indexOf("y") > -1)
                        addYear = n;
                    if (cp.AppointmentMaxDate.indexOf("m") > -1)
                        addMonth = n;
                    if (cp.AppointmentMaxDate.indexOf("d") > -1)
                        addDay = n;
                }
            }
            // Default.
            if (addYear === 0 && addMonth === 0 && addDay === 0) {
                addYear = 1;
            }

            $input.scroller({
                preset: 'datetime',
                minDate: new Date(now.getFullYear(), now.getMonth(), now.getDate()),
                maxDate: new Date(now.getFullYear() + addYear, now.getMonth() + addMonth, now.getDate() + addDay),
                dateFormat: gen.common.DATE_PICKER_FORMAT,
                dateOrder: gen.common.DATE_PICKER_FORMAT,
                showNow: false,
                theme: 'jqm',
                display: 'bubble',
                mode: 'mixed',
                setText: setText,
                cancelText: cancelText,
                onSelect: function (valueText, inst) {
                    _feedback.selectedOptionDate = inst.getDate(inst.values)
                },
                buttons: [
                    'set',
                    {
                        text: 'Custom',
                        icon: 'checkmark',
                        cssClass: 'my-btn'
                    },
                    'cancel'
                ]
            });

            $detailsForm2.append($field);

            // Add the reason.
            $field = $('<div id="optionReasonHolder" data-role="fieldcontain" style="display:none;"/>');
            var title = _common.getLocalization("optionReason", "Reason");
            $field.append('<label for="reasonReason" class="select">' + title + comp + '</label>');
            $field.append('<select name="reasonReason" id="reasonReason" data-mini="true" placeholder="' + title + '" required/>');
            $detailsForm2.append($field);

            // Add the comment.
            var commentsVisible = true;
            if (gen.SelectedConfiguration.Feedback.Comments && gen.SelectedConfiguration.Feedback.Comments.Visible && gen.SelectedConfiguration.Feedback.Comments.Visible == "false")
                commentsVisible = false;

            if (commentsVisible) {
                $field = $('<div data-role="fieldcontain"/>');
                var title = _common.getLocalization("comment", "Comment");
                $field.append('<label for="reasonComment" class="select">' + title + '</label>');
                $field.append('<textarea name="reasonComment" id="reasonComment" data-mini="true" placeholder="' + title + '"></textarea>');
                $detailsForm2.append($field);
            }

            //• ADD PASS-ON CAMPAIGN
            $field = $("#passOnHolder");
            title = _common.getLocalization("passOn", "Pass On")
            $field.append("<h3>" + title + "</h3>");

            $fieldSet = $('<div data-role="fieldcontain" class="ui-field-contain">');
            title = _common.getLocalization("campaign", "Campaign");
            $fieldSet.append('<label for="passOnCampaign" class="select">' + title + '</label>');
            var $select = $('<select name="passOnCampaign" id="passOnCampaign" data-mini="true" placeholder="' + title + '" required/>');
            $fieldSet.append($select);
            $field.append($fieldSet);

            if (gen.CampaignConfiguration.Campaigns.length > 1) {
                title = _common.getLocalization("selectCampaign", "Please Select Campaign");
                $select.append('<option value="0">' + title + '</option>');
            }

            $.each(gen.CampaignConfiguration.Campaigns.Campaign, function (i, item) {
                if (item.AllowPassOn && item.AllowPassOn == "true") {
                    var campaignID = item.Id;
                    var campaignDescription = "Campaign " + campaignID;
                    if (item.DescriptionLocalizationKey)
                        campaignDescription = _common.getLocalization(item.DescriptionLocalizationKey, campaignDescription);

                    $select.append('<option value="' + campaignID + '">' + campaignDescription + '</option>');
                }
            });

            //• ADD PASS-ON SALES PERSON
            title = _common.getLocalization("salesPerson", "Sales Person");
            $field.append('<label for="passOnSalesPerson" class="select">' + title + '</label>');
            $select = $('<select name="passOnSalesPerson" id="passOnSalesPerson" data-mini="true" placeholder="' + title + '" required/>');
            $field.append($select);

            title = _common.getLocalization("selectSalesPerson", "Please Select Sales Person");
            $select.append('<option value="0">' + title + '</option>');

            _feedback.sortByKey(gen.salesPeople, 'lastName');

            $.each(gen.salesPeople, function (i, item) {
                var addSalesPerson = false;
                if (_feedback.passingOnSubordinates) {
                    if (_feedback.subordinatesList[gen.LoggedInUserId]) {
                        if (_feedback.subordinatesList[gen.LoggedInUserId].indexOf(item.suppliedId) >= 0) {
                            addSalesPerson = true;

                            if (_feedback.passingOnIDs.length > 0) {
                                if (_feedback.passingOnIDType == "Include")
                                    addSalesPerson = false;

                                $.each(_feedback.passingOnIDs, function (i, idItem) {
                                    if (_feedback.passingOnIDType == "Include") {
                                        if (_feedback.passingOnIDPosition == "StartsWith") {
                                            if (item.suppliedId.match("^" + idItem))
                                                addSalesPerson = true;
                                        } else {
                                            if (item.suppliedId.match(idItem + "$"))
                                                alloaddSalesPersonwed = true;
                                        }
                                    } else {
                                        if (_feedback.passingOnIDPosition == "StartsWith") {
                                            if (item.suppliedId.match("^" + idItem))
                                                addSalesPerson = false;
                                        } else {
                                            if (item.suppliedId.match(idItem + "$"))
                                                addSalesPerson = false;
                                        }
                                    }
                                });
                            }
                        }
                    }
                } else
                    addSalesPerson = true;

                if (addSalesPerson)
                    $select.append('<option value="' + item.suppliedId + '">' + item.firstName + " " + item.lastName + ' (' + item.suppliedId + ')' + '</option>');
            });

            for (counter = 0; counter < _feedback.lead_new.length; counter++) {
                var updatetableProperty = _feedback.lead_new[counter];
                var detailItem = updatetableProperty.DetailItem;

                var detailCaption = detailItem.Caption;
                if (detailItem.LocalizationKey)
                    detailCaption = _common.getLocalization(detailItem.LocalizationKey, detailItem.Caption);

                var detailName = updatetableProperty.Property;

                var currentValue = "";
                if (detailItem.PropertyType && detailItem.PropertyType == "Feedback") {
                    if (_feedback.lead.matches && _feedback.lead.matches.length > 0 && _feedback.lead.matches[0][detailName] !== undefined)
                        currentValue = _feedback.lead.matches[0][detailName];
                } else {
                    if (_feedback.lead[detailName])
                        currentValue = _feedback.lead[detailName];
                }

                var requiredText = "";
                var requiredAttribute = "";
                if (detailItem.Required && detailItem.Required.length > 0) {
                    requiredText = "<span id='" + detailName + "Required' class='comp'>*</span>";
                    requiredAttribute = 'required="required"';
                }

                if (detailItem.Validation && detailItem.Validation.contains("required"))
                    requiredText = "<span id='" + detailName + "Required' class='comp'>*</span>";

                var id = detailName + "Input2";
                var id2 = detailName + "InputSet2";
                $field2 = $('<div id="' + id2 + '" data-role="fieldcontain" class="ui-field-contain"/>');
                $field2.append('<label for="' + id + '">' + detailCaption + requiredText + '</label>');
                $field.append($field2);

                switch (updatetableProperty.Type) {
                    case "boolean":
                        {
                            _feedback.appendFeedbackDetailBoolean(detailItem, $field, id, detailCaption, currentValue);
                        }
                        break;
                    case "date":
                        {
                            _feedback.appendFeedbackDetailDate(detailItem, $field, id, detailCaption, requiredAttribute);
                        }
                        break;
                    case "textarea":
                        {
                            _feedback.appendFeedbackDetailTextArea($field, id, requiredAttribute, text);
                        }
                        break;
                    case "textbox":
                        {
                            _feedback.appendFeedbackDetailTextbox(detailItem, $field, id, detailCaption, currentValue, requiredAttribute);
                        }
                        break;
                    case "combo":
                        {
                            if (detailItem.Values)
                                _feedback.appendFeedbackDetailCombo(detailItem, $field, id);
                        }
                        break;
                }
            }

            var context = this;
            this.ui.optionSet.find("input[type='radio']").on("change", function (e) {
                var $radio = $(e.target).closest('input[name="feedOptions"]');

                if ($radio.attr("data-date") == "true")
                    $("#optionDateHolder").fadeIn();
                else
                    $("#optionDateHolder").hide();

                //• SHOW DETAILS FOR SELECTED FEEDBACK OPTION
                var detailsToShow = false;
                if (gen.SelectedConfiguration.Feedback.Details) {
                    $.each(gen.SelectedConfiguration.Feedback.Details, function (details, detailItem) {
                        if (detailItem.OptionTypes && detailItem.OptionTypes.indexOf($radio.attr("value")) != -1) {
                            detailsToShow = true;
                            $("#" + details + "InputSet").fadeIn();

                            if (detailItem.Required) {
                                if (detailItem.Required.toUpperCase() == "TRUE") {
                                    $("#" + details + "Required").fadeIn();
                                } else if (detailItem.Required.toUpperCase() == "FALSE") {
                                    $("#" + details + "Required").fadeOut();
                                } else {
                                    if (detailItem.Required.indexOf($radio.val()) != -1)
                                        $("#" + details + "Required").fadeIn();
                                    else
                                        $("#" + details + "Required").fadeOut();
                                }
                            }
                        } else
                            $("#" + details + "InputSet").hide();
                    });
                }

                if (detailsToShow == true)
                    $("#detailSet").fadeIn();
                else
                    $("#detailSet").hide();

                //• SHOW DETAILS GROUPS FOR SELECTED FEEDBACK OPTION

                //• DETAIL CHECK GROUPS
                var detailCheckGroupsToShow = false;
                if (gen.SelectedConfiguration.Feedback.DetailCheckGroups) {
                    $.each(gen.SelectedConfiguration.Feedback.DetailCheckGroups, function (details, detailItem) {
                        if (detailItem.OptionTypes && detailItem.OptionTypes.indexOf($radio.attr("value")) != -1)
                            detailCheckGroupsToShow = true;
                    });
                }

                if (detailCheckGroupsToShow == true)
                    $("#detailCheckGroupSet").fadeIn();
                else
                    $("#detailCheckGroupSet").hide();

                //• DETAIL GROUPS
                var detailGroupsToShow = false;
                if (gen.SelectedConfiguration.Feedback.DetailGroups) {
                    $.each(gen.SelectedConfiguration.Feedback.DetailGroups, function (details, detailItem) {
                        if (detailItem.OptionTypes && detailItem.OptionTypes.indexOf($radio.attr("value")) != -1)
                            detailGroupsToShow = true;
                    });
                }

                if (detailGroupsToShow == true)
                    $("#detailGroupSet").fadeIn();
                else
                    $("#detailGroupSet").hide();

                var idx = parseInt($radio.attr("data-id"));
                var selectedOption = data[idx];

                if ($radio.attr("data-values") == "true") {
                    var $select = $("#reasonReason").empty();
                    var selecReasonLocalization = _common.getLocalization("selectReason", "Select Reason");
                    $select.append('<option value="">' + selecReasonLocalization + '</option>');

                    /* NEW */

                    if (gen.SelectedConfiguration.Feedback) {
                        var options = gen.SelectedConfiguration.Feedback.Options;

                        if (options) {
                            $.each(options.FeedbackType, function (options, optionItem) {
                                if (optionItem.Type == selectedOption.feedbackType) {
                                    $.each(optionItem.Reasons.Reason, function (reasons, reasonItem) {
                                        var localizedReason = _common.getLocalization(reasonItem.LocalizationKey, reasonItem.Caption);
                                        $select.append('<option value="' + localizedReason + '">' + localizedReason + '</option>');
                                    });
                                }
                            });
                        }
                    }

                    $select.selectmenu('refresh');
                    $("#optionReasonHolder").fadeIn();
                } else {
                    $("#optionReasonHolder").hide();
                }

                //• PASS ON
                if (gen.SelectedConfiguration.Feedback) {
                    var options = gen.SelectedConfiguration.Feedback.Options;

                    if (options) {
                        $.each(options.FeedbackType, function (options, optionItem) {
                            if (optionItem.Type == selectedOption.feedbackType) {
                                if (optionItem.PassOn && optionItem.PassOn == "true") {
                                    _feedback.passingOn = true;
                                    $("#passOnHolder").fadeIn();
                                } else {
                                    _feedback.passingOn = false;
                                    $("#passOnHolder").fadeOut();
                                }
                            }
                        });
                    }
                }
            });
        }
    },

    sortByKey: function (array, key) {
        return array.sort(function (a, b) {
            var x = a[key];
            var y = b[key];

            if (typeof x == "string")
                x = x.toLowerCase();
            if (typeof y == "string")
                y = y.toLowerCase();

            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    },

    buildFeedbackOptionsHelper: function ($feedbackForm, title, comp, data) {
        try {
            var options = gen.SelectedConfiguration.Feedback.Options;

            if (options) {
                var context = this;

                var $field = $('<div data-role="fieldcontain"/>');
                context.ui.optionSet = $('<fieldset data-role="controlgroup" data-mini="true"/>');
                context.ui.optionSet.append($('<legend>' + title + comp + '</legend>'));

                $.each(options.FeedbackType, function (options, optionItem) {
                    var feedbackType = options;

                    var feedbackTypeVisible = true;
                    if (optionItem.Visible && optionItem.Visible == "false")
                        feedbackTypeVisible = false;

                    // REQUIRED PROPERTY
                    if (feedbackTypeVisible && optionItem.RequiredProperty && optionItem.RequiredProperty != "") {
                        if (_feedback.selectedLead && !_feedback.selectedLead[optionItem.RequiredProperty]) {
                            feedbackTypeVisible = false;
                        } else {
                            if (optionItem.RequiredPastDate && optionItem.RequiredPastDate == "true") {
                                if (_feedback.selectedLead && _feedback.selectedLead[optionItem.RequiredProperty]) {
                                    var now = new Date();
                                    var itemDate = new Date(_feedback.selectedLead[optionItem.RequiredProperty]);
                                    if (itemDate > now)
                                        feedbackTypeVisible = false;
                                }
                            }
                        }
                    }

                    // EXCLUDED BY PROPERTY
                    if (feedbackTypeVisible && optionItem.ExcludedProperty && optionItem.ExcludedProperty != "") {
                        if (_feedback.selectedLead && _feedback.selectedLead[optionItem.ExcludedProperty]) {
                            if (optionItem.ExcludedPastDate && optionItem.ExcludedPastDate == "true") {
                                var now = new Date();
                                var itemDate = new Date(_feedback.selectedLead[optionItem.ExcludedProperty]);
                                if (itemDate < now)
                                    feedbackTypeVisible = false;
                            } else
                                feedbackTypeVisible = false;
                        }
                    }

                    if (feedbackTypeVisible) {
                        for (var innerCounter = 0; innerCounter < data.length; innerCounter++) {
                            var item = data[innerCounter];
                            if (item.feedbackType == optionItem.Type) {
                                if (feedbackTypeVisible == true) {
                                    hasValues = false;
                                    if (optionItem.Reasons) {
                                        if (optionItem.Reasons.Reason.length > 0)
                                            hasValues = true;
                                    }

                                    context.ui.optionSet.append('<input type="radio" name="feedOptions" tabindex="-1" id="option' + innerCounter + '" value="' + item.feedbackType + '" data-id="' + innerCounter + '" data-date="' + item.requiresDate + '" data-values="' + hasValues + '" selected="false"/>');

                                    var localizedLabel = item.displayName;
                                    if (optionItem.LocalizationKey)
                                        localizedLabel = _common.getLocalization(optionItem.LocalizationKey, localizedLabel);

                                    context.ui.optionSet.append('<label for="option' + innerCounter + '">' + localizedLabel + '</label>');
                                }

                                break;
                            }
                        }
                    }
                });
            }

            $field.append(this.ui.optionSet);
            $feedbackForm.append($field);
        } catch (error) {
            console.log(error);
        }
    },

    buildFeedbackDetailsHelper: function ($container) {
        try {
            //• DETAILS
            if (gen.SelectedConfiguration.Feedback.Details) {
                var details = gen.SelectedConfiguration.Feedback.Details;

                var $detailsForm = $('<form id="form_Details"/>');
                $container.append($detailsForm);
                _feedback.attachVerify($detailsForm);

                var $detailSet = $('<div id="detailSet" style="display:none;" data-mini="true"/>');
                $.each(details, function (detailName, detailItem) {
                    _feedback.appendFeedbackDetail($detailSet, detailName, detailItem, false, detailItem.OptionTypes);
                });

                $detailsForm.append($detailSet);
            }
        } catch (error) {
            console.log(error);
            _common.showNotification("An error has occured loading the Feedback page");
        }
    },

    buildFeedbackDetailGroupsHelper: function ($container) {
        try {
            //• DETAIL GROUPS
            if (gen.SelectedConfiguration.Feedback.DetailGroups) {
                var detailGroups = gen.SelectedConfiguration.Feedback.DetailGroups;

                var counter = 0;
                var $detailGroupSet = $('<div id="detailGroupSet" style="display:none;" data-mini="true"/>');
                $detailGroupSet.append($('<hr/>'));

                var firstGroup = true;
                $.each(detailGroups, function (detailGroupName, detailGroupItem) {
                    if (detailGroupName != "Order") {
                        var groupVisible = counter == 0 ? '' : 'style="display:none;"';
                        var $detailGroup = $('<div id="detailGroup_' + detailGroupName + '" ' + groupVisible + ' data-mini="true"/>');

                        var collapseButton = '';
                        if (!firstGroup)
                            collapseButton = '<div style="float:right;margin-top:-6px;"><a data-role="button" style="margin:0 4px 0 0;padding:.4em .8em;" onClick="_feedback.clearInputs($(detailGroup_' + detailGroupName + ')); $(detailGroup_' + detailGroupName + ').fadeOut();">-</a></div>';

                        var expandButton = '';
                        if (detailGroupItem.Shows)
                            expandButton = '<div style="float:right;margin-top:-6px;"><a data-role="button" style="margin:0;padding:.4em .8em;" onClick="_feedback.expandIfValidated($(form_' + detailGroupName + '), $(detailGroup_' + detailGroupItem.Shows + '));">+</a></div>';

                        var localizedGroupCaption = detailGroupItem.Caption;
                        if (detailGroupItem.LocalizationKey)
                            localizedGroupCaption = _common.getLocalization(detailGroupItem.LocalizationKey, detailGroupItem.Caption);

                        var $detailGroupHeader = $('<div class="forLabel" style="font-variant:small-caps;">' + localizedGroupCaption + expandButton + collapseButton + '</div>');
                        $detailGroup.append($detailGroupHeader);
                        counter++;

                        $.each(detailGroupItem, function (detailName, detailItem) {
                            if (typeof (detailItem) == "object")
                                _feedback.appendFeedbackDetail($detailGroup, detailName, detailItem, false, detailGroupItem.OptionTypes);
                        });
                        $detailGroup.append($('<hr/>'));
                        $detailGroupSet.append($detailGroup);

                        var $detailGroupForm = $('<form id="form_' + detailGroupName + '"/>');
                        $detailGroupForm.append($detailGroup);
                        $detailGroupSet.append($detailGroupForm);
                        _feedback.attachVerify($detailGroupForm);
                        firstGroup = false;
                    }
                });

                $container.append($detailGroupSet);
            }
        } catch (error) {
            console.log(error);
        }
    },

    buildFeedbackDetailCheckGroupsHelper: function ($container) {
        try {
            //• DETAIL CHECK GROUPS
            if (gen.SelectedConfiguration.Feedback.DetailCheckGroups) {
                var detailCheckGroups = gen.SelectedConfiguration.Feedback.DetailCheckGroups;

                var counter = 0;
                var $detailCheckGroupSet = $('<div id="detailCheckGroupSet" style="display:none;" data-mini="true"/>');
                $detailCheckGroupSet.append($('<hr/>'));

                $.each(detailCheckGroups, function (detailCheckGroupName, detailCheckGroupItem) {
                    if (detailCheckGroupName != "Order") {
                        var $detailCheckGroup = $('<div id="detailCheckGroup_' + detailCheckGroupName + '" data-mini="true"/>');

                        var localizedGroupCaption = detailCheckGroupItem.Caption;
                        if (detailCheckGroupItem.LocalizationKey)
                            localizedGroupCaption = _common.getLocalization(detailCheckGroupItem.LocalizationKey, detailCheckGroupItem.Caption);

                        var $detailCheckGroupHeader = $('<div class="forLabel" style="font-variant:small-caps;">' + localizedGroupCaption + '</div>');
                        $detailCheckGroup.append($detailCheckGroupHeader);
                        counter++;

                        $.each(detailCheckGroupItem, function (detailName, detailItem) {
                            if (typeof (detailItem) == "object")
                                _feedback.appendFeedbackDetail($detailCheckGroup, detailName, detailItem, true, detailCheckGroupItem.OptionTypes);
                        });
                        $detailCheckGroupSet.append($detailCheckGroup);

                        var $detailCheckGroupForm = $('<form id="form_Check' + detailCheckGroupName + '" requiredCount="' + detailCheckGroupItem.RequiredCount + '"/>');
                        $detailCheckGroupForm.append($detailCheckGroup);
                        $detailCheckGroupSet.append($detailCheckGroupForm);
                        _feedback.attachVerify2($detailCheckGroupForm);
                    }
                });

                $container.append($detailCheckGroupSet);
            }
        } catch (error) {
            console.log(error);
        }
    },

    expandIfValidated: function (containerToValidate, containerToExpand) {
        var context = this;
        gen.firstFail = true;
        containerToValidate.validate(function (result) {
            if (result == true)
                containerToExpand.fadeIn();
        });
    },

    clearInputs: function (container) {
        container.find(':input').each(function () {
            $(this).val("");
        });
    },

    appendFeedbackDetail: function ($detailSet, detailName, detailItem, isCheckGroup, optionTypes) {
        //• NEW ARRAY FROM HOLDING PROPERTIES TO BE UPDATED BACK TO API ON SUBMIT
        var updatetableProperty = {};
        updatetableProperty.DetailItem = detailItem;
        updatetableProperty.Property = detailName;
        updatetableProperty.Type = detailItem.Type;
        updatetableProperty.OptionTypes = optionTypes;

        if (detailItem.Value)
            updatetableProperty.Value = detailItem.Value;

        if (detailItem.PropertyType && detailItem.PropertyType == "Feedback")
            updatetableProperty.PropertyType = "Feedback";
        else
            updatetableProperty.PropertyType = "Lead";

        _feedback.lead_existing.push(updatetableProperty);

        if (detailItem.PassOn && detailItem.PassOn == "true")
            _feedback.lead_new.push(updatetableProperty);
        //• NEW ARRAY FROM HOLDING PROPERTIES TO BE UPDATED BACK TO API ON SUBMIT

        if (isCheckGroup && isCheckGroup == true) {
            _feedback.appendFeedbackDetailBooleanRequired(detailItem, $detailSet, detailName + 'Input', currentValue);

            if (detailItem.PassOn && detailItem.PassOn == "true")
                _feedback.appendFeedbackDetailBooleanRequired(detailItem, $("#passOnHolder"), detailName + 'Input2', currentValue);
        } else {
            var detailCaption = detailItem.Caption;
            if (detailItem.LocalizationKey)
                detailCaption = _common.getLocalization(detailItem.LocalizationKey, detailItem.Caption);

            var currentValue = "";

            if (detailItem.PropertyType && detailItem.PropertyType == "Feedback") {
                if (_feedback.lead.matches && _feedback.lead.matches.length > 0 && _feedback.lead.matches[0][detailName] !== undefined)
                    currentValue = _feedback.lead.matches[0][detailName];
            } else {
                if (_feedback.lead[detailName])
                    currentValue = _feedback.lead[detailName];
            }

            //• REQUIRED, INPUT TYPES
            var requiredText = "";
            var requiredAttribute = "";
            if (detailItem.Required && detailItem.Required.length > 0) {
                requiredText = "<span id='" + detailName + "Required' class='comp'>*</span>";
                requiredAttribute = 'required="required"';
            }

            if (detailItem.Validation && detailItem.Validation.contains("required"))
                requiredText = "<span id='" + detailName + "Required' class='comp'>*</span>";

            $field = $('<div id="' + detailName + 'InputSet" data-role="fieldcontain" class="ui-field-contain"/>');
            $field.append('<label for="' + detailName + 'Input">' + detailCaption + requiredText + '</label>');

            var inputType = "textbox";
            if (detailItem.Type)
                inputType = detailItem.Type;

            switch (inputType) {
                case "boolean":
                    {
                        _feedback.appendFeedbackDetailBoolean(detailItem, $detailSet, detailName + 'Input', detailCaption, currentValue);
                    }
                    break;
                case "date":
                    {
                        _feedback.appendFeedbackDetailDate(detailItem, $detailSet, detailName + 'Input', detailCaption, requiredAttribute);
                    }
                    break;
                case "textarea":
                    {
                        _feedback.appendFeedbackDetailTextArea($detailSet, detailName + 'Input', requiredAttribute, text);
                    }
                    break;
                case "textbox":
                    {
                        _feedback.appendFeedbackDetailTextbox(detailItem, $detailSet, detailName + 'Input', detailCaption, currentValue, requiredAttribute);
                    }
                    break;
                case "combo":
                    {
                        if (detailItem.Values)
                            _feedback.appendFeedbackDetailCombo(detailItem, $detailSet, detailName + 'Input');
                    }
                    break;
            }
        }
    },

    appendFeedbackDetailBoolean: function (detailItem, $detailSet, id, detailCaption, currentValue) {
        var $fieldSet = $('<fieldset data-role="controlgroup">');
        $fieldSet.append($('<legend>' + detailCaption + '</legend>'));

        var value = "true";
        if (detailItem.Value)
            value = detailItem.Value;

        if (detailItem.ValueLocalizationKey)
            value = _common.getLocalization(detailItem.ValueLocalizationKey, value);

        var checkedString = "";
        if (currentValue == value)
            checkedString = "checked";

        $fieldSet.append($('<input type="checkbox" name="' + id + '" id="' + id + '" class="custom" value="' + value + '" ' + checkedString + '/>'));
        $fieldSet.append($('<label for="' + id + '" class="checkBoxLabel">' + value + '</label>'));

        var $field2 = $('<div id="' + id + 'Set" data-role="fieldcontain"/>');
        $field2.append($fieldSet);
        $detailSet.append($field2);
    },

    appendFeedbackDetailBooleanRequired: function (detailItem, $detailSet, id, currentValue) {
        var $fieldSet = $('<fieldset data-role="controlgroup" class="detailCheckGroup">');

        var value = "true";
        if (detailItem.Value)
            value = detailItem.Value;

        if (detailItem.ValueLocalizationKey)
            value = _common.getLocalization(detailItem.ValueLocalizationKey, value);

        var checkedString = "";
        if (currentValue == value)
            checkedString = "checked";

        $fieldSet.append($('<input type="checkbox" name="' + id + '" id="' + id + '" class="custom" value="' + value + '" ' + checkedString + ' data-validate="required"/>'));
        $fieldSet.append($('<label for="' + id + '" class="checkBoxLabel">' + value + '</label>'));

        $detailSet.append($fieldSet);
    },

    appendFeedbackDetailDate: function (detailItem, $detailSet, id, detailCaption, requiredAttribute) {
        if (detailItem.Validation && detailItem.Validation !== "")
            dataVal = ' data-validate="' + detailItem.Validation + '"';

        var $input = $('<input type="text" id="' + id + '" class="ui-input-text ui-body-d ui-corner-all ui-shadow-inset ui-mini" placeholder="' + detailCaption + '" ' + requiredAttribute + dataVal + '/>');
        var setText = _common.getLocalization("set", "Set");
        var cancelText = _common.getLocalization("cancel", "Cancel");

        $input.scroller({
            preset: 'datetime',
            dateFormat: gen.common.DATE_PICKER_FORMAT,
            dateOrder: gen.common.DATE_PICKER_FORMAT,
            showNow: false,
            theme: 'jqm',
            display: 'bubble',
            mode: 'mixed',
            setText: setText,
            cancelText: cancelText
        });

        $field.append($input);
        $detailSet.append($field);
    },

    appendFeedbackDetailTextArea: function ($detailSet, id, requiredAttribute, text) {
        $field.append('<textarea id="' + id + '" class="ui-input-text ui-body-d ui-corner-all ui-shadow-inset ui-mini" ' + requiredAttribute + '>' + text + '</textarea>');
        $detailSet.append($field);
    },

    appendFeedbackDetailTextbox: function (detailItem, $detailSet, id, detailCaption, currentValue, requiredAttribute) {
        var dataVal = "";
        if (detailItem.Validation && detailItem.Validation !== "")
            dataVal = ' data-validate="' + detailItem.Validation + '"';
        $field.append('<input name="' + id + '" id="' + id + '" data-mini="true" placeholder="' + detailCaption + '" value="' + currentValue + '" ' + requiredAttribute + dataVal + '/>');
        $detailSet.append($field);
    },

    appendFeedbackDetailCombo: function (detailItem, $detailSet, id) {
        $field = $('<div id="' + detailItem + 'Holder" data-role="fieldcontain"/>');
        var title = _common.getLocalization(detailItem.LocalizationKey, detailItem.Caption);
        $field.append('<label id="' + id.replace("Input", "") + 'Label" for="' + id + '" class="select">' + title + '</label>');
        $select = $('<select name="' + id + '" id="' + id + '" data-mini="true" placeholder="' + title + '" required data-container="' + $detailSet[0].id + '"/>');
        $field.append($select);
        $detailSet.append($field);

        var localizedValue = _common.getLocalization("pleaseSelect", "Please Select");
        $select.append('<option value="' + localizedValue + '">' + localizedValue + '</option>');

        $.each(detailItem.Values.Value, function (values, valueItem) {
            localizedValue = _common.getLocalization(valueItem.LocalizationKey, valueItem.Caption);
            $select.append('<option value="' + localizedValue + '">' + localizedValue + '</option>');
        });
    },

    onResetClick: function () {
        var prompt = _common.getLocalization("clearForm", "Clear the Feedback Form?")
        var okText = _common.getLocalization("ok", "OK")
        var cancelText = _common.getLocalization("cancel", "Cancel")

        noty({
            text: prompt,
            type: 'alert',
            dismissQueue: true,
            closeWith: ['click', 'backdrop'],
            modal: true,
            layout: 'center',
            theme: 'defaultTheme',
            buttons: [{
                    addClass: 'ui-btn ui-shadow ui-btn-corner-all ui-mini ui-btn-up-d ui-btn-inner',
                    text: okText,
                    onClick: function ($noty) {
                        $noty.close();
                        _feedback.buildFormInit();
                    }
                },
                {
                    addClass: 'ui-btn ui-shadow ui-btn-corner-all ui-mini ui-btn-up-d ui-btn-inner',
                    text: cancelText,
                    onClick: function ($noty) {
                        $noty.close();
                    }
                }
            ]
        });
    },

    onSubmitClick: function () {
        _feedback.disableSubmit();

        //• CUSTOM VALIDATION
        var specifyOptionMessage = _common.getLocalization("optionRequired", "Please select a feedback option before submitting");
        var specifyReasonMessage = _common.getLocalization("reasonRequired", "Please select a feedback reason before submitting");
        var specifyOptionDateMessage = _common.getLocalization("optionDateRequired", "Please select a feedback date before submitting");

        var okText = _common.getLocalization("ok", "OK");

        // Check if the required fields are populated.
        var $selectedRadio = $('input[name="feedOptions"]:checked');
        if ($selectedRadio.length == 0) {
            _common.showNotification(specifyOptionMessage);
            _feedback.enableSubmit();
            return;
        }

        if ($selectedRadio.attr("data-date") == "true") {
            if ($("#optionDate").val() == "") {
                _common.showNotification(specifyOptionDateMessage);
                _feedback.enableSubmit();
                return;
            }
        }
        if ($selectedRadio.attr("data-values") == "true") {
            if ($("#reasonReason").val() == "") {
                _common.showNotification(specifyReasonMessage);
                _feedback.enableSubmit();
                return;
            }
        }

        //• VERIFY.JS VALIDATION
        gen.firstFail = true;
        var failed = false;
        $("form").each(function () {
            var form = $(this);
            if (form.attr('requiredCount')) {
                gen.requiredCount = parseInt(form.attr('requiredCount'));
                gen.validCount = 0;
                form.validate(function (res) {});
            } else {
                form.validate(function (res) {
                    if (res === false)
                        failed = true;
                });
            }
        });

        setTimeout(function () {
            if (gen.validCount < gen.requiredCount) {
                failed = true;

                var message = _common.getLocalization("requiredCount", "Please select minimum amount of options");
                _common.showNotification(message);
            }
        }, 250);


        setTimeout(function () {
            if (!failed)
                _feedback.doSubmit();
            else
                _feedback.enableSubmit();
        }, 500);
    },

    enableSubmit: function () {
        this.ui.submitButton.off().on("click", $.proxy(this.onSubmitClick, this));
        this.ui.submitButton.css("cssText", "");
    },

    disableSubmit: function () {
        this.ui.submitButton.off();
        this.ui.submitButton.css("cssText", "background-color: grey !important;");
    },

    doSubmit: function () {
        $.mobile.loading('show', {
            text: "Submitting Feedback",
            textVisible: false,
            theme: "a",
            html: ""
        });

        var validated = true;

        if (!validated) {
            _feedback.enableSubmit();
            return;
        }

        var postData = $.extend({}, this.lead);

        if (gen.selectedCampaignConfiguration.PreferredSalesPerson && gen.selectedCampaignConfiguration.PreferredSalesPerson == "true")
            postData.preferredSalesPerson = gen.LoggedInUserId;

        var optionDate = _feedback.selectedOptionDate.toString("yyyy-MM-ddTHH:mm:ss") + _common.getTimeZoneOffset(_feedback.selectedOptionDate);

        postData.feedbackResult = {
            actionDate: optionDate,
            comments: $("#reasonComment").val(),
            feedbackType: $('input:radio[name=feedOptions]:checked').val(),
            reason: ($("#optionReasonHolder").css("display") == "none" ? null : $("#reasonReason").val())
        };

        _feedback.setPostDataProperties(postData, _feedback.lead_existing, "Input", false);

        // Post the feedback
        debugger;
        //_feedback.requiredDetailsProvided = false;

        if (_feedback.requiredDetailsProvided == true) {
            var method = "PUT";
            var uri = this.lead.uri || "";
            if (_feedback.capture) {
                method = "POST";
                uri = gen.common.BaseMobileProxyUrl + "/api/campaigns/" + gen.selectedCampaign.id + "/leads";
            }

            if (_feedback.passingOn)
                _feedback.postData = postData;
            _feedback.doSubmitGD(method, uri, postData);

        } else {
            $.mobile.loading('hide', {});
            _feedback.enableSubmit();
        }
    },

    doSubmitGD: function (method, uri, postData) {
        var url = gen.common.PROXY;
        var timeout = 30;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;

        var submitFeedbackRequest = window.plugins.GDHttpRequest.createRequest("POST", gen.common.PROXY, timeout, isAsync, user, password, auth, isIncremental);
        submitFeedbackRequest.disablePeerVerification = false;
        submitFeedbackRequest.disableHostVerification = false;
        submitFeedbackRequest.addRequestHeader("dataType", "jsonp");
        submitFeedbackRequest.addRequestHeader("cache-control", "no-cache");
        submitFeedbackRequest.addRequestHeader("Accept-Language", gen.LanguageCode);
        submitFeedbackRequest.addRequestHeader("contentType", "application/x-www-form-urlencoded");
        submitFeedbackRequest.addRequestHeader("crossDomain", "true");
        submitFeedbackRequest.addPostParameter("dataType", "jsonp");
        submitFeedbackRequest.addPostParameter("callback", "getdata");
        submitFeedbackRequest.addPostParameter("token", gen.token.access_token);
        submitFeedbackRequest.addPostParameter("refreshToken", gen.token.refresh_token);
        submitFeedbackRequest.addPostParameter("action", uri);
        submitFeedbackRequest.addPostParameter("method", method);
        submitFeedbackRequest.addPostParameter("base", "false");
        submitFeedbackRequest.addPostParameter("data", JSON.stringify(postData));

        function sendSuccess(response) {
            try {
                console.log("Received valid response from the send request");
                var responseObj = window.plugins.GDHttpRequest.parseHttpResponse(response);
                var data = _common.FormatJson(responseObj.responseText.toString());

                var resultMessage = "";

                if (_feedback.passingOn) {
                    _feedback.setPostDataProperties(_feedback.postData, _feedback.lead_new, "Input2", true);

                    if (data.newId != undefined) {
                        //If its a new lead with immediate pass on, then apply the feedback first
                        leadId = data.newId;
                        _feedback.doFeedback(leadId, gen.selectedCampaign.id, postData, false);
                    }

                    var salesPersonSuppliedId = $("#passOnSalesPerson").val();
                    if (salesPersonSuppliedId != "0") {
                        _feedback.postData.preferredSalesPerson = salesPersonSuppliedId;
                    }
                    _feedback.doPassOn($("#passOnCampaign").val(), _feedback.postData);
                } else {
                    if (_feedback.capture) {
                        var newLeadId = data.newId;
                        _feedback.doFeedback(newLeadId, gen.selectedCampaign.id, postData, true);
                    } else {
                        resultMessage = _common.getLocalization("submitSucceeded", "Feedback received successfully");
                        _common.showNotification(resultMessage);
                        $.mobile.loading('hide', {});
                        $("#navToLeadsLink").trigger("click")
                        _leads.init();
                    }
                }
            } catch (e) {
                debugger;
                $.mobile.loading('hide', {});

                var resultMessage = _common.getLocalization("submitFailed", "Failed to submit the feedback!")
                _common.showNotification(resultMessage);
                _feedback.enableSubmit()
            };
        }

        function sendFail() {
          
            $.mobile.loading('hide', {});
            var resultMessage = _common.getLocalization("submitFailed", "Failed to submit the feedback!")
            _common.showNotification(resultMessage);
            _feedback.enableSubmit()
        }
        submitFeedbackRequest.send(sendSuccess, sendFail);
    },
    setPostDataProperties: function (postData, properties_array, control_name, passOn) {
        _feedback.requiredDetailsProvided = true;
        for (var counter = 0; counter < properties_array.length; counter++) {
            var updatetableProperty = properties_array[counter];

            var propertyValue = null;
            if (passOn || updatetableProperty.OptionTypes === undefined || updatetableProperty.OptionTypes.indexOf(postData.feedbackResult.feedbackType) != -1) {
                if (updatetableProperty.Type == "boolean") {
                    if ($("#" + updatetableProperty.Property + control_name).is(":checked")) {
                        propertyValue = $("#" + updatetableProperty.Property + control_name).val();
                    } else {
                        if ($("#" + updatetableProperty.Property + control_name).attr('required')) {
                            var specifyValue = _common.getLocalization("specifyValue", "Please specify a value: ")
                            _common.showNotification(specifyValue + $("#" + updatetableProperty.Property + "Label").val());
                            _feedback.requiredDetailsProvided = false;
                            break;
                        } else
                            propertyValue = null;
                    }
                } else if (updatetableProperty.Type == "combo") {
                    var dropDownData = $("#" + updatetableProperty.Property + control_name).select2('data');
                    if (dropDownData === null && $("#" + updatetableProperty.Property + control_name).attr('required')) {
                        var specifyValue = _common.getLocalization("specifyValue", "Please specify a value: ")
                        _common.showNotification(specifyValue + $("#" + updatetableProperty.Property + "Label").val());
                        _feedback.requiredDetailsProvided = false;
                        break;
                    } else {
                        if (dropDownData[0].selectedOptions.length > 0)
                            propertyValue = dropDownData[0].selectedOptions[0].text;
                        else
                            propertyValue = _common.getLocalization("pleaseSelect", "Please Select");

                        var localizedValue = _common.getLocalization("pleaseSelect", "Please Select");
                        if (propertyValue == localizedValue) {
                            propertyValue = null;

                            var parentId = dropDownData[0].attributes["data-container"];
                            if (parentId) {
                                if ($("#" + parentId.value).is(":visible")) {
                                    var specifyValue = _common.getLocalization("specifyValue", "Please specify a value: ")
                                    _common.showNotification(specifyValue + $("#" + updatetableProperty.Property + "Label")[0].innerHTML);
                                    _feedback.requiredDetailsProvided = false;
                                    break;
                                }
                            }
                        }
                    }
                } else
                    propertyValue = $("#" + updatetableProperty.Property + control_name).val();
            }

            if (propertyValue) {
                if (updatetableProperty.PropertyType == "Lead")
                    postData[updatetableProperty.Property] = propertyValue;
                else
                    postData.feedbackResult[updatetableProperty.Property] = propertyValue;
            }
        }

        postData.matches = undefined;
    },

    doFeedback: function (newLeadId, campaignId, postData, showNotification) {
        _feedback.doFeedbackGD(newLeadId, campaignId, postData, showNotification);
    },
    doFeedbackGD: function (newLeadId, campaignId, postData, showNotification) {
        var uri = gen.common.BaseMobileProxyUrl + "/api/campaigns/" + campaignId + "/leads/" + newLeadId;
        var url = gen.common.PROXY;
        var timeout = 30;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;
        var doFeedbackRequest = window.plugins.GDHttpRequest.createRequest("POST", gen.common.PROXY, timeout, isAsync, user, password, auth, isIncremental);
        doFeedbackRequest.disablePeerVerification = false;
        doFeedbackRequest.disableHostVerification = false;
        doFeedbackRequest.addRequestHeader("dataType", "jsonp");
        doFeedbackRequest.addRequestHeader("cache-control", "no-cache");
        doFeedbackRequest.addRequestHeader("Accept-Language", gen.LanguageCode);
        doFeedbackRequest.addRequestHeader("contentType", "application/x-www-form-urlencoded");
        doFeedbackRequest.addRequestHeader("crossDomain", "true");
        doFeedbackRequest.addPostParameter("dataType", "jsonp");
        doFeedbackRequest.addPostParameter("callback", "getdata");
        doFeedbackRequest.addPostParameter("token", gen.token.access_token);
        doFeedbackRequest.addPostParameter("refreshToken", gen.token.refresh_token);
        doFeedbackRequest.addPostParameter("action", "uri");
        doFeedbackRequest.addPostParameter("method", "PUT");
        doFeedbackRequest.addPostParameter("base", false);
        doFeedbackRequest.addPostParameter("data", JSON.stringify(postData));

        function sendSuccess(response) {
            console.log("Received valid response from the send request");
            var responseObj = window.plugins.GDHttpRequest.parseHttpResponse(response);
            var data = _common.FormatJson(responseObj.responseText.toString());
            try {
                resultMessage = _common.getLocalization("captureSucceeded", "Lead captured successfully");
                resultMessage = resultMessage + ": " + newLeadId;
                if (showNotification) {
                    _common.showNotification(resultMessage);
                }
                $.mobile.loading('hide', {});
                $("#navToLeadsLink").trigger("click")
                _leads.init();
            } catch (e) {
                resultMessage = _common.getLocalization("captureFailed", "Failed to capture the lead!");
                _common.showNotification(resultMessage);
                $.mobile.loading('hide', {});
                _feedback.enableSubmit();
            }
        };

        function sendFail() {
            resultMessage = _common.getLocalization("captureFailed", "Failed to capture the lead!");
            _common.showNotification(resultMessage);
            $.mobile.loading('hide', {});
            _feedback.enableSubmit();
        }
        doFeedbackRequest.send(sendSuccess, sendFail);

    },
    doPassOn: function (campaignId, postData) {
        var method = "POST";
        var uri = gen.common.BaseMobileProxyUrl + "/api/campaigns/" + campaignId + "/leads";
        postData.feedbackResult.feedbackType = "8";
        postData.matches = undefined;
        _feedback.doPassOnGD(uri, method, postData);
    },
    doPassOnGD: function (uri, method, postData) {
        var method = "POST";
        var url = gen.common.PROXY;
        var timeout = 30;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;

        var submitFeedbackRequest = window.plugins.GDHttpRequest.createRequest("POST", gen.common.PROXY, timeout, isAsync, user, password, auth, isIncremental);
        submitFeedbackRequest.disablePeerVerification = false;
        submitFeedbackRequest.disableHostVerification = false;
        submitFeedbackRequest.addRequestHeader("dataType", "jsonp");
        submitFeedbackRequest.addRequestHeader("cache-control", "no-cache");
        submitFeedbackRequest.addRequestHeader("Accept-Language", gen.LanguageCode);
        submitFeedbackRequest.addRequestHeader("contentType", "application/x-www-form-urlencoded");
        submitFeedbackRequest.addRequestHeader("crossDomain", "true");
        submitFeedbackRequest.addPostParameter("dataType", "jsonp");
        submitFeedbackRequest.addPostParameter("callback", "getdata");
        submitFeedbackRequest.addPostParameter("token", gen.token.access_token);
        submitFeedbackRequest.addPostParameter("refreshToken", gen.token.refresh_token);
        submitFeedbackRequest.addPostParameter("action", uri);
        submitFeedbackRequest.addPostParameter("method", method);
        submitFeedbackRequest.addPostParameter("base", "false");
        submitFeedbackRequest.addPostParameter("data", JSON.stringify(postData));

        function sendSuccess(response) {
            console.log("Received valid response from the send request");
            var responseObj = window.plugins.GDHttpRequest.parseHttpResponse(response);
            var data = _common.FormatJson(responseObj.responseText.toString());
            try {
                debugger;
                var resultMessage = "";
                resultMessage = _common.getLocalization("captureSucceeded", "Lead passed on successfully");
                resultMessage = resultMessage + ": " + data.newId;

                _common.showNotification(resultMessage);
                $.mobile.loading('hide', {});
                $("#navToLeadsLink").trigger("click");
                _leads.init();
            } catch (e) {
                $.mobile.loading('hide', {});
                var resultMessage = _common.getLocalization("submitFailed", "Failed to submit the feedback!")
                _common.showNotification(resultMessage);
                _feedback.enableSubmit();
            }
        };

        function sendFail() {
            $.mobile.loading('hide', {});

            var resultMessage = _common.getLocalization("submitFailed", "Failed to submit the feedback!")
            _common.showNotification(resultMessage);
            _feedback.enableSubmit();
        }
        submitFeedbackRequest.send(sendSuccess, sendFail);

    },
}