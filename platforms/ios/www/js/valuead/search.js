var _search = {

    init: function () {
        $("#searchInput").off().on("keypress", $.proxy(this.onSearchEnter, this));
        $("#searchButton").off().on("click", $.proxy(this.search, this));
        $("#pageLoading").fadeOut(500);
        $("#pageLoadingLogo").fadeOut(500);

        var languageSelectContainer = $("#languageSelectContainerSearch");

        if (gen.common.EnableGlobalization) {
            languageSelectContainer.show();
        } else {
            languageSelectContainer.hide();
        }
    },

    onSearchEnter: function () {
        
        if (event.which == 13) {
            event.preventDefault();
            this.search();
        }
    },

    search: function () {
alert("search");
        
        var $holder = $("#searchResult");
        $holder.empty();
        _search.SearchGD();
    },

    SearchGD: function () {
        var method = "POST";
        var url = window.location.protocol + '//' + window.location.hostname + ":" + window.location.port + '/MobileProxy/JsonProxy';
        var timeout = 50;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;
        var searchRequest = window.plugins.GDHttpRequest.createRequest("POST", url, timeout, isAsync, user, password, auth, isIncremental);
        searchRequest.disablePeerVerification = false;
        searchRequest.disableHostVerification = false;
        searchRequest.addRequestHeader("dataType", "json");
        searchRequest.addRequestHeader("cache-control", "no-cache");
        searchRequest.addRequestHeader("Accept-Language", "en");
        searchRequest.addRequestHeader("crossDomain", "true");
        searchRequest.addPostParameter("method", "GET");
        searchRequest.addPostParameter("action", gen.common.BaseMobileProxyUrl + "/api/campaigns/1/leads/" + $("#searchInput").val());
        searchRequest.addPostParameter("base", "");
        searchRequest.addPostParameter("data", "");
        searchRequest.disablePeerVerification = false;
        searchRequest.disableHostVerification = false;
        searchRequest.disableFollowLocation = false;
        searchRequest.disableHttpProxy();

        function sendSuccess(response) {
            console.log("Received valid response from the campaign request");
            var responseObj = window.plugins.GDHttpRequest.parseHttpResponse(response);
            var data = _common.FormatJson(responseObj.responseText.toString());
            try {
                _search.showLead(data);

            } catch (e) {
                $holder.append('</br>');
                $holder.append('<label>The lead could not be found.</label>');
            };
        }
        function sendFail() {
            $holder.append('</br>');
            $holder.append('<label>The lead could not be found.</label>');
        }
        searchRequest.send(sendSuccess, sendFail);
    },
    showLead: function (data) {
        var $holder = $("#searchResult");
        $holder.empty();

        if (data.length == 0) {
            $holder.append('</br>');
            $holder.append('<label>The lead could not be found.</label>');
        } else {
            $holder.append('<div data-role="fieldcontain" class="ui-field-contain">' +
                '  <label for="nameLabel" class="forLabel expandedControl">Client name</label>' +
                '  <input type="text" name="nameLabel" id="nameLabel" class="expandedControl ui-input-text ui-body-d ui-corner-all ui-shadow-inset ui-mini" placeholder="Name" value="' + data.firstName + ' ' + data.lastName + '" data-theme="b"/>' +
                '</div>');

            if (data.matches) {
                var match = data.matches[0];

                $holder.append('<div data-role="fieldcontain" class="ui-field-contain">' +
                    '  <label for="allocatedToLabel" class="forLabel expandedControl">Allocated To</label>' +
                    '  <input type="text" name="allocatedToLabel" id="allocatedToLabel" class="expandedControl ui-input-text ui-body-d ui-corner-all ui-shadow-inset ui-mini" placeholder="Allocated To" value="' + match.salesPerson.firstName + ' ' + match.salesPerson.lastName + '" data-theme="b"/>' +
                    '</div>');


                $holder.append('<div data-role="fieldcontain" class="ui-field-contain">' +
                    '  <label for="allocatedOnLabel" class="forLabel expandedControl">Allocated On</label>' +
                    '  <input type="text" name="allocatedOnLabel" id="allocatedOnLabel" class="expandedControl ui-input-text ui-body-d ui-corner-all ui-shadow-inset ui-mini" placeholder="Allocated On" value="' + new Date(match.matchDate).toString("dd-MM-yyyy HH:mm:ss") + '" data-theme="b"/>' +
                    '</div>');

                $holder.append('<div data-role="fieldcontain" class="ui-field-contain">' +
                    '  <label for="leadStatusLabel" class="forLabel expandedControl">Lead Status</label>' +
                    '  <input type="text" name="leadStatusLabel" id="leadStatusLabel" class="expandedControl ui-input-text ui-body-d ui-corner-all ui-shadow-inset ui-mini" placeholder="Lead Status" value="' + match.leadStatus + '" data-theme="b"/>' +
                    '</div>');
            }
        }
    }
};