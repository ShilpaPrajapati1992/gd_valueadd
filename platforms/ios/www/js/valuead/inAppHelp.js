var _inAppHelp =
{
    init: function ()
    {
        this.ui = {};
        this.ui.btnHelpQuestion = $("#btnHelpQuestion");
        this.ui.btnHelpSuggestion = $("#btnHelpSuggestion");
        this.ui.btnHelpBug = $("#btnHelpBug");

        if(!gen.common.helpQuestionURL){
            this.ui.btnHelpQuestion.hide();
        }
        if(!gen.common.helpSuggestionAddress){
            this.ui.btnHelpSuggestion.hide();
        }
        if(!gen.common.helpBugAddress){
            this.ui.btnHelpBug.hide();
        }
    },

    openHelpFaq: function(){
        window.open(gen.common.helpQuestionURL, '_system');
    },

    openEmailClient: function(isSuggestion){
        window.open(
            isSuggestion ? gen.common.helpSuggestionAddress : 
            gen.common.helpBugAddress
            , '_system');
    }
}