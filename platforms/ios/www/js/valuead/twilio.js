var _twilio =
{
    init: function (dialInfo, cliNumber)
    {
        $('#statusMessage').text('.');

        _twilio.cliNumber = cliNumber;
        _twilio.phoneNumber = dialInfo[0].Value;
        _twilio.phoneNumberDisplay = _twilio.phoneNumber;
        if (gen.common.MaskDialNumbers)
            _twilio.phoneNumberDisplay = _twilio.phoneNumber.substring(0, 7) + '....';

        $('#dialInfoContainer').empty();
        for (var counter = 0; counter < dialInfo.length; counter++)
        {
            var value = dialInfo[counter].Value;
            if (dialInfo[counter].Label === "Number")
            {
                if (gen.common.MaskDialNumbers)
                    value = value.substring(0, 7) + '....';
            }

            $('#dialInfoContainer').append('<input placeholder="" value="' + value + '" readonly="readonly" class="dialInputLocked">');
            $('#dialInfoContainer').append('<div class="dialInfoLabel">' + dialInfo[counter].Label + '</div>');
        }
        _twilio.disableConnectButton();
        _twilio.disableDisconnectButton();

        var tokenRequest = new XMLHttpRequest();
        tokenRequest.onload = function()
        {
            $('#statusMessage').text('..');
            try
            {
                Twilio.Device.setup(tokenRequest.responseText);
                var connection = null;

                Twilio.Device.ready(function(device)
                {
                    $('#connectButton').show();
                    if (_twilio.cliNumber)
                        $('#statusMessage').text('Ready to dial from ' + _twilio.cliNumber);
                    else
                        $('#statusMessage').text('Ready to dial');
                    _twilio.enableConnectButton();
                });

                Twilio.Device.incoming(function(conn)
                {
                    if (confirm('Accept incoming call from ' + conn.parameters.From + '?'))
                    {
                        connection = conn;
                        conn.accept();
                    }
                });

                Twilio.Device.offline(function(device)
                {
                    $('#connectButton').show();
                    if (_twilio.cliNumber)
                        $('#statusMessage').text('Ready to dial from ' + _twilio.cliNumber);
                    else
                        $('#statusMessage').text('Ready to dial');
                    _twilio.enableConnectButton();
                    console.log("Offline!");
                });

                Twilio.Device.error(function(error)
                {
                    //$('#statusMessage').text(JSON.stringify(error));
                    $('#statusMessage').text(error.message);
                    _twilio.disableDisconnectButton();
                });

                Twilio.Device.connect(function(conn)
                {
                    $('#statusMessage').text("Call started");
                    _twilio.disableConnectButton();
                    _twilio.enableDisconnectButton();
                });

                Twilio.Device.disconnect(function(conn)
                {
                    $('#statusMessage').text("Call ended");
                    _twilio.enableConnectButton();
                    _twilio.disableDisconnectButton();
                });
            }
            catch(e)
            {
                if (_twilio.cliNumber)
                    $('#statusMessage').text(e + " (" + _twilio.cliNumber + ")");
                else
                    $('#statusMessage').text(e);

                console.log("error:" + e);
            }
        };

        tokenRequest.open('GET', gen.common.TwilioURL);
        tokenRequest.send();
    },

    enableConnectButton: function()
    {
        $('#connectButton').css("opacity", 1.0);
        $("#connectButton").off().on("click", function()
        {
            _twilio.disableConnectButton();
            _twilio.enableDisconnectButton();

            var params = {};
            params.tocall = _twilio.phoneNumber;
            params.campaign_id = gen.selectedCampaign.id;
            params.lead_id = gen.selectedLead.id;
            params.salesperson_id = gen.LoggedInUserId;

            if (_twilio.cliNumber)
                params.callerId = _twilio.cliNumber;

            connection = Twilio.Device.connect(params);
            $('#statusMessage').text("Dialing: " + _twilio.phoneNumberDisplay);
        });
    },

    disableConnectButton: function()
    {
        $('#connectButton').css("opacity", 0.25);
        $("#connectButton").off();
    },

    enableDisconnectButton: function()
    {
        gen.dialing = true;
        $('#navToFeedbackLink').hide();
        $('#disconnectButton').css("opacity", 1.0);
        $("#disconnectButton").off().on("click", function()
        {
            Twilio.Device.disconnectAll();
            $('#navToFeedbackLink').show();
            _twilio.disableDisconnectButton();
            _twilio.enableConnectButton();
        });
    },

    disableDisconnectButton: function()
    {
        gen.dialing = false;
        $('#disconnectButton').css("opacity", 0.25);
        $("#disconnectButton").off();
        $('#navToFeedbackLink').show();
        Twilio.Device.disconnectAll();
    },

    getTwilioErrorDescription: function(errorCode)
    {
        switch (errorCode)
        {
            case "31000": { return "Generic Twilio Client error. No further information available. Check the debugger for more info."; } break;
            case "31001": { return "Application not found."; } break;
            case "31002": { return "Connection declined. Check the debugger for more information on the actual cause."; } break;
            case "31003": { return "Connection timeout."; } break;

            case "31100": { return "Generic malformed request."; } break;
            case "31101": { return "Missing parameter array in request."; } break;
            case "31102": { return "Authorization token missing in request."; } break;
            case "31103": { return "Length of parameters cannot exceed MAX_PARAM_LENGTH."; } break;
            case "31104": { return "Invalid bridge token."; } break;
            case "31105": { return "Invalid client name."; } break;

            case "31201": { return "Generic unknown error."; } break;
            case "31202": { return "JWT signature validation failed."; } break;
            case "31203": { return "No valid account."; } break;
            case "31204": { return "Invalid JWT token."; } break;
            case "31205": { return "JWT token expired."; } break;
            case "31206": { return "Rate exceeded authorized limit."; } break;
            case "31207": { return "JWT token expiration too long."; } break;
            case "31208": { return "User denied access to microphone."; } break;

            default: { return errorCode; } break;
        }
    }
}