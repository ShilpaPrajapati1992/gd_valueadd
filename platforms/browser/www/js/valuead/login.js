var _login = {
    preInit: function () {
        $.ajaxSetup({
            cache: false
        });

        this.ui = {};

        this.ui.versionNumber = $("#versionNumber");
        this.ui.versionNumber.text(gen.VERSION);

        this.ui.username = $("#loginUsername");
        this.ui.username.val(_common.getCookie("user_name"));

        this.ui.password = $("#loginPassword");

        this.ui.signInButton = $("#signInButton");
        this.ui.forgotPasswordButton = $("#forgotPasswordButton");

        this.ui.password.off().on("keypress", $.proxy(this.onSignInEnter, this));
        this.ui.password.on("focus", $.proxy(this.ensureVisible, this));
        this.ui.signInButton.off().on("click", $.proxy(this.onSignInClick, this));
        this.ui.forgotPasswordButton.off().on("click", $.proxy(this.onForgotClick, this));

        this.ui.userNameContainer = $("#userNameContainer");
        this.ui.passwordContainer = $("#passwordContainer");
        this.ui.oneTimePinContainer = $("#oneTimePinContainer");

        this.ui.loginButtonsContainer = $("#loginButtonsContainer");
        this.ui.oneTimePinButtonsContainer = $("#oneTimePinButtonsContainer");
        this.ui.loginOTP = $("#loginOTP");
        this.ui.oneTimePinButton = $("#oneTimePinButton");
        this.ui.oneTimePinButton.off().on("click", $.proxy(this.onOTPClick, this));
        this.ui.cancelOneTimePinButton = $("#cancelOneTimePinButton");
        this.ui.cancelOneTimePinButton.off().on("click", $.proxy(this.onCancelOTPClick, this));

        this.ui.oneTimePinContainer.hide();
        this.ui.oneTimePinButtonsContainer.hide();

        this.ui.unlinkClientButton = $("#unlinkClientButton");
        this.ui.unlinkClientButton.off().on("click", $.proxy(this.onUnlinkClick, this));

       // this.ui.signInButton.hide();

        if (!gen.common.PHONEGAP) {
            this.ui.unlinkClientButton.hide();
        }
    },

    init: function () {
try {
        var languageSelectContainer = $("#languageSelectContainer");

        if (gen.common.EnableGlobalization) {
            languageSelectContainer.show();
        } else {
            languageSelectContainer.hide();
        }
        gen.buildPostData = function (formId) {
            var result = {};
            $("#" + formId).find("*[name]").each(function (index) {
                result[$(this).attr("name")] = $(this).attr("value");
            });
            return result;
        };
        alert("Build postdatat"+JSON.stringify(gen.buildPostData));
        _common.displayLanguage();

        this.ui.username.attr('autocomplete', gen.common.EnableLoginAutocomplate);
        this.ui.password.attr('autocomplete', gen.common.EnableLoginAutocomplate);

        
            alert("This is signin start"+this.ui.signInButton);
            this.ui.signInButton.show();
            alert("This is signin end");

        } catch (error) {
            alert(error);
        }


    },

    ensureVisible: function (element) {
        $("#loginPassword")[0].scrollIntoView();
    },


    onUnlinkClick: function () {
        var unlinking = _common.getLocalization("unlinking", "Unlinking Client");

        $.mobile.loading('show', {
            text: unlinking,
            textVisible: true,
            theme: "a",
            html: ""
        });

        this.ui.username.val("");
        this.ui.password.val("");

        gen.common.FOLDER = "";
        gen.common.PROXY = "";
        gen.common.FORGOT = "";
        gen.common.RESET = "";

        localStorage.removeItem('folder');
        localStorage.removeItem('proxy');
        localStorage.removeItem('forgot');
        localStorage.removeItem('reset');
        _common.goToClient();
    },

    onSignInEnter: function (event) {
        if (event.which == 13) {
            event.preventDefault();
            alert("method called");
            this.onSignInClick();
        }
    },

    enableSignIn: function () {
        this.ui.signInButton.off().on("click", $.proxy(this.onSignInClick, this));
        this.ui.signInButton.css("cssText", "");
    },

    disableSignIn: function () {
        this.ui.signInButton.off();
        this.ui.signInButton.css("cssText", "background-color: grey !important;");
    },

    onSignInClick: function () {
        gen.FirstLoad = true;

        if (this.ui.username.val() === "" || this.ui.password.val() === "") {
            var noLoginDetails = _common.getLocalization("noLoginDetails", "You have not entered any login details.");
            _common.showNotification(noLoginDetails);
            return;
        }

        _common.setCookieInDays("user_name", this.ui.username.val());
        gen.LoggedInUserId = this.ui.username.val();

        _login.disableSignIn();
        this.ui.username.blur();
        this.ui.password.blur();

        var signingIn = _common.getLocalization("signingIn", "Signing In");

        $.mobile.loading('show', {
            text: signingIn,
            textVisible: true,
            theme: "a",
            html: ""
        });
        _login.onSignInClickGD();

    },
    onSignInClickGD: function () {
        var context = this;
        var method = "POST";
        var url = gen.common.PROXY;
        var timeout = 30;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;

        //-- createRequest
        var loginRequest = window.plugins.GDHttpRequest.createRequest("POST", gen.common.PROXY, timeout, isAsync, user, password, auth, isIncremental);

        loginRequest.disablePeerVerification = false;
        loginRequest.disableHostVerification = false;


        //-- addRequestHeader
        loginRequest.addRequestHeader("dataType", "jsonp");
        loginRequest.addRequestHeader("cache-control", "no-cache");
        loginRequest.addRequestHeader("Accept-Language", gen.LanguageCode);
        loginRequest.addPostParameter("user", this.ui.username.val());
        loginRequest.addPostParameter("pass", this.ui.password.val());
        loginRequest.addPostParameter("dataType", "jsonp");
        loginRequest.addPostParameter("callback", "getdata");

        function sendSuccess(response) {
            console.log("Received valid response from the send request");

            try {

                var responseObj = window.plugins.GDHttpRequest.parseHttpResponse(response);
                $.mobile.loading('hide', {});
                var data = _common.FormatJson(responseObj.responseText.toString());
                alert("response status" + responseObj.status);
                _login.enableSignIn();
                if (responseObj.status == 200) {
                    // alert("initialized");
                    // if (typeof window.plugins.GDSecureStorage == undefined) {

                    //     alert("window.plugins.GDSecureStorage does not exist");
                    // } else {
                    //     alert("window.plugins.GDSecureStorage does  exist");
                    // }
                    // // var localStorage = new window.plugins.GDSecureStorage();
                    // // alert("storage"+localStorage);
                    // localStorage.setItem("user", "VA03");
                    // // alert("value set");
                    //  var val = localStorage.getItem("user");
                    // alert("Stored value" + val);
                    gen.token = data;
                    _common.setCookieInMinutes("refresh_token", data.refresh_token);

                    context.ui.password.val("");

                    if (window.location.search.length > 0)
                        _common.showPreLoaders();

                    // Navigate to the main view.
                    $("#navToMainLink").trigger("click");

                    var loading = _common.getLocalization('loading', 'Loading ... ')

                    $.mobile.loading('show', {
                        text: loading,
                        textVisible: true,
                        theme: "a",
                        html: ""
                    });

                    if (gen.common.PHONEGAP)
                        _notifications.initPushNotifications();

                    _campaigns.preInit();
                    setTimeout(_common.linkNavigation, 500);

                } else if (responseObj.status == 204) {
                    gen.OTP_URL = jqXHR.getResponseHeader('Content-Location');
                    $.mobile.loading('hide', {});
                    context.ui.userNameContainer.hide();
                    context.ui.passwordContainer.hide();
                    context.ui.loginButtonsContainer.hide();
                    context.ui.oneTimePinContainer.show();
                    context.ui.oneTimePinButtonsContainer.show();
                    context.ui.forgotPasswordButton.hide();
                } else {
                   _login.enableSignIn();
                        _common.showNotification("Login Error");
                }
            } catch (e) {
                _login.enableSignIn();
                $.mobile.loading('hide', {});
                if (data.error) {
                    if (data.error_description)
                        _common.showNotification(data.error_description);
                    else if (data.message)
                        _common.showNotification(data.message);
                    else
                        _common.showNotification("Login Error");
                } else
                    _common.showNotification("Login Error");

            }
        };

        function sendFail() {
            _login.enableSignIn();
            $.mobile.loading('hide', {});

            if (data.error) {
                if (data.error_description)
                    _common.showNotification(data.error_description);
                else if (data.message)
                    _common.showNotification(data.message);
                else
                    _common.showNotification("Login Error");
            } else
                _common.showNotification("Login Error");

        }
        loginRequest.send(sendSuccess, sendFail);
    },
    onOTPClick: function () {
        if (this.ui.loginOTP.val() == "") {
            var noOTP = _common.getLocalization("noOneTimePin", "You have not entered a One Time Pin.");
            _common.showNotification(noOTP);
            return;
        }

        var otpValue = this.ui.loginOTP.val();
        this.ui.loginOTP.val("");

        var signingIn = _common.getLocalization("signingIn", "Signing In");

        $.mobile.loading('show', {
            text: signingIn,
            textVisible: true,
            theme: "a",
            html: ""
        });

        var context = this;
        $.ajax({
                type: "POST",
                dataType: "jsonp",
                headers: {
                    "cache-control": "no-cache"
                },
                data: {
                    method: "POST",
                    action: gen.OTP_URL,
                    data: "2faToken=" + otpValue,
                    base: false
                },
                beforeSend: function (request) {
                    request.setRequestHeader("Accept-Language", gen.LanguageCode);
                },
                url: gen.common.PROXY,
                statusCode: {
                    400: function () {
                        debugger;
                        console.log("400");
                    }
                }
            })
            .done(function (data, textStatus, jqXHR) {
                debugger;
                $.mobile.loading('hide', {});
                context.ui.userNameContainer.show();
                context.ui.passwordContainer.show();
                context.ui.loginButtonsContainer.show();
                context.ui.oneTimePinContainer.hide();
                context.ui.oneTimePinButtonsContainer.hide();
                context.ui.forgotPasswordButton.show();

                if (jqXHR.status == 200) {
                    if (data === undefined) {
                        $.mobile.loading('hide', {});
                        var loginFailed = _common.getLocalization("loginFailed", "Login failed3!");
                        _common.showNotification(loginFailed);
                        return;
                    } else {
                        gen.token = data;
                        _common.setCookieInMinutes("refresh_token", data.refresh_token);

                        context.ui.password.val("");

                        if (window.location.search.length > 0)
                            _common.showPreLoaders();

                        if (gen.common.PHONEGAP)
                            _notifications.initPushNotifications();

                        $("#navToMainLink").trigger("click");
                        _campaigns.preInit();
                        setTimeout(_common.linkNavigation, 100);
                    }
                } else {
                    $.mobile.loading('hide', {});
                    var loginFailed = _common.getLocalization("loginFailed", "Login failed1!");
                    _common.showNotification(loginFailed);
                    return;
                }
            })
            .fail(function (jqXHR, textStatus) {
                debugger;
                $.mobile.loading('hide', {});
                context.ui.userNameContainer.show();
                context.ui.passwordContainer.show();
                context.ui.loginButtonsContainer.show();
                context.ui.oneTimePinContainer.hide();
                context.ui.oneTimePinButtonsContainer.hide();
                context.ui.forgotPasswordButton.show();

                var loginFailed = _common.getLocalization("loginFailed", "Login failed4 !");
                _common.showNotification(loginFailed);
            });
    },

    onCancelOTPClick: function () {
        this.ui.loginOTP.text("");
        this.ui.userNameContainer.show();
        this.ui.passwordContainer.show();
        this.ui.loginButtonsContainer.show();
        this.ui.oneTimePinContainer.hide();
        this.ui.oneTimePinButtonsContainer.hide();
        this.ui.forgotPasswordButton.show();
    },

    onSignOutClick: function () {
        _login.onSignInClickGD();

    },
    onSignOutClickGD: function () {
        alert("signout called")
        var method = "POST";
        var url = gen.common.PROXY;
        var timeout = 30;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;

        //-- createRequest
        signOutClickRequest = window.plugins.GDHttpRequest.createRequest("POST", gen.common.PROXY + "?callback=getdata", timeout, isAsync, user, password, auth, isIncremental);

        signOutClickRequest.disablePeerVerification = false;
        signOutClickRequest.disableHostVerification = false;
        //-- addRequestHeader
        signOutClickRequest.addRequestHeader("dataType", "jsonp");
        signOutClickRequest.addRequestHeader("cache-control", "no-cache");
        signOutClickRequest.addRequestHeader("Accept-Language", "en");
        signOutClickRequest.addRequestHeader("crossDomain", "true");
        signOutClickRequest.addPostParameter("dataType", "jsonp");
        signOutClickRequest.addPostParameter("token", gen.token.access_token);
        signOutClickRequest.addPostParameter("refreshToken", gen.token.refresh_token);
        signOutClickRequest.addPostParameter("method", "POST");
        signOutClickRequest.addPostParameter("action", "/oauth/logout");
        signOutClickRequest.addPostParameter("base", false);
        signOutClickRequest.addPostParameter("data", "");
        alert("Signout request" + JSON.stringify(signOutClickRequest))

        function sendSuccess(response) {
            console.log("Received valid response from logout request");
            alert("logout");
            try {
                gen.token = {};
                _common.deleteCookie("token");
                _common.deleteCookie("refresh_token");
            } catch (e) {
                alert("Logout failed! ");
            }
        }

        function sendFail() {
            alert("Logout failed! ");
        }
        signOutClickRequest.send(sendSuccess, sendFail);

    },

    onForgotClick: function () {
        alert("forgot password click")

        if (this.ui.username.val() == "") {
            var noLoginDetails = _common.getLocalization("noLoginDetails", "You have not entered any login details.");
            _common.showNotification(noLoginDetails);
            return;
        }

        var context = this;
        var forgotPasswordPrompt = _common.getLocalization("forgotPasswordPrompt", "Are you sure you want to reset your password?");
        var okText = _common.getLocalization("ok", "OK")
        var cancelText = _common.getLocalization("cancel", "Cancel");

        noty({
            text: forgotPasswordPrompt,
            type: 'alert',
            dismissQueue: true,
            closeWith: ['click', 'backdrop'],
            modal: true,
            layout: 'center',
            theme: 'defaultTheme',
            buttons: [{
                    addClass: 'ui-btn ui-shadow ui-btn-corner-all ui-mini ui-btn-up-d ui-btn-inner',
                    text: okText,
                    onClick: function ($noty) {
                        $noty.close();

                        _common.deleteCookie("refresh_token");

                        var forgotPassword = _common.getLocalization("forgotPassword", "Forgot Password");
                        $.mobile.loading('show', {
                            text: forgotPassword,
                            textVisible: true,
                            theme: "a",
                            html: ""
                        });

                        context.ui.username.blur();
                        _login.onForgotClickGD(context);
                    }
                },
                {
                    addClass: 'ui-btn ui-shadow ui-btn-corner-all ui-mini ui-btn-up-d ui-btn-inner',
                    text: cancelText,
                    onClick: function ($noty) {
                        $noty.close();
                    }
                }
            ]
        });
    },
    onForgotClickGD: function (context) {
        alert("reset password called" + context.ui.username.val());
        var method = "POST";
        var url = gen.common.PROXY;
        var timeout = 30;
        var isAsync = false;
        var user = null;
        var password = null;
        var auth = null;
        var isIncremental = true;

        //-- createRequest
        var forgotClickRequest = window.plugins.GDHttpRequest.createRequest("POST", gen.common.PROXY, timeout, isAsync, user, password, auth, isIncremental);


        //-- addRequestHeader
        forgotClickRequest.addRequestHeader("dataType", "jsonp");
        forgotClickRequest.addRequestHeader("cache-control", "no-cache");
        forgotClickRequest.addRequestHeader("Accept-Language", gen.LanguageCode);
        forgotClickRequest.addPostParameter("callback", "getdata");
        forgotClickRequest.addPostParameter("method", "POST");
        forgotClickRequest.addPostParameter("action", "private/password/forgot");
        forgotClickRequest.addPostParameter("base", "true");
        forgotClickRequest.addPostParameter("data", JSON.stringify({
            usernameOrEmail: context.ui.username.val(),
            resetUri: gen.common.RESET_PAGE
        }));


        function sendSuccess(response) {
            try {

                //-- parseHttpResponse
                var responseObj = window.plugins.GDHttpRequest.parseHttpResponse(response);
                $.mobile.loading('hide', {});
                var data = _common.FormatJson(responseObj.responseText.toString());
                if (responseObj.status == 200) {
                    $.mobile.loading('hide', {});
                    var resetMessage = _common.getLocalization("resetMessage", "An email with instructions to reset your password has been sent to the address on record")
                    _common.showNotification(resetMessage);
                } else {
                    $.mobile.loading('hide', {});
                    if (data.error) {
                        if (data.error_description)
                            _common.showNotification(data.error_description);
                        else if (data.message)
                            _common.showNotification(data.message);
                        else
                            _common.showNotification("Reset Error");
                    } else
                        _common.showNotification("Reset Error");
                }
            } catch (e) {
                $.mobile.loading('hide', {});
                _common.showNotification("Reset Error");
            }
        };

        function sendFail() {
            _common.showNotification("Reset Error");
        }
        forgotClickRequest.send(sendSuccess, sendFail);
    }
};