# Value Ad Mobile Repository
This document serves as a getting started guide for any developer who wish to contribute to the project.
## Table of Contents
1. [Getting Started](#getting-started)
### Getting Started
This is a cross platform mobile app and mobile website, build in [Cordova](https://cordova.apache.org/) for Android and iOS. The project uses [Jquery Mobile](https://jquerymobile.com/) as display framework. 

#### Requirements
1. [Node JS](https://nodejs.org/en/)
2. [Cordova](https://cordova.apache.org/)
3. [Android Studio](https://developer.android.com/studio/index.html)
4. xCode

#### Installation
Install version 6.3.1 of NodeJs on the desired development machine. 
After installed, run the following commands:

```
npm install cordova
```

1. Checkout the git solution
2. Go into the project folder and open a terminal

Run: 

```
cordova platform rm ios
```
```
cordova platform rm android
```
```
cordova platform add ios
```
```
cordova platform add android
```


#### Plugins
The below plugins is used for the Good Dynamic Integration App
1. [com.phonegap.plugins.PushPlugin]


> * `BBD Cordova Base plugin`
> * `BBD Cordova HttpRequest plugin`
> * `BBD Cordova Storage plugin`

To install the below Good Dynamic plugins use the below command:

 cordova plugin add ../cordova-plugin-bbd-xmlhttprequest

 It will install all other dependent required plugins.

 To build the ipa file:

 First change the below things in build.json file resides in app root folder

 provisioningProfile: uuid  from provision profile certificate
 developmentTeam:team from provision profile certificate

 After setting in build  run the following commands
 cordova build ios --device --release

 it will create ipa file in build folder.

 Install the ipa through itunes to test on your Iphone.

